cmake_minimum_required(VERSION 3.12.3)

set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

include(cmake/utils.cmake)
include(cmake/sys.cmake)
include(cmake/sys.cmake.ext OPTIONAL)

set_sys_from_env_paradis_sys()
set(EXE_MODE "SERIAL" CACHE STRING "SERIAL or PARALLEL")
set_c_cxx_compiler(${SYS} ${EXE_MODE})
set_cuda_compiler()

find_package(Python3 COMPONENTS Interpreter)
print(Python3_EXECUTABLE)

project(ParaDiS VERSION 2.7 LANGUAGES C CXX)
print(CMAKE_CUDA_COMPILER)
if (NOT ${CMAKE_CUDA_COMPILER} MATCHES "NOTFOUND")
  enable_language(CUDA)
endif()

set(OPT_FLAGS "-O3;-g" CACHE STRING "Optimization flags")
option(XLIB_MODE "X-window display" OFF)

add_paradis_option(_SUBCYCLING "Subcycling time integration" ON)
add_paradis_option(_RETROCOLLISIONS "Retroactive collision detection" ON)
add_paradis_option(_FIX_PLASTIC_STRAIN "Bug fixes for accumulating plastic strain" ON)
add_paradis_option(_THERMAL_ACTIVATED_CROSSSLIP "Thermally activated cross slip" OFF)
add_paradis_option(_OP_REC "Record all topological and motion operations during simulation" OFF)
add_paradis_option(_NONSINGULAR_SELF_FORCE "Use non-singular expression for self force" OFF)
add_paradis_option(_BUILD_HALFSPACE "Build halfspace module" ON)

list(APPEND CXX_FLAGS ${OPT_FLAGS})
if (NOT XLIB_MODE)
    list(APPEND CXX_FLAGS -DNO_XWINDOW)
endif()

message("Configure ${PROJECT_NAME}")
message("")
print(${PARADIS_OPTIONS})
message("")
print(CXX_FLAGS)
message("")

# consider changing this to target_compile_definitions
add_compile_options(${CXX_FLAGS})

configure_file(include/PARADISConfig.h.in PARADISConfig.h)

add_subdirectory(src bin)

# only build halfspace in master branch
find_git_branch()
print(GIT_BRANCH)
if (NOT ${GIT_BRANCH} MATCHES "hcp")
  if (_BUILD_HALFSPACE)
    add_subdirectory(halfspace halfspace)
  endif()
endif()

enable_testing()
add_subdirectory(ctests)
