set(extern_c_srcs
      Collision.c    
      CommSendCoord.c          
      CommSendGhosts.c         
      CommSendGhostPlanes.c    
      CommSendMirrorNodes.c    
      CommSendRemesh.c         
      CommSendSecondaryGhosts.c 
      CommSendSegments.c       
      CommSendVelocity.c       
      CommSendVelocitySub.c    
      CorrectionTable.c        
      ParadisInit.c            
      DebugFunctions.c         
      Decomp.c                 
      DisableUnneededParams.c  
      DLBfreeOld.c             
      deWitInteraction.c       
      FindPreciseGlidePlane.c  
      FixRemesh.c              
      FMComm.c                 
      FMSigma2.c               
      FMSupport.c              
      FreeInitArrays.c         
      GetDensityDelta.c        
      GetNewNativeNode.c       
      GetNewGhostNode.c        
      Gnuplot.c                
      Heap.c                   
      InitCellDomains.c        
      InitCellNatives.c        
      InitCellNeighbors.c      
      InitHome.c               
      InitRemoteDomains.c      
      InitSendDomains.c        
      LoadCurve.c              
      Matrix.c                 
      Meminfo.c                
      MemCheck.c               
      Migrate.c                
      MobilityLaw_FCC_0b.c     
      MobilityLaw_FCC_climb.c  
      NodeVelocity.c           
      OsmoticForce.c           
      ParadisThread.c          
      Parse.c                  
      PickScrewGlidePlane.c    
      QueueOps.c               
      ReadRestart.c	       
      ReadBinaryRestart.c      
      RBDecomp.c               
      RSDecomp.c               
      RemapInitialTags.c       
      RemeshRule_3.c           
      RemoteSegForces.c        
      RemoveNode.c             
      ResetGlidePlanes.c       
      SegSegList.c             
      SortNativeNodes.c        
      SortNodes.c              
      SortNodesForCollision.c  
      SplitSurfaceNodes.c      
      Tecplot.c                
      WriteArms.c              
      WriteAtomEye.c           
      WriteBinaryRestart.c     
      WriteDensFlux.c          
      WriteDensityField.c      
      WriteForce.c             
      WriteFragments.c         
      WriteParaview.c          
      WritePoleFig.c           
      WritePovray.c            
      WriteProp.c              
      WriteVelocity.c          
      WriteVisit.c
)

set(extern_cpp_srcs
      DisplayC.C
      display.C
)

set(extern_incs
      Cell.h           
      DisplayC.h               
      Init.h                   
      RemoteDomain.h           
      WriteProp.h              
      Comm.h                   
      FM.h                     
      M33.h                    
      OpList.h                 
      Parse.h                  
      display.h                
      Constants.h              
      Matrix.h                 
      ParadisGen.h             
      Param.h             
      QueueOps.h               
      SubCyc.h                 
      Typedefs.h               
      DebugFunctions.h         
      MirrorDomain.h           
      RBDecomp.h               
      Decomp.h                 
      InData.h                 
      ParadisThread.h          
      RSDecomp.h               
      Tag.h                    
      V3.h
)

set(ext_inc_dir
      IncludeExt
)

# Files with a change of name are separately copied
set(paradistf_copy_incs
      Include/Home.h       
      Include/Node.h       
      Include/Topology.h   
      Include/Yoffe.h      
      Include/Force.h      
#      Include/HS.h         
      Include/ParadisProto.h
      Include/Util.h       
      Include/Mobility.h   
      Include/Timer.h      
      Include/Restart.h    
      Include/SubcycleGPU.h           
)

# Files with a change of name are separately copied
set(paradistf_copy_c_srcs
#      HS_Main.c                
      AllSegmentStress.c       
      AllYoffeStress.c         
      CellCharge.c
      CrossSlipFCC.c           
      CrossSlipBCC.c           
      PredictiveCollision.c    
      ProximityCollision.c     
      Compute.c                
      CrossSlip.c              
      DeltaPlasticStrain.c     
      DeltaPlasticStrain_BCC.c     
      DeltaPlasticStrain_FCC.c     
      ForwardEulerIntegrator.c 
      Fourier_transforms.c     
      Initialize.c             
      InputSanity.c            
      LocalSegForces.c         
      MobilityLaw_BCC_0.c      
      MobilityLaw_FCC_0.c      
      MobilityLaw_BCC_0b.c     
      MobilityLaw_BCC_glide.c
      MobilityLaw_BCC_glide_0.c  
      MobilityLaw_Relax.c      
      MobilityLaw_FCC_Tab.c    
      NodeForce.c              
      ParadisStep.c            
      Param.c                  
      PrintStress.c            
      RemeshRule_2.c           
      Remesh.c                 
      SegmentStress.c          
      SemiInfiniteSegSegForce.c    
      Timer.c                  
      TrapezoidIntegrator.c    
      Topology.c               
      Util.c		            
      Yoffe.c                  
      Yoffe_corr.c             
      RetroactiveCollision.c   
      RKFIntegrator.c          
      SubcycleIntegrator.c     
      NodeForceList.c          
      GenerateOutput.c         
      WriteRestart.c           
      ParadisFinish.c
)

set(paradistf_copy_cuda_srcs
      SubcycleGPU.cu
)

set(paradishs_c_srcs
      HS_Util.c                
      ABCcoeff.c               
      DispStress.c             
      Minvmatrix.c             
      Plot.c                   
      HalfSpace_Remesh.c       
      halfspace.c              
      Indentation.c            
      Nucleation.c             
      WriteParaviewSpec.c      
      AbaqusIndentation.c
)

set(PARADISHS_INCS "${PARADISHS_INCS_${SYS}}")
set(PARADISHS_LIBS "${PARADISHS_LIBS_${SYS}}")
set(PARADISHS_LIBS_SHARED "${PARADISHS_LIBS_SHARED_${SYS}}")

set(extern_c_srcs_full_path "${extern_c_srcs}")
list(TRANSFORM extern_c_srcs_full_path PREPEND "${PROJECT_SOURCE_DIR}/src/")
set(extern_cpp_srcs_full_path "${extern_cpp_srcs}")
list(TRANSFORM extern_cpp_srcs_full_path PREPEND "${PROJECT_SOURCE_DIR}/src/")

function(copy_extern_incs_files)
  foreach(file IN LISTS extern_incs)
      file(COPY "${PROJECT_SOURCE_DIR}/include/${file}" DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/IncludeExt")
  endforeach()
endfunction()

function(convert_file_tf_to_hs file1 file2)
  set(file1_full_path ${PROJECT_SOURCE_DIR}/thinfilm/${file1})
  set(file2_full_path ${CMAKE_CURRENT_BINARY_DIR}/${file2})
  file(READ ${file1_full_path} file_content)
  string(REPLACE "paradistf"        "paradishs"  new_file_content "${file_content}")
  string(REPLACE "TF_halfthickness" "HS_Lzinf"   new_file_content "${new_file_content}")
  string(REPLACE "tf_halfthickness" "hs_Lzinf"   new_file_content "${new_file_content}")
  string(REPLACE "TF"               "HS"         new_file_content "${new_file_content}")
  string(REPLACE "tf_"              "hs_"        new_file_content "${new_file_content}")
  string(REPLACE "THINFILM"         "HALFSPACE"  new_file_content "${new_file_content}")
  string(REPLACE "THIN FILM"        "HALF SPACE" new_file_content "${new_file_content}")
  string(REPLACE "ThinFilm"         "HalfSpace"  new_file_content "${new_file_content}")
  string(REPLACE "thinfilm"         "halfspace"  new_file_content "${new_file_content}")
  string(REPLACE "thin film"        "half space" new_file_content "${new_file_content}")
  string(REPLACE "Thin Film"        "Half Space" new_file_content "${new_file_content}")
  string(REPLACE "Thin film"        "Half space" new_file_content "${new_file_content}")
  file(WRITE ${file2_full_path} "${new_file_content}")
endfunction()

function(copy_files_to_hs)
  copy_extern_incs_files()

  convert_file_tf_to_hs("TF_Main.c"              "HS_Main.c")
  foreach(file IN LISTS paradistf_copy_c_srcs paradistf_copy_cpp_srcs paradistf_copy_cuda_srcs)
    convert_file_tf_to_hs(${file} ${file})
  endforeach()

  convert_file_tf_to_hs("Include/TF.h"           "Include/HS.h")
  foreach(file IN LISTS paradistf_copy_incs)
    convert_file_tf_to_hs(${file} ${file})
  endforeach()
endfunction()

function(regexp_replace_file match_expr replace_expr file2)
  set(file2_full_path ${CMAKE_CURRENT_BINARY_DIR}/${file2})
  file(READ ${file2_full_path} file_content)
  string(REGEX REPLACE "${match_expr}" "${replace_expr}" new_file_content "${file_content}")
  file(WRITE ${file2_full_path} "${new_file_content}")
endfunction()

function(adjust_hs_files)
  regexp_replace_file("t = halfspace[^\n]*\n" " t = 0.0;\n" "AllSegmentStress.c")

  regexp_replace_file("[^s]t = halfspace[^\n]*\n" " t = 0.0;\n" "AllYoffeStress.c")
  regexp_replace_file([[fabs\(zm\)]]              "zm"          "AllYoffeStress.c")
  regexp_replace_file("\\(\\(rs\\[2\\]"           "((1.0"       "AllYoffeStress.c")
  regexp_replace_file("\\(\\(rsZ"                 "((1.0"       "AllYoffeStress.c")
  regexp_replace_file("\\(fabs\\(rsZ\\) > t \\|\\| fabs\\(rsZ\\) <  t\\)" "(rsZ > t)" "AllYoffeStress.c")

  regexp_replace_file("h = 2\\*param"            "h = 1\*param" "Initialize.c")
  regexp_replace_file("HS\\_Lzinf"               "HS_halfthickness" "Param.c") # This one seems strange

  regexp_replace_file("halfspace->t"             " 0.0" "PrintStress.c")

  regexp_replace_file("hs\\_Lzinf"               "hs_halfthickness"  "RetroactiveCollision.c") # This one seems strange

  regexp_replace_file("nx,ny"                    "nx,ny,numimages"      "Include/HS.h")
  regexp_replace_file("HSLx,HSLy,t"              "HSLx,HSLy,HSLzinf"    "Include/HS.h")
  regexp_replace_file("COMPLEX\\*[\t ]*Txp;"     "COMPLEX\*     Tx;"    "Include/HS.h")
  regexp_replace_file("COMPLEX\\*[\t ]*Typ;"     "COMPLEX\*     Ty;"    "Include/HS.h")
  regexp_replace_file("COMPLEX\\*[\t ]*Tzp;"     "COMPLEX\*     Tz;"    "Include/HS.h")
  regexp_replace_file("COMPLEX\\*[\t ]*Txm;"     "//COMPLEX\*     Tx;"  "Include/HS.h")
  regexp_replace_file("COMPLEX\\*[\t ]*Tym;"     "//COMPLEX\*     Ty;"  "Include/HS.h")
  regexp_replace_file("COMPLEX\\*[\t ]*Tzm;"     "//COMPLEX\*     Tz;"  "Include/HS.h")
  regexp_replace_file("COMPLEX\\*\\*[\t ]*E;"    "//COMPLEX\*\* E;"     "Include/HS.h")
  regexp_replace_file("COMPLEX\\*\\*[\t ]*F;"    "//COMPLEX\*\* F;"     "Include/HS.h")
  regexp_replace_file("COMPLEX\\*\\*[\t ]*G;"    "//COMPLEX\*\* G;"     "Include/HS.h")
  regexp_replace_file("COMPLEX\\*\\*[\t ]*MsInv" "COMPLEX\*\* MInv"     "Include/HS.h")
  regexp_replace_file("COMPLEX\\*\\*[\t ]*MaInv" "//COMPLEX\*\* MaInv"  "Include/HS.h")
  regexp_replace_file("hs\\_ptr"                 "tf_ptr"               "Include/HS.h") # This one seems strange

endfunction()