#include "Home.h"
#include "HS.h"

#define Stress  (halfspace->Stress)
#define grids   (halfspace->Grid)

void DispStress(HalfSpace_t *halfspace,real8 r[3], real8 stress[3][3])
{
/***************************************************************************
 * This program computes the stress from the displacement field using 
 * elasticity theory in a thin film and tractions computed in HSUtil.c
 * from AllSegmentStress.c
 * Sylvie Aubry, Wed Feb 27 2008
 * 
 * When option _GRID_INDENTER_STRESS is enabled, the stress is interpolated
 * from a regular grid to avoid the expensive O(nx*ny) calculation. This
 * option allows for significant computational savings when dense surface
 * grids (e.g. nx,ny>=64) are used.
 * Nicolas Bertin, May 5 2017
 *
 **************************************************************************/

  int i, j, k, m, n;
  double kx, ky;
  double kz, kx2, ky2, kz2, kz3, kxy, kxz, kyz,kxmy;
  double lm, l2m,lpm,lmpm,l2mpm;
  double alpha;
  double x,y,z;

  double complex expk;
  
  int nim    = halfspace->numimages;
  int nx     = halfspace->nx;
  int ny     = halfspace->ny;

  double HSLx     = halfspace->HSLx;
  double HSLy     = halfspace->HSLy;
  double mu       = halfspace->mu;
  double lambda   = halfspace->lambda;
  
  // Average traction to specify k=0 mode (Nicolas)
  complex Tx0 = halfspace->Tx0;
  complex Ty0 = halfspace->Ty0;
  complex Tz0 = halfspace->Tz0;

  lm  = lambda + mu;
  l2m = lm     + mu;
  lmpm= lambda/lm;
  l2mpm= l2m/lm;
  lpm = mu/lm;

  // r is the point at the mid segment between two nodes.
  // stress is evaluated for each kx,ky in [1:nx]*[1:ny] at this point
  
  for (i=0; i<3; i++) 
    for (j = 0; j < 3; j++)
      stress[i][j] = 0.0;
  
  z = r[2];
  
  // Do not evaluate the stress if the query point
  // is outside of the halfspace (Nicolas).
  if (z > 0.0) return;
#ifdef _GRID_INDENTER_STRESS
  if (-z > halfspace->HSLzinf) return;
#endif
  
//#if 0
#ifdef _GRID_INDENTER_STRESS

	double rd = Normal(r);
	
	if (rd >= 0.0) {

		// Interoplate stress on grid //
		int nxg, nyg, nzg, g[3], npd, npe;
		double p[3], q[3], xi[3];
		
		int linear = 0;
		
		if (linear) {
			// Linear interpolation
			nxg = nx;
			nyg = ny;
			nzg = nx;
			npd = 2;
		} else {
			// Quadratic interpolation
			if (nx%2 !=0 || ny%2 != 0) {
				Fatal("Surface grid resolution nx and ny must be multiple of 2 "
				      "for option -D_GRID_INDENTER_STRESS");
			}
			nxg = nx/2;
			nyg = ny/2;
			nzg = nx/2;
			npd = 3;
		}
		npe = npd*npd*npd;
		
		p[0] = r[0] + 0.5*HSLx;
		p[1] = r[1] + 0.5*HSLy;
		p[2] = -r[2];
		
		q[0] = p[0] / (HSLx / nxg);
		q[1] = p[1] / (HSLy / nyg);
		q[2] = p[2] / (halfspace->HSLzinf / nzg);
		
		g[0] = (int)floor(q[0]);
		g[1] = (int)floor(q[1]);
		g[2] = (int)floor(q[2]);
		
		xi[0] = 2.0*(q[0]-g[0]) - 1.0;
		xi[1] = 2.0*(q[1]-g[1]) - 1.0;
		xi[2] = 2.0*(q[2]-g[2]) - 1.0;
		
		// Determine elements for interpolation
		// and apply PBC in the x,y directions
		if(g[2] == nzg) { g[2]--; xi[2] = 1.0; }
		int ind1d[3][npd];
		for (i = 0; i < npd; i++) {
			ind1d[0][i] = ((npd-1)*g[0]+i)%nx;
			if (ind1d[0][i] < 0) ind1d[0][i] += nx;
			ind1d[1][i] = ((npd-1)*g[1]+i)%ny;
			if (ind1d[1][i] < 0) ind1d[1][i] += ny;
			ind1d[2][i] = (npd-1)*g[2]+i;
		}
		
		// 1d shape functions
		double phi1d[3][npd];
		if (linear) {
			for (i = 0; i < 3; i++) {
				phi1d[i][0] = 0.5*(1.0-xi[i]);
				phi1d[i][1] = 0.5*(1.0+xi[i]);
			}
		} else {
			for (i = 0; i < 3; i++) {
				phi1d[i][0] = 0.5*xi[i]*(xi[i]-1.0);
				phi1d[i][1] = -(xi[i]+1.0)*(xi[i]-1.0);
				phi1d[i][2] = 0.5*xi[i]*(xi[i]+1.0);
			}
		}
		
		// 3d shape functions and indices
		int ind[npe];
		double phi[npe];
		for (k = 0; k < npd; k++) {
			for (j = 0; j < npd; j++) {
				for (i = 0; i < npd; i++) {
					phi[i+j*npd+k*npd*npd] = phi1d[0][i]*phi1d[1][j]*phi1d[2][k];
					ind[i+j*npd+k*npd*npd] = ind1d[0][i] + ind1d[1][j]*nx + ind1d[2][k]*nx*ny;
				}
			}
		}
		
		// Interpolate stress from the grid points
		double s[6];
		for (i = 0; i < 6; i++) {
			s[i] = 0.0;
			for (j = 0; j < npe; j++) {
				s[i] += phi[j] * halfspace->S_indenter[ind[j]][i];
			}
		}
		
		stress[0][0] = s[0];
		stress[1][1] = s[1];
		stress[2][2] = s[2];
		stress[0][1] = s[3];
		stress[2][0] = s[4];
		stress[1][2] = s[5];
	
	} else

#endif

	{
 
#if 1

  for (j=0; j<ny; j++) 
    {
      ky = halfspace->ky[j];
      ky2 = ky*ky;
      
      for (i=0; i<nx; i++) 
	{
	  kx = halfspace->kx[i];
	  kx2 = kx*kx;
	  
	  kz2 = kx2+ky2;
	  kxmy = kx2-ky2;
	      
	  kz  = sqrt(kz2);
	  kz3 = kz2*kz;
	  
	  kxz = kx*kz;
	  kyz = ky*kz;
	  
	  kxy = kx*ky;
	  
	  double complex A = halfspace->A[i][j];
	  double complex B = halfspace->B[i][j];
	  double complex C = halfspace->C[i][j];
	  
	  for (n=-nim; n<nim+1; n++) 
	    for (m=-nim; m<nim+1; m++) 
	      { 
		// Translated for FFT origin accuracy
		x = r[0] + HSLx*0.5 - n*HSLx;
		y = r[1] + HSLy*0.5 - m*HSLy;
		
		alpha = kx*x+ky*y;
		expk  = mu*exp(kz*z)*(cos(alpha)+I*sin(alpha));
		
		Stress[0][0][i][j] = 2*expk*((I*A*kx2*z-I*B*kxy-C*kx2)+I*A*kz*lmpm);
		Stress[1][1][i][j] = 2*expk*((I*A*ky2*z+I*B*kxy-C*ky2)+I*A*kz*lmpm);
		Stress[2][2][i][j] = 2*expk*((-I*A*z+C)*kz2+I*A*kz*l2mpm);

		Stress[2][0][i][j] = expk*((2*A*kx*z-B*ky+2*I*C*kx)*kz-2*A*kx*lpm);
		Stress[1][2][i][j] = expk*((2*A*ky*z+B*kx+2*I*C*ky)*kz-2*A*ky*lpm);
		Stress[0][1][i][j] = I*expk*(2*A*kxy*z+B*kxmy+2*I*C*kxy);
		
		// Specify traction vector for k=0 mode (Nicolas)
		if (kx == 0.0 && ky == 0.0) {
			Stress[2][2][i][j] = Tz0;
			Stress[2][0][i][j] = Tx0;
			Stress[1][2][i][j] = Ty0;
		}
		
	      }
	}
    }
  int k, l; 
  for (k=0; k<3; k++) 
    for (l=0; l<3; l++) 
      for (i=0; i<nx; i++) 
	for (j=0; j<ny; j++) 
	  {	
	    stress[k][l] += creal(Stress[k][l][i][j]);
	  }
	  
#else

/*	
 * 	Use only a subset of frequencies depending 
 * 	on the distance to the surface. 
 * 	PLEASE DO NOT USE THIS FUNCTION.
 */
	
	complex s11k, s22k, s33k, s12k, s13k, s23k;
	
	if (nx != ny) Fatal("nx != ny, Please use the old version of function DispStress");
	if (nx != 32 && nx != 128) Fatal("nx,ny != 32, Please use the old version of function DispStress");
	
	int ki, kj, nk;
	int maxnf = nx/2 + nx % 2;
	
	double zrel = -z/MAX(HSLx, HSLy);
	double zf[nx/2];
	
	if (nx == 32) {
		/*
		double zf16[16] = {0.5215,0.3544,0.2653,0.2102,0.1732,0.1481,0.1281,0.1131,
		                   0.1011,0.0911,0.0821,0.0741,0.0661,0.0551,0.0360,0.0};
		*/
		double zf16[16] = {0.6000,0.5215,0.3544,0.2653,0.2102,0.1732,0.1481,0.1281,
			               0.1131,0.1011,0.0911,0.0821,0.0741,0.0661,0.0551,0.0};
		memcpy(zf, zf16, sizeof(zf));
	} else if (nx == 128) {
		double zf64[64] = {0.60661,0.32633,0.21421,0.15716,0.12312,0.1011,0.086086,
			               0.075075,0.066066,0.059059,0.054054,0.049049,0.045045,
			               0.042042,0.039039,0.037037,0.034034,0.032032,0.031031,
			               0.029029,0.028028,0.026026,0.025025,0.024024,0.023023,
			               0.022022,0.022022,0.021021,0.02002,0.019019,0.019019,
			               0.018018,0.017017,0.017017,0.016016,0.016016,0.015015,
			               0.015015,0.014014,0.014014,0.013013,0.013013,0.013013,
			               0.012012,0.012012,0.011011,0.011011,0.01001,0.01001,
			               0.01001,0.009009,0.009009,0.008008,0.008008,0.007007,
			               0.006006,0.006006,0.005005,0.004004,0.003003,0.003003,
			               0.003003,0.003003,0.0};
		memcpy(zf, zf64, sizeof(zf));
	}
	
	for (i = 0; i < maxnf; i++) {
		if (zrel >= zf[i]) {
			nk = 4+i*2;
			break;
		}
	}
	
	if (nk > nx) nk = nx;
	int nkm = (nk-2)/2;
	nk = nx;
	
	for (kj=0; kj<nk; kj++) {
		
		if (nk == nx) j = kj;
		else {
			if (kj <= nkm) j = kj;
			else j = nx-kj+nkm;
		}
	
		ky = halfspace->ky[j];
		ky2 = ky*ky;
		
		for (ki=0; ki<nk; ki++) {
			
			if (nk == nx) i = ki;
			else {
				if (ki <= nkm) i = ki;
				else i = nx-ki+nkm;
			}
			
			kx = halfspace->kx[i];
			kx2 = kx*kx;

			kz2 = kx2+ky2;
			kz  = sqrt(kz2);
			
			kxmy = kx2-ky2;
			kxy = kx*ky;

			double complex A = halfspace->A[i][j];
			double complex B = halfspace->B[i][j];
			double complex C = halfspace->C[i][j];

			// Translated for FFT origin accuracy
			x = r[0] + HSLx*0.5;
			y = r[1] + HSLy*0.5;

			alpha = kx*x+ky*y;
			expk  = mu*exp(kz*z)*(cos(alpha)+I*sin(alpha));

			s11k = 2*expk*((I*A*kx2*z-I*B*kxy-C*kx2)+I*A*kz*lmpm);
			s22k = 2*expk*((I*A*ky2*z+I*B*kxy-C*ky2)+I*A*kz*lmpm);
			s33k = 2*expk*((-I*A*z+C)*kz2+I*A*kz*l2mpm);

			s13k = expk*((2*A*kx*z-B*ky+2*I*C*kx)*kz-2*A*kx*lpm);
			s23k = expk*((2*A*ky*z+B*kx+2*I*C*ky)*kz-2*A*ky*lpm);
			s12k = I*expk*(2*A*kxy*z+B*kxmy+2*I*C*kxy);

			// Specify traction vector for k=0 mode (Nicolas)
			if (kx == 0.0 && ky == 0.0) {
				s13k = Tx0;
				s23k = Ty0;
				s33k = Tz0;
			}

			stress[0][0] += creal(s11k);
			stress[1][1] += creal(s22k);
			stress[2][2] += creal(s33k);
			stress[2][0] += creal(s13k);
			stress[1][2] += creal(s23k);
			stress[0][1] += creal(s12k);
		}
	}

#endif

	}

  //printf("stress %f %f %f\n",r[0],r[1],r[2]);
  //printf("%f %f %f %f %f %f\n",stress[0][2],stress[1][2]);

  stress[0][2] = stress[2][0];
  stress[2][1] = stress[1][2];
  stress[1][0] = stress[0][1];

  return;
}

  
