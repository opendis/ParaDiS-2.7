/***************************************************************************
 *
 *  Module      : Indentation.c
 *  Description : Implements the function required for nano-indentation
 *
 **************************************************************************/

#include "Home.h"
#include <time.h>

#ifdef _HALFSPACE
#include "HS.h"

#ifndef _CYGWIN
#include <fftw3.h>
#endif

#define PI 3.14159265

#if 1
/*---------------------------------------------------------------------------
 *
 *      Function:     RandomAtMost
 *      Description:  Returns a random integer in the range [0,max]
 *
 *-------------------------------------------------------------------------*/
static long RandomAtMost(long max)
{
		unsigned long
		// max <= RAND_MAX < ULONG_MAX, so this is okay.
		num_bins = (unsigned long) max + 1,
		num_rand = (unsigned long) RAND_MAX + 1,
		bin_size = num_rand / num_bins,
		defect   = num_rand % num_bins;

		long x;
		do {
			x = random();
		}
		// This is carefully written not to overflow
		while (num_rand - defect <= (unsigned long)x);

		// Truncated division is intentional
		return x/bin_size;
}

#ifdef _SURFACE_STEPS
/*-------------------------------------------------------------------------
 *
 *      Function:     CreatePrismaticStep
 *      Description:  Create surface steps associated with the nucleation
 *                    of a prismatic dislocation loop.
 *
 *-----------------------------------------------------------------------*/
void CreatePrismaticStep(Home_t *home, HalfSpace_t *halfspace,
                         real8 *px, real8 *py, real8 *pz, 
                         real8 bx, real8 by, real8 bz, 
                         real8 n1x, real8 n1y, real8 n1z,
                         real8 n2x, real8 n2y, real8 n2z)
{
	int i, n1, n2, newSurfSeg;
	surfaceStepSeg_t *surfStepSegList;
	
	//printf("CreatePrismaticStep\n");
	
	// Create surface segments
	for (i = 0; i < 4; i++) {
		
		n1 = i;
		n2 = (i==3)?0:(i+1);
		
		newSurfSeg = CreateNewSurfaceSegment(halfspace);
		surfStepSegList = halfspace->surfStepSegList;
					
		surfStepSegList[newSurfSeg].virtual1 = 1;
		surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
		surfStepSegList[newSurfSeg].pos1[0] = px[n1];
		surfStepSegList[newSurfSeg].pos1[1] = py[n1];
		surfStepSegList[newSurfSeg].pos1[2] = pz[n1];
		
		surfStepSegList[newSurfSeg].virtual2 = 1;
		surfStepSegList[newSurfSeg].node2 = (Node_t *)NULL;
		surfStepSegList[newSurfSeg].pos2[0] = px[n2];
		surfStepSegList[newSurfSeg].pos2[1] = py[n2];
		surfStepSegList[newSurfSeg].pos2[2] = pz[n2];
		
		surfStepSegList[newSurfSeg].burg[0] = bx;
		surfStepSegList[newSurfSeg].burg[1] = by;
		surfStepSegList[newSurfSeg].burg[2] = bz;
		
		if (i % 2 == 0) {
			surfStepSegList[newSurfSeg].norm[0] = n1x;
			surfStepSegList[newSurfSeg].norm[1] = n1y;
			surfStepSegList[newSurfSeg].norm[2] = n1z;
		} else {
			surfStepSegList[newSurfSeg].norm[0] = n2x;
			surfStepSegList[newSurfSeg].norm[1] = n2y;
			surfStepSegList[newSurfSeg].norm[2] = n2z;
		}
		
	}
	
	return;
}
#endif

/*-------------------------------------------------------------------------
 *
 *      Function:     DebugNucleation
 *
 *-----------------------------------------------------------------------*/
void DebugNucleation(Home_t *home, char loc_string[])
{
	int i ,nbr1ArmID, nbr2ArmID;
	Node_t *node, *nbr1, *nbr2;
	
	printf("DebugNucleation: %s\n",loc_string);
	
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((node = home->nodeKeys[i]) == (Node_t *)NULL) {
			continue;
		}
		
		if (node->numNbrs != 2) {
			printf(" node %d, numNbrs = %d\n",node->myTag.index,node->numNbrs);
		} else {
			
			nbr1 = GetNeighborNode(home, node, 0);
            nbr2 = GetNeighborNode(home, node, 1);
            
            nbr1ArmID = GetArmID(home, nbr1, node);
            nbr2ArmID = GetArmID(home, nbr2, node);
            
            if (nbr1ArmID == -1) {
				printf(" nbr1 %d, nbr1ArmID = %d, numNbrs = %d\n",nbr1->myTag.index,nbr1ArmID,nbr1->numNbrs);
			}
			if (nbr2ArmID == -1) {
				printf(" nbr2 %d, nbr2ArmID = %d, numNbrs = %d\n",nbr2->myTag.index,nbr2ArmID,nbr2->numNbrs);
			}
			
		}
		
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     PrismaticNucleation
 *      Description:  Nucleate three prismatic loops to accommodate the
 *                    indenter imprint (Fivel's approach). This function
 *                    can only be used with a (111) free surface.
 *
 *-----------------------------------------------------------------------*/
void PrismaticNucleation(Home_t *home, HalfSpace_t *halfspace)
{
	int     i, j, nx, ny;
	double  HSLx, HSLy, px, py;
	double  radius, maxRadius;
	Param_t *param;
	
	int loop_type;
	//static int loop_type = 2;
	
	param = home->param;
	
	if (!param->nucleation) return;
	
	//if (halfspace->d_indenter < 1.0) return; // WARNING
	//if (home->cycle > halfspace->unloading && halfspace->unloading != -1) return;
	//int nucleationFreq = 10;
	//if (home->cycle % nucleationFreq != 0) return;
	//if (home->cycle != 34) return;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	HSLx = halfspace->HSLx;
	HSLy = halfspace->HSLy;
	
	printf("PrismaticNucleation\n");
	//printf("  labFrameZDir = %e %e %e\n", param->labFrameZDir[0], param->labFrameZDir[1], param->labFrameZDir[2]);
	
	/* Check that (111) free surface */
	if (!isCollinear(param->labFrameZDir[0], param->labFrameZDir[1], param->labFrameZDir[2], 1.0, 1.0, 1.0)) {
		Fatal("PrismaticNucleation requires (111) free surface");
	}
	
	
	/* Imprint radius */
	maxRadius = halfspace->contact_radius;
	//printf("  imprint radius = %e\n", maxRadius);
	if (maxRadius == 0.0) return;
	
	maxRadius *= 2.0;
	

	double b1m10[3] = {1.0,-1.0,0.0};
	NormalizeVec(b1m10);

	double n1m10[3];
	NormalizedCrossVector(b1m10, param->labFrameZDir, n1m10);

	if (param->useLabFrame) {
		real8 bRot[3];

		Matrix33Vector3Multiply(home->rotMatrix, b1m10, bRot);
		b1m10[0] = bRot[0];
		b1m10[1] = bRot[1];
		b1m10[2] = bRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, n1m10, bRot);
		n1m10[0] = bRot[0];
		n1m10[1] = bRot[1];
		n1m10[2] = bRot[2];
	}

	//printf("  [1,-1,0] Burgers = %e %e %e\n", b1m10[0], b1m10[1], b1m10[2]);
	//printf("  [1,-1,0] orthdir = %e %e %e\n", n1m10[0], n1m10[1], n1m10[2]);
	
	double h, a;
	double p1[3], pa[3], pb[3], pc[3];
	double tab[3], tbc[3], tca[3];
	
	h = 0.5*sqrt(3.0);
	a = 2.0*h*maxRadius;
	
	p1[0] = maxRadius*n1m10[0];
	p1[1] = maxRadius*n1m10[1];
	p1[2] = maxRadius*n1m10[2];
	
	/* Define vertices of imprint triangle */
	pa[0] = 0.5*p1[0] + p1[1]*h;
	pa[1] = 0.5*p1[1] - p1[0]*h;
	pa[2] = 0.5*p1[2];
	
	pb[0] = 0.5*p1[0] - p1[1]*h;
	pb[1] = 0.5*p1[1] + p1[0]*h;
	pb[2] = 0.5*p1[2];
	
	pc[0] = -p1[0];
	pc[1] = -p1[1];
	pc[2] = -p1[2];
	
	/*
	// Offset imprint
	double offset[3] = {0.6,0.3,0.0};
	for (j = 0; j < 3; j++) {
		pa[j] += 0.5*maxRadius*offset[j];
		pb[j] += 0.5*maxRadius*offset[j];
		pc[j] += 0.5*maxRadius*offset[j];
	}
	*/
	
	//printf("  pa = %e %e %e\n", pa[0], pa[1], pa[2]);
	//printf("  pb = %e %e %e\n", pb[0], pb[1], pb[2]);
	//printf("  pc = %e %e %e\n", pc[0], pc[1], pc[2]);
	
	tab[0] = pb[0] - pa[0];
	tab[1] = pb[1] - pa[1];
	tab[2] = pb[2] - pa[2];
	
	tbc[0] = pc[0] - pb[0];
	tbc[1] = pc[1] - pb[1];
	tbc[2] = pc[2] - pb[2];
	
	tca[0] = pa[0] - pc[0];
	tca[1] = pa[1] - pc[1];
	tca[2] = pa[2] - pc[2];
	
	double Xp[4], Yp[4], Zp[4];
	double Xps[4], Yps[4], Zps[4];
	double nab[3] = {1.0,1.0,-1.0};
	double nbc[3] = {-1.0,1.0,1.0};
	double nca[3] = {1.0,-1.0,1.0};
	NormalizeVec(nab);
	NormalizeVec(nbc);
	NormalizeVec(nca);
	
	if (param->useLabFrame) {
		real8 nRot[3];

		Matrix33Vector3Multiply(home->rotMatrix, nab, nRot);
		nab[0] = nRot[0];
		nab[1] = nRot[1];
		nab[2] = nRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, nbc, nRot);
		nbc[0] = nRot[0];
		nbc[1] = nRot[1];
		nbc[2] = nRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, nca, nRot);
		nca[0] = nRot[0];
		nca[1] = nRot[1];
		nca[2] = nRot[2];
	}
	
	
	/* Nucleation depth */
	//double dnucl = 0.48*maxRadius;
	double dnucl = 1.5*maxRadius;
	//double dnucl = 200.0;
	
	/* Prismatic loop type */
	//loop_type = RandomAtMost(2);
	
	//int seed = time(0) + getpid();
	//loop_type = floor(randm(&seed) * 3.0);
	//if (loop_type == 3) loop_type = 2;
	

	for (loop_type = 0; loop_type < 3; loop_type++) {
		
		//printf("  nucleate prismatic loop type %d\n", loop_type+1);
		
		double b[3], t1[3], t2[3], n1[3], n2[3];
		
		switch (loop_type) {
			case 0:
				/* Burgers vector */
				b[0] = 0.0; b[1] = -1.0; b[2] = -1.0;
				
				/* Loop center, directions, and normals */
				for (j = 0; j < 3; j++) {
					p1[j] = pa[j];
					t1[j] = tab[j]; t2[j] = tca[j];
					n1[j] = nab[j]; n2[j] = nca[j];
				}
				break;
			case 1:
				/* Burgers vector */
				b[0] = -1.0; b[1] = 0.0; b[2] = -1.0;
				
				/* Loop center, directions, and normals */
				for (j = 0; j < 3; j++) {
					p1[j] = pb[j];
					t1[j] = tbc[j]; t2[j] = tab[j];
					n1[j] = nbc[j]; n2[j] = nab[j];
				}
				break;
			case 2:
				/* Burgers vector */
				b[0] = -1.0; b[1] = -1.0; b[2] = 0.0;
				
				/* Loop center, directions, and normals */
				for (j = 0; j < 3; j++) {
					p1[j] = pc[j];
					t1[j] = tca[j]; t2[j] = tbc[j];
					n1[j] = nca[j]; n2[j] = nbc[j];
				}
				break;
		}
		
		NormalizeVec(b);
		double db = fabs(b[0]*param->labFrameZDir[0]+b[1]*param->labFrameZDir[1]+b[2]*param->labFrameZDir[2]);
		
		if (param->useLabFrame) {
			real8 bRot[3];

			Matrix33Vector3Multiply(home->rotMatrix, b, bRot);
			b[0] = bRot[0];
			b[1] = bRot[1];
			b[2] = bRot[2];
		}
		
		Xp[0] = p1[0] + b[0]*dnucl/db;
		Yp[0] = p1[1] + b[1]*dnucl/db;
		Zp[0] = p1[2] + b[2]*dnucl/db;
		
		Xp[1] = Xp[0] + t1[0];
		Yp[1] = Yp[0] + t1[1];
		Zp[1] = Zp[0] + t1[2];
		
		Xp[2] = Xp[0] + t1[0] - t2[0];
		Yp[2] = Yp[0] + t1[1] - t2[1];
		Zp[2] = Zp[0] + t1[2] - t2[2];
		
		Xp[3] = Xp[0] - t2[0];
		Yp[3] = Yp[0] - t2[1];
		Zp[3] = Zp[0] - t2[2];
		
		for (i = 0; i < 4; i++) {
			Xps[i] = Xp[i] - b[0]*dnucl/db;
			Yps[i] = Yp[i] - b[1]*dnucl/db;
			Zps[i] = Zp[i] - b[2]*dnucl/db;
		}
		
#if 1
		/* Project the loop onto the plane normal to the Burgers */
		for (i = 1; i < 4; i++) {
			double dp = (Xp[i]-Xp[0])*b[0]+(Yp[i]-Yp[0])*b[1]+(Zp[i]-Zp[0])*b[2];
			Xp[i] -= dp*b[0]; 
			Yp[i] -= dp*b[1];
			Zp[i] -= dp*b[2];
		}
#endif		
		//printf("  dnucl = %e, z1 = %e, z2 = %e, rloop = %e\n", dnucl,Zp[0],Zp[1],sqrt(3.0)*maxRadius);
#if 1
		/* Check critical nucleation stress */
		p1[0] = p1[1] = p1[2] = 0.0;
		for (i = 0; i < 4; i++) {
			p1[0] += Xp[i];
			p1[1] += Yp[i];
			p1[2] += Zp[i];
		}
		p1[0] *= 0.25; p1[1] *= 0.25; p1[2] *= 0.25;
		
		double s[3][3], rss1, rss2;
		DispStress(halfspace, p1, s);
#if 1		
		double ds[3][3];
		AllSegmentStress(home, halfspace, p1[0], p1[1], p1[2], ds);
		for (i=0; i<3; i++) 
			for (j=0; j<3; j++)
				s[i][j] += ds[i][j];
#endif		
		rss1 = rss2 = 0.0;
		for (i=0; i<3; i++) {
			for (j=0; j<3; j++) {
				rss1 += s[i][j]*n1[i]*b[j];
				rss2 += s[i][j]*n2[i]*b[j];
			}
		}
		rss1 = MAX(fabs(rss1),fabs(rss2));
		//printf("    RSS at nucleation location = %e\n", rss1);
		//printf("    location = %e %e %e\n", p1[0], p1[1], p1[2]);
#endif
		
		/* Nucleate loop */
		NucleatePrismaticLoop(home, Xp, Yp, Zp, -b[0], -b[1], -b[2],
							  n1[0], n1[1], n1[2], n2[0], n2[1], n2[2]);

#ifdef _SURFACE_STEPS
		/* Create surface step */
		CreatePrismaticStep(home, halfspace, Xps, Yps, Zps, -b[0], -b[1], -b[2],
							n1[0], n1[1], n1[2], n2[0], n2[1], n2[2]);
#endif
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     GlissileNucleation
 *      Description:  Nucleate three glissile loops to accommodate the
 *                    indenter imprint.
 *
 *-----------------------------------------------------------------------*/
void GlissileNucleation(Home_t *home, HalfSpace_t *halfspace)
{
	int     i, j, nx, ny;
	double  HSLx, HSLy, px, py;
	double  radius, maxRadius;
	Param_t *param;
	
	int loop_type;
	//static int loop_type = 2;
	
	param = home->param;
	
	if (!param->nucleation) return;
	
	//if (halfspace->d_indenter < 1.0) return; // WARNING
	//if (home->cycle > halfspace->unloading && halfspace->unloading != -1) return;
	//int nucleationFreq = 10;
	//if (home->cycle % nucleationFreq != 0) return;
	//if (home->cycle != 2) return;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	HSLx = halfspace->HSLx;
	HSLy = halfspace->HSLy;
	
	printf("GlissileNucleation\n");
	//printf("  labFrameZDir = %e %e %e\n", param->labFrameZDir[0], param->labFrameZDir[1], param->labFrameZDir[2]);
	
	if (!isCollinear(param->labFrameZDir[0], param->labFrameZDir[1], param->labFrameZDir[2], 1.0, 1.0, 1.0)) {
		Fatal("PrismaticNucleation requires (111) free surface");
	}
	
	
	/* Imprint radius */
	maxRadius = halfspace->contact_radius;
	//printf("  imprint radius = %e\n", maxRadius);
	if (maxRadius == 0.0) return;
	

	double b1m10[3] = {1.0,-1.0,0.0};
	NormalizeVec(b1m10);

	double n1m10[3];
	NormalizedCrossVector(b1m10, param->labFrameZDir, n1m10);

	if (param->useLabFrame) {
		real8 bRot[3];

		Matrix33Vector3Multiply(home->rotMatrix, b1m10, bRot);
		b1m10[0] = bRot[0];
		b1m10[1] = bRot[1];
		b1m10[2] = bRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, n1m10, bRot);
		n1m10[0] = bRot[0];
		n1m10[1] = bRot[1];
		n1m10[2] = bRot[2];
	}

	//printf("  [1,-1,0] Burgers = %e %e %e\n", b1m10[0], b1m10[1], b1m10[2]);
	//printf("  [1,-1,0] orthdir = %e %e %e\n", n1m10[0], n1m10[1], n1m10[2]);
	
	double h, a;
	double p1[3], pa[3], pb[3], pc[3];
	double tab[3], tbc[3], tca[3];
	
	h = 0.5*sqrt(3.0);
	a = 2.0*h*maxRadius;
	
	p1[0] = maxRadius*n1m10[0];
	p1[1] = maxRadius*n1m10[1];
	p1[2] = maxRadius*n1m10[2];
	
	/* Define vertices of imprint triangle */
	pa[0] = 0.5*p1[0] + p1[1]*h;
	pa[1] = 0.5*p1[1] - p1[0]*h;
	pa[2] = 0.5*p1[2];
	
	pb[0] = 0.5*p1[0] - p1[1]*h;
	pb[1] = 0.5*p1[1] + p1[0]*h;
	pb[2] = 0.5*p1[2];
	
	pc[0] = -p1[0];
	pc[1] = -p1[1];
	pc[2] = -p1[2];
	
	//printf("  pa = %e %e %e\n", pa[0], pa[1], pa[2]);
	//printf("  pb = %e %e %e\n", pb[0], pb[1], pb[2]);
	//printf("  pc = %e %e %e\n", pc[0], pc[1], pc[2]);
	
	tab[0] = pb[0] - pa[0];
	tab[1] = pb[1] - pa[1];
	tab[2] = pb[2] - pa[2];
	
	tbc[0] = pc[0] - pb[0];
	tbc[1] = pc[1] - pb[1];
	tbc[2] = pc[2] - pb[2];
	
	tca[0] = pa[0] - pc[0];
	tca[1] = pa[1] - pc[1];
	tca[2] = pa[2] - pc[2];
	
	double Xp[4], Yp[4], Zp[4];
	double Xps[4], Yps[4], Zps[4];
	double nab[3] = {1.0,1.0,-1.0};
	double nbc[3] = {-1.0,1.0,1.0};
	double nca[3] = {1.0,-1.0,1.0};
	NormalizeVec(nab);
	NormalizeVec(nbc);
	NormalizeVec(nca);
	
	if (param->useLabFrame) {
		real8 nRot[3];

		Matrix33Vector3Multiply(home->rotMatrix, nab, nRot);
		nab[0] = nRot[0];
		nab[1] = nRot[1];
		nab[2] = nRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, nbc, nRot);
		nbc[0] = nRot[0];
		nbc[1] = nRot[1];
		nbc[2] = nRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, nca, nRot);
		nca[0] = nRot[0];
		nca[1] = nRot[1];
		nca[2] = nRot[2];
	}
	
	
	/* Nucleation depth */
	//double dnucl = 0.48*maxRadius;
	double dnucl = 1.5*maxRadius;
	
	/* Prismatic loop type */
	//loop_type = RandomAtMost(2);
	
	//int seed = time(0) + getpid();
	//loop_type = floor(randm(&seed) * 3.0);
	//if (loop_type == 3) loop_type = 2;
	

	for (loop_type = 0; loop_type < 1; loop_type++) {
		
		//printf("  nucleate prismatic loop type %d\n", loop_type+1);
		
		double b[3], t1[3], t2[3], n1[3], n2[3];
		
		switch (loop_type) {
			case 0:
				/* Burgers vector */
				b[0] = 0.0; b[1] = -1.0; b[2] = -1.0;
				
				/* Loop center, directions, and normals */
				for (j = 0; j < 3; j++) {
					p1[j] = pa[j];
					t1[j] = tab[j]; t2[j] = tca[j];
					n1[j] = nab[j]; n2[j] = nca[j];
				}
				break;
			case 1:
				/* Burgers vector */
				b[0] = -1.0; b[1] = 0.0; b[2] = -1.0;
				
				/* Loop center, directions, and normals */
				for (j = 0; j < 3; j++) {
					p1[j] = pb[j];
					t1[j] = tbc[j]; t2[j] = tab[j];
					n1[j] = nbc[j]; n2[j] = nab[j];
				}
				break;
			case 2:
				/* Burgers vector */
				b[0] = -1.0; b[1] = -1.0; b[2] = 0.0;
				
				/* Loop center, directions, and normals */
				for (j = 0; j < 3; j++) {
					p1[j] = pc[j];
					t1[j] = tca[j]; t2[j] = tbc[j];
					n1[j] = nca[j]; n2[j] = nbc[j];
				}
				break;
		}
		
		NormalizeVec(b);
		double db = fabs(b[0]*param->labFrameZDir[0]+b[1]*param->labFrameZDir[1]+b[2]*param->labFrameZDir[2]);
		
		if (param->useLabFrame) {
			real8 bRot[3];

			Matrix33Vector3Multiply(home->rotMatrix, b, bRot);
			b[0] = bRot[0];
			b[1] = bRot[1];
			b[2] = bRot[2];
		}
		
		Xp[0] = p1[0] + b[0]*dnucl/db;
		Yp[0] = p1[1] + b[1]*dnucl/db;
		Zp[0] = p1[2] + b[2]*dnucl/db;
		
		Xp[1] = Xp[0] + t1[0];
		Yp[1] = Yp[0] + t1[1];
		Zp[1] = Zp[0] + t1[2];
		
		Xp[2] = Xp[0] + t1[0] - t2[0];
		Yp[2] = Yp[0] + t1[1] - t2[1];
		Zp[2] = Zp[0] + t1[2] - t2[2];
		
		Xp[3] = Xp[0] - t2[0];
		Yp[3] = Yp[0] - t2[1];
		Zp[3] = Zp[0] - t2[2];
		
		for (i = 0; i < 4; i++) {
			Xps[i] = Xp[i] - b[0]*dnucl/db;
			Yps[i] = Yp[i] - b[1]*dnucl/db;
			Zps[i] = Zp[i] - b[2]*dnucl/db;
		}
		
		//printf("  dnucl = %e, z1 = %e, z2 = %e\n", dnucl,Zp[0],Zp[1]);
#if 0
		/* Check critical nucleation stress */
		p1[0] = p1[1] = p1[2] = 0.0;
		for (i = 0; i < 4; i++) {
			p1[0] += Xp[i];
			p1[1] += Yp[i];
			p1[2] += Zp[i];
		}
		p1[0] *= 0.25; p1[1] *= 0.25; p1[2] *= 0.25;
		
		double s[3][3], rss1, rss2;
		DispStress(halfspace, p1, s);
#if 1		
		double ds[3][3];
		AllSegmentStress(home, halfspace, p1[0], p1[1], p1[2], ds);
		for (i=0; i<3; i++) 
			for (j=0; j<3; j++)
				s[i][j] += ds[i][j];
#endif		
		rss1 = rss2 = 0.0;
		for (i=0; i<3; i++) {
			for (j=0; j<3; j++) {
				rss1 += s[i][j]*n1[i]*b[j];
				rss2 += s[i][j]*n2[i]*b[j];
			}
		}
		rss1 = MAX(fabs(rss1),fabs(rss2));
		//printf("    RSS at nucleation location = %e\n", rss1);
		//printf("    location = %e %e %e\n", p1[0], p1[1], p1[2]);
#endif
		
		int numNodes = 8;
		double loopRadius = 0.8*0.5*sqrt(3.0)*maxRadius;
		double cx, cy, cz;
		
		cx = 0.5*(Xp[0]+Xp[1]);
		cy = 0.5*(Yp[0]+Yp[1]);
		cz = 0.5*(Zp[0]+Zp[1]);
		
		NucleateGlissileLoop(home, numNodes, loopRadius, cx, cy, cz, 
	                     b[0], b[1], b[2], n1[0], n1[1], n1[2]);

		
		cx = 0.5*(Xp[0]+Xp[3]);
		cy = 0.5*(Yp[0]+Yp[3]);
		cz = 0.5*(Zp[0]+Zp[3]);
		
		NucleateGlissileLoop(home, numNodes, loopRadius, cx, cy, cz, 
	                     b[0], b[1], b[2], n2[0], n2[1], n2[2]);

		/* Nucleate loop */
		//NucleatePrismaticLoop(home, Xp, Yp, Zp, -b[0], -b[1], -b[2],
		//					  n1[0], n1[1], n1[2], n2[0], n2[1], n2[2]);
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     HalfLoopNucleation
 *      Description:  Nucleate a glissile dislocation half-loop from the
 *                    surface.
 *
 *-----------------------------------------------------------------------*/
void HalfLoopNucleation(Home_t *home, HalfSpace_t *halfspace)
{
	int     i, j, nx, ny;
	double  HSLx, HSLy, px, py;
	double  radius, maxRadius, loopRadius;
	Param_t *param;
	
	//int loop_type;
	static int loop_type = 2;
	
	param = home->param;
	
	if (!param->nucleation) return;
	
	//if (halfspace->d_indenter < 1.0) return; // WARNING
	//int nucleationFreq = 10;
	//if (home->cycle % nucleationFreq != 0) return;
	//if (home->cycle != 20) return;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	HSLx = halfspace->HSLx;
	HSLy = halfspace->HSLy;
	
	printf("HalfLoopNucleation\n");
	
	/* Imprint radius */
	maxRadius = halfspace->contact_radius;
	//printf("  imprint radius = %e\n", maxRadius);
	//if (maxRadius == 0.0) return;
	if (maxRadius == 0.0) maxRadius = 2.0e2;
	
	
	/* Nucleation depth */
	double dnucl = 0.0;
	
	int numNodes = 4;
	loopRadius = param->radiusNucleation*maxRadius;
	
	double b[3] = {-1.0,0.0,-1.0};
	NormalizeVec(b);
	
	if (param->useLabFrame) {
		real8 bRot[3];

		Matrix33Vector3Multiply(home->rotMatrix, b, bRot);
		b[0] = bRot[0];
		b[1] = bRot[1];
		b[2] = bRot[2];
	}
	
	double nab[3] = {1.0,1.0,-1.0};
	double nbc[3] = {-1.0,1.0,1.0};
	double nca[3] = {1.0,-1.0,1.0};
	NormalizeVec(nab);
	NormalizeVec(nbc);
	NormalizeVec(nca);
	
	if (param->useLabFrame) {
		real8 nRot[3];

		Matrix33Vector3Multiply(home->rotMatrix, nab, nRot);
		nab[0] = nRot[0];
		nab[1] = nRot[1];
		nab[2] = nRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, nbc, nRot);
		nbc[0] = nRot[0];
		nbc[1] = nRot[1];
		nbc[2] = nRot[2];
		
		Matrix33Vector3Multiply(home->rotMatrix, nca, nRot);
		nca[0] = nRot[0];
		nca[1] = nRot[1];
		nca[2] = nRot[2];
	}
	
	double l[3], cx, cy, cz;
	
	//loop_type++;
	//if (loop_type == 3) loop_type = 0;
	loop_type = 0;
	
	//printf("  nucleate half-loop type %d\n", loop_type+1);
	
	switch (loop_type) {
	case 0:
	{
		/* Loop 1 */
		NormalizedCrossVector(nab, b, l);
		cx = 0.0;
		cy = 0.0;
		cz = -dnucl;
		
		NucleateHalfLoop(home, numNodes, loopRadius, cx, cy, 
					     b[0], b[1], b[2], nab[0], nab[1], nab[2], halfspace);
		
		// DONT FORGET TO CREATE SURFACE STEP HERE...
		
		break;
	}
	case 1:
	{
		/* Loop 2 */
		NormalizedCrossVector(nbc, b, l);                 
		cx = 0.0;
		cy = 0.0;
		cz = -dnucl;
#if 0		
		NucleateHalfLoop(home, numNodes, loopRadius, cx, cy, 
							 b[0], b[1], b[2], nbc[0], nbc[1], nbc[2]);
#endif
		break;
	}
	case 2:
	{
		/* Loop 3 */
		NormalizedCrossVector(nca, b, l);                 
		cx = 0.0;
		cy = 0.0;
		cz = -dnucl;
#if 0		
		NucleateHalfLoop(home, numNodes, loopRadius, cx, cy, 
							 b[0], b[1], b[2], nca[0], nca[1], nca[2]);
#endif
		break;
	}
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     BCCSlipSystemBurgersNormal
 *      Description:  Returns the Burgers vector and the plane normal
 *                    associated with a given slip system in BCC. Only
 *                    consider {110} and {112} types of glide planes.
 *
 *-----------------------------------------------------------------------*/
void BCCSlipSystemBurgersNormal(Home_t *home, int SysID, double norm[3], double burg[3])
{
		int sn[24], sb[24], i;
		double n[18][3], b[4][3];
		
		// Slip planes
		n[0][0] =  0.0; n[0][1] =  1.0; n[0][2] =  1.0; // Slip plane A(0)
		n[1][0] =  1.0; n[1][1] =  0.0; n[1][2] =  1.0; // Slip plane B(1)
		n[2][0] =  1.0; n[2][1] =  1.0; n[2][2] =  0.0; // Slip plane C(2)
		n[3][0] =  0.0; n[3][1] = -1.0; n[3][2] =  1.0; // Slip plane D(3)
		n[4][0] =  1.0; n[4][1] =  0.0; n[4][2] = -1.0; // Slip plane E(4)
		n[5][0] = -1.0; n[5][1] =  1.0; n[5][2] =  0.0; // Slip plane F(5)

		n[6][0] =  1.0; n[6][1] =  1.0; n[6][2] =  2.0; // Slip plane G(6)
		n[7][0] = -1.0; n[7][1] =  1.0; n[7][2] =  2.0; // Slip plane H(7)
		n[8][0] =  1.0; n[8][1] = -1.0; n[8][2] =  2.0; // Slip plane I(8)		
		n[9][0] =  1.0; n[9][1] =  1.0; n[9][2] = -2.0; // Slip plane J(9)
		n[10][0] =  1.0; n[10][1] =  2.0; n[10][2] =  1.0; // Slip plane K(10)
		n[11][0] = -1.0; n[11][1] =  2.0; n[11][2] =  1.0; // Slip plane L(11)

		n[12][0] =  1.0; n[12][1] = -2.0; n[12][2] =  1.0; // Slip plane M(12)
		n[13][0] =  1.0; n[13][1] =  2.0; n[13][2] = -1.0; // Slip plane N(13)
		n[14][0] =  2.0; n[14][1] =  1.0; n[14][2] =  1.0; // Slip plane O(14)
		n[15][0] = -2.0; n[15][1] =  1.0; n[15][2] =  1.0; // Slip plane P(15)
		n[16][0] =  2.0; n[16][1] = -1.0; n[16][2] =  1.0; // Slip plane Q(16)
		n[17][0] =  2.0; n[17][1] =  1.0; n[17][2] = -1.0; // Slip plane R(17)

		// Burgers
		b[0][0] =  1.0; b[0][1] = -1.0; b[0][2] =  1.0; // Burgers 1(0)
		b[1][0] =  1.0; b[1][1] =  1.0; b[1][2] = -1.0; // Burgers 2(1)
		b[2][0] = -1.0; b[2][1] =  1.0; b[2][2] =  1.0; // Burgers 3(2)
		b[3][0] =  1.0; b[3][1] =  1.0; b[3][2] =  1.0; // Burgers 4(3)
		
		// Slip systems
		sn[0] = 0; sb[0] = 0; // (0)
		sn[1] = 0; sb[1] = 1; // (1)
		sn[2] = 1; sb[2] = 2; // (2)
		sn[3] = 1; sb[3] = 1; // (3)
		sn[4] = 2; sb[4] = 2; // (4)
		sn[5] = 2; sb[5] = 0; // (5)
		sn[6] = 3; sb[6] = 3; // (6)
		sn[7] = 3; sb[7] = 2; // (7)
		sn[8] = 4; sb[8] = 3; // (8)
		sn[9] = 4; sb[9] = 0; // (9)
		sn[10] = 5; sb[10] = 3; // (10)
		sn[11] = 5; sb[11] = 1; // (11)
		sn[12] = 6; sb[12] = 1; // (12)
		sn[13] = 7; sb[13] = 0; // (13)
		sn[14] = 8; sb[14] = 2; // (14)
		sn[15] = 9; sb[15] = 3; // (15)
		sn[16] = 10; sb[16] = 0; // (16)
		sn[17] = 11; sb[17] = 1; // (17)
		sn[18] = 12; sb[18] = 3; // (18)
		sn[19] = 13; sb[19] = 2; // (19)
		sn[20] = 14; sb[20] = 2; // (20)
		sn[21] = 15; sb[21] = 3; // (21)
		sn[22] = 16; sb[22] = 1; // (22)
		sn[23] = 17; sb[23] = 0; // (23)
		
		
		for (i = 0; i < 3; i++) {
			norm[i] = n[sn[SysID]][i];
			burg[i] = b[sb[SysID]][i];
		}

	
		// Normalize and rotate slip vectors
		NormalizeVec(norm);
		NormalizeVec(burg);
		
/*
 *  	If needed, rotate the slip plane vectors from the
 *  	crystal frame to the laboratory frame.
 */
		if (home->param->useLabFrame) {
			real8 normRot[3];
			real8 burRot[3];

			Matrix33Vector3Multiply(home->rotMatrix, norm, normRot);
			Matrix33Vector3Multiply(home->rotMatrix, burg, burRot);

			norm[0] = normRot[0]; norm[1] = normRot[1]; norm[2] = normRot[2];
			burg[0] = burRot[0]; burg[1] = burRot[1]; burg[2] = burRot[2];
		}
	
		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     FCCSlipSystemBurgersNormal
 *      Description:  Returns the Burgers vector and the plane normal
 *                    associated with a given slip system in FCC.
 *
 *-----------------------------------------------------------------------*/
void FCCSlipSystemBurgersNormal(Home_t *home, int SysID, double norm[3], double burg[3])
{
		int sn[12], sb[12], i;
		double n[4][3], b[6][3];
		
		// Slip planes
		n[0][0] = -1.0; n[0][1] =  1.0; n[0][2] =  1.0; // Slip plane A(0)
		n[1][0] =  1.0; n[1][1] =  1.0; n[1][2] =  1.0; // Slip plane B(1)
		n[2][0] = -1.0; n[2][1] = -1.0; n[2][2] =  1.0; // Slip plane C(2)
		n[3][0] =  1.0; n[3][1] = -1.0; n[3][2] =  1.0; // Slip plane D(3)
		
		// Burgers
		b[0][0] =  0.0; b[0][1] =  1.0; b[0][2] =  1.0; // Burgers 1(0)
		b[1][0] =  0.0; b[1][1] = -1.0; b[1][2] =  1.0; // Burgers 2(1)
		b[2][0] =  1.0; b[2][1] =  0.0; b[2][2] =  1.0; // Burgers 3(2)
		b[3][0] = -1.0; b[3][1] =  0.0; b[3][2] =  1.0; // Burgers 4(3)
		b[4][0] = -1.0; b[4][1] =  1.0; b[4][2] =  0.0; // Burgers 5(4)
		b[5][0] =  1.0; b[5][1] =  1.0; b[5][2] =  0.0; // Burgers 6(5)
		
		// Slip systems
		sn[0] = 0; sb[0] = 1; // A2 (0)
		sn[1] = 0; sb[1] = 2; // A3 (1)
		sn[2] = 0; sb[2] = 5; // A6 (2)
		sn[3] = 1; sb[3] = 1; // B2 (if (home->cycle != 1) return;3)
		sn[4] = 1; sb[4] = 3; // B4 (4)
		sn[5] = 1; sb[5] = 4; // B5 (5)
		sn[6] = 2; sb[6] = 0; // C1 (6)
		sn[7] = 2; sb[7] = 2; // C3 (7)
		sn[8] = 2; sb[8] = 4; // C5 (8)
		sn[9] = 3; sb[9] = 0; // D1 (9)
		sn[10] = 3; sb[10] = 3; // D4 (10)
		sn[11] = 3; sb[11] = 5; // D6 (11)
		
		
		for (i = 0; i < 3; i++) {
			norm[i] = n[sn[SysID]][i];
			burg[i] = b[sb[SysID]][i];
		}

	
		// Normalize and rotate slip vectors
		NormalizeVec(norm);
		NormalizeVec(burg);
		
/*
 *  	If needed, rotate the slip plane vectors from the
 *  	crystal frame to the laboratory frame.
 */
		if (home->param->useLabFrame) {
			real8 normRot[3];
			real8 burRot[3];

			Matrix33Vector3Multiply(home->rotMatrix, norm, normRot);
			Matrix33Vector3Multiply(home->rotMatrix, burg, burRot);

			norm[0] = normRot[0]; norm[1] = normRot[1]; norm[2] = normRot[2];
			burg[0] = burRot[0]; burg[1] = burRot[1]; burg[2] = burRot[2];
		}
	
		return;
}

/*--------------------------------------------------------------------------------------
 *
 *      Function:     FindBCCMaximumRSSLocation
 *      Description:  Find the location of the maximum resolved shear stress (RSS) 
 *                    in the volume and the corresponding slip system for two types 
 *                    of slip systems in BCC materials ({110} and {112}). These locations  
 *                    will be considered as potential nucleation sites.
 *
 *------------------------------------------------------------------------------------*/
void FindBCCMaximumRSSLocation(Home_t *home, HalfSpace_t *halfspace, double rmax1[3], double rmax2[3],
                            int *sysmax1, int *sysmax2, double *rssmax1, double *rssmax2)
{
	//printf("Find Maximum RSS Location for twos types of slip systems in BCC materials\n");
	
	Param_t *param;
	param = home->param;
	
	double r[3], s[3][3], rss, sysrss1, sysrss2;
	int i, j, k, l, m, p, nx, ny, sys1, sys2;
	double p1, p2;
	
	double kx, ky;
	double kz, kx2, ky2, kz2, kxy, kxmy;
	double lm, l2m, lpm, lmpm, l2mpm;
	double alpha;
	double x, y, z;
	
	double complex expk;

	double HSLx   = halfspace->HSLx;
	double HSLy   = halfspace->HSLy;
	double mu     = halfspace->mu;
	double lambda = halfspace->lambda;

	/* Average traction to specify k=0 mode */
	complex Tx0 = halfspace->Tx0;
	complex Ty0 = halfspace->Ty0;
	complex Tz0 = halfspace->Tz0;

	lm    = lambda + mu;
	l2m   = lm + mu;
	lmpm  = lambda/lm;
	l2mpm = l2m/lm;
	lpm   = mu/lm;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
#ifndef _CYGWIN
	fftw_complex *s11k, *s22k, *s33k, *s12k, *s13k, *s23k;
	fftw_complex *s11, *s22, *s33, *s12, *s13, *s23;
#else
	complex *s11k, *s22k, *s33k, *s12k, *s13k, *s23k;
	complex *s11, *s22, *s33, *s12, *s13, *s23;
#endif

	rmax1[0] = 0.0;
	rmax1[1] = 0.0;
	rmax1[2] = 0.0;

	rmax2[0] = 0.0;
	rmax2[1] = 0.0;
	rmax2[2] = 0.0;

	*rssmax1 = 0.0;
	*rssmax2 = 0.0;

	*sysmax1 = 0;
	*sysmax2 = 0;
	
/*	
 * 	Compute the Schmid tensor associated with each slip plane
 */
	if (param->materialType != MAT_TYPE_BCC) {
		Fatal("FindBCCMaximumRSSLocation: Schmid tensors can only be calculated for BCC materials\n");
	}
	
	double n[24][3], b[24][3];
	double norm[3], burg[3];
	
	for (m = 0; m < 24; m++) {
		BCCSlipSystemBurgersNormal(home, m, norm, burg);
		for (i = 0; i < 3; i++) {
			n[m][i] = norm[i];
			b[m][i] = burg[i];
		}
	}
	

	s11k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s22k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s33k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s12k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s13k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s23k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	
	s11 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s22 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s33 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s12 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s13 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s23 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	
	
/*	
 * 	Compute the indenter stress on some planes at different 
 *  depths below the free surface. To maximize efficiency, the
 *  stress is only evaluated on nslab planes in a small region
 *  below the indenter where the analytical stress is known to 
 *  be maximum. The stress on each plane is calculated on a grid
 *  using FFTs.
 */
	int nslab = 3;
	for (p = 0; p < nslab; p++) {
		// z is the grid position
		z = -0.125*0.5*home->param->hs_Lzinf*(1.0+2.0*p/(nslab-1)); // 0.5 factor
		
		for (j=0; j<ny; j++) {
			ky = halfspace->ky[j];
			ky2 = ky*ky;

			for (i=0; i<nx; i++) {
				kx = halfspace->kx[i];
				kx2 = kx*kx;

				kz2 = kx2+ky2;
				kz  = sqrt(kz2);
				
				kxmy = kx2-ky2;
				kxy = kx*ky;
				
				expk = mu*exp(kz*z);

				double complex A = halfspace->A[i][j];
				double complex B = halfspace->B[i][j];
				double complex C = halfspace->C[i][j];
				
				s11k[j+i*ny] = 2*expk*((I*A*kx2*z-I*B*kxy-C*kx2)+I*A*kz*lmpm);
				s22k[j+i*ny] = 2*expk*((I*A*ky2*z+I*B*kxy-C*ky2)+I*A*kz*lmpm);
				s33k[j+i*ny] = 2*expk*((-I*A*z+C)*kz2+I*A*kz*l2mpm);

				s13k[j+i*ny] = expk*((2*A*kx*z-B*ky+2*I*C*kx)*kz-2*A*kx*lpm);
				s23k[j+i*ny] = expk*((2*A*ky*z+B*kx+2*I*C*ky)*kz-2*A*ky*lpm);
				s12k[j+i*ny] = I*expk*(2*A*kxy*z+B*kxmy+2*I*C*kxy);
				
				if (kx == 0.0 && ky == 0.0) {
					s13k[j+i*ny] = Tx0;
					s23k[j+i*ny] = Ty0;
					s33k[j+i*ny] = Tz0;
				}
				
			}
		}
		
		// Inverse Fourier Transforms
		fourier_transform_backward(s11,(fftw_complex *)s11k,nx,ny);
		fourier_transform_backward(s22,(fftw_complex *)s22k,nx,ny);
		fourier_transform_backward(s33,(fftw_complex *)s33k,nx,ny);
		fourier_transform_backward(s12,(fftw_complex *)s12k,nx,ny);
		fourier_transform_backward(s13,(fftw_complex *)s13k,nx,ny);
		fourier_transform_backward(s23,(fftw_complex *)s23k,nx,ny);
		
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				
				p1 = param->minSideX + i*(param->maxSideX-param->minSideX)/nx;
				p2 = param->minSideY + j*(param->maxSideY-param->minSideY)/ny;
				
				r[0] = p1;
				r[1] = p2;
				r[2] = z;
				
				s[0][0] = creal(s11[j+i*ny]);
				s[1][1] = creal(s22[j+i*ny]);
				s[2][2] = creal(s33[j+i*ny]);
				s[0][1] = creal(s12[j+i*ny]);
				s[0][2] = creal(s13[j+i*ny]);
				s[1][2] = creal(s23[j+i*ny]);
				
				s[1][0] = s[0][1];
				s[2][0] = s[0][2];
				s[2][1] = s[1][2];
				
#if 1
/*
 * 				Add the stress due to the dislocation segments
 */
				double ds[3][3];
				AllSegmentStress(home, halfspace, r[0], r[1], r[2], ds);
				for (k=0; k<3; k++) 
					for (l=0; l<3; l++)
						s[k][l] += ds[k][l];
#endif
				
/*
 * 				Calculate RSS for each slip system and find out the
 *              the maximum values (rssmax1 and rssmax2) and  
 *              corresponding location (rmax1 and rmax2) for 
 *              each type of slip system over all slip systems
 */
				sys1 = 0;
				sysrss1 = 0.0;
				for (m = 0; m < 12; m++) {
					rss = 0.0;
					for (k=0; k<3; k++) {
						for (l=0; l<3; l++) {
							rss += s[k][l]*n[m][k]*b[m][l];
						}
					}
					if (fabs(rss) > fabs(sysrss1)) {
						sys1 = m;
						sysrss1 = rss;
					}
				}

				sys2 = 0;
				sysrss2 = 0.0;
				for (m = 12; m < 24; m++) {
					rss = 0.0;
					for (k=0; k<3; k++) {
						for (l=0; l<3; l++) {
							rss += s[k][l]*n[m][k]*b[m][l];
						}
					}
					if (fabs(rss) > fabs(sysrss2)) {
						sys2 = m;
						sysrss2 = rss;
					}
				}

				
				// Find RSS max for each type of slip system
				if (fabs(sysrss1) > fabs(*rssmax1)) {
					rmax1[0] = r[0];
					rmax1[1] = r[1];
					rmax1[2] = r[2];
					*rssmax1 = sysrss1;
					*sysmax1 = sys1;
				}

				if (fabs(sysrss2) > fabs(*rssmax2)) {
					rmax2[0] = r[0];
					rmax2[1] = r[1];
					rmax2[2] = r[2];
					*rssmax2 = sysrss2;
					*sysmax2 = sys2;
				}

			}
		}
	}
	
	
	free(s11);
	free(s22);
	free(s33);
	free(s12);
	free(s13);
	free(s23);
	
	free(s11k);
	free(s22k);
	free(s33k);
	free(s12k);
	free(s13k);
	free(s23k);
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     FindFCCMaximumRSSLocation
 *      Description:  Find the location of the maximum resolved shear 
 *                    stress (RSS) in the volume and the corresponding 
 *                    slip system in FCC materials. This location will 
 *                    be considered as a potential nucleation site.
 *
 *-----------------------------------------------------------------------*/
void FindFCCMaximumRSSLocation(Home_t *home, HalfSpace_t *halfspace, double rmax[3], 
                            int *sysmax, double *rssmax)
{
	//printf("Find Maximum RSS Location in FCC materials\n");
	
	Param_t *param;
	param = home->param;
	
	double r[3], s[3][3], rss, sysrss;
	int i, j, k, l, m, p, nx, ny, sys;
	double p1, p2;
	
	double kx, ky;
	double kz, kx2, ky2, kz2, kxy, kxmy;
	double lm, l2m, lpm, lmpm, l2mpm;
	double alpha;
	double x, y, z;
	
	double complex expk;

	double HSLx   = halfspace->HSLx;
	double HSLy   = halfspace->HSLy;
	double mu     = halfspace->mu;
	double lambda = halfspace->lambda;

	/* Average traction to specify k=0 mode */
	complex Tx0 = halfspace->Tx0;
	complex Ty0 = halfspace->Ty0;
	complex Tz0 = halfspace->Tz0;

	lm    = lambda + mu;
	l2m   = lm + mu;
	lmpm  = lambda/lm;
	l2mpm = l2m/lm;
	lpm   = mu/lm;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
#ifndef _CYGWIN
	fftw_complex *s11k, *s22k, *s33k, *s12k, *s13k, *s23k;
	fftw_complex *s11, *s22, *s33, *s12, *s13, *s23;
#else
	complex *s11k, *s22k, *s33k, *s12k, *s13k, *s23k;
	complex *s11, *s22, *s33, *s12, *s13, *s23;
#endif

	rmax[0] = 0.0;
	rmax[1] = 0.0;
	rmax[2] = 0.0;
	*rssmax = 0.0;
	*sysmax = 0;
	
/*	
 * 	Compute the Schmid tensor associated with each slip plane
 */
	if (param->materialType != MAT_TYPE_FCC) {
		Fatal("FindFCCMaximumRSSLocation: Schmid tensors can only be calculated for FCC materials\n");
	}
	
	double n[12][3], b[12][3];
	double norm[3], burg[3];
	
	for (m = 0; m < 12; m++) {
		FCCSlipSystemBurgersNormal(home, m, norm, burg);
		for (i = 0; i < 3; i++) {
			n[m][i] = norm[i];
			b[m][i] = burg[i];
		}
	}
	

	s11k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s22k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s33k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s12k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s13k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s23k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	
	s11 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s22 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s33 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s12 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s13 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s23 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	
	
/*	
 * 	Compute the indenter stress on some planes at different 
 *  depths below the free surface. To maximize efficiency, the
 *  stress is only evaluated on nslab planes in a small region
 *  below the indenter where the analytical stress is known to 
 *  be maximum. The stress on each plane is calculated on a grid
 *  using FFTs.
 */
	int nslab = 3;
	for (p = 0; p < nslab; p++) {
		// z is the grid position
		z = -0.125*0.5*home->param->hs_Lzinf*(1.0+2.0*p/(nslab-1)); // 0.5 factor
		
		for (j=0; j<ny; j++) {
			ky = halfspace->ky[j];
			ky2 = ky*ky;

			for (i=0; i<nx; i++) {
				kx = halfspace->kx[i];
				kx2 = kx*kx;

				kz2 = kx2+ky2;
				kz  = sqrt(kz2);
				
				kxmy = kx2-ky2;
				kxy = kx*ky;
				
				expk = mu*exp(kz*z);

				double complex A = halfspace->A[i][j];
				double complex B = halfspace->B[i][j];
				double complex C = halfspace->C[i][j];
				
				s11k[j+i*ny] = 2*expk*((I*A*kx2*z-I*B*kxy-C*kx2)+I*A*kz*lmpm);
				s22k[j+i*ny] = 2*expk*((I*A*ky2*z+I*B*kxy-C*ky2)+I*A*kz*lmpm);
				s33k[j+i*ny] = 2*expk*((-I*A*z+C)*kz2+I*A*kz*l2mpm);

				s13k[j+i*ny] = expk*((2*A*kx*z-B*ky+2*I*C*kx)*kz-2*A*kx*lpm);
				s23k[j+i*ny] = expk*((2*A*ky*z+B*kx+2*I*C*ky)*kz-2*A*ky*lpm);
				s12k[j+i*ny] = I*expk*(2*A*kxy*z+B*kxmy+2*I*C*kxy);
				
				if (kx == 0.0 && ky == 0.0) {
					s13k[j+i*ny] = Tx0;
					s23k[j+i*ny] = Ty0;
					s33k[j+i*ny] = Tz0;
				}
				
			}
		}
		
		// Inverse Fourier Transforms
		fourier_transform_backward(s11,(fftw_complex *)s11k,nx,ny);
		fourier_transform_backward(s22,(fftw_complex *)s22k,nx,ny);
		fourier_transform_backward(s33,(fftw_complex *)s33k,nx,ny);
		fourier_transform_backward(s12,(fftw_complex *)s12k,nx,ny);
		fourier_transform_backward(s13,(fftw_complex *)s13k,nx,ny);
		fourier_transform_backward(s23,(fftw_complex *)s23k,nx,ny);
		
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				
				p1 = param->minSideX + i*(param->maxSideX-param->minSideX)/nx;
				p2 = param->minSideY + j*(param->maxSideY-param->minSideY)/ny;
				
				r[0] = p1;
				r[1] = p2;
				r[2] = z;
				
				s[0][0] = creal(s11[j+i*ny]);
				s[1][1] = creal(s22[j+i*ny]);
				s[2][2] = creal(s33[j+i*ny]);
				s[0][1] = creal(s12[j+i*ny]);
				s[0][2] = creal(s13[j+i*ny]);
				s[1][2] = creal(s23[j+i*ny]);
				
				s[1][0] = s[0][1];
				s[2][0] = s[0][2];
				s[2][1] = s[1][2];
				
#if 1
/*
 * 				Add the stress due to the dislocation segments
 */
				double ds[3][3];
				AllSegmentStress(home, halfspace, r[0], r[1], r[2], ds);
				for (k=0; k<3; k++) 
					for (l=0; l<3; l++)
						s[k][l] += ds[k][l];
#endif
				
/*
 * 				Calculate RSS for each slip system and find out the
 *              the maximum value (rssmax) and corresponding location 
 *              (rmax[0], rmax[1], rmax[2]) over all slip systems
 */
				sys = 0;
				sysrss = 0.0;
				for (m = 0; m < 12; m++) {
					rss = 0.0;
					for (k=0; k<3; k++) {
						for (l=0; l<3; l++) {
							rss += s[k][l]*n[m][k]*b[m][l];
						}
					}
					if (fabs(rss) > fabs(sysrss)) {
						sys = m;
						sysrss = rss;
					}
				}
				
				// Find RSS max
				if (fabs(sysrss) > fabs(*rssmax)) {
					rmax[0] = r[0];
					rmax[1] = r[1];
					rmax[2] = r[2];
					*rssmax = sysrss;
					*sysmax = sys;
				}
				
			}
		}
	}
	
	
	free(s11);
	free(s22);
	free(s33);
	free(s12);
	free(s13);
	free(s23);
	
	free(s11k);
	free(s22k);
	free(s33k);
	free(s12k);
	free(s13k);
	free(s23k);
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     GlissileNucleationPosition
 *      Description:  Nucleation of a glissile dislocation loop on a given
 *                    slip system at a specific location within the volume.
 *
 *-----------------------------------------------------------------------*/
void GlissileNucleationPosition(Home_t *home, HalfSpace_t *halfspace, 
                                double rnucl[3], int sysnucl, double rssnucl)
{
	int     i, j, nx, ny;
	double  HSLx, HSLy, px, py;
	double  radius, maxRadius, loopRadius;
	Param_t *param;
	
	param = home->param;
	if (!param->nucleation) return;
	
	//int nucleationFreq = 10;
	//if (home->cycle % nucleationFreq != 0) return;
	//if (home->cycle != 1) return;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	HSLx = halfspace->HSLx;
	HSLy = halfspace->HSLy;
	
	//printf("GlissileNucleationPosition\n");
	
	/* Imprint radius */
	maxRadius = halfspace->contact_radius;
	//printf("  imprint radius = %e\n", maxRadius);
	if (maxRadius == 0.0) return;
	
	
	int numNodes = 8;

	if (param->loopRadiusType == 0) {
		loopRadius = param->radiusNucleation*maxRadius;
		printf("  loopRadiusType = 0\n");
	} else if (param->loopRadiusType == 1) {
		loopRadius = param->loopRadius;
		printf("  loopRadiusType = 1\n");
	}

    printf("  imprint radius = %e\n", maxRadius);
	printf("  introduced loop radius = %e\n", loopRadius);

	//loopRadius = 5.0;
	//loopRadius = 0.25*maxRadius;
	//loopRadius = 2.0*maxRadius;
	//loopRadius = 0.25*maxRadius;
	//loopRadius = param->radiusNucleation*maxRadius;
	
	double n[3], b[3];

	switch(param->materialType) {
		case MAT_TYPE_BCC:
		    BCCSlipSystemBurgersNormal(home, sysnucl, n, b);
			break;
		case MAT_TYPE_FCC:
		    FCCSlipSystemBurgersNormal(home, sysnucl, n, b);
			break;
	}
	
	double sign = 1.0;
	if (rssnucl < 0.0) sign = -1.0;
	
	NucleateGlissileLoop(home, numNodes, loopRadius, rnucl[0], rnucl[1], rnucl[2], 
	                     sign*b[0], sign*b[1], sign*b[2], n[0], n[1], n[2]);
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     Nucleation
 *      Description:  
 *
 *-----------------------------------------------------------------------*/
void Nucleation(Home_t *home, HalfSpace_t *halfspace)
{
	Param_t *param;
	
	param = home->param;
	if (!param->nucleation) return;
	
	
	// Do not nucleate if not in equilibrium state
	if (!halfspace->equilibrium) {
		printf("  Not in equilibrium, leave nucleation\n");
		return;
	}
	
#if 0
	// List slip systems
	printf("Slip systems:\n");
	int m;
	double norm[3], burg[3];
	for (m = 0; m < 12; m++) {
		FCCSlipSystemBurgersNormal(home, m, norm, burg);
		printf("   id=%d, norm = %f %f %f, burg = %f %f %f\n",
		m,norm[0],norm[1],norm[2],burg[0],burg[1],burg[2]);
	}
#endif

	//**********************************************
	// ONLY NUCLEATE DURING LOADING
	//**********************************************
	/*
	if (halfspace->unloading != -1 && home->cycle > halfspace->unloading) {
		return;
	}
	*/
	// for BCC materials, nucleation can be 0 and 1;
	// for FCC materials, nucleation can be 0, 1 and 2
	if ((param->nucleation > 2) && (param->nucleation < 0)) {
		Fatal("The nucleation parameter should not be more than 2 or less than 0!");
	} else if (param->nucleation == 2) {
		switch(param->materialType) {
			case MAT_TYPE_BCC:
			    Fatal("The nucleation parameter for BCC materials should not be more than 1! Prismatic nucleation is only for FCC materials");
				break;
			case MAT_TYPE_FCC:
		        PrismaticNucleation(home, halfspace);
				break;
		}
	} else if (param->nucleation == 1 || param->nucleation == -1) {
		if (param->materialType == MAT_TYPE_BCC) {
			int  sysmax1, sysmax2;
			double  rmax1[3], rmax2[3], rssmax1, rssmax2;

		    // Find location with maximum RSS
		    FindBCCMaximumRSSLocation(home, halfspace, rmax1, rmax2, &sysmax1, &sysmax2,
			                          &rssmax1, &rssmax2);
			halfspace->BCCmaxRSS1 = fabs(rssmax1);
			halfspace->BCCmaxRSS2 = fabs(rssmax2);

		    // Critical nucleation stress
		    if ((fabs(rssmax1) < param->BCCtauNucleation1) && (fabs(rssmax2) < param->BCCtauNucleation2)) {
			    printf(" BCCRSSmax1 = %e < BCCtauNucleation1 = %e and BCCRSSmax2 = %e < BCCtauNucleation2 = %e. There is no nucleation.\n", fabs(rssmax1), param->BCCtauNucleation1, fabs(rssmax2), param->BCCtauNucleation2);
			return;
		    }


		    if (((fabs(rssmax1) - param->BCCtauNucleation1)/param->BCCtauNucleation1 > (fabs(rssmax2) - param->BCCtauNucleation2)/param->BCCtauNucleation2)) {
			    GlissileNucleationPosition(home, halfspace, rmax1, sysmax1, rssmax1);
		    } else {
				GlissileNucleationPosition(home, halfspace, rmax2, sysmax2, rssmax2);
			}

#if 0
		    if (fabs(rssmax1) >= param->BCCtauNucleation1) {
			    GlissileNucleationPosition(home, halfspace, rmax1, sysmax1, rssmax1);
		    }

		    if (fabs(rssmax2) >= param->BCCtauNucleation2) {
			    GlissileNucleationPosition(home, halfspace, rmax2, sysmax2, rssmax2);
		    }
#endif

		} else if (param->materialType == MAT_TYPE_FCC) {
			int  sysmax;
			double  rmax[3], rssmax;

		    // Find location with maximum RSS
		    FindFCCMaximumRSSLocation(home, halfspace, rmax, &sysmax, &rssmax);
		    halfspace->FCCmaxRSS = fabs(rssmax);
		
		    //printf(" MaximumRSSLocation: %e %e %e,\n system = %d, RSS = %e\n", 
		    //rmax[0], rmax[1], rmax[2], sysmax, rssmax);
		
		    // Critical nucleation stress
		    if (fabs(rssmax) < param->FCCtauNucleation) {
			    printf(" FCCRSSmax = %e < FCCtauNucleation = %e. There is no nucleation.\n", fabs(rssmax), param->FCCtauNucleation);
			return;
		    }

		    if (fabs(rssmax) > param->FCCtauNucleation) {
			    printf(" FCCRSSmax = %e > FCCtauNucleation = %e.\n", fabs(rssmax), param->FCCtauNucleation);
		    }

		    GlissileNucleationPosition(home, halfspace, rmax, sysmax, rssmax);
		
	    }
	}
	
	//PrismaticNucleation(home, halfspace);
	//GlissileNucleation(home, halfspace);
	//HalfLoopNucleation(home, halfspace);
}
#endif


#ifdef _SURFACE_STEPS
/*-------------------------------------------------------------------------
 *
 *      Function:     CreateNewSurfaceSegment
 *      Description:  Add a new surface segment to the surface segment
 *                    list.
 *
 *-----------------------------------------------------------------------*/
int CreateNewSurfaceSegment(HalfSpace_t *halfspace)
{
	int newSurfSeg = halfspace->surfStepSegCount++;
	
	if (newSurfSeg >= halfspace->surfStepSegAllocSize) {
		halfspace->surfStepSegAllocSize += 100;
		surfaceStepSeg_t *newSurfStepSegList;
		newSurfStepSegList = (surfaceStepSeg_t*)realloc(halfspace->surfStepSegList,
						      sizeof(surfaceStepSeg_t)*halfspace->surfStepSegAllocSize);
		if (newSurfStepSegList == (surfaceStepSeg_t*)NULL) {
			Fatal("Error reallocating surface segment memory!\n");
		} else {
			halfspace->surfStepSegList = newSurfStepSegList;
		}
	}
	
	halfspace->surfStepSegList[newSurfSeg].open = 1;
	
	return newSurfSeg;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     ReadSurfaceSegment
 *      Description:  Read surface segments information from the restart
 *                    files.
 *
 *-----------------------------------------------------------------------*/
void ReadSurfaceSegment(Home_t *home, HalfSpace_t *halfspace)
{
	int     newSurfSeg, id1, id2;
	double  pos1[3], pos2[3], burg[3], norm[3];
	surfaceStepSeg_t *surfStepSegList;
	
	printf("Read surface segments\n");
	
	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	char tmpFile[256], segFile[256];
	char *start, *sep;
	
/*
 *  Set surface segments file name based on datafile
 *  name. Both files MUST have the same name, only their
 *  extension should be different.
 *  e.g.: simulation1.data and simulation1.surf
 */
	sprintf(tmpFile, "../%s", home->param->dataFile);
	start = strrchr(segFile, '/');
	if (start == (char *)NULL) start = tmpFile;
	sep = strrchr(start, '.');
	if ((sep != (char *)NULL) && (sep > start)) *sep = 0;
	sprintf(segFile, "%s.surf", tmpFile);
	
	printf(" Surface segments file: %s\n", segFile);
	
	fp = fopen(segFile, "r");
	if (fp == NULL) {
		Fatal("Cannot open surface segment file %s", segFile);
	}
	
	while (1) {
		if (fscanf(fp, "%i %i %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
		&id1, &id2, pos1, pos1+1, pos1+2, pos2, pos2+1, pos2+2, 
		burg, burg+1, burg+2, norm, norm+1, norm+2) != 14) break;
		
		/* Create surface segment */
		newSurfSeg = CreateNewSurfaceSegment(halfspace);
		surfStepSegList = halfspace->surfStepSegList;
		
		if (id1 == -1) {
			surfStepSegList[newSurfSeg].virtual1 = 1;
			surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
		} else {
			surfStepSegList[newSurfSeg].virtual1 = 0;
			surfStepSegList[newSurfSeg].node1 = home->nodeKeys[id1];
		}
		
		if (id2 == -1) {
			surfStepSegList[newSurfSeg].virtual2 = 1;
			surfStepSegList[newSurfSeg].node2 = (Node_t *)NULL;
		} else {
			surfStepSegList[newSurfSeg].virtual2 = 0;
			surfStepSegList[newSurfSeg].node2 = home->nodeKeys[id2];
		}
		
		surfStepSegList[newSurfSeg].pos1[0] = pos1[0];
		surfStepSegList[newSurfSeg].pos1[1] = pos1[1];
		surfStepSegList[newSurfSeg].pos1[2] = pos1[2];
		
		surfStepSegList[newSurfSeg].pos2[0] = pos2[0];
		surfStepSegList[newSurfSeg].pos2[1] = pos2[1];
		surfStepSegList[newSurfSeg].pos2[2] = pos2[2];
		
		surfStepSegList[newSurfSeg].burg[0] = burg[0];
		surfStepSegList[newSurfSeg].burg[1] = burg[1];
		surfStepSegList[newSurfSeg].burg[2] = burg[2];
	
		surfStepSegList[newSurfSeg].norm[0] = norm[0];
		surfStepSegList[newSurfSeg].norm[1] = norm[1];
		surfStepSegList[newSurfSeg].norm[2] = norm[2];
		
	}

	fclose(fp);
	if (line) free(line);
	
	printf(" %d surface segments have been read\n\n", halfspace->surfStepSegCount);
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     InitSurfaceSteps
 *      Description:  Initialize surface segments
 *
 *-----------------------------------------------------------------------*/
void InitSurfaceSteps(Home_t *home, HalfSpace_t *halfspace)
{
	/* Initialize surface step list */
	halfspace->surfStepSegAllocSize = 100;
	halfspace->surfStepSegCount = 0;
	halfspace->surfStepSegList = (surfaceStepSeg_t*)malloc(sizeof(surfaceStepSeg_t)*
	                              halfspace->surfStepSegAllocSize);
	
	/* Restart simulation */
	if (home->param->cycleStart > 0) {
		ReadSurfaceSegment(home, halfspace);
	}
}

/*-------------------------------------------------------------------------
 *
 *      Function:     TrigDisplacementField
 *      Description:  Computes the displacement field at field point r
 *                    for a triangular dislocation loop.
 *      Warning:      For computational efficiency, only the solid angle
 *                    contribution is computed. This should provide a 
 *                    good approximation for the surface steps.
 *
 *-----------------------------------------------------------------------*/
void TrigDisplacementField(double x, double y, double z, double pax, double pay, double paz, 
		                   double pbx, double pby, double  pbz, double pcx, double pcy, double pcz, 
		                   double bx, double by, double bz, double NU, double u[3])
{
	double  Tab_x, Tab_y, Tab_z;
	double  Tbc_x, Tbc_y, Tbc_z;
	double  Tca_x, Tca_y, Tca_z;
	double  Pa_x, Pa_y, Pa_z, Ra;
	double  Pb_x, Pb_y, Pb_z, Rb;
	double  Pc_x, Pc_y, Pc_z, Rc;
	double  La_x, La_y, La_z;
	double  Lb_x, Lb_y, Lb_z;
	double  Lc_x, Lc_y, Lc_z;
	double  LaLb, LbLc, LcLa, Lan;
	double  nx, ny, nz;
	double  a0, b0, c0, s0;
	double  tana, tanb, tanc, tans, tanp;
	double  mag, sign, Omega;
	
	double tol = 1e-10;
	
	Tab_x = pbx - pax;
	Tab_y = pby - pay;
	Tab_z = pbz - paz;
	mag = sqrt(Tab_x*Tab_x + Tab_y*Tab_y + Tab_z*Tab_z);
	if (mag > tol) {
		Tab_x = Tab_x / mag;
		Tab_y = Tab_y / mag;
		Tab_z = Tab_z / mag;
	} else {
		Tab_x = 0.0;
		Tab_y = 0.0;
		Tab_z = 0.0;
	}
	
	Tbc_x = pcx - pbx;
	Tbc_y = pcy - pby;
	Tbc_z = pcz - pbz;
	mag = sqrt(Tbc_x*Tbc_x + Tbc_y*Tbc_y + Tbc_z*Tbc_z);
	if (mag > tol) {
		Tbc_x = Tbc_x / mag;
		Tbc_y = Tbc_y / mag;
		Tbc_z = Tbc_z / mag;
	} else {
		Tbc_x = 0.0;
		Tbc_y = 0.0;
		Tbc_z = 0.0;
	}
	
	Tca_x = pax - pcx;
	Tca_y = pay - pcy;
	Tca_z = paz - pcz;
	mag = sqrt(Tca_x*Tca_x + Tca_y*Tca_y + Tca_z*Tca_z);
	if (mag > tol) {
		Tca_x = Tca_x / mag;
		Tca_y = Tca_y / mag;
		Tca_z = Tca_z / mag;
	} else {
		Tca_x = 0.0;
		Tca_y = 0.0;
		Tca_z = 0.0;
	}
	
	Pa_x = pax - x;
	Pa_y = pay - y;
	Pa_z = paz - z;
	Ra = sqrt(Pa_x*Pa_x + Pa_y*Pa_y + Pa_z*Pa_z);
	if (Ra > tol) {
		La_x = Pa_x / Ra;
		La_y = Pa_y / Ra;
		La_z = Pa_z / Ra;
	} else {
		La_x = 0.0;
		La_y = 0.0;
		La_z = 0.0;
	}
	
	Pb_x = pbx - x;
	Pb_y = pby - y;
	Pb_z = pbz - z;
	Rb = sqrt(Pb_x*Pb_x + Pb_y*Pb_y + Pb_z*Pb_z);
	if (Rb > tol) {
		Lb_x = Pb_x / Rb;
		Lb_y = Pb_y / Rb;
		Lb_z = Pb_z / Rb;
	} else {
		Lb_x = 0.0;
		Lb_y = 0.0;
		Lb_z = 0.0;
	}
	
	Pc_x = pcx - x;
	Pc_y = pcy - y;
	Pc_z = pcz - z;
	Rc = sqrt(Pc_x*Pc_x + Pc_y*Pc_y + Pc_z*Pc_z);
	if (Rc > tol) {
		Lc_x = Pc_x / Rc;
		Lc_y = Pc_y / Rc;
		Lc_z = Pc_z / Rc;
	} else {
		Lc_x = 0.0;
		Lc_y = 0.0;
		Lc_z = 0.0;
	}
	
	/* Counter-clockwise normal */
	xvector(Tab_x, Tab_y, Tab_z, -Tca_x, -Tca_y, -Tca_z, &nx, &ny, &nz);
	mag = sqrt(nx*nx + ny*ny + nz*nz);
	if (mag > tol) {
		nx = nx / mag;
		ny = ny / mag;
		nz = nz / mag;
	} else {
		nx = 0.0;
		ny = 0.0;
		nz = 0.0;
	}
	
	LaLb = La_x*Lb_x + La_y*Lb_y + La_z*Lb_z;
	LbLc = Lb_x*Lc_x + Lb_y*Lc_y + Lb_z*Lc_z;
	LcLa = Lc_x*La_x + Lc_y*La_y + Lc_z*La_z;
	Lan  = La_x*nx + La_y*ny + La_z*nz;
	
	LaLb = MIN(MAX(LaLb, -1.0), 1.0);
	LbLc = MIN(MAX(LbLc, -1.0), 1.0);
	LcLa = MIN(MAX(LcLa, -1.0), 1.0);
	
	a0 = acos(LbLc);
	b0 = acos(LcLa);
	c0 = acos(LaLb);
	s0 = 0.5*(a0+b0+c0);
	
	tans = tan(0.5*s0);
	tana = tan(0.5*(s0-a0));
	tanb = tan(0.5*(s0-b0));
	tanc = tan(0.5*(s0-c0));
	tanp = MAX(0.0, tans*tana*tanb*tanc);
	sign = (Lan < -tol) ? -1 : (Lan > tol);
	Omega = -sign*(4.0*atan(sqrt(tanp)));
	
	if isnan(Omega) {
		printf("Omega Nan\n");
		printf(" a0 = %e, b0 = %e, c0 = %e, s0 = %e\n",
		       a0,b0,c0,s0);
		printf(" tans = %e, tana = %e, tanb = %e, tanc = %e, tanp = %e\n",
		       tans,tana,tanb,tanc,tanp);
	}
	
	/* WARNING: solid angle only */
	u[0] = -Omega/4.0/M_PI*bx;
	u[1] = -Omega/4.0/M_PI*by;
	u[2] = -Omega/4.0/M_PI*bz;
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     LoopDisplacementField
 *      Description:  Computes the displacement field at field point r
 *                    for a close planar dislocation loop by decomposing 
 *                    it into a series of triangular loops.
 *
 *-----------------------------------------------------------------------*/
void LoopDisplacementField(Home_t *home, int numNodes, double *Xp, double *Yp, double *Zp, 
                           double b[3], double n[3], double r[3], double u[3])
{
	int    i, i1;
	double cx, cy, cz, dot, NU, u0[3];
	
	NU = home->param->pois;
	VECTOR_ZERO(u);
	
	/* Center of the loop */
	cx = cy = cz = 0.0;
	for (i = 0; i < numNodes; i++) {
		cx += Xp[i];
		cy += Yp[i];
		cz += Zp[i];
	}
	cx /= numNodes;
	cy /= numNodes;
	cz /= numNodes;
	
	/* Calculate displacement field for each triangle */
	for (i = 0; i < numNodes; i++) {
		if ((i1 = i+1) == numNodes) i1 = 0;
		
		// Check that segment is comprised in the plane
		dot = (Xp[i1]-Xp[i])*n[0]+(Yp[i1]-Yp[i])*n[1]+(Zp[i1]-Zp[i])*n[2];
		if (dot > 1e-5) {
			Fatal("Loop is not coplanar");
		}
		
		TrigDisplacementField(r[0], r[1], r[2], Xp[i], Yp[i], Zp[i], 
		                      Xp[i1], Yp[i1], Zp[i1], cx, cy, cz, 
		                      b[0], b[1], b[2], NU, u0);
		VECTOR_ADD(u, u0);
	}
}

/*-------------------------------------------------------------------------
 *
 *      Function:     UpdateSurfaceSteps
 *      Description:  Computes the plastic displacement associated with
 *                    the surface steps. To gain efficiency, only the
 * 	                  displacements of open surface segments are calculated
 *                    at each step, while those of closed surface segments
 *                    are retrieved from previous calculations.
 *
 *-----------------------------------------------------------------------*/
void UpdateSurfaceSteps(Home_t *home, HalfSpace_t *halfspace)
{
	int    i, j, k, nx, ny, storeStep;
	double HSLx, HSLy, r[3], L, magt, tol;
	double p1[3], p2[3], b[3], n[3], t[3], z[3], u[3];
	double Xd[4], Yd[4], Zd[4];
	Node_t *node1, *node2;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	HSLx = halfspace->HSLx;
	HSLy = halfspace->HSLy;
	
	L = halfspace->LenVirtualSeg;
	tol = 1e-10;
	
	printf(" Update surface steps, surface segments = %d\n", halfspace->surfStepSegCount);
	
/*
 * 	Initialize surface plastic displacement with closed 
 *  surface segments.
 */
	for (i = 0; i < nx; i++) {
		for (j = 0; j < ny; j++) {
			halfspace->Uz_plas[j+i*ny] = halfspace->Uz_steps[j+i*ny];
		}
	}
	
	if (halfspace->surfStepSegCount == 0) return;

/*
 * 	Add dispacement of open surface segments.
 */
	surfaceStepSeg_t *surfStepSegList = halfspace->surfStepSegList;
	for (k = 0; k < halfspace->surfStepSegCount; k++) {
		
		if (surfStepSegList[k].open == 0) continue;
		storeStep = 0;
/*
 * 		If both nodes are virtual, the surface segment has become
 * 		closed and its corresponding surface displacement should
 * 		be stored in Uz_steps to avoid recomputing it during the
 *      subsequent steps.
 */		
		if (surfStepSegList[k].virtual1 && surfStepSegList[k].virtual2) {
			storeStep = 1;
			surfStepSegList[k].open = 0;
		}
		
		p1[0] = surfStepSegList[k].pos1[0];
		p1[1] = surfStepSegList[k].pos1[1];
		p1[2] = surfStepSegList[k].pos1[2];
		
		p2[0] = surfStepSegList[k].pos2[0];
		p2[1] = surfStepSegList[k].pos2[1];
		p2[2] = surfStepSegList[k].pos2[2];
		
		PBCPOSITION(home->param, p1[0], p1[1], p1[2], &p2[0], &p2[1], &p2[2]);
		
		t[0] = p2[0]-p1[0];
		t[1] = p2[1]-p1[1];
		t[2] = p2[2]-p1[2];
		
		magt = t[0]*t[0]+t[1]*t[1]+t[2]*t[2];
		if (magt < tol) continue;
		
		NormalizeVec(t);
		z[0] = 0.0; z[1] = 0.0; z[2] = 1.0;
		NormalizedCrossVector(z, t, n);
		
		/* Only keep the z component */
		b[0] = 0.0;
		b[1] = 0.0;
		b[2] = surfStepSegList[k].burg[2];
		
		if (fabs(b[2]) < tol) continue;
		
		//n[0] = surfStepSegList[k].norm[0];
		//n[1] = surfStepSegList[k].norm[1];
		//n[2] = surfStepSegList[k].norm[2];
		
		/* Construct virtual screw loop dipole */
		Xd[0] = p1[0] + L*z[0];
		Yd[0] = p1[1] + L*z[1];
		Zd[0] = p1[2] + L*z[2];
		Xd[1] = p2[0] + L*z[0];
		Yd[1] = p2[1] + L*z[1];
		Zd[1] = p2[2] + L*z[2];
		Xd[2] = p2[0] - L*z[0];
		Yd[2] = p2[1] - L*z[1];
		Zd[2] = p2[2] - L*z[2];
		Xd[3] = p1[0] - L*z[0];
		Yd[3] = p1[1] - L*z[1];
		Zd[3] = p1[2] - L*z[2];
		
/*
 * 		Calculate surface plastic displacement on the grid 
 */
		VECTOR_ZERO(r);
		for (i = 0; i < nx; i++) {
			r[0] = ((1.0*i)/nx)*HSLx-0.5*HSLx;
			for (j = 0; j < ny; j++) {
				r[1] = ((1.0*j)/ny)*HSLy-0.5*HSLy;
				
				LoopDisplacementField(home, 4, Xd, Yd, Zd, b, n, r, u);
				
				halfspace->Uz_plas[j+i*ny] += u[2];
				if (storeStep) {
					halfspace->Uz_steps[j+i*ny] += u[2];
				}
			}
		}
		
	}
	
#if 0
	/* Output Uz_plas */
	FILE *fp;
	char format[100];
	sprintf(format,"Uz_plas_%d.out",home->cycle);
	fp = fopen(format,"w");
	for (i = 0; i < nx; i++) {
		r[0] = ((1.0*i)/nx)*HSLx-0.5*HSLx;
		for (j = 0; j < ny; j++) {
			r[1] = ((1.0*j)/ny)*HSLy-0.5*HSLy;
			fprintf(fp,"%e %e %e\n", r[0], r[1], halfspace->Uz_plas[j+i*ny]);
		}
	}
	fclose(fp);
#endif
	
	return;
}
#endif


#ifdef _HSIMGSTRESS
#ifdef _INDENTATION
/*-------------------------------------------------------------------------
 *
 *      Function:     HS_Init_Indentation
 *      Description:  Initialize indentation arrays
 *
 *-----------------------------------------------------------------------*/
void HS_Init_Indentation(Home_t *home, HalfSpace_t *halfspace)
{

	Param_t *param;
	param = home->param;

	int    i, j;
	double c[3];
	
	int nx = halfspace->nx;
	int ny = halfspace->ny;
	
	/* Initialize surface fields */
	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
			halfspace->Uz_elas[j+i*ny] = 0.0;
			halfspace->Uz_plas[j+i*ny] = 0.0;
			halfspace->Tzext[j+i*ny]   = 0.0;
		}
	}
	
	/* Initialize indenter shape at the center of the free surface */
	c[0] = halfspace->p_indenter[0] + halfspace->HSLx/2.0;
	c[1] = halfspace->p_indenter[1] + halfspace->HSLy/2.0;
	c[2] = halfspace->p_indenter[2];
	
	UpdateIndenterPosition(home, halfspace, 1, c);
	
	/* Initialize max RSS */
	switch(param->materialType) {
		case MAT_TYPE_BCC:
		    halfspace->BCCmaxRSS1 = 0.0;
			halfspace->BCCmaxRSS2 = 0.0;
			halfspace->BCCmaxRSS3 = 0.0;
			break;
		case MAT_TYPE_FCC:
		    halfspace->FCCmaxRSS = 0.0;
			break;
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     PointInPolygon2D
 *      Description:  Determines whether the point (x,y) is inside 
 * 					  a n-sided polygon (Xp,Yp).
 * 					  This uses the ray-casting algorithm.
 *
 *-----------------------------------------------------------------------*/
int PointInPolygon2D(int n, double *Xp, double *Yp, double x, double y)
{
	int i, j, c = 0;
	for (i = 0, j = n-1; i < n; j = i++) {
		if ( ((Yp[i]>y) != (Yp[j]>y)) &&
		     (x < (Xp[j]-Xp[i]) * (y-Yp[i]) / (Yp[j]-Yp[i]) + Xp[i]) )
			c = !c;
	}
	return c;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     SolveBerkovich
 *      Description:  Solve for the height of the blunted Berkovich 
 *                    indenter knowing the surface imprint area and the
 *                    shape coefficients of the indenter.
 *
 *-----------------------------------------------------------------------*/
double SolveBerkovich(double *C, double A)
{
	int i, j, maxiter, conv;
	double h0, h1, f, df, err, dh, A1, A0;
	double tol, eps;
	
	h0  = 1.e-2;
	tol = 1.e-7;
	eps = 1.e-15;

	maxiter = 1000;
	conv = 0;

	if (A < tol) {
		return 0.0;
	}

/* 
 * 	Try to solve using the Newton-Rahpson method
 */
	for (i = 0; i < maxiter; i++) {
    
		if (h0 < 0.0) break;
		
		f = -A;
		df = 0.0;
		for (j = 0; j < 9; j++) {
			f += C[j]*pow(h0, pow(2.0, 1.0-j));
			df += C[j]*pow(2.0, 1.0-j)*pow(h0, pow(2.0, 1.0-j)-1.0);
		}

		if (fabs(df) < eps) break;
		
		h1 = h0 - f/df;
		err = fabs(h1-h0)/fabs(h1);
		if (err <= tol) {
			conv = 1;
			break;
		}
		
		h0 = h1;
	}
	
/* 	
 * 	If the Newton-Rahpson method has failed, 
 *  use tabulated values
 */
	if (conv == 0) {
		dh = 1.e-2;
		h1 = 0.0;
		A1 = 0.0;
		while (A1 < A) {
			h0 = h1;
			A0 = A1;
			h1 += dh;
			A1 = 0.0;
			for (j = 0; j < 9; j++) {
				A1 += C[j]*pow(h1, pow(2.0, 1.0-j));
			}
		}
		h0 = h0+(A-A0)*(h1-h0)/(A1-A0);
	}
	
	return h0;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     UpdateIndenterPosition
 *      Description:  Updates the indenter shape specified by the location 
 *                    of the indenter tip
 *
 *-----------------------------------------------------------------------*/
void UpdateIndenterPosition(Home_t *home, HalfSpace_t *halfspace, int init, double center[3])
{
	int    i, j;
	double px, py;
	
	int nx = halfspace->nx;
	int ny = halfspace->ny;

	double HSLx = halfspace->HSLx;
	double HSLy = halfspace->HSLy;
	
	if (init) printf("******************************************************************\n");
	if (halfspace->indenter_type == 1) {
		
		if (init) printf("Initializing Parabolic indenter with radius R = %e\n", halfspace->R_indenter);
		double R = halfspace->R_indenter;
		// The code below (L1959-1965) corresponds to Equ. A.1 in MSMSE 16(2018)055004
		for (i=0; i<nx; i++) {
			px = ((1.0*i)/nx)*HSLx;
			for (j=0; j<ny; j++) {
				py = ((1.0*j)/ny)*HSLy;
				halfspace->Uz_indenter[j+i*ny] = ((px-center[0])*(px-center[0])+
				                                  (py-center[1])*(py-center[1]))/(2.0*R) + center[2];
			}
		}

	} else if (halfspace->indenter_type == 2) {
		
#if 0		
/*		Initialize discretized perfect Berkovich indenter
 * 		by reconstructing each facet
 */			
		if (init) printf("Initializing Berkovich indenter with parameter h = %e\n", halfspace->h_indenter);
		double h, b, a, l, c;
		double Xv[4], Yv[4], Zv[4];
		double Xp1[3], Yp1[3], Xp2[3], Yp2[3], Xp3[3], Yp3[3];
		double n1x, n1y, n1z;
		double n2x, n2y, n2z;
		double n3x, n3y, n3z;
		
		/* Berkovitch */
		h = halfspace->h_indenter;
		b = h*tan(65.27*PI/180.0);
		a = b*2.0*sqrt(3.0);
		l = 0.5*sqrt(3.0)*a;
		c = 2.0*b;
		
		/* Indenter vertices */
		Xv[0] = -b;  Yv[0] = 0.5*a;  Zv[0] = h;
		Xv[1] = c;   Yv[1] = 0.0;    Zv[1] = h;
		Xv[2] = -b;  Yv[2] = -0.5*a; Zv[2] = h;
		Xv[3] = 0.0; Yv[3] = 0.0;    Zv[3] = 0.0;

		/* Indenter facets normals */
		xvector(Xv[0], Yv[0], Zv[0], Xv[2], Yv[2], Zv[2], &n1x, &n1y, &n1z);
		Normalize(&n1x, &n1y, &n1z);
		xvector(Xv[1], Yv[1], Zv[1], Xv[0], Yv[0], Zv[0], &n2x, &n2y, &n2z);
		Normalize(&n2x, &n2y, &n2z);
		xvector(Xv[2], Yv[2], Zv[2], Xv[1], Yv[1], Zv[1], &n3x, &n3y, &n3z);
		Normalize(&n3x, &n3y, &n3z);
		
		/* Indenter facets projections */
		Xp1[0] = Xv[0];  Yp1[0] = Yv[0];
		Xp1[1] = Xv[3];  Yp1[1] = Yv[3];
		Xp1[2] = Xv[2];  Yp1[2] = Yv[2];
		
		Xp2[0] = Xv[1];  Yp2[0] = Yv[1];
		Xp2[1] = Xv[3];  Yp2[1] = Yv[3];
		Xp2[2] = Xv[0];  Yp2[2] = Yv[0];
		
		Xp3[0] = Xv[2];  Yp3[0] = Yv[2];
		Xp3[1] = Xv[3];  Yp3[1] = Yv[3];
		Xp3[2] = Xv[1];  Yp3[2] = Yv[1];
		
		for (i=0; i<nx; i++) {
			px = ((1.0*i)/nx)*HSLx-center[0];
			for (j=0; j<ny; j++) {
				py = ((1.0*j)/ny)*HSLy-center[1];
				
				if (PointInPolygon2D(3, Xp1, Yp1, px, py)) {
					halfspace->Uz_indenter[j+i*ny] = (-n1x*px-n1y*py)/n1z;
				} else if (PointInPolygon2D(3, Xp2, Yp2, px, py)) {
					halfspace->Uz_indenter[j+i*ny] = (-n2x*px-n2y*py)/n2z;
				} else if (PointInPolygon2D(3, Xp3, Yp3, px, py)) {
					halfspace->Uz_indenter[j+i*ny] = (-n3x*px-n3y*py)/n3z;
				} else {
					halfspace->Uz_indenter[j+i*ny] = 10.0*h;
				}
				
				halfspace->Uz_indenter[j+i*ny] += center[2];
			}
		}
				
#else

/*		Initialize discretized Berkovich indenter using
 * 		using a Newton-Raphson method to solve for the
 * 		indenter z-coordinate given its cross-section area.
 *      The code below (L2042-2098) corresponds to Equ. A.2-A.3 in MSMSE 16(2018)055004
 */			
		if (init) printf("Initializing Berkovich indenter with tip radius R = %e\n", halfspace->R_indenter);
		
		double scale, C[9], Lx, Ly, hmax;
		double px, py, a, b, A, h;
		
		if (halfspace->R_indenter == 0.0) {
			
			scale = 1.0;
			C[0] = 24.5;
			C[1] = C[2] = C[3] = C[4] = 0.0;
			C[5] = C[6] = C[7] = C[8] = 0.0;
			
		} else if (halfspace->R_indenter > 500.0) {
			Fatal("Error: Berkovich indenter tip must smaller than 500b");
			
		} else {
			
			scale = halfspace->R_indenter/40.0;
			C[0] = 24.65;
			C[1] = 202.7;
			C[2] = 0.03363;
			C[3] = 0.9318;
			C[4] = 0.02827;
			C[5] = 0.03716;
			C[6] = 1.763;
			C[7] = 0.04102;
			C[8] = 1.881;
		}
		
		/* Rescale tip radius */
		Lx = HSLx/scale;
		Ly = HSLy/scale;
		hmax = halfspace->h_indenter;
		
		for (i=0; i<nx; i++) {
			px = ((1.0*i)/nx)*Lx-center[0]/scale;
			for (j=0; j<ny; j++) {
				py = ((1.0*j)/ny)*Ly-center[1]/scale;
				
				if (px < 0.0) {
					b = abs(px);
					a = sqrt(3.0)*2.0*b;
					if (fabs(py) <= 0.5*a) {
						A = sqrt(3.0)/4.0*a*a;
						h = SolveBerkovich(C, A);
						halfspace->Uz_indenter[j+i*ny] = MIN(h, hmax) + center[2];
						continue;
					}
				}
				b = abs(py) + px/sqrt(3.0);
				a = 3.0*b;
				A = sqrt(3.0)/4.0*a*a;
				h = SolveBerkovich(C, A);
				halfspace->Uz_indenter[j+i*ny] = MIN(h, hmax) + center[2];
				
			}
		}
#endif
			
	} else if (halfspace->indenter_type == 0) {
		
		// Flat punch (debug)
		if (init) printf("Initializing flat punch indenter with side a = %e\n", 2.0*halfspace->R_indenter);
		double R = halfspace->R_indenter;
		double hmax = halfspace->h_indenter;
		
		for (i=0; i<nx; i++) {
			px = ((1.0*i)/nx)*HSLx;
			for (j=0; j<ny; j++) {
				py = ((1.0*j)/ny)*HSLy;
				
				if (fabs(px-center[0]) <= R && fabs(py-center[1]) <= R) {
					halfspace->Uz_indenter[j+i*ny] = center[2];
				} else {
					halfspace->Uz_indenter[j+i*ny] = center[2] + hmax;
				}

			}
		}
		
	} else {
		Fatal("Indenter type %d is not a valid type", halfspace->indenter_type);
	}
	if (init) printf("******************************************************************\n\n");

	
	// ---------------------------------------
	//  Store the Indenter Value 
	if (init && home->myDomain == 0) {
		FILE *fp;
		char format[100];
		sprintf(format, "indenter_shape.out");

		fp = fopen(format,"w");

		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				fprintf(fp, "%e ", halfspace->Uz_indenter[j+i*ny]);
			}  
			fprintf(fp,"\n");
		}
		fclose(fp);
	}
	// --------------------------------------
}


/*-------------------------------------------------------------------------
 *
 *      Function:     WriteIndentationInfo
 *      Description:  Write indentation simulation information in a file
 *
 *-----------------------------------------------------------------------*/
void WriteIndentationInfo(Home_t *home, HalfSpace_t *halfspace)
{
	int i, j;
	Node_t *node, *nbr;
	double hdisavg, updens, drx, dry, drz, drlen;
	int nnodes = 0;
	Param_t *param;
	
	param = home->param;
	
	// Save values for restart files
	VECTOR_COPY(param->p_indenter, halfspace->p_indenter);
	param->F_indenter = halfspace->F_indenter;
	
	//if (home->cycle == 0) return;
	
	// Avg depth of dislocation //
	hdisavg = 0.0;
	updens = 0.0;
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		if (node->z > -0.5*halfspace->HSLzinf) {
			for (j = 0; j < node->numNbrs; j++) {
				nbr = GetNeighborNode(home, node, j);
				if (nbr == (Node_t *)NULL) continue;
				/* Avoid double counting */
				if (OrderNodes(node, nbr) != -1) continue;
				drx = node->x - nbr->x;
				dry = node->y - nbr->y;
				drz = node->z - nbr->z;
				ZImage(param, &drx, &dry, &drz);
				updens += sqrt(drx*drx+dry*dry+drz*drz);
			}
		}
		hdisavg += node->z;
		nnodes++;
	}
	if (nnodes != 0) hdisavg /= nnodes;
	updens = updens/(0.5*param->simVol)/param->burgMag/param->burgMag;
	

	if (home->myDomain == 0) {
		FILE *fp;
		char format[100];
		sprintf(format,"indendata.txt");
		fp = fopen(format,"a");

		switch(param->materialType) {
			case MAT_TYPE_BCC:
			    fprintf(fp,"%d %e %e %e %e %e %e %e %e %e %e %e\n", home->cycle,
				        halfspace->p_indenter[0], halfspace->p_indenter[1], -halfspace->p_indenter[2],
						halfspace->area_indent, halfspace->F_indenter, hdisavg, home->param->disloDensity,
						halfspace->BCCmaxRSS1, halfspace->BCCmaxRSS2, halfspace->BCCmaxRSS3, updens);
				break;
			case MAT_TYPE_FCC:
			    fprintf(fp,"%d %e %e %e %e %e %e %e %e %e\n", home->cycle,
				        halfspace->p_indenter[0], halfspace->p_indenter[1], -halfspace->p_indenter[2],
						halfspace->area_indent, halfspace->F_indenter,
						hdisavg, home->param->disloDensity, halfspace->FCCmaxRSS, updens);
				break;
		}

		fclose(fp);
	}
	
	return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:     SolveSurfaceContact
 *      Description:  Solve the surface contact between the free surface
 *                    and the indenter using the iterative spectral approach.
 *
 *-----------------------------------------------------------------------*/
void SolveSurfaceContact(Home_t *home, HalfSpace_t *halfspace)
{

	int    i, j, ind, nx, ny;
	double dx, dy, px, py;
	double *kx, *ky, *kz, *oneoverkz;
	double HSLx, HSLy, Lz;
	double lambda, mu, nu, mult, E;
	double areasum, forcesum, radius, r;
	double gap, minGap;
	double load_dir[3], ct, h, c[3];
	
	double dincr, F;
	double *pzext; // External pressure due to indentation, i.e., the contact pressure mentioned in the paper
	double *uz;    // Displacement due to indentation
	
	double tol, delta, error;
	int maxiter, iter, indarea;

	int filediv = 100;
	
	double tnow   = home->param->timeNow;
	double realdt = home->param->realdt;
	int cyclenow  = home->cycle;
	
	kx = halfspace->kx;
	ky = halfspace->ky;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	HSLx = halfspace->HSLx;
	HSLy = halfspace->HSLy;
	
	dx = (1.0/nx)*HSLx;
	dy = (1.0/ny)*HSLy;
	
	// Height of the simulation volume
	Lz = home->param->hs_Lzinf;
	
	// Elastic parameters
	lambda = halfspace->lambda;
	mu     = halfspace->mu;
	nu     = lambda/(2.0*(lambda+mu));
	mult   = (1.0-nu)/mu;
	E      = 2.0*mu*(1.0+nu);

#ifndef _CYGWIN
	fftw_complex *pzkext;
	fftw_complex *uzk;
	fftw_complex *uztmp;
	fftw_complex *pz;
#else
	complex *pzkext;
	complex *uzk;
	complex *uztmp;
	complex *pz;
#endif

	halfspace->equilibrium = 1;
	
	double sincr;
	if (home->cycle <= halfspace->unloading || halfspace->unloading == -1) {
		/* Loading */
		sincr = 1.0;
		printf(" Loading indentation");
	} else {
		/* Unloading */
		sincr = -1.0;
		printf(" Unloading indentation");
	}
	
	VECTOR_COPY(load_dir, halfspace->indenter_load_dir);
	printf(", direction = [%e %e %e]\n",load_dir[0],load_dir[1],load_dir[2]);
	
	if (halfspace->indenter_load < 2) {
	
		// Displacement control
		if (halfspace->indenter_load == 0) {
			
			/* Constant displacement rate */
			dincr = sincr*halfspace->v_indenter*realdt;
			
			/*
			// Constant acceleration rate 
			//v_indenter = sincr*halfspace->v_indenter*(halfspace->unloading-home->cycle)/halfspace->unloading;
			double v0 = 2.83e11;
			double acc = v0/(halfspace->unloading*home->param->maxDT);
			halfspace->v_indenter += -sincr*acc*realdt;
			halfspace->d_indenter += sincr*halfspace->v_indenter*realdt;
			printf("\n v_indenter = %e\n",halfspace->v_indenter);
			*/
			
			/*
			// Smooth turn-around
			double v0 = 7.08e10;
			int turn_steps = 500;
			double acc0 = v0/turn_steps/realdt;
			if (home->cycle <= 1000) {
				halfspace->d_indenter += v0*realdt;
			} else if (home->cycle <= (1000+turn_steps)) {
				halfspace->d_indenter += (v0-(home->cycle-1000)*acc0*realdt)*realdt;
			} else if (home->cycle <= (1000+2*turn_steps)) {
				halfspace->d_indenter -= ((home->cycle-1000-turn_steps)*acc0*realdt)*realdt;
			} else {
				halfspace->d_indenter -= v0*realdt;
			}
			*/
			
			
		} else if (halfspace->indenter_load == 1) {
			/* Quasi-static displacement 
			 * The indenter depth is incremented only
			 * if equilibrium has been reached */
			halfspace->equilibrium = CheckEquilibrium(home);
			if (halfspace->equilibrium) {
				dincr = sincr*halfspace->dd_indenter;
			}
		}
		
		// Update indenter position
		halfspace->p_indenter[0] += dincr*load_dir[0];
		halfspace->p_indenter[1] += dincr*load_dir[1];
		halfspace->p_indenter[2] += dincr*load_dir[2];
		
		c[0] = halfspace->p_indenter[0] + halfspace->HSLx/2.0;
		c[1] = halfspace->p_indenter[1] + halfspace->HSLy/2.0;
		c[2] = halfspace->p_indenter[2];
		h = c[2];
		
		UpdateIndenterPosition(home, halfspace, 0, c);
		
		printf(" d_z = %e\n", -h);
		
	
		// Check if we are expecting a contact problem
		int contact = 1;
	
#ifdef _SURFACE_STEPS
		minGap = Lz;
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				gap = halfspace->Uz_indenter[j+i*ny]-halfspace->Uz_plas[j+i*ny];
				minGap = MIN(gap, minGap);
			}
		}
		if (minGap > 0.0 || h >= 0.0) contact = 0;
#else
		if (h >= 0.0) contact = 0;
#endif
	
		if (contact == 0) {
			
			// No contact
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
					halfspace->Uz_elas[j+i*ny] = 0.0;
					halfspace->Tzext[j+i*ny] = 0.0;
				}
			}
			halfspace->F_indenter = 0.0;
			halfspace->area_indent = 0.0;
			halfspace->contact_radius = 0.0;
			
			WriteIndentationInfo(home, halfspace);
			
			printf(" no surface contact, minGap = %e > 0 or d_z <= 0\n", minGap);
			
			return;
		}
	
	} else {
		
		// Load control
		if (halfspace->indenter_load == 2) {
			/* Constant force rate */
			halfspace->F_indenter += sincr*halfspace->v_indenter*realdt;
		} else if (halfspace->indenter_load == 3) {
			/* Quasi-static force 
			 * The indenter depth is incremented only
			 * if equilibrium has been reached */
			halfspace->equilibrium = CheckEquilibrium(home);
			if (halfspace->equilibrium) {
				halfspace->F_indenter += sincr*halfspace->dF_indenter;
			}
		}
		printf("F_indenter = %e\n", halfspace->F_indenter);

		F = halfspace->F_indenter;
		
		// Check if we are expecting a contact problem
		if (F <= 0.0) {
			
			// No contact
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
					halfspace->Uz_elas[j+i*ny] = 0.0;
					halfspace->Tzext[j+i*ny] = 0.0;
				}
			}
			halfspace->area_indent = 0.0;
			halfspace->contact_radius = 0.0;
			
			WriteIndentationInfo(home, halfspace);
			
			printf(" no surface contact, F <= 0\n");
			
			return;
		}
	}
	
	oneoverkz = (double *)malloc(sizeof(double)*nx*ny);
	pzkext    = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	uzk       = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	uztmp     = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	pz        = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	pzext     = (double *)malloc(sizeof(double)*nx*ny);
	uz        = (double *)malloc(sizeof(double)*nx*ny);
	
	// ---- Allocate 1/kz ------ 
	oneoverkz[0] = 0.0;
	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
			if ((i > 0) || (j > 0)){
				oneoverkz[j+i*ny] = 1.0/sqrt(kx[i]*kx[i]+ky[j]*ky[j]);
			}
		}
	}

	// --------------------- Perform Main Iteration ------------------ //
	int itry, maxtry;
	int convergence;
	double preverror, preverror2, lasterror, maxTol;
	
	maxtry  = 20;   // Max number of trials
	tol     = 1e-6;  // Tolerance
	maxTol  = 1.0;
	maxiter = 10000;  // Max number of iterations
	
	int maxFreq = 10;     // Max frequency for oscillations detection
	double errorFreq[maxFreq], prevFreq[maxFreq];
	

	delta = 0.1/mult; // initial step size for adjusting traction force
	
	itry = 0;
	convergence = 0;
	
	while (itry < maxtry && !convergence) {
		
		//printf("itry = %d\n",itry);
		preverror  = 0.0;
		preverror2 = 0.0;
		
		for (i = 0; i < maxFreq; i++) {
			errorFreq[i] = 0.0;
			prevFreq[i]  = 0.0;
		}

		// Start with previous step solution
		forcesum = 0.0;
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				uz[j+i*ny]    = halfspace->Uz_elas[j+i*ny];
				pzext[j+i*ny] = halfspace->Tzext[j+i*ny];
				forcesum += pzext[j+i*ny];
			}
		}
		
		// Rescale pressure if force control
		if (halfspace->indenter_load > 1) {
			forcesum = (-1.0*forcesum)*dx*dy;
			if (forcesum == 0.0) {
				forcesum = F/(1.0*nx*ny*dx*dy);
				for (i=0; i<nx; i++) {
					for (j=0; j<ny; j++) {
						pzext[j+i*ny] = -forcesum;
					}
				}
			} else {
				for (i=0; i<nx; i++) {
					for (j=0; j<ny; j++) {
						pzext[j+i*ny] *= F/forcesum;
					}
				}
			}
		}
	
		for (iter = 0; iter<maxiter ; iter++) {  
			
			// pz is complex internal variables for performing fourier transform 
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
					pz[j+i*ny] = pzext[j+i*ny];
				}
			}

			// Fourier transform pz to 
			fourier_transform_forward(pz,(fftw_complex *)pzkext,nx,ny);

			// Obtain Fourier transform of uz 
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
					
					if (i == 0 && j == 0) {
						// Specify k=0 term corresponding to the uniform displacement mode
						uzk[0] = Lz/E*pzkext[0];
					} else {
						uzk[j+i*ny] = mult*pzkext[j+i*ny]*oneoverkz[j+i*ny];
					}
					
				}
			}

			/*
			// DEBUG ONLY
			if (iter < 10) {
				printf("   iter = %d, uzk[0] = (re) %e, (im) %e\n", iter, creal(uzk[0]),cimag(uzk[0]));
				printf("   iter = %d, pzkext[0] = (re) %e, (im) %e\n", iter, creal(pzkext[0]),cimag(pzkext[0]));
			}
			*/

			// Inverse Fourier Transform
			fourier_transform_backward(uztmp,(fftw_complex *)uzk,nx,ny);
			
			double hmin = 0.0;
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
					uz[j+i*ny] = creal(uztmp[j+i*ny])/(nx*ny);
					hmin = MIN(uz[j+i*ny], hmin);
				}
			}
			
			// Determine d for force control and update indenter position
			//if (halfspace->indenter_load > 1) d = -dmin;
			if (halfspace->indenter_load > 1) {
				h = hmin;
				halfspace->p_indenter[0] = h*load_dir[0]/load_dir[2];
				halfspace->p_indenter[1] = h*load_dir[1]/load_dir[2];
				halfspace->p_indenter[2] = h;

				c[0] = halfspace->p_indenter[0] + halfspace->HSLx/2.0;
				c[1] = halfspace->p_indenter[1] + halfspace->HSLy/2.0;
				c[2] = halfspace->p_indenter[2];
				
				UpdateIndenterPosition(home, halfspace, 0, c);
			}

			
			int contacts = 0;
			minGap = -Lz;

			// Get back the displacement 
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {

					// Calculate number of contact points
#ifdef _SURFACE_STEPS
					gap = uz[j+i*ny]-halfspace->Uz_indenter[j+i*ny]+halfspace->Uz_plas[j+i*ny];
#else
					gap = uz[j+i*ny]-halfspace->Uz_indenter[j+i*ny];
#endif
					if (gap >= 0.0) {
						contacts++;
					} else {
						minGap = MAX(minGap, gap);
					}
				}
			}

			// No contact
			if (h < 0.0 && contacts == 0) {
				//printf("No surface contact, minGap = %e\n", minGap);
			}

			/*
			// DEBUG ONLY
			if (iter < 10) {
				printf("maxuindz = %e, d = %e, contacts = %d\n",maxuindz,d,contacts);
				//printf("meanuz = %e, uzk[0] = %e\n",meanuz,creal(uzk[0])/(nx*ny));
			}
			*/

			// Adjust pz so that pz->0 where uz < u0-d (gap region)
			forcesum = 0.0;
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {  

#ifdef _SURFACE_STEPS
					pzext[j+i*ny] = pzext[j+i*ny]-(uz[j+i*ny]-halfspace->Uz_indenter[j+i*ny]+halfspace->Uz_plas[j+i*ny])*delta;
#else 
					pzext[j+i*ny] = pzext[j+i*ny]-(uz[j+i*ny]-halfspace->Uz_indenter[j+i*ny])*delta; 
#endif
					// Adjust pressure
					if (pzext[j+i*ny] > 0) pzext[j+i*ny] = 0.0;
					forcesum += pzext[j+i*ny];
				}
			}
			
			// Calculate error
			error = 0.0;
			
			if (halfspace->indenter_load < 2) {
				
				// Displacement control
				for (i=0; i<nx; i++) {
					for (j=0; j<ny; j++) {
						// Error estimate
#ifdef _SURFACE_STEPS
						error += fabs(halfspace->Uz_indenter[j+i*ny]-uz[j+i*ny]-halfspace->Uz_plas[j+i*ny])*fabs(pzext[j+i*ny]);
#else
						error += fabs(halfspace->Uz_indenter[j+i*ny]-uz[j+i*ny])*fabs(pzext[j+i*ny]);
#endif
					}
				}
				error /= (fabs(h)*nx*ny);
				
			} else {
				
				// Force control
				if (forcesum == 0.0) {
					error = 0.0;
				} else {
					forcesum = (-1.0*forcesum)*dx*dy;
					for (i=0; i<nx; i++) {
						for (j=0; j<ny; j++) {
							// Rescale pressure
							pzext[j+i*ny] *= F/forcesum;
							
							// Error estimate
							error += fabs(pzext[j+i*ny]-pz[j+i*ny]);
						}
					}
					error *= dx*dy/F;
				}
			}
			

			/*
			// DEBUG ONLY
			if (iter < 10) {

				FILE   *fp;
				char   format[20];
				double r[3];
				sprintf(format, "indenter_%d_%d_%d.out", home->cycle,itry,iter);
				fp = fopen(format,"w");

				for (i=0; i<nx; i++) {
					for (j=0; j<ny; j++) {

						r[0] = halfspace->Grid[0][i][j];
						r[1] = halfspace->Grid[1][i][j];
						r[2] = halfspace->Grid[2][i][j];

						fprintf(fp,"%e %e %e %e %e %e %e %e %e %e\n",
								r[0],r[1],r[2],halfspace->Uz_indenter[j+i*ny],uz[j+i*ny],
								halfspace->Uz_plas[j+i*ny],pzext[j+i*ny],0.0,0.0,0.0);
					}
				}

				fclose(fp);
			}
			*/
			
			/*
			// DEBUG ONLY
			if (iter < 10 || iter % (int)round(maxiter/10) == 0) {
				printf(" itry = %d, iter = %d, error = %e\n", itry, iter, error);
				//printf("   error-preverror = %e, error = %e, preverror = %e\n", fabs(error-preverror), error, preverror);
				//printf("   uzk[0] = (re) %e, (im) %e\n", creal(uzk[0]),cimag(uzk[0]));
				//printf("   pzkext[0] = (re) %e, (im) %e\n", creal(pzkext[0]),cimag(pzkext[0]));
			}
			*/
			
			if (isnan(error)) {
				Fatal("SurfaceContact error is NaN, itry = %d, iter = %d", itry, iter);
			}

			// ----- Check error with tolerance -----
			
			// If error = 0, there is probably no contact, check if this is normal
			// If error increases during the first steps, reduce delta parameter
			// If error does not decrease, we have probably reached convergence
			
			if (iter == 0) {
				lasterror = error;
			} else if (iter % (int)round(maxiter/10) == 0) {
				if (error > 10.0*lasterror) {
					//printf("Error is increasing, reducing delta parameter to delta = %e\n",0.1*delta);
					break;
				} else {
					lasterror = error;
				}
			}
			
			//if (error == 0.0 || contacts == 0) {
			//if (contacts == 0 && fabs(minGap) > tol) {
			if (contacts == 0 && fabs(minGap) > tol && error == 0.0) {
				//printf("No surface contact, reducing delta parameter to delta = %e\n",0.1*delta);
				break;
			}
			
			/* 
			if ((fabs(error-preverror) < tol*tol || fabs(preverror-preverror2) < tol*tol) && 
			    error < maxTol && iter != 0) {
				printf("Error stagnates, convergence has been reached:\n error = %e, preverror = %e, preverror2 = %e\n",
				       error,preverror,preverror2);
				convergence = 1;
				break;
			}
			*/
			
			// Check if error oscillates around value with a given frequency
			for (i = 0; i < maxFreq; i++) {
				if (iter % (i+1) == 0) {
					if (iter > 0) {
						if (fabs(errorFreq[i]-prevFreq[i]) < tol*tol && error < maxTol) {
							//printf("Error oscillation with frequency %d has been detected\n"
							//       " Convergence has been reached: error = %e\n", i+1, error);
							convergence = 1;
							break;
						}
						prevFreq[i]  = errorFreq[i];
						errorFreq[i] = 0.0;
					}
				}
				errorFreq[i] += error;
			}
			if (convergence) break;
			
			
			if (error < tol) {
				//printf("Error is smaller than tolerance, convergence has been reached\n");
				//printf(" contacts = %d, minGap = %e, error = %e\n",contacts,fabs(minGap),error);
				convergence = 1;
				break;
			}
			
			preverror = error;

			// ============== End of loop for iteration ========= 

		}
	
		// Decrease delta
		if (!convergence) {
			delta *= 0.1;
			//printf("Error = %e has not converged, reducing delta parameter to delta = %e\n",error,delta);
		}
		itry++;
	}

	printf(" Solved surface contact: itry = %d, iter = %d, error = %e\n", itry, iter, error);
	
	if (itry == maxtry && !convergence) {
		Fatal("Convergence has not been reached to match identer shape with surface displacement!\n");
	}
	
	
#if 0
	if (home->myDomain == 0) {
		/* Contact problem convergence information */
		FILE *fp;
		char format[100];
		sprintf(format,"indenconv.txt");
		fp = fopen(format,"a");
		fprintf(fp,"%d %d %e\n", home->cycle, iter, error);
		fclose(fp);
	}
#endif	

	// ----------- Assign the converged value to global variable ----------- 

	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
			halfspace->Uz_elas[j+i*ny] = uz[j+i*ny];
			halfspace->Tzext[j+i*ny] = pzext[j+i*ny];
		}
	}

	// ----------- Find contact radius, area and total force -----------

	indarea = 0;
	forcesum = 0.0;
	areasum = 0.0;
	radius = 0.0;

	for (i = 0; i<nx; i++) {
		px = ((1.0*i)/nx)*HSLx;
		for (j = 0; j<ny; j++) {
			py = ((1.0*j)/ny)*HSLy;
			
			forcesum += halfspace->Tzext[j+i*ny];

			if (halfspace->Tzext[j+i*ny] < 0.0) {
				r = (px-c[0])*(px-c[0])+(py-c[1])*(py-c[1]);
				radius = MAX(radius, r);
				indarea++;
			}
		}
	}

	areasum = (1.0*indarea)*dx*dy;
	forcesum = (-1.0*forcesum)*dx*dy;
	radius = sqrt(radius);

	halfspace->F_indenter = forcesum;
	halfspace->area_indent = areasum;
	halfspace->contact_radius = radius;
	
	// =============================================================================== //
	// ----------------------- Write file name for indentation ----------------------- // 

	WriteIndentationInfo(home, halfspace);

	// ============================================================================= //   

	free(oneoverkz);
	free(pzkext);
	free(uzk);
	free(uztmp);
	free(pz);
	free(pzext);
	free(uz);
}


#ifdef _GRID_INDENTER_STRESS
/*-------------------------------------------------------------------------
 *
 *      Function:    GridIndenterStress
 *      Description: Tabulate indenter stress field on a grid. When this
 *                   approach is used, the indenter stresses will be 
 *                   interpolated on the grid instead of being explicitly
 *                   calculated during time-integration of dislocation motion.
 *
 *------------------------------------------------------------------------*/
void GridIndenterStress(Home_t *home, HalfSpace_t *halfspace)
{
	int    i, j, p, nx, ny;
	double kx, ky;
	double kz, kx2, ky2, kz2, kxy, kxmy;
	double lm, l2m, lpm, lmpm, l2mpm;
	double alpha, z;
	
	double complex expk;

	Param_t *param;
	param = home->param;

	double HSLx   = halfspace->HSLx;
	double HSLy   = halfspace->HSLy;
	double mu     = halfspace->mu;
	double lambda = halfspace->lambda;

	// Average traction to specify k=0 mode
	complex Tx0 = halfspace->Tx0;
	complex Ty0 = halfspace->Ty0;
	complex Tz0 = halfspace->Tz0;

	lm    = lambda + mu;
	l2m   = lm + mu;
	lmpm  = lambda/lm;
	l2mpm = l2m/lm;
	lpm   = mu/lm;
	
	nx = halfspace->nx;
	ny = halfspace->ny;
	
	TimerStart(home, GRID_INDENTER_STRESS);
	
#ifndef _CYGWIN
	fftw_complex *s11k, *s22k, *s33k, *s12k, *s13k, *s23k;
	fftw_complex *s11, *s22, *s33, *s12, *s13, *s23;
#else
	complex *s11k, *s22k, *s33k, *s12k, *s13k, *s23k;
	complex *s11, *s22, *s33, *s12, *s13, *s23;
#endif

	s11k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s22k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s33k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s12k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s13k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s23k = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	
	s11 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s22 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s33 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s12 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s13 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	s23 = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
	
/*
 * 	The 3D grid is composed of subsequent 2D grids on
 * 	planes at increasing depths parallel to the free surface.
 *  A FFT is used to calculate the stress on each plane.
 */
	int nz = nx+1;
	for (p = 0; p < nz; p++) {
		
		z = -p*home->param->hs_Lzinf/(nz-1);
		
		for (j=0; j<ny; j++) {
			ky = halfspace->ky[j];
			ky2 = ky*ky;

			for (i=0; i<nx; i++) {
				kx = halfspace->kx[i];
				kx2 = kx*kx;

				kz2 = kx2+ky2;
				kz  = sqrt(kz2);
				
				kxmy = kx2-ky2;
				kxy = kx*ky;
				
				expk = mu*exp(kz*z);

				double complex A = halfspace->A[i][j];
				double complex B = halfspace->B[i][j];
				double complex C = halfspace->C[i][j];
				
				s11k[j+i*ny] = 2*expk*((I*A*kx2*z-I*B*kxy-C*kx2)+I*A*kz*lmpm);
				s22k[j+i*ny] = 2*expk*((I*A*ky2*z+I*B*kxy-C*ky2)+I*A*kz*lmpm);
				s33k[j+i*ny] = 2*expk*((-I*A*z+C)*kz2+I*A*kz*l2mpm);

				s13k[j+i*ny] = expk*((2*A*kx*z-B*ky+2*I*C*kx)*kz-2*A*kx*lpm);
				s23k[j+i*ny] = expk*((2*A*ky*z+B*kx+2*I*C*ky)*kz-2*A*ky*lpm);
				s12k[j+i*ny] = I*expk*(2*A*kxy*z+B*kxmy+2*I*C*kxy);
				
				if (kx == 0.0 && ky == 0.0) {
					s13k[j+i*ny] = Tx0;
					s23k[j+i*ny] = Ty0;
					s33k[j+i*ny] = Tz0;
				}
				
			}
		}
		
		// Inverse Fourier Transforms
		fourier_transform_backward(s11,(fftw_complex *)s11k,nx,ny);
		fourier_transform_backward(s22,(fftw_complex *)s22k,nx,ny);
		fourier_transform_backward(s33,(fftw_complex *)s33k,nx,ny);
		fourier_transform_backward(s12,(fftw_complex *)s12k,nx,ny);
		fourier_transform_backward(s13,(fftw_complex *)s13k,nx,ny);
		fourier_transform_backward(s23,(fftw_complex *)s23k,nx,ny);
		
		for (j=0; j<ny; j++) {
			for (i=0; i<nx; i++) {
				
				halfspace->S_indenter[i+j*nx+p*nx*ny][0] = creal(s11[j+i*ny]);
				halfspace->S_indenter[i+j*nx+p*nx*ny][1] = creal(s22[j+i*ny]);
				halfspace->S_indenter[i+j*nx+p*nx*ny][2] = creal(s33[j+i*ny]);
				halfspace->S_indenter[i+j*nx+p*nx*ny][3] = creal(s12[j+i*ny]);
				halfspace->S_indenter[i+j*nx+p*nx*ny][4] = creal(s13[j+i*ny]);
				halfspace->S_indenter[i+j*nx+p*nx*ny][5] = creal(s23[j+i*ny]);
				
			}
		}
	}
	
	free(s11);
	free(s22);
	free(s33);
	free(s12);
	free(s13);
	free(s23);
	
	free(s11k);
	free(s22k);
	free(s33k);
	free(s12k);
	free(s13k);
	free(s23k);
	
	TimerStop(home, GRID_INDENTER_STRESS);
	
#if 0
/*
 * 	Output grid fields in vtk format for Paraview
 */		
	if (home->cycle % 100 == 0) {
	
		char fileName[256];
		char baseFileName[256];
		FILE *pFile;
		
		sprintf(baseFileName, "stress_field_%d", home->cycle);
		snprintf(fileName, sizeof(fileName), "%s/%s.vtk",
				 DIR_PARAVIEW, baseFileName);

		pFile = fopen(fileName, "w");
		
		printf(" +++ Writing Paraview file(s) %s\n", baseFileName);
		
		fprintf(pFile, "# vtk DataFile Version 3.0\n");
		fprintf(pFile, "Indenter stress field\n");
		fprintf(pFile, "ASCII\n");
		fprintf(pFile, "DATASET STRUCTURED_GRID\n");
		fprintf(pFile, "DIMENSIONS %d %d %d\n", nx, ny, nz);
		fprintf(pFile, "POINTS %d float\n", nx*ny*nz);

		for (p = 0; p < nz; p++) {
			for(j = 0; j < ny; j++) {
				for(i = 0; i < nx; i++) {
					kx =  (i + 0.5)*halfspace->HSLx/nx + home->param->minSideX;
					ky =  (j + 0.5)*halfspace->HSLy/ny + home->param->minSideY;
					z = -p*home->param->hs_Lzinf/(nz-1);
					fprintf(pFile, "%f %f %f\n", kx, ky, z);
				}
			}
		}

		fprintf(pFile, "POINT_DATA %d\n", nx*ny*nz);
		
		#if 0
		fprintf(pFile, "SCALARS S33_indenter float\n");
		fprintf(pFile, "LOOKUP_TABLE default\n");
		for (p = 0; p < nz; p++) {
			for (j = 0; j < ny; j++) {
				for (i = 0; i < nx; i++) {
					fprintf(pFile, "%e\n", halfspace->S_indenter[i+j*nx+p*nx*ny][2]);
				}
			}
		}
		#else
		// Grab the first node
		Node_t *node1;
		if (home->newNodeKeyPtr == 0) return;
		for (i = 0; i < home->newNodeKeyPtr; i++) {
			node1 = home->nodeKeys[i];
			if (!node1) continue;
			else break;
		}
		if (!node1) return;
		
		double n[3], b[3];
		/* plane normal */
		if(param->mobilityType == MOB_FCC_0) {
			n[0] = 1.0;
			n[1] = 1.0;
			n[2] = 1.0;
		} else if (param->mobilityType == MOB_BCC_0B || param->mobilityType == MOB_BCC_GLIDE_0 || param->mobilityType == MOB_BCC_GLIDE) {
			n[0] =  1.0;
			n[1] =  0.0;
			n[2] = -1.0;
		} //Here, the RSS for (1 0 -1) plane is considered. If the other planes are chosen, (n[0], n[1], n[2]) needs to be revised.
		
		/* Burgers direction */
		b[0] = node1->burgX[0];
		b[1] = node1->burgY[0];
		b[2] = node1->burgZ[0];
		
		fprintf(pFile, "SCALARS RSS float\n");
		fprintf(pFile, "LOOKUP_TABLE default\n");
		for (p = 0; p < nz; p++) {
			for (j = 0; j < ny; j++) {
				for (i = 0; i < nx; i++) {
					
					double s[3][3];
					s[0][0] = halfspace->S_indenter[i+j*nx+p*nx*ny][0];
					s[1][1] = halfspace->S_indenter[i+j*nx+p*nx*ny][1];
					s[2][2] = halfspace->S_indenter[i+j*nx+p*nx*ny][2];
					s[0][1] = halfspace->S_indenter[i+j*nx+p*nx*ny][3];
					s[0][2] = halfspace->S_indenter[i+j*nx+p*nx*ny][4];
					s[1][2] = halfspace->S_indenter[i+j*nx+p*nx*ny][5];
					s[1][0] = s[0][1];
					s[2][0] = s[0][2];
					s[2][1] = s[1][2];
					
					/* Calculate RSS in the direction of the Burgers vector */
					double rss = 0.0;
					int ii, jj;
					for (ii=0; ii<3; ii++) {
						for (jj=0; jj<3; jj++) {
							rss += s[ii][jj]*n[ii]*b[jj];
						}
					}
					fprintf(pFile, "%e\n", rss);
				}
			}
		}
		#endif
		
		fclose(pFile);
	}
#endif
	
}
#endif

#endif
#endif


/*-------------------------------------------------------------------------
 *
 *      Function:    CheckEquilibrium
 *      Description: Check if the dislocation structure has reached an 
 *                   equilibrium configuration from which no further 
 *                   motion is expected.
 *                   Returns 1 if equilibrium is reached, 0 otherwise.
 *
 *------------------------------------------------------------------------*/
int CheckEquilibrium(Home_t *home)
{
	
	int i, j, tot_nodes;
	double drx, dry, drz;
	Node_t *node, *nbr;
	
	static int    start_cycle = 0;
	static int    last_cycle = 0;
	static int    last_nodes = 0;
	static double last_time = 0.0;
	static double last_length = 0.0;
	
	tot_nodes = 0;
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		tot_nodes++;
	}
	
	if (tot_nodes == 0 || home->param->max_relax <= 1) {
		start_cycle = home->cycle;
		return 1;
	}
	
	int check_interval = 10; // minimum interval for checking the equilibrium
	if (home->param->max_relax != -1) check_interval = MIN(check_interval, home->param->max_relax);
	
	if (home->cycle < last_cycle + check_interval && home->cycle != 0) return 0;
	
	int equilibrium = 0;
	
	/* Compute total dislocation lines length */
	tot_nodes = 0;
	double tot_length = 0.0;
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		tot_nodes++;
		
		for (j=0; j < node->numNbrs; j++) {
			nbr = GetNeighborNode(home, node, j);
			if (OrderNodes(node, nbr) >= 0) continue;

			drx = node->x - nbr->x;
			dry = node->y - nbr->y;
			drz = node->z - nbr->z;
			ZImage(home->param, &drx, &dry, &drz);

			tot_length += sqrt(drx*drx+dry*dry+drz*drz);
		}
	}
	
	printf("CheckEquilibrium - line length variation = %f\n", fabs(last_length-tot_length)/last_length);
	
	if ((fabs(last_length-tot_length)/last_length < 1e-4 && tot_nodes == last_nodes) ||
	    tot_nodes == 0 || home->cycle == 0) {
		printf("  Equilibrium has been reached\n");
		start_cycle = home->cycle;
		equilibrium = 1;
	} else if (home->cycle >= start_cycle + home->param->max_relax && home->param->max_relax != -1) {
		printf("  Max number of relax steps has been reached\n");
		start_cycle = home->cycle;
		equilibrium = 1;
	} else {
		printf("  Equilibrium has NOT been reached\n");
	}
	
	last_cycle = home->cycle;
	last_nodes = tot_nodes;
	last_time = home->param->timeNow;
	last_length = tot_length;
	
	//sleep(2.0);
	
	return equilibrium;
}

#ifdef _GRID_FIELDS
/*---------------------------------------------------------------------------
 *
 *      Function:     BoxIntersection
 * 		Description:  Test if segment intersects an axis-aligned box
 *
 *-------------------------------------------------------------------------*/
int BoxIntersection(double p1[3], double p2[3], double box[3][2])
{
		int     i;
		double  c[3], e[3], m[3], d[3], ad[3];
		double  eps = 1.e-8;
		
		for (i = 0; i < 3; i++) {
			c[i] = 0.5*(box[i][0] + box[i][1]);
			e[i] = box[i][1] - c[i];
			m[i] = 0.5*(p1[i] + p2[i]);
			d[i] = p1[i] - m[i];
			m[i] -= c[i];
			ad[i] = fabs(d[i]);
			
			if (fabs(m[i]) > e[i] + ad[i]) return 0;
		}
		
		if (fabs(m[1]*d[2] - m[2]*d[1]) > e[1]*ad[2] + e[2]*ad[1]) return 0;
		if (fabs(m[2]*d[0] - m[0]*d[2]) > e[0]*ad[2] + e[2]*ad[0]) return 0;
		if (fabs(m[0]*d[1] - m[1]*d[0]) > e[0]*ad[1] + e[1]*ad[0]) return 0;

		return 1;
}

/*---------------------------------------------------------------------------
 *
 *      Function:     OutsideBox
 * 		Description:  Test if point is outside of an axis-aligned box
 *
 *-------------------------------------------------------------------------*/
int OutsideBox(double p[3], double box[3][2])
{
		if (p[0] < box[0][0] || p[0] > box[0][1]) return 1;
		if (p[1] < box[1][0] || p[1] > box[1][1]) return 1;
		if (p[2] < box[2][0] || p[2] > box[2][1]) return 1;
		
		return 0;
}

/*---------------------------------------------------------------------------
 *
 *      Function:     SegPlaneIntersection
 * 		Description:  Computes the intersection between a plane defined by 
 *                    its normal and a point, and a segment defined by 
 *                    two points
 *
 *-------------------------------------------------------------------------*/
int SegPlaneIntersection(double n[3], double p[3], double p1[3], 
                         double p2[3], double pint[3])
{
		double  t[3], D, w[3], N, st;
		double  eps = 1.e-5;
		
		VECTOR_COPY(pint, p1);
		
		t[0] = p2[0] - p1[0];
		t[1] = p2[1] - p1[1];
		t[2] = p2[2] - p1[2];
		
		D = n[0]*t[0] + n[1]*t[1] + n[2]*t[2];
		if (fabs(D) < eps) return 0;
		
		w[0] = p1[0] - p[0];
		w[1] = p1[1] - p[1];
		w[2] = p1[2] - p[2];
		
		N = -n[0]*w[0] - n[1]*w[1] - n[2]*w[2];
		st = N/D;
		if (st < 0.0 || st > 1.0) return 0;
		
		pint[0] += st*t[0];
		pint[1] += st*t[1];
		pint[2] += st*t[2];
		return 1;
}

/*---------------------------------------------------------------------------
 *
 *      Function:     SegBoxIntersectionPosition
 * 		Description:  Determine the intersection position between segment 
 *                    pin-pout and an axis-aligned box
 *
 *-------------------------------------------------------------------------*/
int SegBoxIntersectionPosition(double pin[3], double pout[3], 
                               double box[3][2], double pint[3])
{
		int     i, j;
		double  p[3], n[3];
		double  eps = 1.e-10;
		
		VECTOR_COPY(pint, pout);
		
		for (i = 0; i < 3; i++) {
			for (j = 0; j < 2; j++) {
				
				if (j == 0) {
					if (pout[i] >= box[i][0]) continue;
				} else {
					if (pout[i] <= box[i][1]) continue;
				}
				
				// Point on the surface
				VECTOR_ZERO(p);
				p[i] = box[i][j];
				
				// Normal to the surface
				VECTOR_ZERO(n);
				n[i] = 2.0*(j - 0.5);
				
				// Compute intersection
				if (!SegPlaneIntersection(n, p, pin, pout, pint)) continue;
				
				// Correct rounding errors
				if (fabs(p[i]-pint[i]) < eps) pint[i] = p[i];
				
				if (!OutsideBox(pint, box)) return 1;
				else VECTOR_COPY(pint, pout);
			}
		}
		
		return 0;
}

/*---------------------------------------------------------------------------
 *
 *      Function:     LineBoxIntersectionPosition
 * 		Description:  Determine the intersection position between a line
 *                    and an axis-aligned box
 *
 *-------------------------------------------------------------------------*/
int LineBoxIntersectionPosition(double p[3], double d[3], double box[3][2], 
                                double pint1[3], double pint2[3])
{
		int     i;
		double  tmin, tmax, dinv, t1, t2, tmp;
		double  eps = 1.e-10;
		
		VECTOR_COPY(pint1, p);
		VECTOR_COPY(pint2, p);
		
		tmin = 0.0;
		tmax = 1.e20;
		
		for (i = 0; i < 3; i++) {
			if (fabs(d[i]) < eps) {
				if (p[i] < box[i][0] || p[i] > box[i][1]) return 0;
			} else {
				dinv = 1.0/d[i];
				t1 = (box[i][0] - p[i])*dinv;
				t2 = (box[i][1] - p[i])*dinv;
				if (t1 > t2) {
					tmp = t2;
					t2 = t1;
					t1 = tmp;
				}
				if (t1 > tmin) tmin = t1;
				if (t2 < tmax) tmax = t2;
				if (tmin > tmax) return 0;
			}
		}
		
		pint1[0] += tmin*d[0];
		pint1[1] += tmin*d[1];
		pint1[2] += tmin*d[2];
		
		pint2[0] += tmax*d[0];
		pint2[1] += tmax*d[1];
		pint2[2] += tmax*d[2];
		
		return 1;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     CrossVector
 *
 *------------------------------------------------------------------------*/
void CrossVector(real8 a[3], real8 b[3], real8 c[3])
{
        c[0] = a[1]*b[2] - a[2]*b[1];
        c[1] = a[2]*b[0] - a[0]*b[2];
        c[2] = a[0]*b[1] - a[1]*b[0];

        return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:     id_list_comp
 *
 *-------------------------------------------------------------------------*/
struct id_list
{
	int id;
	double val;
};

int id_list_comp(const void *a, const void *b)
{
	struct id_list *ia = (struct id_list *)a;
	struct id_list *ib = (struct id_list *)b;
	return (ia->val > ib->val) - (ia->val < ib->val);
}

/*---------------------------------------------------------------------------
 *
 *      Function:     GridDensity
 *      Description:  This function transforms discrete quantities of the
 *                    dislocation network (dislocation density, plastic shear)
 *                    into continuous fields on a regular grid.
 *                    Nicolas.
 *
 *-------------------------------------------------------------------------*/
void GridDensity(Home_t *home, HalfSpace_t *halfspace)
{
		int     i, j, k, l, nc;
		int     kx, ky, kz, ib, jb, kb, N, sys;
		int     imin, jmin, kmin, imax, jmax, kmax;
		int     in1, in2, is[3], i1, i2, start;
		double  H[3], Hmax;
		double  p1[3], p2[3], x1[3], x2[3], x0[3];
		double  b[3], n[3], t[3], alpha[3][3], dt;
		double  R[3], Rt, d[3], x10[3], x20[3];
		double  s1, s2, s[3], ss1, ss2;
		double  bc[3], bs[3], bst[3], dc, box[3][2];
		double  ex, ey, ez, hhx, hhy, hhz, nx, ny, nz, da, dal, magb;
		double  xmin, ymin, zmin, xmax, ymax, zmax;
		double  ltot, atot, wtot, stot, W, A, B, C, V;
		double  *densityGrid;
		Node_t  *node, *nbr;
		Param_t *param;

		param = home->param;
		
		if (param->gridfields <= 0 || param->gridfieldsfreq <= 0) return;
			
		
		N = param->gridfields;
		
		H[0] = halfspace->HSLx/N;
		H[1] = halfspace->HSLy/N;
		H[2] = halfspace->HSLzinf/N;
		Hmax = MAX(H[0], MAX(H[1], H[2]));
		V = H[0]*H[1]*H[2];
		
		ltot = 0.0;
		wtot = 0.0;
		atot = 0.0;
		stot = 0.0;
		
		densityGrid = (double*)malloc(sizeof(double)*N*N*N);
		
		for (kx = 0; kx < N; kx++)
			for (ky = 0; ky < N; ky++)
				for (kz = 0; kz < N; kz++)
					densityGrid[kz+N*ky+N*N*kx] = 0.0;
	

		for (i = 0; i < home->newNodeKeyPtr; i++) {
			
			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
			nc = node->numNbrs;
			
			p1[0] = node->x;
			p1[1] = node->y;
			p1[2] = node->z;
			        
			for (j = 0; j < nc; j++) {
				
				nbr = GetNeighborNode(home, node, j);
				if (nbr == (Node_t *)NULL) continue;
				
				if (OrderNodes(node, nbr) != -1) continue;
				
				b[0] = node->burgX[j];
				b[1] = node->burgY[j];
				b[2] = node->burgZ[j];
				
				n[0] = node->nx[j];
				n[1] = node->ny[j];
				n[2] = node->nz[j];
				
				magb = Normal(b);
				sys = GetSlipSystem(home, b[0], b[1], b[2], n[0], n[1], n[2]);
				
				ex = nbr->x - node->oldx; 
				ey = nbr->y - node->oldy; 
				ez = nbr->z - node->oldz; 

				hhx = node->x - nbr->oldx;
				hhy = node->y - nbr->oldy;
				hhz = node->z - nbr->oldz;

				ZImage(param, &ex, &ey, &ez);
				ZImage(param, &hhx, &hhy, &hhz);

				nx = (ey*hhz - ez*hhy);
				ny = (ez*hhx - ex*hhz);
				nz = (ex*hhy - ey*hhx);
				da = 0.5*sqrt(nx*nx + ny*ny + nz*nz);
				atot += da;
				
				p2[0] = nbr->x;
				p2[1] = nbr->y;
				p2[2] = nbr->z;
				
				PBCPOSITION(param, p1[0], p1[1], p1[2], &p2[0], &p2[1], &p2[2]);
				
				t[0] = p2[0] - p1[0];
				t[1] = p2[1] - p1[1];
				t[2] = p2[2] - p1[2];
				dt = sqrt(t[0]*t[0] + t[1]*t[1] + t[2]*t[2]);
				if (dt < 1.e-10) continue;
				ltot += dt;
				
				dal = da/dt;
				
				t[0] /= dt;
				t[1] /= dt;
				t[2] /= dt;
				
				xmin = MIN(p1[0], p2[0]) - 0.5*H[0] - param->minSideX;
				xmax = MAX(p1[0], p2[0]) - 0.5*H[0] - param->minSideX;
				ymin = MIN(p1[1], p2[1]) - 0.5*H[1] - param->minSideY;
				ymax = MAX(p1[1], p2[1]) - 0.5*H[1] - param->minSideY;
				zmin = MIN(p1[2], p2[2]) - 0.5*H[2] - param->minSideZ;
				zmax = MAX(p1[2], p2[2]) - 0.5*H[2] - param->minSideZ;
				
				imin = floor(xmin/H[0]);
				imax = floor(xmax/H[0]) + 1;
				jmin = floor(ymin/H[1]);
				jmax = floor(ymax/H[1]) + 1;
				kmin = floor(zmin/H[2]);
				kmax = floor(zmax/H[2]) + 1;
 				
				for (ib = imin; ib <= imax; ib++) {
					for (jb = jmin; jb <= jmax; jb++) {
						for (kb = kmin; kb <= kmax; kb++) {
							
							kx = ib % N;
							if (kx < 0) kx += N;
							ky = jb % N;
							if (ky < 0) ky += N;
							kz = kb % N;
							if (kz < 0) kz += N;
							
							double bc[3];
							bc[0] =  (ib + 0.5)*H[0] + param->minSideX;
							bc[1] =  (jb + 0.5)*H[1] + param->minSideY;
							bc[2] =  (kb + 0.5)*H[2] + param->minSideZ;
							
							bs[0] = bc[0] - p1[0];
							bs[1] = bc[1] - p1[1];
							bs[2] = bc[2] - p1[2];
							CrossVector(bs, t, bst);
							dc = Normal(bst);
							if (dc > sqrt(3.0)*Hmax) continue;
							
							box[0][0] = bc[0] - H[0];
							box[0][1] = bc[0] + H[0];
							box[1][0] = bc[1] - H[1];
							box[1][1] = bc[1] + H[1];
							box[2][0] = bc[2] - H[2];
							box[2][1] = bc[2] + H[2];
							if (!BoxIntersection(p1, p2, box)) continue;
							
							in1 = !OutsideBox(p1, box);
							in2 = !OutsideBox(p2, box);
							if (in1 && in2) {
								VECTOR_COPY(x1, p1);
								VECTOR_COPY(x2, p2);
							} else if (in1) {
								VECTOR_COPY(x1, p1);
								SegBoxIntersectionPosition(p1, p2, box, x2);
							} else if (in2) {
								SegBoxIntersectionPosition(p2, p1, box, x1);
								VECTOR_COPY(x2, p2);
							} else {
								if (!LineBoxIntersectionPosition(p1, t, box, x1, x2)) {
									continue;
								}
							}
							
							d[0] = x2[0] - x1[0];
							d[1] = x2[1] - x1[1];
							d[2] = x2[2] - x1[2];
							if (Normal(d) < 1.e-10) continue;
							
							R[0] = bc[0] - x1[0];
							R[1] = bc[1] - x1[1];
							R[2] = bc[2] - x1[2];
							Rt = R[0]*t[0] + R[1]*t[1] + R[2]*t[2];
							x0[0] = x1[0] + Rt*t[0];
							x0[1] = x1[1] + Rt*t[1];
							x0[2] = x1[2] + Rt*t[2];
							d[0] = R[0] - Rt*t[0];
							d[1] = R[1] - Rt*t[1];
							d[2] = R[2] - Rt*t[2];
							x10[0] = x1[0] - x0[0];
							x10[1] = x1[1] - x0[1];
							x10[2] = x1[2] - x0[2];
							x20[0] = x2[0] - x0[0];
							x20[1] = x2[1] - x0[1];
							x20[2] = x2[2] - x0[2];
							s1 = x10[0]*t[0] + x10[1]*t[1] + x10[2]*t[2];
							s2 = x20[0]*t[0] + x20[1]*t[1] + x20[2]*t[2];
							
							if (fabs(t[0]) < 1.e-20) s[0] = s2+1; else s[0] = d[0]/t[0];
							if (fabs(t[1]) < 1.e-20) s[1] = s2+1; else s[1] = d[1]/t[1];
							if (fabs(t[2]) < 1.e-20) s[2] = s2+1; else s[2] = d[2]/t[2];
							
							W = s2 - s1;
							
							for (k = 0; k < 3; k++) {
								A = 0.0;
								if (s[k] > s1 && s[k] < s2) {
									A += fabs(0.5*(s[k]-s1)*(2*d[k]-t[k]*(s[k]+s1)));
									A += fabs(0.5*(s2-s[k])*(2*d[k]-t[k]*(s2+s[k])));
								} else {
									A += fabs(0.5*(s2-s1)*(2*d[k]-t[k]*(s2+s1)));
								}
								W -= 1.0/H[k]*A;
							}
							
							for (k = 0; k < 3; k++) {
								i1 = k;
								if (i1 == 2) i2 = 0; else i2 = i1+1;
								struct id_list ss[] = {{0, s1}, {1, s2}, {2, s[i1]}, {3, s[i2]}};
								size_t ss_len = sizeof(ss)/sizeof(struct id_list);
								qsort(ss, ss_len, sizeof(struct id_list), id_list_comp);
								start = 0;
								B = 0.0;
								for (l = 0; l < ss_len-1; l++) {
									if (ss[l].id == 0) start = 1;
									if (start) {
										ss1 = ss[l].val;
										ss2 = ss[l+1].val;
										B += fabs(d[i1]*d[i2]*(ss2-ss1)
										     -0.5*(d[i1]*t[i2]+d[i2]*t[i1])*(ss2*ss2-ss1*ss1)
										     +1.0/3.0*t[i1]*t[i2]*(ss2*ss2*ss2-ss1*ss1*ss1));
									}
									if (ss[l+1].id == 1) break;
								}
								W += 1.0/H[i1]/H[i2]*B;
							}
							
							struct id_list ss[] = {{0, s1}, {1, s2}, {2, s[0]}, {3, s[1]}, {4, s[2]}};
							size_t ss_len = sizeof(ss)/sizeof(struct id_list);
							qsort(ss, ss_len, sizeof(struct id_list), id_list_comp);
							start = 0;
							C = 0.0;
							for (l = 0; l < ss_len-1; l++) {
								if (ss[l].id == 0) start = 1;
								if (start) {
									ss1 = ss[l].val;
									ss2 = ss[l+1].val;
									C += fabs(d[0]*d[1]*d[2]*(ss2-ss1)
									     -0.5*(t[0]*d[1]*d[2]+d[0]*t[1]*d[2]+d[0]*d[1]*t[2])*(ss2*ss2-ss1*ss1)
									     +1.0/3.0*(d[0]*t[1]*t[2]+t[0]*d[1]*t[2]+t[0]*t[1]*d[2])*(ss2*ss2*ss2-ss1*ss1*ss1)
									     -0.25*(t[0]*t[1]*t[2])*(ss2*ss2*ss2*ss2-ss1*ss1*ss1*ss1));
								}
								if (ss[l+1].id == 1) break;
							}
							W -= 1.0/H[0]/H[1]/H[2]*C;
							
							wtot += W;
							densityGrid[kz+N*ky+N*N*kx] += W/V;
							
							stot += W*dal;
							if (sys != 0) {
								halfspace->plasticShear[kz+N*ky+N*N*kx][sys-1] += W*dal*magb/V;
							}
							
						}
					}
				}
				
			}
		}


#ifdef PARALLEL
		double localVals[4], globalVals[4];
		localVals[0] = ltot;
		localVals[1] = wtot;
		localVals[2] = atot;
		localVals[3] = stot;
		
		MPI_Reduce(localVals, globalVals, 4, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		
		if (home->myDomain == 0) {
			ltot = globalVals[0];
			wtot = globalVals[1];
			atot = globalVals[2];
			stot = globalVals[3];
		}
		
		if (home->myDomain == 0) {
			MPI_Reduce(MPI_IN_PLACE, densityGrid, N*N*N, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
			//MPI_Reduce(MPI_IN_PLACE, halfspace->plasticShear, 12*N*N*N, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		} else {
			MPI_Reduce(densityGrid, densityGrid, N*N*N, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
			//MPI_Reduce(halfspace->plasticShear, halfspace->plasticShear, 12*N*N*N, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		}
#endif
		
#if 0
		if (home->myDomain == 0) {
			/* Debug info */
			printf("ltot = %e, wtot = %e, wtot/ltot = %e\n", ltot, wtot, wtot/ltot);
			printf("dens = %e\n", ltot/param->simVol/param->burgMag/param->burgMag);
			printf("atot = %e, stot = %e, stot/atot = %e\n", atot, stot, stot/atot);
		}
#endif	
		
		if (home->myDomain == 0 && home->cycle % param->gridfieldsfreq == 0) {
			
			char fileName[256];
			char baseFileName[256];
			FILE *pFile;
			
/*
 * 			Output grid fields in plain data format
 */		
			sprintf(fileName, "grid_fields_%d.out", home->cycle);
			pFile = fopen(fileName, "w");
			
			printf(" +++ Writing grid field file %s\n", fileName);
			
			for (k = 0; k < N; k++) {
				for (j = 0; j < N; j++) {
					for (i = 0; i < N; i++) {
						
						bc[0] =  (i + 0.5)*H[0] + param->minSideX;
						bc[1] =  (j + 0.5)*H[1] + param->minSideY;
						bc[2] =  (k + 0.5)*H[2] + param->minSideZ;
						
						fprintf(pFile, "%e %e %e", bc[0], bc[1], bc[2]);
						fprintf(pFile, " %e ", densityGrid[k+N*j+N*N*i]/param->burgMag/param->burgMag);
                        if (param->materialType == MAT_TYPE_FCC) {
                            for (l = 0; l < 12; l++) {
                                fprintf(pFile, " %e", halfspace->plasticShear[k+N*j+N*N*i][l]);
                            }
                        } else if (param->materialType == MAT_TYPE_BCC) {
                            for (l = 0; l < 24; l++) {
                                fprintf(pFile, " %e", halfspace->plasticShear[k+N*j+N*N*i][l]);
                            }
                        }
						fprintf(pFile, "\n");
					}
				}
			}
			
			fclose(pFile);

/*
 * 			Output grid fields in vtk format for Paraview
 */		
			if (param->gridfieldsparaview && param->paraview) {
				
				sprintf(baseFileName, "grid_fields_%d", home->cycle);
				snprintf(fileName, sizeof(fileName), "%s/%s.vtk",
                         DIR_PARAVIEW, baseFileName);
			
				pFile = fopen(fileName, "w");
				
				printf(" +++ Writing Paraview file(s) %s\n", baseFileName);
				
				fprintf(pFile, "# vtk DataFile Version 3.0\n");
				fprintf(pFile, "Dislocation fields\n");
				fprintf(pFile, "ASCII\n");
				fprintf(pFile, "DATASET STRUCTURED_GRID\n");
				fprintf(pFile, "DIMENSIONS %d %d %d\n", N, N, N);
				fprintf(pFile, "POINTS %d float\n", N*N*N);
			
				for(k = 0; k < N; k++) {
					for(j = 0; j < N; j++) {
						for(i = 0; i < N; i++) {
							bc[0] =  (i + 0.5)*H[0] + param->minSideX;
							bc[1] =  (j + 0.5)*H[1] + param->minSideY;
							bc[2] =  (k + 0.5)*H[2] + param->minSideZ;
							fprintf(pFile, "%f %f %f\n", bc[0], bc[1], bc[2]);
						}
					}
				}
			
				fprintf(pFile, "POINT_DATA %d\n", N*N*N);
				
				fprintf(pFile, "SCALARS Density float\n");
				fprintf(pFile, "LOOKUP_TABLE default\n");
				int counter = 0;
				for (k = 0; k < N; k++) {
					for (j = 0; j < N; j++) {
						for (i = 0; i < N; i++) {
							fprintf(pFile, "%e\n", densityGrid[k+N*j+N*N*i]/param->burgMag/param->burgMag);
						}
					}
				}
				
                if (param->materialType == MAT_TYPE_FCC) {
                    for (l = 0; l < 12; l++) {
  
                      fprintf(pFile, "SCALARS Shear%d float\n", l+1);
                      fprintf(pFile, "LOOKUP_TABLE default\n");
                      int counter = 0;
                      for (k = 0; k < N; k++) {
                        for (j = 0; j < N; j++) {
                          for (i = 0; i < N; i++) {
                            fprintf(pFile, "%e\n", halfspace->plasticShear[k+N*j+N*N*i][l]);
                          }
                        }
                      }
                    }
                } else if (param->materialType == MAT_TYPE_BCC) {
                    for (l = 0; l < 24; l++) {
  
                      fprintf(pFile, "SCALARS Shear%d float\n", l+1);
                      fprintf(pFile, "LOOKUP_TABLE default\n");
                      int counter = 0;
                      for (k = 0; k < N; k++) {
                        for (j = 0; j < N; j++) {
                          for (i = 0; i < N; i++) {
                            fprintf(pFile, "%e\n", halfspace->plasticShear[k+N*j+N*N*i][l]);
                          }
                        }
                      }
                    }
                }
				
				fclose(pFile);
		
			}
	
		}
		
		free(densityGrid);
	
		return;
}
#endif

#endif
