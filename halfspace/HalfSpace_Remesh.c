/*****************************************************************************
 *
 *      Module:         HalfSpace_Remesh.c
 *      Description:    This module contains functions to handle node
 *                      collision with the halfspace surface
 *                      
 *****************************************************************************/


/***************************************************************************** 
Algorithm
*********

- Remove all segments with both nodes flagged 6 (i.e., halfspace surface nodes).
It can happen that during remesh, a node has been deleted that leads to two nodes 
connected with flag 6.

- Flag all nodes that are outside the HS to 6.

- Split segments with a node inside (flag 0, unconstrained) and a node outside the HS
(flag 6). We need to be careful here to remove the neighbors and not
the node since splitting a node affect its pointer.

- Remove segments with both nodes flagged 6.

When the HS Remesh routine is finished, nodes that are flagged 6 are
on the surface and have only one arm. However some remesh rules of
ParaDiS such as SplitMultiNode, MergeNode and HandleCollisions can
affect these properties. Some nodes with flag 6 can have multiarms or
move inside or outside the HS.

Before starting the algorithm, we need to ensure that 
- all segments with both nodes at flag 6 have been removed 
- that nodes with flag 6 are outside or on the surface of the HS.
So we call routines that
- Check whether a node flagged 6 is still on the surface and has one
arm.

Problems
********

(1) A node flagged 6 inside the HS with multiple arms. If this happens and
the Burgers vector at that node is not conserved, we need to
abort. This happens in parallel.  Need to check how this can happen.

(2) In parallel, the split can fail because the segment does not
belong to the processor.

(3) A node that has a neighbor on the surface is not remeshed even if
the segment is getting small. This reduces the timestep a lot. 


(4) Setting nodes to 6 is not well communicated in parallel. The node is set to 
6 corretly but not its neighbor if the segment sits on two procs.


Solutions
*********

(1) Modifications of Remesh rules of ParaDiS

In RemeshRule2.c:
We don't remove a node with flag 6 in MeshCoarsen. 

In Collisions.c:
When two nodes A and B are going to collide, if node A has a  flag 6. 
the final collision point is the node A. That means that this node won't
move inside the HS during a collision.

This corrects the problem of having a node inside with flag 6.

(2) If the loop is done over the node to be removed then there is no problems anymore because
it belongs to the processors calling SplitNode. We still have to be careful with pointers so we
use a while routine.


(3) We manually remesh a node for which the distance between the
surface is less than minSeg.

(4) The constraint 6 is set on the node and its neighbors when the run is done
in parallel

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "Home.h"
#include "Util.h"
#include "Mobility.h"

#define _ENABLE_LINE_CONSTRAINT 1 /* Should take same value as in MobilityLaw_XXX.c */

#ifdef _HALFSPACE

#include "HS.h"
#include "QueueOps.h"

#define iprint 0



#ifdef _SURFACE_STEPS
/*-------------------------------------------------------------------------
 *
 *      Function:    IsSurfaceNode
 *
 *------------------------------------------------------------------------*/
int IsSurfaceNode(Node_t *node, double t)
{
	return (node->constraint == HALFSPACE_SURFACE_NODE && 
	        fabs(node->z-t) < 1.e-5);
}

/*-------------------------------------------------------------------------
 *
 *      Function:    FCCNodeGlideConstraints
 *                   This function is built for FCC_0 Mobility
 *
 *------------------------------------------------------------------------*/
void FCCNodeGlideConstraints(Home_t *home, Node_t *node, int *ndeg, double dir[3])
{
	int     i, j, nc, ngc, nlc;
	real8   normX[20], normY[20], normZ[20];
	real8   normx[20], normy[20], normz[20];
	real8   lineX[20], lineY[20], lineZ[20];
	real8   t, a, b;
	Node_t  *nbr;
	Param_t *param;
	
	t = 0.0;
	VECTOR_ZERO(dir);

	/* Pinned or sessile node */
	if ((node->constraint == PINNED_NODE) ||
	     NodeHasSessileBurg(home, node)) {
		*ndeg = 0;
		return;
	}
	
	param = home->param;
	nc = node->numNbrs;
	
	/* Only works for FCC mobility */
	if(param->mobilityType != MOB_FCC_0) {
		Fatal("FCCNodeGlideConstraints only works for FCC_0 mobility");
	}

	/* Copy glide plane constraints and determine line constraints */
	for (i = 0; i < nc; i++) {
		
		normX[i] = normx[i] = node->nx[i];
		normY[i] = normy[i] = node->ny[i];
		normZ[i] = normz[i] = node->nz[i];

/*
 *      If needed, rotate the glide plane normals from the
 *      laboratory frame to the crystal frame.
 */
		if (param->useLabFrame) {
			real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
			real8 normRot[3];

			Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);

			normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
			normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
		}

		if ( (fabs(fabs(normX[i]) - fabs(normY[i])) > FFACTOR_NORMAL) ||
		     (fabs(fabs(normY[i]) - fabs(normZ[i])) > FFACTOR_NORMAL) ) { 

			/* not {111} plane */	
			nbr = GetNeighborNode(home,node,i);
			lineX[i] = nbr->x - node->x;
			lineY[i] = nbr->y - node->y;
			lineZ[i] = nbr->z - node->z;
			ZImage(param, lineX+i, lineY+i, lineZ+i);
/*
 *          If needed, rotate the line sense from the laboratory frame to
 *          the crystal frame.
 */
			if (param->useLabFrame) {
				real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
				real8 lDirRot[3];

				Matrix33Vector3Multiply(home->rotMatrixInverse, lDir, lDirRot);

				lineX[i] = lDirRot[0];
				lineY[i] = lDirRot[1];
				lineZ[i] = lDirRot[2];
			}
			
		} else { 
			/* No line constraint */
			lineX[i] = lineY[i] = lineZ[i] = 0;
		}
	}

	/* Surface node constraint */
	if (IsSurfaceNode(node, t)) {
		
		real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *          If needed, rotate the free surface normal from the laboratory
 *          frame to the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		lineX[nc] = 0.0; lineY[nc] = 0.0; lineZ[nc] = 0.0;
		nc++;
	}

	/* Normalize glide plane normal vectors and lc line vectors*/
	for (i = 0; i < nc; i++) {
		a = sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
		b = sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

		if(a>0) {
			normX[i]/=a;
			normY[i]/=a;
			normZ[i]/=a;

			normx[i]/=a;
			normy[i]/=a;
			normz[i]/=a;
		}
		if(b>0) {
			lineX[i]/=b;
			lineY[i]/=b;
			lineZ[i]/=b;
		}
	}

	/* Find independent glide constraints */ 
	ngc = nc;
	for (i = 0; i < nc; i++) {
		for (j = 0; j < i; j++) {
			Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
		}
		if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH) {
			normX[i] = normY[i] = normZ[i] = 0;
			ngc--;
		}
	}
	
	ngc = 0;
	for (i = 0; i < nc; i++) {
		if ((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0)) {	
/*
 *  		If needed, rotate the normal vector back to the laboratory frame
 *  		from the crystal frame
 */
			if (param->useLabFrame) {
				real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
				real8 normRot[3];

				Matrix33Vector3Multiply(home->rotMatrix, normTmp, normRot);

				normX[i] = normRot[0];
				normY[i] = normRot[1];
				normZ[i] = normRot[2];
			}
			
			normX[ngc] = normX[i];
			normY[ngc] = normY[i];
			normZ[ngc] = normZ[i];
			ngc++;
		}
	}

	/* Find independent line constraints */
	nlc = 0;
	for (i = 0; i < nc; i++) {
		for (j = 0; j < i; j++) {
			Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
		}
		if ((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH) {
			lineX[i] = lineY[i] = lineZ[i] = 0;
		} else {
			nlc++;
		}
	}
	
	nlc = 0;
#if _ENABLE_LINE_CONSTRAINT
	for (i = 0; i < nc; i++) {
		if ((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0)) {
/*
 *  		If needed, rotate the line vector back to the laboratory frame
 *  		from the crystal frame
 */
			if (param->useLabFrame) {
				real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
				real8 lDirRot[3];

				Matrix33Vector3Multiply(home->rotMatrix, lDir, lDirRot);

				lineX[i] = lDirRot[0];
				lineY[i] = lDirRot[1];
				lineZ[i] = lDirRot[2];
			}
			
			lineX[nlc] = lineX[i];
			lineY[nlc] = lineY[i];
			lineZ[nlc] = lineZ[i];
			nlc++;
		}
	}
#endif
	
	/* Determine degree of freedom of the node and its glide constraint(s) */
	if (ngc > 2 || nlc > 1) {
		*ndeg = 0;
	} else {
		
		if (ngc == 1) {
			
			*ndeg = 2;
			dir[0] = normX[0];
			dir[1] = normY[0];
			dir[2] = normZ[0];
			
			if (nlc == 1) {
				if (fabs(lineX[0]*dir[0]+lineY[0]*dir[1]+lineZ[0]*dir[2]) < FFACTOR_ORTH) {
					*ndeg = 1;
					dir[0] = lineX[0];
					dir[1] = lineY[0];
					dir[2] = lineZ[0];
				} else {
					*ndeg = 0;
					VECTOR_ZERO(dir);
				}
			}
			
		} else if (ngc == 2) {
			
			*ndeg = 1;
			double n1[3] = {normX[0], normY[0], normZ[0]};
			double n2[3] = {normX[1], normY[1], normZ[1]};
			NormalizedCrossVector(n1, n2, dir);
			
			if (nlc == 1) {
				if (fabs(fabs(lineX[0]*dir[0]+lineY[0]*dir[1]+lineZ[0]*dir[2])-1.0) > FFACTOR_ORTH) {
					*ndeg = 0;
					VECTOR_ZERO(dir);
				}
			}
			
		}
		
	}
	
	return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    BCCNodeGlideConstraints_0
 *                   This function is built for BCC_glide_0 Mobility
 *
 *------------------------------------------------------------------------*/
void BCCNodeGlideConstraints_0(Home_t *home, Node_t *node, int *ndeg, double dir[3])
{
	int     i, j, nc, ngc, nlc;
	real8   normX[20], normY[20], normZ[20];
	real8   normx[20], normy[20], normz[20];
	real8   burgX[20], burgY[20], burgZ[20];
	real8   burgx[20], burgy[20], burgz[20];	
	real8   lineX[20], lineY[20], lineZ[20];
	real8   t, a, b;
	Node_t  *nbr;
	Param_t *param;
	
	t = 0.0;
	VECTOR_ZERO(dir);

	/* Pinned or sessile node */
	if ((node->constraint == PINNED_NODE) ||
	     NodeHasSessileBurg(home, node)) {
		*ndeg = 0;
		return;
	}
	
	param = home->param;
	nc = node->numNbrs;
	
	/* Only works for BCC mobility */
	if(param->mobilityType != MOB_BCC_GLIDE_0) {
		Fatal("BCCNodeGlideConstraints_0 only works for BCC_GLIDE_0 mobility");
	}

	/* Copy glide plane constraints and determine line constraints */
	for (i = 0; i < nc; i++) {
		
		burgX[i] = burgx[i] = node->burgX[i];
		burgY[i] = burgy[i] = node->burgY[i];
		burgZ[i] = burgz[i] = node->burgZ[i];

/*
 *      If needed, rotate the Burgers vector from the
 *      laboratory frame to the crystal frame.
 */
		if (param->useLabFrame) {
			real8 burgTmp[3] = {burgX[i], burgY[i], burgZ[i]};
			real8 burgRot[3];

			Matrix33Vector3Multiply(home->rotMatrixInverse, burgTmp, burgRot);

			burgX[i] = burgRot[0]; burgY[i] = burgRot[1]; burgZ[i] = burgRot[2];
			burgx[i] = burgRot[0]; burgy[i] = burgRot[1]; burgz[i] = burgRot[2];
		}

		if ( (fabs(fabs(burgX[i]) - fabs(burgY[i])) > FFACTOR_NORMAL) ||
		     (fabs(fabs(burgY[i]) - fabs(burgZ[i])) > FFACTOR_NORMAL) )
        {	/* not <111> type Burgers vector */
			nbr = GetNeighborNode(home,node,i);
			lineX[i] = nbr->x - node->x;
			lineY[i] = nbr->y - node->y;
			lineZ[i] = nbr->z - node->z;
			ZImage(param, lineX+i, lineY+i, lineZ+i);
/*
 *          If needed, rotate the line sense from the laboratory frame to
 *          the crystal frame.
 */
			if (param->useLabFrame) {
				real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
				real8 lDirRot[3];

				Matrix33Vector3Multiply(home->rotMatrixInverse, lDir, lDirRot);

				lineX[i] = lDirRot[0];
				lineY[i] = lDirRot[1];
				lineZ[i] = lDirRot[2];
			}

	    }
	    else
	    {
            lineX[i] = lineY[i] = lineZ[i] = 0;
	    }
	}

	/* Surface node constraint */
	if (IsSurfaceNode(node, t)) {
		
		real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *          If needed, rotate the free surface normal from the laboratory
 *          frame to the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		lineX[nc] = 0.0; lineY[nc] = 0.0; lineZ[nc] = 0.0;
		nc++;
	}

	/* Normalize glide plane normal vectors and lc line vectors*/
	for (i = 0; i < nc; i++) {
		a = sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
		b = sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

		if(a>0) {
			normX[i]/=a;
			normY[i]/=a;
			normZ[i]/=a;

			normx[i]/=a;
			normy[i]/=a;
			normz[i]/=a;
		}
		if(b>0) {
			lineX[i]/=b;
			lineY[i]/=b;
			lineZ[i]/=b;
		}
	}

	/* Find independent glide constraints */ 
	ngc = nc;
	for (i = 0; i < nc; i++) {
		for (j = 0; j < i; j++) {
			Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
		}
		if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH) {
			normX[i] = normY[i] = normZ[i] = 0;
			ngc--;
		}
	}
	
	ngc = 0;
	for (i = 0; i < nc; i++) {
		if ((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0)) {	
/*
 *  		If needed, rotate the normal vector back to the laboratory frame
 *  		from the crystal frame
 */
			if (param->useLabFrame) {
				real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
				real8 normRot[3];

				Matrix33Vector3Multiply(home->rotMatrix, normTmp, normRot);

				normX[i] = normRot[0];
				normY[i] = normRot[1];
				normZ[i] = normRot[2];
			}
			
			normX[ngc] = normX[i];
			normY[ngc] = normY[i];
			normZ[ngc] = normZ[i];
			ngc++;
		}
	}

	/* Find independent line constraints */
	nlc = 0;
	for (i = 0; i < nc; i++) {
		for (j = 0; j < i; j++) {
			Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
		}
		if ((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH) {
			lineX[i] = lineY[i] = lineZ[i] = 0;
		} else {
			nlc++;
		}
	}
	
	nlc = 0;
#if _ENABLE_LINE_CONSTRAINT
	for (i = 0; i < nc; i++) {
		if ((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0)) {
/*
 *  		If needed, rotate the line vector back to the laboratory frame
 *  		from the crystal frame
 */
			if (param->useLabFrame) {
				real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
				real8 lDirRot[3];

				Matrix33Vector3Multiply(home->rotMatrix, lDir, lDirRot);

				lineX[i] = lDirRot[0];
				lineY[i] = lDirRot[1];
				lineZ[i] = lDirRot[2];
			}
			
			lineX[nlc] = lineX[i];
			lineY[nlc] = lineY[i];
			lineZ[nlc] = lineZ[i];
			nlc++;
		}
	}
#endif
	
	/* Determine degree of freedom of the node and its glide constraint(s) */
	if (ngc > 2 || nlc > 1) {
		*ndeg = 0;
	} else {
		
		if (ngc == 1) {
			
			*ndeg = 2;
			dir[0] = normX[0];
			dir[1] = normY[0];
			dir[2] = normZ[0];
			
			if (nlc == 1) {
				if (fabs(lineX[0]*dir[0]+lineY[0]*dir[1]+lineZ[0]*dir[2]) < FFACTOR_ORTH) {
					*ndeg = 1;
					dir[0] = lineX[0];
					dir[1] = lineY[0];
					dir[2] = lineZ[0];
				} else {
					*ndeg = 0;
					VECTOR_ZERO(dir);
				}
			}
			
		} else if (ngc == 2) {
			
			*ndeg = 1;
			double n1[3] = {normX[0], normY[0], normZ[0]};
			double n2[3] = {normX[1], normY[1], normZ[1]};
			NormalizedCrossVector(n1, n2, dir);
			
			if (nlc == 1) {
				if (fabs(fabs(lineX[0]*dir[0]+lineY[0]*dir[1]+lineZ[0]*dir[2])-1.0) > FFACTOR_ORTH) {
					*ndeg = 0;
					VECTOR_ZERO(dir);
				}
			}
			
		}
		
	}
	
	return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    BCCNodeGlideConstraints
 *                   This function is built for BCC_glide Mobility
 *
 *------------------------------------------------------------------------*/
void BCCNodeGlideConstraints(Home_t *home, Node_t *node, int *ndeg, double dir[3])
{
	int     i, j, nc, ngc;
	real8   normX[20], normY[20], normZ[20];
	real8   normx[20], normy[20], normz[20];
	real8   t, a, b;
	Node_t  *nbr;
	Param_t *param;
	
	t = 0.0;
	VECTOR_ZERO(dir);

	/* Pinned or sessile node */
	if ((node->constraint == PINNED_NODE) ||
	     NodeHasSessileBurg(home, node)) {
		*ndeg = 0;
		return;
	}
	
	param = home->param;
	nc = node->numNbrs;
	
	/* Only works for BCC mobility */
	if(param->mobilityType != MOB_BCC_GLIDE) {
		Fatal("BCCNodeGlideConstraints only works for BCC_GLIDE mobility");
	}

	/* Copy glide plane constraints */
	for (i = 0; i < nc; i++) {
		
		normX[i] = normx[i] = node->nx[i];
		normY[i] = normy[i] = node->ny[i];
		normZ[i] = normz[i] = node->nz[i];

/*
 *      If needed, rotate the glide plane normals from the
 *      laboratory frame to the crystal frame.
 */
		if (param->useLabFrame) {
			real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
			real8 normRot[3];

			Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);

			normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
			normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
		}
	}

	/* Surface node constraint */
	if (IsSurfaceNode(node, t)) {
		
		real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *          If needed, rotate the free surface normal from the laboratory
 *          frame to the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		nc++;
	}

	/* Normalize glide plane normal vectors*/
	for (i = 0; i < nc; i++) {
		a = sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);

		if(a>0) {
			normX[i]/=a;
			normY[i]/=a;
			normZ[i]/=a;

			normx[i]/=a;
			normy[i]/=a;
			normz[i]/=a;
		}
	}

	/* Find independent glide constraints */ 
	ngc = nc;
	for (i = 0; i < nc; i++) {
		for (j = 0; j < i; j++) {
			Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
		}
		if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH) {
			normX[i] = normY[i] = normZ[i] = 0;
			ngc--;
		}
	}
	
	ngc = 0;
	for (i = 0; i < nc; i++) {
		if ((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0)) {	
/*
 *  		If needed, rotate the normal vector back to the laboratory frame
 *  		from the crystal frame
 */
			if (param->useLabFrame) {
				real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
				real8 normRot[3];

				Matrix33Vector3Multiply(home->rotMatrix, normTmp, normRot);

				normX[i] = normRot[0];
				normY[i] = normRot[1];
				normZ[i] = normRot[2];
			}
			
			normX[ngc] = normX[i];
			normY[ngc] = normY[i];
			normZ[ngc] = normZ[i];
			ngc++;
		}
	}
	
	/* Determine degree of freedom of the node and its glide constraint(s) */
	if (ngc > 2) {
		*ndeg = 0;
	} else {
		
		if (ngc == 1) {
			
			*ndeg = 2;
			
		} else if (ngc == 2) {
			
			*ndeg = 1;
			double n1[3] = {normX[0], normY[0], normZ[0]};
			double n2[3] = {normX[1], normY[1], normZ[1]};
			NormalizedCrossVector(n1, n2, dir);
			
		}
		
	}
	
	return;
}


/*-------------------------------------------------------------------------
 *
 *      Function:    ProjectedNodePosition
 *
 *------------------------------------------------------------------------*/
void ProjectedNodePosition(Node_t *node, double dir[3], double t, double pos[3])
{
	double x, y, z;
	double x0, y0, z0;
	
	x = node->x;
	y = node->y;
	z = node->z;
	
	x0 = x + dir[0];
	y0 = y + dir[1];
	z0 = z + dir[2];
	
	if (fabs(z0-z) < 1.e-5) { 
		// already at the surface or points parallel to the surface
		pos[0] = x;
		pos[1] = y;
	} else {
		double q = (t - z)/(z0 - z);
		pos[0] = x + (x0 - x)*q;
		pos[1] = y + (y0 - y)*q;
	}
	pos[2] = t;
	
}
#endif


/*-------------------------------------------------------------------------
 *
 *      Function:    RemoveSurfaceSegments
 *      Description: remove segments connecting constrained node outside the HS
 *
 *------------------------------------------------------------------------*/
#ifdef _SURFACE_STEPS
static void RemoveSurfaceSegments67(Home_t *home,HalfSpace_t *halfspace,real8 t)
#else
static void RemoveSurfaceSegments67(Home_t *home,real8 t)
#endif
{
  int i, j, k, constra, constrb;
  Node_t *nodea, *nodeb, *nodec;
  
  int thisDomain;
  thisDomain = home->myDomain;

  int dirty = 1;

  while (dirty) 
    {
      dirty = 0;
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;
       
	  constra = nodea->constraint;

	  /*
	   *   Node A is constrained to either 6 (HALFSPACE_SURFACE_NODE) or 7 (PINNED_NODE) and is outside the halfspace
	   */

	  //if (constra==0) continue;
	  if (  (constra == PINNED_NODE && nodea->numNbrs == 1)||  (constra == PINNED_NODE && nodea->z >=t) || (constra == HALFSPACE_SURFACE_NODE) )
	    {

	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j); 
		  constrb = nodeb->constraint;
		  
		  if(OrderNodes(nodea,nodeb)>0) continue;
		  
		  /*
		   *   Node B is constrained to either 6 or 7 and is outside the halfspace
		   */
		  //if (constrb==0) continue;
		  if (  (constrb == PINNED_NODE && nodeb->numNbrs == 1)|| (constrb == PINNED_NODE && nodeb->z >=t) || (constrb == HALFSPACE_SURFACE_NODE) )
		    {
		      
		      if (iprint) printf("(6-7) Domain %d -- Found a segment on the surface to be deleted (%d,%d)-(%d,%d) zA=%f zB=%f\n",
					 home->myDomain,nodea->myTag.domainID, nodea->myTag.index,
					 nodeb->myTag.domainID, nodeb->myTag.index,nodea->z,nodeb->z);
		      // If serial mode is applied, there is only one domain and DomainOwnsSeg() returns 1
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
			{
			  if (iprint) printf("(6-7) Domain %d -- Remove arms (%d,%d)-(%d,%d) zA=%f zB=%f\n",
					     home->myDomain,nodea->myTag.domainID, nodea->myTag.index,
					     nodeb->myTag.domainID, nodeb->myTag.index,nodea->z,nodeb->z);
			  // 0, 0, 0, 0, 0, 0 indicates the arm will be removed.
			  ChangeArmBurg(home, nodea, &nodeb->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
					0.0, 0.0, 0.0,
#endif
					1, DEL_SEG_HALF);
			  ChangeArmBurg(home, nodeb, &nodea->myTag, 0, 0, 0, 0, 0, 0,
#ifdef _STACKINGFAULT
					0.0, 0.0, 0.0,
#endif
					1, DEL_SEG_HALF);
#if 0			  
			  if (nodeb->numNbrs == 0 && nodeb->myTag.domainID == thisDomain) 
			    {
			      if (iprint) printf("(6-7) Removing node (%d,%d)\n",nodeb->myTag.domainID, nodeb->myTag.index);
			      RemoveNode(home, nodeb, 1);
			    }
#endif
			  dirty = 1; // If ChangeArmBurg() is invoked, break will work below.
			}
		    } // if
		  if (dirty) break;
		}// for (j) 
	      if (dirty) break;
	    } // if
	}// for (i)
    }//while

  
   /* 
    * Remove any nodes with zero neighbors
    */
   for (i = 0; i < home->newNodeKeyPtr; i++) 
     {
       nodea = home->nodeKeys[i];
       if (nodea == (Node_t *)NULL) continue;
       
       if (nodea->numNbrs == 0 && nodea->myTag.domainID == thisDomain)
	 {
	   if (iprint) printf("(6-7) Removing node (%d,%d)\n",nodea->myTag.domainID, nodea->myTag.index);
	   
#ifdef _SURFACE_STEPS
/* 		
 * 		Flag node as virtual within the surface segment list
 */
		surfaceStepSeg_t *surfStepSegList = halfspace->surfStepSegList;
		for (j = 0; j < halfspace->surfStepSegCount; j++) {
			if (surfStepSegList[j].virtual1 == 0) {
				if (surfStepSegList[j].node1->myTag.index == nodea->myTag.index) {
					surfStepSegList[j].virtual1 = 1;
					surfStepSegList[j].node1 = (Node_t *)NULL;
					if (iprint) printf("-virtual1 node %d\n",nodea->myTag.index);
				}
			}
			if (surfStepSegList[j].virtual2 == 0) {
				if (surfStepSegList[j].node2->myTag.index == nodea->myTag.index) {
					surfStepSegList[j].virtual2 = 1;
					surfStepSegList[j].node2 = (Node_t *)NULL;
					if (iprint) printf("-virtual2 node %d\n",nodea->myTag.index);
				}
			}
		}
#endif

	   RemoveNode(home, nodea, 1);
	 }
     }
}



/*-------------------------------------------------------------------------
 *
 *      Function:    Halfspace_Remesh
 *      Description: Handle node collision with halfspace surface.
 * 	                 When surface steps are used, this function also 
 *                   reconstructs the path between surface nodes to create
 *                   virtual surface segments.
 *
 *------------------------------------------------------------------------*/
void HalfSpace_Remesh(Home_t *home,HalfSpace_t *halfspace)
{
  int i, j, k, splitstatus, isDomOwnsSeg=0;
  double t;
  Node_t *nodea, *nodeb;
  Param_t *param;
  int icall;

  int thisDomain;
  thisDomain = home->myDomain;

  param = home->param;
  t = 0.0;


  // During remesh, a node can been deleted that leads to two nodes 
  // connected and with flag 6 or/and  7.
  // Remove those nodes.
#ifdef _SURFACE_STEPS
  RemoveSurfaceSegments67(home,halfspace,t);
#else
  RemoveSurfaceSegments67(home,t);
#endif

  // Flag to 6 nodes that are outside the half space
  for (i = 0; i < home->newNodeKeyPtr; i++) 
    {
      nodea = home->nodeKeys[i];
      if (nodea == (Node_t *)NULL) continue;
      if (nodea->constraint == PINNED_NODE) continue;
      if (nodea->constraint == HALFSPACE_SURFACE_NODE) continue;

      /* if a node is outside half space, flag it to a surface node (6) */ 
      if (nodea->z >= t)  
	{
	  nodea->constraint = HALFSPACE_SURFACE_NODE;
	}

#if PARALLEL
      // For a segment located on two procs and ouside the HS, both nodes are set to 6
      // but on the proc that does not contain the segment, the node is not properly set  to 6.
      for (j = 0; j < nodea->numNbrs; j++) 
	{
	  nodeb = GetNeighborNode(home, nodea, j);
	  if (nodeb->constraint == PINNED_NODE) continue;
	  if (nodeb->z >= t)  
	    {
	      nodeb->constraint = HALFSPACE_SURFACE_NODE;
	    }
	}
#endif
    }

#ifdef _SURFACE_STEPS
/*	
 * 	Move multi-arms nodes that are outside the volume back 
 *  at the surface to ease the construction of surface segments
 */
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((nodea = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		if (nodea->constraint != HALFSPACE_SURFACE_NODE) continue;
		if (nodea->z > t && nodea->numNbrs > 2) {
			
			int ndeg;
			double dir[3], pos[3];
			if(param->mobilityType == MOB_FCC_0) {
			    FCCNodeGlideConstraints(home, nodea, &ndeg, dir); //the ndeg value returns from FCCNodeGlideConstraints()
			} else if (param->mobilityType == MOB_BCC_0B) {
				ndeg = 2;
			} else if (param->mobilityType == MOB_BCC_GLIDE) {
				BCCNodeGlideConstraints(home, nodea, &ndeg, dir); //the ndeg value returns from BCCNodeGlideConstraints()
			} else if (param->mobilityType == MOB_BCC_GLIDE_0) {
				BCCNodeGlideConstraints_0(home, nodea, &ndeg, dir); //the ndeg value returns from BCCNodeGlideConstraints_0()
			}

			if (ndeg == 2) {
				dir[0] = nodea->vX;
				dir[1] = nodea->vY;
				dir[2] = nodea->vZ;
				ProjectedNodePosition(nodea, dir, t, pos);
				nodea->x = pos[0];
				nodea->y = pos[1];
				nodea->z = pos[2];
				if (iprint) printf("project A multi-node %d on the surface at p = %e %e %e\n",
				nodea->myTag.index,pos[0],pos[1],pos[2]);
			} else if (ndeg == 1) {
				ProjectedNodePosition(nodea, dir, t, pos);
				nodea->x = pos[0];
				nodea->y = pos[1];
				nodea->z = pos[2];
				if (iprint) printf("project B multi-node %d on the surface at p = %e %e %e\n",
				nodea->myTag.index,pos[0],pos[1],pos[2]);
			}
			
		}
	}
#endif
  

  /* The only remaining case is a surface nodea (6) 
   * outside the film with a neibhoring normal nodeb (0) 
   * inside the film 
   * nodea  belongs to the current proc. but nodea
   * may belong to another processor.
   * Once SplitNode has been used, nodea pointers have changed.
   * We need to start from the begining of the loop again.
   */


  /* 
   * First, treat the case where A is outside, B is inside the halfspace
   */

  int dirty6 = 1;
  
  while (dirty6) 
    {
      dirty6 = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;

	  // Node A is outside the half space
	  if (nodea->constraint != HALFSPACE_SURFACE_NODE) continue;
	  if (nodea->z > t)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);
		  
		  // Node b is inside
		  if (nodeb->constraint == UNCONSTRAINED) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodea->myTag))
			{
			  if (iprint) printf("(6) Domain %d A-Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				 nodea->myTag.domainID, nodea->myTag.index,
				 nodeb->myTag.domainID, nodeb->myTag.index);
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodea,nodeb,t);
			  if (splitstatus == SPLIT_SUCCESS) dirty6 = 1;
			}
		    }
		  if (dirty6) break;
		} // for j
	      if (dirty6) break;
	    }
	}// for i
    }//while


  /* 
   * Second, treat the case where A is inside, B is outside the halfspace
   */

  dirty6 = 1;
  
  while (dirty6) 
    {
      dirty6 = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;

	  // Node A is inside	  
	  if (nodea->constraint == UNCONSTRAINED)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);
		  
		  // Node b is outside the halfspace
		  if (nodeb->constraint != HALFSPACE_SURFACE_NODE) continue;
		  if (nodeb->z > t) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
			{		      
			  if (iprint) printf("(6) Domain %d B-Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				 nodea->myTag.domainID, nodea->myTag.index,
				 nodeb->myTag.domainID, nodeb->myTag.index);
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodeb,nodea,t);
			  if (splitstatus == SPLIT_SUCCESS) dirty6 = 1;
			}
		    }
		  if (dirty6) break;
		} // for j
	      if (dirty6) break;
	    }
	}// for i
    }//while


#ifdef _SURFACE_STEPS
/*
 * 	Loop over all surface nodes to identify surface nodes pairs
 * 	and create the associated virtual surface segments.
 */
	
	int     iSurfNodes, prevNode, neiOut, neiIn, newSurfSeg, nsurfSeg, findSeg;
	int     glideChange1, glideChange2, nextNei;
	int     *surfNodesFlag;
	double  burg1[3], burg2[3];
	double  glid1[3], glid2[3], n1[3], n2[3];
	double  n1n2, b1b2, pos[3], dir[3];
	Node_t  *snode;
	surfaceStepSeg_t *surfStepSegList;
	
	surfStepSegList = halfspace->surfStepSegList;
	
	/* Flag surface nodes */
	iSurfNodes = 0;
	surfNodesFlag = malloc(home->newNodeKeyPtr*sizeof(int));
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		surfNodesFlag[i] = 0;
		nodea = home->nodeKeys[i];
		if (nodea == (Node_t *)NULL) continue;
		if (IsSurfaceNode(nodea, t)) {
			surfNodesFlag[i] = 1;
			iSurfNodes++;
		}
	}
	
	if (iSurfNodes > 0) {
		
		if (iprint) printf("%d SURFACE NODES HAVE BEEN FOUND\n", iSurfNodes);
			
		/* Pair nodes */
		for (i = 0; i < home->newNodeKeyPtr; i++) {
			if ((nodea = home->nodeKeys[i]) == (Node_t *)NULL) continue;
			if (surfNodesFlag[i] != 1) continue;
			
			snode = nodea;
			prevNode = nodea->myTag.index;
			
			//printf("-surfnode %d\n",prevNode);
			
			neiOut = neiIn = 0;     //neiOut and neiIn represent the outside and inside neighboring nodes, respectively.
			int neiOutList[nodea->numNbrs];
			for (j = 0; j < nodea->numNbrs; j++) {
				nodeb = GetNeighborNode(home, nodea, j);
				if (nodeb->z > t) {
					neiOutList[neiOut] = j;
					neiOut++;
				} else if (nodeb->z < t) {
					neiIn++;
				}
			}
			
			/* Special case: surface is a multi-node whose 
			 * several arms are outside of the volume */
			if (neiOut > 1) {
				if (neiIn == 0) continue;
				printf("node %d: neiIn = %d, neiOut = %d\n",nodea->myTag.index,neiIn,neiOut);
				//Fatal("surface node %d has %d arms outside",nodea->myTag.index,neiOut);
			}
			
			/* If no neighbors are out of the volume, make sure that
			 * the node is part of an existing surface segment */
			if (neiOut == 0) {
				//printf("--no neighbor out\n");
				
				if (nodea->numNbrs != 1) continue;
				
				/* Look if node is in the surface segment list */
				findSeg = 0;
				for (j = 0; j < halfspace->surfStepSegCount; j++) {
					if (surfStepSegList[j].virtual1 == 0) {
						if (surfStepSegList[j].node1->myTag.index == nodea->myTag.index) {
							findSeg = 1;
							break;
						}
					}
					if (surfStepSegList[j].virtual2 == 0) {
						if (surfStepSegList[j].node2->myTag.index == nodea->myTag.index) {
							findSeg = 1;
							break;
						}
					}
				}
				
				/* Add new surface segment if node was not found */
				if (findSeg == 0) {
					
					if (iprint) printf("--add new initial surface segment for node %d\n", prevNode);
					
					newSurfSeg = CreateNewSurfaceSegment(halfspace);
					surfStepSegList = halfspace->surfStepSegList;
					
					surfStepSegList[newSurfSeg].virtual1 = 1;
					surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
					
					surfStepSegList[newSurfSeg].pos1[0] = nodea->x;
					surfStepSegList[newSurfSeg].pos1[1] = nodea->y;
					surfStepSegList[newSurfSeg].pos1[2] = nodea->z;
					
					surfStepSegList[newSurfSeg].virtual2 = 0;
					surfStepSegList[newSurfSeg].node2 = nodea;
					
					surfStepSegList[newSurfSeg].burg[0] = nodea->burgX[0];
					surfStepSegList[newSurfSeg].burg[1] = nodea->burgY[0];
					surfStepSegList[newSurfSeg].burg[2] = nodea->burgZ[0];
				
					surfStepSegList[newSurfSeg].norm[0] = nodea->nx[0];
					surfStepSegList[newSurfSeg].norm[1] = nodea->ny[0];
					surfStepSegList[newSurfSeg].norm[2] = nodea->nz[0];
					
				}
				
				continue;
			}
			
			/* Unflag surface node */
			surfNodesFlag[nodea->myTag.index] = 0;
			
			/* Loop over outside nodes */
			for (k = 0; k < neiOut; k++) {
			
			nodea = snode;
			prevNode = nodea->myTag.index;
			
			/* Grab outside neighbor node */
			nodeb = GetNeighborNode(home, nodea, neiOutList[k]);
			
			burg1[0] = nodea->burgX[neiOutList[k]];
			burg1[1] = nodea->burgY[neiOutList[k]];
			burg1[2] = nodea->burgZ[neiOutList[k]];
			glid1[0] = nodea->nx[neiOutList[k]];
			glid1[1] = nodea->ny[neiOutList[k]];
			glid1[2] = nodea->nz[neiOutList[k]];
			
			burg2[0] = burg1[0];
			burg2[1] = burg1[1];
			burg2[2] = burg1[2];
			
			if (iprint) printf("-surfnode %d   (%f,%f,%f)\n",prevNode,nodea->x,nodea->y,nodea->z);
			if (iprint) printf("--neighbor %d   (%f,%f,%f)\n",nodeb->myTag.index,nodeb->x,nodeb->y,nodeb->z);
			
/* 
 * 	 		Loop over node connections to reconstruct surface step path.
 *   		If the slip system changes along the path, we need to create
 *   		distinct surface segements in order for the imprint to remain
 *   		consistent with the different slip systems.
 */


			
			glideChange1 = 0;
			glideChange2 = 1;
			
			while (glideChange2 == 1) {
				glideChange2 = 0;
				nextNei = 0;
			
				while (!IsSurfaceNode(nodeb, t) && glideChange2 == 0) {
					
					/* Special case: outside node is a multi-node */
					if (nodeb->numNbrs > 2) Fatal("outside node %d has %d arms",nodeb->myTag.index,nodeb->numNbrs);
					
					for (j = 0; j < nodeb->numNbrs; j++) {
						if (nodeb->nbrTag[j].index == prevNode) continue;
						
						prevNode = nodeb->myTag.index;
						burg2[0] = nodeb->burgX[j];
						burg2[1] = nodeb->burgY[j];
						burg2[2] = nodeb->burgZ[j];
						
						glid2[0] = nodeb->nx[j];
						glid2[1] = nodeb->ny[j];
						glid2[2] = nodeb->nz[j];

						if ((param->mobilityType == MOB_FCC_0 || param->mobilityType == MOB_BCC_GLIDE_0 || param->mobilityType == MOB_BCC_GLIDE) && !isCollinear(glid1[0], glid1[1], glid1[2], glid2[0], glid2[1], glid2[2])) {
							/* Change of glide plane */
							if (iprint) printf("--change of glide plane\n");
							glideChange2 = 1;
							nextNei = j;
						} else {
							nodeb = GetNeighborNode(home, nodeb, j);
							if (iprint) printf("--neighbor %d   (%f,%f,%f)\n",nodeb->myTag.index,nodeb->x,nodeb->y,nodeb->z);
						}
						break;
					}
				}
				
				if (iprint) printf("--found pair %d - %d\n",nodea->myTag.index,nodeb->myTag.index);
				
				/* Check that both nodes have the same Burgers */
				if (!isCollinear(burg1[0], burg1[1], burg1[2], burg2[0], burg2[1], burg2[2])) {
					printf("Error: pair of nodes %d-%d don't have the same Burgers...\n",
					nodea->myTag.index,nodeb->myTag.index);
					printf(" b1 = %e %e %e, b2 = %e %e %e\n", burg1[0], burg1[1], burg1[2], 
					                                          burg2[0], burg2[1], burg2[2]);
					Fatal("leaving");
				}
				
				
				/* Unflag node */
				surfNodesFlag[nodeb->myTag.index] = 0;
				
				/* Create surface segment */
				newSurfSeg = CreateNewSurfaceSegment(halfspace);
				surfStepSegList = halfspace->surfStepSegList;
				
				if (iprint) printf("-- Create surface segment %d\n", newSurfSeg);
				
				
				surfStepSegList[newSurfSeg].burg[0] = burg1[0];
				surfStepSegList[newSurfSeg].burg[1] = burg1[1];
				surfStepSegList[newSurfSeg].burg[2] = burg1[2];
				
				surfStepSegList[newSurfSeg].norm[0] = glid1[0];
				surfStepSegList[newSurfSeg].norm[1] = glid1[1];
				surfStepSegList[newSurfSeg].norm[2] = glid1[2];
				
				if (glideChange1 == 1) {
					if (iprint) printf("--glideChange1\n");
					surfStepSegList[newSurfSeg].virtual1 = 1;
					surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
					
					/* Project nodea onto surface along planes intersection */
					n1[0] = nodea->nx[0];
					n1[1] = nodea->ny[0];
					n1[2] = nodea->nz[0];
					n2[0] = nodea->nx[1];
					n2[1] = nodea->ny[1];
					n2[2] = nodea->nz[1];
					NormalizedCrossVector(n1, n2, dir);
					ProjectedNodePosition(nodea, dir, t, pos);
					
					surfStepSegList[newSurfSeg].pos1[0] = pos[0];
					surfStepSegList[newSurfSeg].pos1[1] = pos[1];
					surfStepSegList[newSurfSeg].pos1[2] = pos[2];
					
				} else {
					surfStepSegList[newSurfSeg].virtual1 = 0;
					surfStepSegList[newSurfSeg].node1 = nodea;
				}
				
				if (glideChange2 == 1) {
					if (iprint) printf("--glideChange2\n");
					surfStepSegList[newSurfSeg].virtual2 = 1;
					surfStepSegList[newSurfSeg].node2 = (Node_t *)NULL;
					
					/* Project nodeb onto surface along planes intersection */
					NormalizedCrossVector(glid1, glid2, dir);
					ProjectedNodePosition(nodeb, dir, t, pos);
					
					surfStepSegList[newSurfSeg].pos2[0] = pos[0];
					surfStepSegList[newSurfSeg].pos2[1] = pos[1];
					surfStepSegList[newSurfSeg].pos2[2] = pos[2];
					
					/* Set start node for next segment */
					glideChange1 = 1;
					nodea = nodeb;
					nodeb = GetNeighborNode(home, nodea, nextNei);
					glid1[0] = glid2[0];
					glid1[1] = glid2[1];
					glid1[2] = glid2[2];
					
				} else {
					surfStepSegList[newSurfSeg].virtual2 = 0;
					surfStepSegList[newSurfSeg].node2 = nodeb;
				}
				
			}
			
			}
		
		}
	
	}
	
	free(surfNodesFlag);
	
	
#endif
	

  /* 
   * Remesh surface segments that are too small to avoid 
   * the significant drop of timesteps
   * SWL (063011)
   */

  int mergestatus;
  Node_t *mergedNode;
  real8 position[3],xB,yB,zB;
  real8 r;
  double minSurfSegL;

  int p, q, nodePos;
  int nc, sn, line_surfseg, numglidecon, nconstraint, numlinecon, nlconstraint;
  Node_t *nbr, *node_check;
  real8 a, b, eps, z_check;
  real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
  real8 burgX[100], burgY[100], burgZ[100], burgx[100], burgy[100], burgz[100];
  real8 lineX[100], lineY[100], lineZ[100], linex[100], liney[100], linez[100];
  real8 surfnode_x[100], surfnode_y[100], surfnode_z[100];
  real8 surf2_x, surf2_y, surf2_z, surf_mag;
  real8 normx_temp[100], normy_temp[100], normz_temp[100], linex_temp[100], liney_temp[100], linez_temp[100];
  real8 normX_temp1, normY_temp1, normZ_temp1, normX_temp2, normY_temp2, normZ_temp2;
  real8 lineX_temp1, lineY_temp1, lineZ_temp1, lineX_temp2, lineY_temp2, lineZ_temp2;
  real8 z0, intersec1, intersec2, intersec3, intersec_mag, transnode;
  
#ifdef _SURFACE_STEPS

/* 
 * 	It would be cumbersome to handle all special remeshing cases
 * 	when surface steps are used. Therefore, surface remesh operations
 * 	will only be performed when certain conditions are met
 */
	 
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((nodea = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		if (!IsSurfaceNode(nodea, t)) continue; // if nodea is surface node, no continue
#if 0
		/* Search for small segments */
		int smallSegs = 0;
		for (j = 0; j < nodea->numNbrs; j++) {
			nodeb = GetNeighborNode(home, nodea, j);
			xB = nodeb->x;
			yB = nodeb->y;
			zB = nodeb->z;
			position[0] = nodea->x;
			position[1] = nodea->y;
			position[2] = nodea->z;
			PBCPOSITION(param, position[0], position[1], position[2], &xB, &yB, &zB);
			r = sqrt( (xB-position[0])*(xB-position[0]) + 
					  (yB-position[1])*(yB-position[1]) + 
					  (zB-position[2])*(zB-position[2]) );
			if (r < home->param->minSeg) {
				if (iprint) printf("--found small segment %d-%d, l = %e\n",
				       nodea->myTag.index,nodeb->myTag.index,r);
				smallSegs++;
			}
		}
		//if (smallSegs > 0) sleep(1);
#endif
		if(nodea->numNbrs != 1) continue;
		
		/* Find which surface segment nodea is part of */
		findSeg = -1;
		nodePos = 0;
		for (j = 0; j < halfspace->surfStepSegCount; j++) {
			if (surfStepSegList[j].virtual1 == 0) {
				if (surfStepSegList[j].node1->myTag.index == nodea->myTag.index) {
					findSeg = j;
					nodePos = 1;
					break;
				}
			}
			if (surfStepSegList[j].virtual2 == 0) {
				if (surfStepSegList[j].node2->myTag.index == nodea->myTag.index) {
					findSeg = j;
					nodePos = 2;
					break;
				}
			}
		}
		if (findSeg == -1) continue;
		
		nodeb = GetNeighborNode(home, nodea, 0);
		if (nodeb->constraint == HALFSPACE_SURFACE_NODE) continue; //if nodea and nodeb are both surface nodes, continue
		
		if (!DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag)) continue;
		
		xB = nodeb->x;
		yB = nodeb->y;
		zB = nodeb->z;
		position[0] = nodea->x;
		position[1] = nodea->y;
		position[2] = nodea->z;
		PBCPOSITION(param, position[0], position[1], position[2], &xB, &yB, &zB);
		r = sqrt( (xB-position[0])*(xB-position[0]) + 
		          (yB-position[1])*(yB-position[1]) + 
		          (zB-position[2])*(zB-position[2]) );
		          
		if (r >= 0.25*home->param->minSeg) continue; //if the segment length is large enough, continue
		
		
		if(param->mobilityType == MOB_FCC_0 || param->mobilityType == MOB_BCC_GLIDE_0) {
			nc = nodeb->numNbrs ;  // nc : the number of neighbor nodes

			/* copy glide plane constraints and determine line constraints */
			sn = 0;
			for(p=0;p<nc;p++) { 
				normX[p] = nodeb->nx[p];
				normY[p] = nodeb->ny[p];
				normZ[p] = nodeb->nz[p];
				
				if (param->useLabFrame) {
					real8 normTmp[3] = {normX[p], normY[p], normZ[p]};
					real8 normRot[3];

					Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);
					normX[p] = normRot[0]; normY[p] = normRot[1]; normZ[p] = normRot[2];
				}

				#define FFACTOR_NORMAL2 1.0e-3
				if(param->mobilityType == MOB_FCC_0) {
				    if ( (fabs(fabs(normX[p]) - fabs(normY[p])) > FFACTOR_NORMAL2) ||
			             (fabs(fabs(normY[p]) - fabs(normZ[p])) > FFACTOR_NORMAL2) ) { 
				    	/* not {111} plane (FCC_0) */
				    	nbr=GetNeighborNode(home,nodeb,p);
				    	lineX[p] = nbr->x - nodeb->x;
				    	lineY[p] = nbr->y - nodeb->y;
				    	lineZ[p] = nbr->z - nodeb->z;
				    	ZImage (param, lineX+p, lineY+p, lineZ+p);
				    	if (nbr->constraint == HALFSPACE_SURFACE_NODE) {
				    		line_surfseg = 1; 
				    	} else if(nbr->constraint != HALFSPACE_SURFACE_NODE) {
				    		line_surfseg = 0;
				    	}
				    } else { 
				    	/* no line constraint */
				    	lineX[p] = lineY[p] = lineZ[p] = 0;
    
				    	nbr=GetNeighborNode(home,nodeb,p);
				    	if ((nbr->constraint == HALFSPACE_SURFACE_NODE) && (r > home->param->minSeg)) {
				    		surfnode_x[sn] = nbr->x;
				    		surfnode_y[sn] = nbr->y;
				    		surfnode_z[sn] = nbr->z;
				    		sn = sn + 1;
				    	}
				    }
				} else if(param->mobilityType == MOB_BCC_GLIDE_0) {
		            if ( (fabs(fabs(burgX[i]) - fabs(burgY[i])) > FFACTOR_NORMAL2) ||
					     (fabs(fabs(burgY[i]) - fabs(burgZ[i])) > FFACTOR_NORMAL2) ) 
					{
						/* not <111> type Burgers vector */
				    	nbr=GetNeighborNode(home,nodeb,p);
				    	lineX[p] = nbr->x - nodeb->x;
				    	lineY[p] = nbr->y - nodeb->y;
				    	lineZ[p] = nbr->z - nodeb->z;
				    	ZImage (param, lineX+p, lineY+p, lineZ+p);
				    	if (nbr->constraint == HALFSPACE_SURFACE_NODE) {
				    		line_surfseg = 1; 
				    	} else if(nbr->constraint != HALFSPACE_SURFACE_NODE) {
				    		line_surfseg = 0;
				    	}
				    } else { 
				    	/* no line constraint for segment with <111> type Burgers vector */
				    	lineX[p] = lineY[p] = lineZ[p] = 0;
    
				    	nbr=GetNeighborNode(home,nodeb,p);
				    	if ((nbr->constraint == HALFSPACE_SURFACE_NODE) && (r > home->param->minSeg)) {
				    		surfnode_x[sn] = nbr->x;
				    		surfnode_y[sn] = nbr->y;
				    		surfnode_z[sn] = nbr->z;
				    		sn = sn + 1;
				    	}
				    }
				}
			}

			/* normalize glide plane normal vectors and lc line vectors*/
			for(p=0;p<nc;p++) {
				a=sqrt(normX[p]*normX[p]+normY[p]*normY[p]+normZ[p]*normZ[p]);
				b=sqrt(lineX[p]*lineX[p]+lineY[p]*lineY[p]+lineZ[p]*lineZ[p]);

				if(a>0) {
					normX[p]/=a;
					normY[p]/=a;
					normZ[p]/=a;
				}
				if(b>0) {
					lineX[p]/=b;
					lineY[p]/=b;
					lineZ[p]/=b;
				}
			}

			/* Find independent glide constraints */ 
			numglidecon = 0;
			for(p=0;p<nc;p++) {
				nconstraint = 1;
				for(q=0;q<p;q++) {
					if(q<p) {
						normX_temp1 = normX[p]; normY_temp1 = normY[p]; normZ_temp1 = normZ[p];
						normX_temp2 = normX[q]; normY_temp2 = normY[q]; normZ_temp2 = normZ[q];
						Orthogonalize(&normX_temp1,&normY_temp1,&normZ_temp1,
						              normX_temp2,normY_temp2,normZ_temp2);

						#define FFACTOR_ORTH2 0.05
						if((normX_temp1*normX_temp1+normY_temp1*normY_temp1
			                +normZ_temp1*normZ_temp1)<FFACTOR_ORTH2) {
							nconstraint = 0;
						}
					}
				}
				if(nconstraint>0) {
					if((normX[p]!=0)||(normY[p]!=0)||(normZ[p]!=0)) {
						if(param->mobilityType == MOB_FCC_0) {
						    if ( (fabs(fabs(normX[p]) - fabs(normY[p])) < FFACTOR_NORMAL2) &&
			                     (fabs(fabs(normY[p]) - fabs(normZ[p])) < FFACTOR_NORMAL2) ) 
							{
						    	normx_temp[numglidecon] = normX[p];
						    	normy_temp[numglidecon] = normY[p];
						    	normz_temp[numglidecon] = normZ[p];
						    	// count the number of {111} type glide constraints
						    	numglidecon = numglidecon + 1;
						    }
						} else if(param->mobilityType == MOB_BCC_GLIDE_0) {
		                    if ( ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(normZ[i])<FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(normY[i])<FFACTOR_NORMAL2)) ||
                                 ((fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(normX[i])<FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL2)) ||
                                 ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL2)) ) 
							{
						    	normx_temp[numglidecon] = normX[p];
						    	normy_temp[numglidecon] = normY[p];
						    	normz_temp[numglidecon] = normZ[p];
						    	// count the number of {110}, {112} or{123} type glide constraints
						    	numglidecon = numglidecon + 1;
						    }
						}
					}
				}
			}

			/* Find independent line constraints */
			numlinecon = 0;
#if _ENABLE_LINE_CONSTRAINT
			for(p=0;p<nc;p++) {
				nlconstraint = 1;
				for(q=0;q<p;q++) {
					if(q<p) {
						lineX_temp1 = lineX[p]; lineY_temp1 = lineY[p]; lineZ_temp1 = lineZ[p];
						lineX_temp2 = lineX[q]; lineY_temp2 = lineY[q]; lineZ_temp2 = lineZ[q];
						Orthogonalize(&lineX_temp1,&lineY_temp1,&lineZ_temp1,lineX_temp2,lineY_temp2,lineZ_temp2);

						if((lineX_temp1*lineX_temp1+lineY_temp1*lineY_temp1+lineZ_temp1*lineZ_temp1)<FFACTOR_ORTH2) {
							nlconstraint = 0;
						}
					}
				}
				if(nlconstraint>0) {
					if((lineX[p]!=0)||(lineY[p]!=0)||(lineZ[p]!=0)) {
						linex_temp[numlinecon] = lineX[p];
						liney_temp[numlinecon] = lineY[p];
						linez_temp[numlinecon] = lineZ[p];
						// count the number of line constraints (e.g. LC junction)
						numlinecon = numlinecon + 1;
					}
				}
			}
#endif
			if (iprint) printf("found small surface seg: numglidecon = %d, numlinecon = %d\n", numglidecon, numlinecon);
			//sleep(2.0);
			if (numlinecon <= 1) {
				if (numglidecon <= 2) {
					
					/* Compute planes intersection if necessary */
					if (numglidecon == 2) {
						
						if (param->useLabFrame) {
							real8 normRot[3];
							
							real8 normTmp0[3] = {normx_temp[0], normy_temp[0], normz_temp[0]};
							Matrix33Vector3Multiply(home->rotMatrix, normTmp0, normRot);
							normx_temp[0] = normRot[0]; normy_temp[0] = normRot[1]; normz_temp[0] = normRot[2];
							
							real8 normTmp1[3] = {normx_temp[1], normy_temp[1], normz_temp[1]};
							Matrix33Vector3Multiply(home->rotMatrix, normTmp1, normRot);
							normx_temp[1] = normRot[0]; normy_temp[1] = normRot[1]; normz_temp[1] = normRot[2];
						}
						
						double n1[3] = {normx_temp[0], normy_temp[0], normz_temp[0]};
						double n2[3] = {normx_temp[1], normy_temp[1], normz_temp[1]};
						NormalizedCrossVector(n1, n2, dir);
					}
					
					/* Check compatibility of line constraint */
					if (numlinecon == 1) {
						if (numglidecon == 2) {
							/* Check that line constraint is the plane intersection */
							if (!isCollinear(dir[0], dir[1], dir[2], linex_temp[0], liney_temp[0], linez_temp[0])) {
								printf(" line constraint not in planes intersection, aborting\n");
								continue;
							}
							printf(" line constraint is in planes intersection, continuing\n");
						} else if (numglidecon == 1) {
							/* Check that line contraint is in the plane */
							if (param->useLabFrame) {
								real8 normRot[3];
								real8 normTmp0[3] = {normx_temp[0], normy_temp[0], normz_temp[0]};
								Matrix33Vector3Multiply(home->rotMatrix, normTmp0, normRot);
								normx_temp[0] = normRot[0]; normy_temp[0] = normRot[1]; normz_temp[0] = normRot[2];
							}
							if (fabs(normx_temp[0]*linex_temp[0]+normy_temp[0]*liney_temp[0]+normz_temp[0]*linez_temp[0]) > FFACTOR_ORTH2) {
								continue;
							}
						} else {
							/* Project nodeb onto surface along line constraint */
							// lines constraint have not been rotated
							dir[0] = linex_temp[0];
							dir[1] = liney_temp[0];
							dir[2] = linez_temp[0];
							ProjectedNodePosition(nodeb, dir, t, position);
						}
					}
					
					if (numglidecon == 2) {
						//if (nodeb->numNbrs != 2) continue; // don't remesh that, needs to be done properly....
						
						/* Project nodeb onto surface along planes intersection */
						ProjectedNodePosition(nodeb, dir, t, position);
					}
					
					MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
					if ((mergestatus & MERGE_SUCCESS) == 1) {
						mergedNode->constraint = HALFSPACE_SURFACE_NODE;
						if (nodePos == 1) {
							surfStepSegList[findSeg].node1 = mergedNode;
						} else {
							surfStepSegList[findSeg].node2 = mergedNode;
						}
						mergedNode->x = position[0];
						mergedNode->y = position[1];
						mergedNode->z = position[2];
						
						if (iprint) printf("MergeNode %d - %d at position %e %e %e\n", 
						       nodea->myTag.index, nodeb->myTag.index, 
						       mergedNode->x, mergedNode->y, mergedNode->z);
						if(param->mobilityType == MOB_FCC_0) {
						    if (iprint) printf("Surface remesh is done for %d {111} type glide constraint(s) in FCC materials\n", numglidecon); 
						} else if(param->mobilityType == MOB_BCC_GLIDE_0) {
							if (iprint) printf("Surface remesh is done for %d {110}, {112} or {123} type glide constraint(s) in BCC materials\n", numglidecon);
						}
					}
				}
				
			}

		} else if (param->mobilityType == MOB_BCC_GLIDE) {
			nc = nodeb->numNbrs ;  // nc : the number of neighbor nodes

			/* copy glide plane constraints */
			sn = 0;
			for(p=0;p<nc;p++) {
				normX[p] = nodeb->nx[p];
				normY[p] = nodeb->ny[p];
				normZ[p] = nodeb->nz[p];
				
				if (param->useLabFrame) {
					real8 normTmp[3] = {normX[p], normY[p], normZ[p]};
					real8 normRot[3];

					Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);
					normX[p] = normRot[0]; normY[p] = normRot[1]; normZ[p] = normRot[2];
				}

			    nbr=GetNeighborNode(home,nodeb,p);
			    if ((nbr->constraint == HALFSPACE_SURFACE_NODE) && (r > home->param->minSeg)) {
			    	surfnode_x[sn] = nbr->x;
			    	surfnode_y[sn] = nbr->y;
			    	surfnode_z[sn] = nbr->z;
			    	sn = sn + 1;
			    }
			}

			/* normalize glide plane normal vectors */
			for(p=0;p<nc;p++) {
				a=sqrt(normX[p]*normX[p]+normY[p]*normY[p]+normZ[p]*normZ[p]);

				if(a>0) {
					normX[p]/=a;
					normY[p]/=a;
					normZ[p]/=a;
				}
			}

			/* Find independent glide constraints */
			numglidecon = 0;
			for(p=0;p<nc;p++) {
				nconstraint = 1;
				for(q=0;q<p;q++) {
					if(q<p) {
						normX_temp1 = normX[p]; normY_temp1 = normY[p]; normZ_temp1 = normZ[p];
						normX_temp2 = normX[q]; normY_temp2 = normY[q]; normZ_temp2 = normZ[q];
						Orthogonalize(&normX_temp1,&normY_temp1,&normZ_temp1,
						              normX_temp2,normY_temp2,normZ_temp2);

						#define FFACTOR_ORTH2 0.05
						if((normX_temp1*normX_temp1+normY_temp1*normY_temp1
			                +normZ_temp1*normZ_temp1)<FFACTOR_ORTH2) {
							nconstraint = 0;
						}
					}
				}

				#define FFACTOR_NORMAL2 1.0e-3
				if(nconstraint>0) {
					if((normX[p]!=0)||(normY[p]!=0)||(normZ[p]!=0)) {
		                if ( ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(normZ[i])<FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(normY[i])<FFACTOR_NORMAL2)) ||
                             ((fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(normX[i])<FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL2)) ||
                             ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL2)) ) 
						{
							normx_temp[numglidecon] = normX[p];
							normy_temp[numglidecon] = normY[p];
							normz_temp[numglidecon] = normZ[p];
							// count the number of {110}, {112} or{123} type glide constraints
							numglidecon = numglidecon + 1;
						}
					}
				}
			}

			if (iprint) printf("found small surface seg: numglidecon = %d\n", numglidecon);

			/* Compute planes intersection if necessary */
			if (numglidecon == 2) {
				
				if (param->useLabFrame) {
					real8 normRot[3];
					
					real8 normTmp0[3] = {normx_temp[0], normy_temp[0], normz_temp[0]};
					Matrix33Vector3Multiply(home->rotMatrix, normTmp0, normRot);
					normx_temp[0] = normRot[0]; normy_temp[0] = normRot[1]; normz_temp[0] = normRot[2];
					
					real8 normTmp1[3] = {normx_temp[1], normy_temp[1], normz_temp[1]};
					Matrix33Vector3Multiply(home->rotMatrix, normTmp1, normRot);
					normx_temp[1] = normRot[0]; normy_temp[1] = normRot[1]; normz_temp[1] = normRot[2];
				}
				
				double n1[3] = {normx_temp[0], normy_temp[0], normz_temp[0]};
				double n2[3] = {normx_temp[1], normy_temp[1], normz_temp[1]};
				NormalizedCrossVector(n1, n2, dir);
			}
			
			if (numglidecon == 2) {
				//if (nodeb->numNbrs != 2) continue; // don't remesh that, needs to be done properly....
				
				/* Project nodeb onto surface along planes intersection */
				ProjectedNodePosition(nodeb, dir, t, position);
			}
			
			MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
			if ((mergestatus & MERGE_SUCCESS) == 1) {
				mergedNode->constraint = HALFSPACE_SURFACE_NODE;
				if (nodePos == 1) {
					surfStepSegList[findSeg].node1 = mergedNode;
				} else {
					surfStepSegList[findSeg].node2 = mergedNode;
				}
				mergedNode->x = position[0];
				mergedNode->y = position[1];
				mergedNode->z = position[2];
				
				if (iprint) printf("MergeNode %d - %d at position %e %e %e\n", 
				       nodea->myTag.index, nodeb->myTag.index, 
				       mergedNode->x, mergedNode->y, mergedNode->z);
				if (iprint) printf("Surface remesh is done for %d {110}, {112} or {123} type glide constraint(s) in BCC materials\n", numglidecon);
			}		
		} else if (param->mobilityType == MOB_BCC_0B) {
			numglidecon = 0;
			numlinecon = 0;
			if (iprint) printf("found small surface seg: numglidecon = %d, numlinecon = %d\n", numglidecon, numlinecon);
			MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
			if ((mergestatus & MERGE_SUCCESS) == 1) {
				mergedNode->constraint = HALFSPACE_SURFACE_NODE;
				if (nodePos == 1) {
					surfStepSegList[findSeg].node1 = mergedNode;
				} else {
					surfStepSegList[findSeg].node2 = mergedNode;
				}
				mergedNode->x = position[0];
				mergedNode->y = position[1];
				mergedNode->z = position[2];

				if (iprint) printf("MergeNode %d - %d at position %e %e %e\n",
						nodea->myTag.index, nodeb->myTag.index,
						mergedNode->x, mergedNode->y, mergedNode->z);
				if (iprint) printf("Surface remesh is done for %d glide constraint(s)\n", numglidecon);
			}
		}
	}

#else
  
  for (i = 0; i < home->newNodeKeyPtr; i++) 
  {
      nodea = home->nodeKeys[i];
      if (nodea == (Node_t *)NULL) continue;
      if (nodea->constraint != HALFSPACE_SURFACE_NODE) continue;
      //if (nodea->numNbrs > 1) continue;

      if(nodea->numNbrs == 1)
      {
          nodeb = GetNeighborNode(home, nodea, 0);
          xB = nodeb->x;
          yB = nodeb->y;
          zB = nodeb->z;
      
          position[0] = nodea->x;
          position[1] = nodea->y;
          position[2] = nodea->z;
          PBCPOSITION(param, position[0], position[1], position[2], 
     	          &xB, &yB, &zB);
          r = sqrt( (xB-position[0])*(xB-position[0]) + 
    	        (yB-position[1])*(yB-position[1]) + 
                (zB-position[2])*(zB-position[2]) );
      }
      else if(nodea->numNbrs > 1)
      {
          for(p=0;p<nodea->numNbrs;p++)
          {
              node_check = GetNeighborNode(home, nodea, p);

              xB = node_check->x;
              yB = node_check->y;
              zB = node_check->z;
      
              position[0] = nodea->x;
              position[1] = nodea->y;
              position[2] = nodea->z;
              PBCPOSITION(param, position[0], position[1], position[2], 
   	                  &xB, &yB, &zB);
              r = sqrt( (xB-position[0])*(xB-position[0]) + 
   	                (yB-position[1])*(yB-position[1]) + 
                        (zB-position[2])*(zB-position[2]) );
              
              if(r < home->param->minSeg)
              {
                  nodeb = GetNeighborNode(home, nodea, p);
                  break;
              }
          }
          if(r > home->param->minSeg)
          {
              continue;
          }
      }

      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
      {
         // For the surface segment smaller than minSeg
         if (r < home->param->minSeg)
         {
             if(param->mobilityType == MOB_FCC_0 || param->mobilityType == MOB_BCC_GLIDE_0)
             {
                 nc = nodeb->numNbrs ;  // nc : the number of neighbor nodes
  
                 /* copy glide plane constraints and determine line constraints */
                 sn = 0;
                 for(p=0;p<nc;p++)
                 { 
                     normX[p] = nodeb->nx[p];
                     normY[p] = nodeb->ny[p];
                     normZ[p] = nodeb->nz[p];

                     #define FFACTOR_NORMAL 1.0e-3

					 if(param->mobilityType == MOB_FCC_0)
					 {
                         if ( (fabs(fabs(normX[p]) - fabs(normY[p])) > FFACTOR_NORMAL) ||
                              (fabs(fabs(normY[p]) - fabs(normZ[p])) > FFACTOR_NORMAL) )
                         { /* not {111} plane (FCC_0) */
                             nbr=GetNeighborNode(home,nodeb,p);
                             lineX[p] = nbr->x - nodeb->x;
                             lineY[p] = nbr->y - nodeb->y; 
                             lineZ[p] = nbr->z - nodeb->z;
                             ZImage (param, lineX+p, lineY+p, lineZ+p);
                             if (nbr->constraint == HALFSPACE_SURFACE_NODE)
                             {
                                 line_surfseg = 1;
                             }
                             else if(nbr->constraint != HALFSPACE_SURFACE_NODE)
                             {
                                 line_surfseg = 0;
                             }
  	                     }
   	                     else
  	                     { /* no line constraint */
     	                         lineX[p] = lineY[p] = lineZ[p] = 0;
        
                                 nbr=GetNeighborNode(home,nodeb,p);
                                 if ((nbr->constraint == HALFSPACE_SURFACE_NODE) && (r > home->param->minSeg))
                                 {
                                     surfnode_x[sn] = nbr->x;
                                     surfnode_y[sn] = nbr->y;
                                     surfnode_z[sn] = nbr->z;
                                     sn = sn + 1;
                                 }
        
	                     }
					 } else if(param->mobilityType == MOB_BCC_GLIDE_0) {
		                 if ( (fabs(fabs(burgX[i]) - fabs(burgY[i])) > FFACTOR_NORMAL) ||
					          (fabs(fabs(burgY[i]) - fabs(burgZ[i])) > FFACTOR_NORMAL) )
                         {  
							     /* not <111> type Burgers vector */
                                 nbr=GetNeighborNode(home,nodeb,p);
                                 lineX[p] = nbr->x - nodeb->x;
                                 lineY[p] = nbr->y - nodeb->y; 
                                 lineZ[p] = nbr->z - nodeb->z;
                                 ZImage (param, lineX+p, lineY+p, lineZ+p);
                                 if (nbr->constraint == HALFSPACE_SURFACE_NODE)
                                 {
                                     line_surfseg = 1;
                                 }
                                 else if(nbr->constraint != HALFSPACE_SURFACE_NODE)
                                 {
                                     line_surfseg = 0;
                                 }
  	                     }
   	                     else
  	                     {  
							     /* no line constraint for segment with <111> type Burgers vector */
     	                         lineX[p] = lineY[p] = lineZ[p] = 0;
        
                                 nbr=GetNeighborNode(home,nodeb,p);
                                 if ((nbr->constraint == HALFSPACE_SURFACE_NODE) && (r > home->param->minSeg))
                                 {
                                     surfnode_x[sn] = nbr->x;
                                     surfnode_y[sn] = nbr->y;
                                     surfnode_z[sn] = nbr->z;
                                     sn = sn + 1;
                                 }
	                     }

					 } 
                 }

                 /* normalize glide plane normal vectors and lc line vectors*/
                 for(p=0;p<nc;p++)
                 {
                     a=sqrt(normX[p]*normX[p]+normY[p]*normY[p]+normZ[p]*normZ[p]);
	             b=sqrt(lineX[p]*lineX[p]+lineY[p]*lineY[p]+lineZ[p]*lineZ[p]);

                     if(a>0)
                     {
                         normX[p]/=a;
                         normY[p]/=a;
                         normZ[p]/=a;
                     }
                     if(b>0)
                     {
                         lineX[p]/=b;
                         lineY[p]/=b;
                         lineZ[p]/=b;
                     }
                 }

                 /* Find independent glide constraints */ 
                 numglidecon = 0;
                 for(p=0;p<nc;p++)
                 {
                     nconstraint = 1;
                     for(q=0;q<p;q++)
                     {
                         if(q<p)
                         {
                             normX_temp1 = normX[p]; normY_temp1 = normY[p]; normZ_temp1 = normZ[p];
                             normX_temp2 = normX[q]; normY_temp2 = normY[q]; normZ_temp2 = normZ[q];
                             Orthogonalize(&normX_temp1,&normY_temp1,&normZ_temp1,
                                            normX_temp2,normY_temp2,normZ_temp2);
                 
                             #define FFACTOR_ORTH 0.05
                             if((normX_temp1*normX_temp1+normY_temp1*normY_temp1
                                 +normZ_temp1*normZ_temp1)<FFACTOR_ORTH)
                             {
                                 nconstraint = 0;

                             }
                         }
                     }

                     if(nconstraint>0)
                     {
                         if((normX[p]!=0)&&(normY[p]!=0)&&(normZ[p]!=0)) 
                         {
							if(param->mobilityType == MOB_FCC_0) {
                                if ( (fabs(fabs(normX[p]) - fabs(normY[p])) < FFACTOR_NORMAL) &&
                                     (fabs(fabs(normY[p]) - fabs(normZ[p])) < FFACTOR_NORMAL) )
                                {
                                    normx_temp[numglidecon] = normX[p];
                                    normy_temp[numglidecon] = normY[p];
                                    normz_temp[numglidecon] = normZ[p];
                                    // count the number of {111} type glide constraints
                                    numglidecon = numglidecon + 1; 
                                }
							} else if(param->mobilityType == MOB_BCC_GLIDE_0) {
		                        if ( ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL) && (fabs(normZ[i])<FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL) && (fabs(normY[i])<FFACTOR_NORMAL)) ||
                                     ((fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL) && (fabs(normX[i])<FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL) && (fabs(fabs(normZ[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL) && (fabs(fabs(normZ[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL) && (fabs(fabs(normZ[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL)) ||
                                     ((fabs(normX[i])>FFACTOR_NORMAL) && (fabs(normY[i])>FFACTOR_NORMAL) && (fabs(normZ[i])>FFACTOR_NORMAL) && (fabs(fabs(normX[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL) && (fabs(fabs(normY[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL)) )
                                {
                                    normx_temp[numglidecon] = normX[p];
                                    normy_temp[numglidecon] = normY[p];
                                    normz_temp[numglidecon] = normZ[p];
                                    // count the number of {110}, {112} or{123} type glide constraints
                                    numglidecon = numglidecon + 1; 
                                }
							}
                         }
                     }

                 }

                 /* Find independent line constraints */
                 numlinecon = 0;
                 for(p=0;p<nc;p++)
                 {
                     nlconstraint = 1;
                     for(q=0;q<p;q++)
                     {
                         if(q<p)
                         {
                             lineX_temp1 = lineX[p]; lineY_temp1 = lineY[p]; lineZ_temp1 = lineZ[p];
                             lineX_temp2 = lineX[q]; lineY_temp2 = lineY[q]; lineZ_temp2 = lineZ[q];
                             Orthogonalize(&lineX_temp1,&lineY_temp1,&lineZ_temp1,lineX_temp2,lineY_temp2,lineZ_temp2);

                             if((lineX_temp1*lineX_temp1+lineY_temp1*lineY_temp1+lineZ_temp1*lineZ_temp1)<FFACTOR_ORTH)
                             {
                                 nlconstraint = 0;
                             }
                         }
                     }
                  
                     if(nlconstraint>0)
                     {
                         if((lineX[p]!=0)&&(lineY[p]!=0)&&(lineZ[p]!=0))
                         {
                             linex_temp[numlinecon] = lineX[p];
                             liney_temp[numlinecon] = lineY[p];
                             linez_temp[numlinecon] = lineZ[p];
                             // count the number of line constraints (e.g. LC junction)
                             numlinecon = numlinecon + 1;
                         }
                     }
                 }

                 z0 = param->hs_Lzinf;
                 if (nodeb->z<0) {
                     z0 = (-1)*z0;
                 }


//                 if(numlinecon == 1)
//                {
//                     printf("The numbers of glide and line constraints are %d and %d, respectively.\n", numglidecon, numlinecon); 
//                     printf("FindInnerNode: node(%d,%d)\n", nodeb->myTag.domainID,nodeb->myTag.index); 
//                 }

                 if(numlinecon == 0)
                 {
                     if((numglidecon == 1) && (r < home->param->minSeg))
                     {
                         printf("FindInnerNode: nodea_surf = node(%d,%d)\n", nodea->myTag.domainID,nodea->myTag.index); 
                         printf("FindInnerNode: nodeb_inner = node(%d,%d)\n", nodeb->myTag.domainID,nodeb->myTag.index); 
                         MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                         if ((mergestatus & MERGE_SUCCESS) == 1)
                         {
                             nodeb->constraint = HALFSPACE_SURFACE_NODE;
							 if(param->mobilityType == MOB_FCC_0) {
                                 printf("Surface remesh is done for one {111} type glide constraint in FCC material. \n");
							 } else if(param->mobilityType == MOB_BCC_GLIDE_0) {
								 printf("Surface remesh is done for one {110} or {112} or {123} type glide constraint in BCC material. \n");
							 }
                         }
                     }
                     else if(numglidecon == 2)
                     {
                         if(r < home->param->minSeg)
                         {
                             xvector(normx_temp[0], normy_temp[0], normz_temp[0], normx_temp[1], normy_temp[1],
                                     normz_temp[1], &intersec1, &intersec2, &intersec3);

                             intersec_mag = intersec1*intersec1+intersec2*intersec2+intersec3*intersec3;
                             if((intersec_mag!=0) && (intersec3!=0))
                             {
                                 intersec1 = intersec1/intersec_mag;
                                 intersec2 = intersec2/intersec_mag;
                                 intersec3 = intersec3/intersec_mag;
#if 0
                                 transnode = (z0 - nodeb->z) / intersec3;
                                 nodeb->x += transnode*intersec1;
                                 nodeb->y += transnode*intersec2;
                                 nodeb->z += transnode*intersec3;
                                 nodeb->constraint = HALFSPACE_SURFACE_NODE; 
                                 printf("Surface remesh is done for two {111} type glide constraints. \n"); 
#endif
                             }
                             else 
                             {
                                 MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                                 if ((mergestatus & MERGE_SUCCESS) == 1)
                                 {
                                     nodeb->constraint = HALFSPACE_SURFACE_NODE; 
									 if(param->mobilityType == MOB_FCC_0) {
                                         printf("Surface remesh is done for two {111} type glide constraints by the simple merge. \n"); 
									 } else if(param->mobilityType == MOB_BCC_GLIDE_0) {
										 printf("Surface remesh is done for two {110} or {112} or {123} type glide constraints by the simple merge. \n");
									 }
                                     Fatal("WEIRD - DEBUG...");
                                 }
                             }
                         }
                         else if((r > home->param->minSeg) && (sn == 2)) // special case (mimic surface node collision)
                         {
                             surf2_x = (surfnode_x[0]-surfnode_x[1])*(surfnode_x[0]-surfnode_x[1]);
                             surf2_y = (surfnode_y[0]-surfnode_y[1])*(surfnode_y[0]-surfnode_y[1]);
                             surf2_z = (surfnode_z[0]-surfnode_z[1])*(surfnode_z[0]-surfnode_z[1]);
                             surf_mag = sqrt(surf2_x + surf2_y + surf2_z);

                             if(surf_mag < (home->param->rann)) // when two surface nodes are very close.
                             {
                                 xvector(normx_temp[0], normy_temp[0], normz_temp[0], normx_temp[1], normy_temp[1],
                                         normz_temp[1], &intersec1, &intersec2, &intersec3);

                                 intersec_mag = intersec1*intersec1+intersec2*intersec2+intersec3*intersec3;
                                 if((intersec_mag!=0) && (intersec3!=0))
                                 {
                                     intersec1 = intersec1/intersec_mag;
                                     intersec2 = intersec2/intersec_mag;
                                     intersec3 = intersec3/intersec_mag;

                                     transnode = (z0 - nodeb->z) / intersec3;
                                     nodeb->x += transnode*intersec1;
                                     nodeb->y += transnode*intersec2;
                                     nodeb->z += transnode*intersec3;
                                     nodeb->constraint = HALFSPACE_SURFACE_NODE;
									 if(param->mobilityType == MOB_FCC_0) { 
                                         printf("Surface remesh is done for two {111} type glide constraints. \n");
									 } else if(param->mobilityType == MOB_BCC_GLIDE_0) {
										 printf("Surface remesh is done for two {110} or {112} or {123} type glide constraints. \n");
									 }
                                 }
                                 else 
                                 {
                                     MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                                     if ((mergestatus & MERGE_SUCCESS) == 1)
                                     {
                                         nodeb->constraint = HALFSPACE_SURFACE_NODE;
									     if(param->mobilityType == MOB_FCC_0) {
                                             printf("Surface remesh is done for two {111} type glide constraints by the simple merge. \n"); 
									     } else if(param->mobilityType == MOB_BCC_GLIDE_0) {
									    	 printf("Surface remesh is done for two {110} or {112} or {123} type glide constraints by the simple merge. \n");
									     } 
                                     }
                                 }
                             } 
                         }
                     }
                     else if((numglidecon > 2) && (r < home->param->minSeg)) // Very unlucky case, but not frequent
                     {
                         MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                         if ((mergestatus & MERGE_SUCCESS) == 1)
                         {
                             nodeb->constraint = HALFSPACE_SURFACE_NODE; 
							 if(param->mobilityType == MOB_FCC_0) { 
                                 printf("Surface remesh is done for more than two {111} type glide constraints. \n");
							 } else if(param->mobilityType == MOB_BCC_GLIDE_0) {
						 	     printf("Surface remesh is done for more than two {110} or {112} or {123} type glide constraints. \n");
							 }
                         }
                     }
                 } 
                 else if( (numlinecon == 1) && (r < (home->param->minSeg)) )
                 {
                     if(linez_temp[0]!=0)
                     {
                         if(line_surfseg==1) // if a line constraint is a surface segment
                         {
                             MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                             if ((mergestatus & MERGE_SUCCESS) == 1) 
                             {
                                 nodeb->constraint = HALFSPACE_SURFACE_NODE;
                                 printf("Surface remesh is done for one line constraint. \n");
                             }

                         }
                         else if(line_surfseg==0) 
                         {   // if a line constraint is an inside segment
                             // there is a tolerance.
                             transnode = (z0 - nodeb->z) / linez_temp[0];
                             nodeb->x += transnode*linex_temp[0];
                             nodeb->y += transnode*liney_temp[0];
                             nodeb->z += transnode*linez_temp[0];
                             nodeb->constraint = HALFSPACE_SURFACE_NODE;
                             printf("Surface remesh is done for one line constraint. \n");
                             Fatal("WEIRD - DEBUG...");
                         }
                     }
                     else
                     {
                         MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                         if ((mergestatus & MERGE_SUCCESS) == 1)
                         {
                             nodeb->constraint = HALFSPACE_SURFACE_NODE; 
                             printf("Surface remesh is done for one line constraint by the simple merge. \n");
                         }
                     }
                 }
                 else if((numlinecon > 1) && (r < home->param->minSeg)) // Very unlucky case, but not frequent 
                 {
                     MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                     if ((mergestatus & MERGE_SUCCESS) == 1) 
                     {
                         nodeb->constraint = HALFSPACE_SURFACE_NODE;
                         printf("Surface remesh is done for two or more than two line constraints. \n");
                     }
                 }  

             }  else if (param->mobilityType == MOB_BCC_GLIDE) {
			     nc = nodeb->numNbrs ;  // nc : the number of neighbor nodes
     
			     /* copy glide plane constraints */
			     sn = 0;
			     for(p=0;p<nc;p++) {
			     	normX[p] = nodeb->nx[p];
			     	normY[p] = nodeb->ny[p];
			     	normZ[p] = nodeb->nz[p];
     
			         nbr=GetNeighborNode(home,nodeb,p);
			         if ((nbr->constraint == HALFSPACE_SURFACE_NODE) && (r > home->param->minSeg)) {
			         	surfnode_x[sn] = nbr->x;
			         	surfnode_y[sn] = nbr->y;
			         	surfnode_z[sn] = nbr->z;
			         	sn = sn + 1;
			         }
			     }

			     /* normalize glide plane normal vectors */
			     for(p=0;p<nc;p++) {
			     	a=sqrt(normX[p]*normX[p]+normY[p]*normY[p]+normZ[p]*normZ[p]);
     
			     	if(a>0) {
			     		normX[p]/=a;
			     		normY[p]/=a;
			     		normZ[p]/=a;
			     	}
			     }

			     /* Find independent glide constraints */
			     numglidecon = 0;
			     for(p=0;p<nc;p++) {
			     	nconstraint = 1;
			     	for(q=0;q<p;q++) {
			     		if(q<p) {
			     			normX_temp1 = normX[p]; normY_temp1 = normY[p]; normZ_temp1 = normZ[p];
			     			normX_temp2 = normX[q]; normY_temp2 = normY[q]; normZ_temp2 = normZ[q];
			     			Orthogonalize(&normX_temp1,&normY_temp1,&normZ_temp1,
			     			              normX_temp2,normY_temp2,normZ_temp2);
     
			     			#define FFACTOR_ORTH2 0.05
			     			if((normX_temp1*normX_temp1+normY_temp1*normY_temp1
			                     +normZ_temp1*normZ_temp1)<FFACTOR_ORTH2) {
			     				nconstraint = 0;
			     			}
			     		}
			     	}
     
			     	if(nconstraint>0) {
			     		if((normX[p]!=0)||(normY[p]!=0)||(normZ[p]!=0)) {
		                     if ( ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(normZ[i])<FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(normY[i])<FFACTOR_NORMAL2)) ||
                                  ((fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(normX[i])<FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 3*fabs(normX[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normX[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 3*fabs(normY[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normZ[i]) - 2*fabs(normY[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL2)) ||
                                  ((fabs(normX[i])>FFACTOR_NORMAL2) && (fabs(normY[i])>FFACTOR_NORMAL2) && (fabs(normZ[i])>FFACTOR_NORMAL2) && (fabs(fabs(normX[i]) - 3*fabs(normZ[i])) < FFACTOR_NORMAL2) && (fabs(fabs(normY[i]) - 2*fabs(normZ[i])) < FFACTOR_NORMAL2)) ) 
			     			{
			     				normx_temp[numglidecon] = normX[p];
			     				normy_temp[numglidecon] = normY[p];
			     				normz_temp[numglidecon] = normZ[p];
			     				// count the number of {110}, {112} or{123} type glide constraints
			     				numglidecon = numglidecon + 1;
			     			}
			     		}
			     	}
			     }

                 z0 = param->hs_Lzinf;
                 if (nodeb->z<0) {
                     z0 = (-1)*z0;
                 }

                 if((numglidecon == 1) && (r < home->param->minSeg))
                 {
                     printf("FindInnerNode: nodea_surf = node(%d,%d)\n", nodea->myTag.domainID,nodea->myTag.index); 
                     printf("FindInnerNode: nodeb_inner = node(%d,%d)\n", nodeb->myTag.domainID,nodeb->myTag.index); 
                     MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                     if ((mergestatus & MERGE_SUCCESS) == 1)
                     {
                         nodeb->constraint = HALFSPACE_SURFACE_NODE;
			 			 printf("Surface remesh is done for one {110} or {112} or {123} type glide constraint in BCC material. \n");
                     }
                 }
                 else if(numglidecon == 2)
                 {
                     if(r < home->param->minSeg)
                     {
                         xvector(normx_temp[0], normy_temp[0], normz_temp[0], normx_temp[1], normy_temp[1],
                                 normz_temp[1], &intersec1, &intersec2, &intersec3) 
                         intersec_mag = intersec1*intersec1+intersec2*intersec2+intersec3*intersec3;
                         if((intersec_mag!=0) && (intersec3!=0))
                         {
                             intersec1 = intersec1/intersec_mag;
                             intersec2 = intersec2/intersec_mag;
                             intersec3 = intersec3/intersec_mag;
#if 0
                             transnode = (z0 - nodeb->z) / intersec3;
                             nodeb->x += transnode*intersec1;
                             nodeb->y += transnode*intersec2;
                             nodeb->z += transnode*intersec3;
                             nodeb->constraint = HALFSPACE_SURFACE_NODE; 
                             printf("Surface remesh is done for two {111} type glide constraints. \n"); 
#endif
                         }
                         else 
                         {
                             MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                             if ((mergestatus & MERGE_SUCCESS) == 1)
                             {
                                 nodeb->constraint = HALFSPACE_SURFACE_NODE; 
								 printf("Surface remesh is done for two {110} or {112} or {123} type glide constraints by the simple merge. \n");
                                 Fatal("WEIRD - DEBUG...");
                             }
                         }
                     }
                     else if((r > home->param->minSeg) && (sn == 2)) // special case (mimic surface node collision)
                     {
                         surf2_x = (surfnode_x[0]-surfnode_x[1])*(surfnode_x[0]-surfnode_x[1]);
                         surf2_y = (surfnode_y[0]-surfnode_y[1])*(surfnode_y[0]-surfnode_y[1]);
                         surf2_z = (surfnode_z[0]-surfnode_z[1])*(surfnode_z[0]-surfnode_z[1]);
                         surf_mag = sqrt(surf2_x + surf2_y + surf2_z);
                         if(surf_mag < (home->param->rann)) // when two surface nodes are very close.
                         {
                             xvector(normx_temp[0], normy_temp[0], normz_temp[0], normx_temp[1], normy_temp[1],
                                     normz_temp[1], &intersec1, &intersec2, &intersec3);
                             intersec_mag = intersec1*intersec1+intersec2*intersec2+intersec3*intersec3;
                             if((intersec_mag!=0) && (intersec3!=0))
                             {
                                 intersec1 = intersec1/intersec_mag;
                                 intersec2 = intersec2/intersec_mag;
                                 intersec3 = intersec3/intersec_mag;
                                 transnode = (z0 - nodeb->z) / intersec3;
                                 nodeb->x += transnode*intersec1;
                                 nodeb->y += transnode*intersec2;
                                 nodeb->z += transnode*intersec3;
                                 nodeb->constraint = HALFSPACE_SURFACE_NODE;
								 printf("Surface remesh is done for two {110} or {112} or {123} type glide constraints. \n");
                             }
                             else 
                             {
                                 MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                                 if ((mergestatus & MERGE_SUCCESS) == 1)
                                 {
                                     nodeb->constraint = HALFSPACE_SURFACE_NODE;
								     printf("Surface remesh is done for two {110} or {112} or {123} type glide constraints by the simple merge. \n");
                                 }
                             }
                         } 
                     }
                 }
                 else if((numglidecon > 2) && (r < home->param->minSeg)) // Very unlucky case, but not frequent
                 {
                     MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                     if ((mergestatus & MERGE_SUCCESS) == 1)
                     {
                         nodeb->constraint = HALFSPACE_SURFACE_NODE; 
					 	 printf("Surface remesh is done for more than two {110} or {112} or {123} type glide constraints. \n");
                     }
                 }
			 }
             else // if not FCC_0, BCC_Glide and BCC_Glide_0
             {
                 MergeNode(home, OPCLASS_REMESH, nodea, nodeb, position, &mergedNode, &mergestatus, 1);
                 if ((mergestatus & MERGE_SUCCESS) == 1) nodeb->constraint = HALFSPACE_SURFACE_NODE;
             } 
         }
      }

  }
  
#endif


  /* 
   * Treat nodes constrained to 7 and their interactions with the halfspace surface
   * Three cases: 
   *    - a segment with one node inside flagged 0 and one outside flagged 7
   *    - a segment with one node inside flagged 7 and one oustide flagged 0
   *    - a segment with two nodes flaged 7 is outside the halfspace
   * The following does not treat the case of a segment with one node 7 outside and a node 7 inside.
   */

  int dirty7 = 1;
  
  while (dirty7) 
    {
      dirty7 = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;

	  // Node A is outside and is fixed (7)
	  if (nodea->constraint != PINNED_NODE) continue;
	  if (nodea->z > t)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);
		  
		  // Node b is inside 
		  if (nodeb->constraint == UNCONSTRAINED) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodea->myTag))
			{
			  if (iprint) printf("(7) Domain %d Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				 nodea->myTag.domainID, nodea->myTag.index,
				 nodeb->myTag.domainID, nodeb->myTag.index);
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodea,nodeb,t);
			  if (splitstatus == SPLIT_SUCCESS) dirty7 = 1;
			}
		    }
		  if (dirty7) break;
		} // for j
	      if (dirty7) break;
	    }
	}// for i
    }//while

  dirty7 = 1;
  
  while (dirty7) 
    {
      dirty7 = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;
	  
	  // Node A is inside
	  if (nodea->constraint == UNCONSTRAINED)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);

		  // Node b is outside and is fixed (7)
		  if (nodeb->constraint != PINNED_NODE) continue;
		  if (nodeb->z > t) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
			{		      
			  if (iprint) printf("(7) Domain %d Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				 nodea->myTag.domainID, nodea->myTag.index,
				 nodeb->myTag.domainID, nodeb->myTag.index);
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodeb,nodea,t);
			  if (splitstatus == SPLIT_SUCCESS) dirty7 = 1;
			}
		    }
		  if (dirty7) break;
		} // for j
	      if (dirty7) break;
	    }
	}// for i
    }//while


  dirty7 = 1;
  
  while (dirty7) 
    {
      dirty7 = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;

	  // Node A is outside and is fixed (7)
	  if (nodea->constraint != PINNED_NODE) continue;
	  if (nodea->z > t)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);
		  
		  // Node b is inside 
		  if (nodeb->constraint != PINNED_NODE) continue;
		  if (nodeb->z < t)
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodea->myTag))
			{
			  if (iprint) printf("(7) Domain %d Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				 nodea->myTag.domainID, nodea->myTag.index,
				 nodeb->myTag.domainID, nodeb->myTag.index);
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodea,nodeb,t);
			  if (splitstatus == SPLIT_SUCCESS) dirty7 = 1;
			}
		    }
		  if (dirty7) break;
		} // for j
	      if (dirty7) break;
	    }
	}// for i
    }//while

  dirty7 = 1;
  
  while (dirty7) 
    {
      dirty7 = 0;
      
      for (i = 0; i < home->newNodeKeyPtr; i++) 
	{
	  nodea = home->nodeKeys[i];
	  if (nodea == (Node_t *)NULL) continue;
	  
	  // Node A is inside
	  if (nodea->constraint == PINNED_NODE && nodea->z < t)
	    {
	      for (j = 0; j < nodea->numNbrs; j++) 
		{
		  nodeb = GetNeighborNode(home, nodea, j);

		  // Node b is outside and is fixed (7)
		  if (nodeb->constraint == PINNED_NODE) continue;
		  if (nodeb->z > t) 
		    {
		      if (DomainOwnsSeg(home, OPCLASS_REMESH, thisDomain, &nodeb->myTag))
			{		      
			  if (iprint) printf("(7) Domain %d Splitting nodes (%d,%d)-(%d,%d)\n",thisDomain,
				 nodea->myTag.domainID, nodea->myTag.index,
				 nodeb->myTag.domainID, nodeb->myTag.index);
			  // Nodea is outside. Nodeb is inside
			  // Find the node in between nodea and nodeb and split the segment
			  splitstatus = Split(home,nodeb,nodea,t);
			  if (splitstatus == SPLIT_SUCCESS) dirty7 = 1;
			}
		    }
		  if (dirty7) break;
		} // for j
	      if (dirty7) break;
	    }
	}// for i
    }//while



#ifdef _SURFACE_STEPS

/* 
 * 	Handle segments that are both surface nodes
 * 	These could have been created during remesh
 * 	These segments will be removed from the simulation,
 * 	we need to make sure their surface step is stored 
 */
	
	/* Flag surface nodes */
	iSurfNodes = 0;
	surfNodesFlag = malloc(home->newNodeKeyPtr*sizeof(int));
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		surfNodesFlag[i] = 0;
		if ((nodea = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		if (IsSurfaceNode(nodea, t)) {
			surfNodesFlag[i] = 1;
			iSurfNodes++;
		}
	}
	
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if (surfNodesFlag[i] == 0) continue;
		nodea = home->nodeKeys[i];
		findSeg = -1;
		for (j = 0; j < nodea->numNbrs; j++) {
			nodeb = GetNeighborNode(home, nodea, j);
			if (surfNodesFlag[nodeb->myTag.index] == 1) {
				findSeg = j;
				break;
			}
		}
		if (findSeg == -1) continue;
		
		if (iprint) printf("--create virtual surface segment %d-%d\n", nodea->myTag.index, nodeb->myTag.index);
		
		/* Create surface segment */
		newSurfSeg = CreateNewSurfaceSegment(halfspace);
		surfStepSegList = halfspace->surfStepSegList;
					
		surfStepSegList[newSurfSeg].virtual1 = 1;
		surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
		
		surfStepSegList[newSurfSeg].pos1[0] = nodea->x;
		surfStepSegList[newSurfSeg].pos1[1] = nodea->y;
		surfStepSegList[newSurfSeg].pos1[2] = nodea->z;
		
		surfStepSegList[newSurfSeg].virtual2 = 1;
		surfStepSegList[newSurfSeg].node2 = (Node_t *)NULL;
		
		surfStepSegList[newSurfSeg].pos2[0] = nodeb->x;
		surfStepSegList[newSurfSeg].pos2[1] = nodeb->y;
		surfStepSegList[newSurfSeg].pos2[2] = nodeb->z;
		
		surfStepSegList[newSurfSeg].burg[0] = nodea->burgX[findSeg];
		surfStepSegList[newSurfSeg].burg[1] = nodea->burgY[findSeg];
		surfStepSegList[newSurfSeg].burg[2] = nodea->burgZ[findSeg];
	
		surfStepSegList[newSurfSeg].norm[0] = nodea->nx[findSeg];
		surfStepSegList[newSurfSeg].norm[1] = nodea->ny[findSeg];
		surfStepSegList[newSurfSeg].norm[2] = nodea->nz[findSeg];
		
		surfNodesFlag[nodea->myTag.index] = 0;
		surfNodesFlag[nodeb->myTag.index] = 0;
		
	}
	free(surfNodesFlag);
	
	
/* 
 * 	Update surface segments nodes positon and make sure surface 
 *  segments are consistent: if the plane at end nodes of surface 
 * 	segments have changed, we need to insert a new surface
 * 	segment with the new slip plane 
 */
	 
	nsurfSeg = halfspace->surfStepSegCount;
	for (i = 0; i < nsurfSeg; i++) {
		
		if (surfStepSegList[i].virtual1 == 0) {
			/* Update nodes position */
			nodea = surfStepSegList[i].node1;
			
			if (IsSurfaceNode(nodea, t)) {
				surfStepSegList[i].pos1[0] = nodea->x;
				surfStepSegList[i].pos1[1] = nodea->y;
				surfStepSegList[i].pos1[2] = nodea->z;
			}
			
			/* Check if surface node has been deleted
			 * during a collision or remeshing event */
			if (nodea->numNbrs == 0 || !IsSurfaceNode(nodea, t)) {
				/* Close this segment */
				surfStepSegList[i].virtual1 = 1;
				surfStepSegList[i].node1 = (Node_t *)NULL;
			}
			
			/* Check surface segment is consistent */
			if (nodea->numNbrs == 1) {
				
				nodeb = GetNeighborNode(home, nodea, 0);
				if (IsSurfaceNode(nodeb, t)) continue;
				
				glid1[0] = surfStepSegList[i].norm[0];
				glid1[1] = surfStepSegList[i].norm[1];
				glid1[2] = surfStepSegList[i].norm[2];
				
				glid2[0] = nodea->nx[0];
				glid2[1] = nodea->ny[0];
				glid2[2] = nodea->nz[0];
				
				n1n2 = glid1[0]*glid2[0] + glid1[1]*glid2[1] + glid1[2]*glid2[2];
				if (fabs(fabs(n1n2)-1.0) > 1.0e-5) {
					
					if (iprint) printf("--open new surface segment\n");
					
					/* Close this segment */
					surfStepSegList[i].virtual1 = 1;
					surfStepSegList[i].node1 = (Node_t *)NULL;
					
					/* Open new segment */
					newSurfSeg = CreateNewSurfaceSegment(halfspace);
					surfStepSegList = halfspace->surfStepSegList;
					
					surfStepSegList[newSurfSeg].virtual1 = 0;
					surfStepSegList[newSurfSeg].node1 = nodea;
					surfStepSegList[newSurfSeg].pos1[0] = nodea->x;
					surfStepSegList[newSurfSeg].pos1[1] = nodea->y;
					surfStepSegList[newSurfSeg].pos1[2] = nodea->z;
					
					surfStepSegList[newSurfSeg].virtual2 = 1;
					surfStepSegList[newSurfSeg].node2 = (Node_t *)NULL;
					surfStepSegList[newSurfSeg].pos2[0] = nodea->x;
					surfStepSegList[newSurfSeg].pos2[1] = nodea->y;
					surfStepSegList[newSurfSeg].pos2[2] = nodea->z;
					
					surfStepSegList[newSurfSeg].burg[0] = surfStepSegList[i].burg[0];
					surfStepSegList[newSurfSeg].burg[1] = surfStepSegList[i].burg[1];
					surfStepSegList[newSurfSeg].burg[2] = surfStepSegList[i].burg[2];
					
					surfStepSegList[newSurfSeg].norm[0] = glid2[0];
					surfStepSegList[newSurfSeg].norm[1] = glid2[1];
					surfStepSegList[newSurfSeg].norm[2] = glid2[2];
					
				}
			}
		}
		
		if (surfStepSegList[i].virtual2 == 0) {
			/* Update nodes position */
			nodeb = surfStepSegList[i].node2;
			
			if (IsSurfaceNode(nodeb, t)) {
				surfStepSegList[i].pos2[0] = nodeb->x;
				surfStepSegList[i].pos2[1] = nodeb->y;
				surfStepSegList[i].pos2[2] = nodeb->z;
			}
			
			/* Check if surface node has been deleted
			 * during a collision or remeshing event */
			if (nodeb->numNbrs == 0 || !IsSurfaceNode(nodeb, t)) {
				/* Close this segment */
				surfStepSegList[i].virtual2 = 1;
				surfStepSegList[i].node2 = (Node_t *)NULL;
			}
			
			/* Check surface segment is consistent */
			if (nodeb->numNbrs == 1) {
				
				nodea = GetNeighborNode(home, nodeb, 0);
				if (IsSurfaceNode(nodea, t)) continue;
				
				glid1[0] = surfStepSegList[i].norm[0];
				glid1[1] = surfStepSegList[i].norm[1];
				glid1[2] = surfStepSegList[i].norm[2];
				
				glid2[0] = nodeb->nx[0];
				glid2[1] = nodeb->ny[0];
				glid2[2] = nodeb->nz[0];
				
				n1n2 = glid1[0]*glid2[0] + glid1[1]*glid2[1] + glid1[2]*glid2[2];
				if (fabs(fabs(n1n2)-1.0) > 1.0e-5) {
					
					if (iprint) printf("--open new surface segment\n");
					
					/* Close this segment */
					surfStepSegList[i].virtual2 = 1;
					surfStepSegList[i].node2 = (Node_t *)NULL;
					
					/* Open new segment */
					newSurfSeg = CreateNewSurfaceSegment(halfspace);
					surfStepSegList = halfspace->surfStepSegList;
					
					surfStepSegList[newSurfSeg].virtual1 = 1;
					surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
					surfStepSegList[newSurfSeg].pos1[0] = nodeb->x;
					surfStepSegList[newSurfSeg].pos1[1] = nodeb->y;
					surfStepSegList[newSurfSeg].pos1[2] = nodeb->z;
					
					surfStepSegList[newSurfSeg].virtual2 = 0;
					surfStepSegList[newSurfSeg].node2 = nodeb;
					surfStepSegList[newSurfSeg].pos2[0] = nodeb->x;
					surfStepSegList[newSurfSeg].pos2[1] = nodeb->y;
					surfStepSegList[newSurfSeg].pos2[2] = nodeb->z;
					
					surfStepSegList[newSurfSeg].burg[0] = surfStepSegList[i].burg[0];
					surfStepSegList[newSurfSeg].burg[1] = surfStepSegList[i].burg[1];
					surfStepSegList[newSurfSeg].burg[2] = surfStepSegList[i].burg[2];
					
					surfStepSegList[newSurfSeg].norm[0] = glid2[0];
					surfStepSegList[newSurfSeg].norm[1] = glid2[1];
					surfStepSegList[newSurfSeg].norm[2] = glid2[2];
					
				}
			}
		}
	
	}

/* 	
 * 	Remesh surface segment: split segments that are longer 
 *  than half of the minimum volume size to preserve PBCs
 */
	if(param->mobilityType == MOB_FCC_0 || param->mobilityType == MOB_BCC_GLIDE_0 || param->mobilityType == MOB_BCC_GLIDE) {
		minSurfSegL = 0.3*MIN(param->Lx, param->Ly);
	} else if (param->mobilityType == MOB_BCC_0B) {
		minSurfSegL = param->maxSeg;
	} 
	
	nsurfSeg = halfspace->surfStepSegCount;
	for (i = 0; i < nsurfSeg; i++) {
		
		position[0] = surfStepSegList[i].pos1[0];
		position[1] = surfStepSegList[i].pos1[1];
		position[2] = surfStepSegList[i].pos1[2];
		
		xB = surfStepSegList[i].pos2[0];
		yB = surfStepSegList[i].pos2[1];
		zB = surfStepSegList[i].pos2[2];
		
		PBCPOSITION(param, position[0], position[1], position[2], &xB, &yB, &zB);
		r = sqrt( (xB-position[0])*(xB-position[0]) + 
		          (yB-position[1])*(yB-position[1]) + 
		          (zB-position[2])*(zB-position[2]) );
		
		/* Remesh long segments */
		if (r > minSurfSegL) {
			
			if (iprint) printf("--split surface segment %d\n",i);
			
			/* Calculate segment midpoint */
			xB = 0.5*(position[0] + xB);
			yB = 0.5*(position[1] + yB);
			zB = 0.5*(position[2] + zB);
			FoldBox(param, &xB, &yB, &zB);
			
			/* Open new segment */
			newSurfSeg = CreateNewSurfaceSegment(halfspace);
			surfStepSegList = halfspace->surfStepSegList;
			
			surfStepSegList[newSurfSeg].virtual1 = 1;
			surfStepSegList[newSurfSeg].node1 = (Node_t *)NULL;
			surfStepSegList[newSurfSeg].pos1[0] = xB;
			surfStepSegList[newSurfSeg].pos1[1] = yB;
			surfStepSegList[newSurfSeg].pos1[2] = zB;
			
			surfStepSegList[newSurfSeg].virtual2 = surfStepSegList[i].virtual2;
			surfStepSegList[newSurfSeg].node2 = surfStepSegList[i].node2;
			surfStepSegList[newSurfSeg].pos2[0] = surfStepSegList[i].pos2[0];
			surfStepSegList[newSurfSeg].pos2[1] = surfStepSegList[i].pos2[1];
			surfStepSegList[newSurfSeg].pos2[2] = surfStepSegList[i].pos2[2];
			
			surfStepSegList[newSurfSeg].burg[0] = surfStepSegList[i].burg[0];
			surfStepSegList[newSurfSeg].burg[1] = surfStepSegList[i].burg[1];
			surfStepSegList[newSurfSeg].burg[2] = surfStepSegList[i].burg[2];
			
			surfStepSegList[newSurfSeg].norm[0] = surfStepSegList[i].norm[0];
			surfStepSegList[newSurfSeg].norm[1] = surfStepSegList[i].norm[1];
			surfStepSegList[newSurfSeg].norm[2] = surfStepSegList[i].norm[2];
			
			/* Close the original segment */
			surfStepSegList[i].virtual2 = 1;
			surfStepSegList[i].node2 = (Node_t *)NULL;
			surfStepSegList[i].pos2[0] = xB;
			surfStepSegList[i].pos2[1] = yB;
			surfStepSegList[i].pos2[2] = zB;
			
			//exit(0);
		}
	}

#endif


  /* 
   * Remove segments with 
   *    - all both nodes flagged 7, 
   *    - both nodes flagged 6,
   *    - a node flagged 6 and the other one flagged 7.
   */

#ifdef _SURFACE_STEPS
  RemoveSurfaceSegments67(home,halfspace,t);
#else
  RemoveSurfaceSegments67(home,t);
#endif


#if 0
/*	
 * 	Free surface nodes whose Burgers vector is conserved 
 */
	double burgSumX, burgSumY, burgSumZ;

	/* Flag surface nodes */
	iSurfNodes = 0;
	surfNodesFlag = malloc(home->newNodeKeyPtr*sizeof(int));
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		surfNodesFlag[i] = 0;
		if ((nodea = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		if (IsSurfaceNode(nodea, t)) {
			surfNodesFlag[i] = 1;
			iSurfNodes++;
		}
	}
	
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if (surfNodesFlag[i] == 0) continue;
		nodea = home->nodeKeys[i];
		
		/* Compute net Burgers vector */
		burgSumX = 0.0;
		burgSumY = 0.0;
		burgSumZ = 0.0;
		for (j = 0; j < nodea->numNbrs; j++) {
			burgSumX += nodea->burgX[j];
			burgSumY += nodea->burgY[j];
			burgSumZ += nodea->burgZ[j];
		}
		if ((fabs(burgSumX) < 1.e-4) &&
			(fabs(burgSumY) < 1.e-4) &&
			(fabs(burgSumZ) < 1.e-4)) {
			nodea->constraint = UNCONSTRAINED;
			
			/* look in surface segment list */
			for (j = 0; j < halfspace->surfStepSegCount; j++) {
				if (surfStepSegList[j].virtual1 == 0) {
					if (surfStepSegList[j].node1->myTag.index == nodea->myTag.index) {
						surfStepSegList[j].virtual1 = 1;
						surfStepSegList[j].node1 = (Node_t *)NULL;
						if (iprint) printf("-virtual1 node %d\n",nodea->myTag.index);
					}
				}
				if (surfStepSegList[j].virtual2 == 0) {
					if (surfStepSegList[j].node2->myTag.index == nodea->myTag.index) {
						surfStepSegList[j].virtual2 = 1;
						surfStepSegList[j].node2 = (Node_t *)NULL;
						if (iprint) printf("-virtual2 node %d\n",nodea->myTag.index);
					}
				}
			}
			
			if (iprint) printf("Free surface node %d\n",nodea->myTag.index);
		}
	}
#endif


#ifdef _SURFACE_STEPS
/*	
 * 	Check that all surface nodes are actually on the surface
 */
	for (i = 0; i < halfspace->surfStepSegCount; i++) {
		if (fabs(surfStepSegList[i].pos1[2]-t) > 1.e-5) {
			if (surfStepSegList[i].virtual1 == 1) {
				printf("surfSeg = %d, node1 = %d, z = %e\n",i,
				-1,surfStepSegList[i].pos1[2]);
			} else {
				printf("surfSeg = %d, node1 = %d, z = %e\n",i,
				surfStepSegList[i].node1->myTag.index,surfStepSegList[i].pos1[2]);
			}
			Fatal("Surface segment node is not on the surface");
		}
		if (fabs(surfStepSegList[i].pos2[2]-t) > 1.e-5) {
			if (surfStepSegList[i].virtual2 == 1) {
				printf("surfSeg = %d, node2 = %d, z = %e\n",i,
				-1,surfStepSegList[i].pos2[2]);
			} else {
				printf("surfSeg = %d, node2 = %d, z = %e\n",i,
				surfStepSegList[i].node2->myTag.index,surfStepSegList[i].pos2[2]);
			}
			Fatal("Surface segment node is not on the surface");
		}
	}
#endif

}

#ifdef _INDENTATION
/*-------------------------------------------------------------------------
 *
 *      Function:    AdjustNodePlane
 *
 *------------------------------------------------------------------------*/
void AdjustNodePlane(Home_t *home, Node_t *node, double p[3], double n[3], double maxDist)
{
	double  dp, vec[3];
	Param_t *param;
	
	param = home->param;
	double  eps = 1.e-5;
	
	vec[0] = node->x - p[0];
	vec[1] = node->y - p[1];
	vec[2] = node->z - p[2];

	dp = vec[0]*n[0] + vec[1]*n[1] + vec[2]*n[2];
	
	/* Node has crossed the boundary */
	if (dp < 0.0) {
				
		int ndeg;
		double dir[3], st, w[3], N, D;

		if(param->mobilityType == MOB_FCC_0) {
		    FCCNodeGlideConstraints(home, node, &ndeg, dir); //the ndeg value returns from FCCNodeGlideConstraints()
		} else if (param->mobilityType == MOB_BCC_0B) {
			ndeg = 2;
		} else if (param->mobilityType == MOB_BCC_GLIDE) {
			BCCNodeGlideConstraints(home, node, &ndeg, dir); //the ndeg value returns from BCCNodeGlideConstraints()
		} else if (param->mobilityType == MOB_BCC_GLIDE_0) {
			BCCNodeGlideConstraints_0(home, node, &ndeg, dir); //the ndeg value returns from BCCNodeGlideConstraints_0()
		}

		if (ndeg == 1 || ndeg == 2) {
		
			if (ndeg == 2) {
				dir[0] = node->vX;
				dir[1] = node->vY;
				dir[2] = node->vZ;
			}
			
			/* Project node on GB plane along glide directions */
			NormalizeVec(dir);
			D = param->normalGB1[0]*dir[0] + 
				param->normalGB1[1]*dir[1] + 
				param->normalGB1[2]*dir[2];
			if (fabs(D) < eps) return;
			st = -dp/D;
			
			if (fabs(st) > 2.0*maxDist) return;

			node->x += st*dir[0];
			node->y += st*dir[1];
			node->z += st*dir[2];
		}
	}
		
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:    AdjustNodesBoundaries
 *      Description: Handle node collision with cell (e.g. faces and bottom) 
 *                   limits and grain boundaries planes.
 *
 *------------------------------------------------------------------------*/
void AdjustNodesBoundaries(Home_t *home, HalfSpace_t *halfspace)
{
	int i;
	Node_t  *node;
	Param_t *param;
	
	param = home->param;
	
	double  eps = 1.e-5;
	
/* 	
 *  Max travel distance
 */
	double drx, dry, drz, drlen;
	double maxDist = 0.0;
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		drx = node->x - node->olderx;
		dry = node->y - node->oldery;
		drz = node->z - node->olderz;
        ZImage(param, &drx, &dry, &drz);
        drlen = sqrt(drx*drx+dry*dry+drz*drz);
        maxDist = MAX(drlen, maxDist);
	}

/* 	
 * 	Adjust nodes that have crossed the GBs
 */
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
		if (node->constraint == PINNED_NODE) continue;
		if (node->constraint == HALFSPACE_SURFACE_NODE) continue;
		
		/* Simulation bottom */
		if (node->z <= -param->hs_Lzinf) {
			double p[3], n[3];
			
			p[0] = 0.0;
			p[1] = 0.0;
			p[2] = -param->hs_Lzinf;
			
			n[0] = 0.0;
			n[1] = 0.0;
			n[2] = 1.0;
			
			AdjustNodePlane(home, node, p, n, param->hs_Lzinf);
		}
		
		/* Grain boundary 1 */
		if (param->planeGB1 == 1) {
			AdjustNodePlane(home, node, param->pointGB1, param->normalGB1, maxDist);
		}
		
		/* Grain boundary 2 */
		if (param->planeGB2 == 1) {
			AdjustNodePlane(home, node, param->pointGB2, param->normalGB2, maxDist);
		}
	}
	
	return;
}
#endif


#undef iprint

#endif
