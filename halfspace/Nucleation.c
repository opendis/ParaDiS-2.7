
#include "Home.h"
#include "sys/stat.h"
#include "sys/types.h"
#include "sys/time.h"
#include "HS.h"

#define DISLOCATION_LINE 0
#define DISLOCATION_LOOP 1

/*---------------------------------------------------------------------------
 *
 *      Function:     RandomAtMost
 *                    Returns a random integer in the range [0,max]
 *
 *-------------------------------------------------------------------------*/
static long RandomAtMost(long max)
{
		unsigned long
		// max <= RAND_MAX < ULONG_MAX, so this is okay.
		num_bins = (unsigned long) max + 1,
		num_rand = (unsigned long) RAND_MAX + 1,
		bin_size = num_rand / num_bins,
		defect   = num_rand % num_bins;

		long x;
		do {
			x = random();
		}
		// This is carefully written not to overflow
		while (num_rand - defect <= (unsigned long)x);

		// Truncated division is intentional
		return x/bin_size;
}


/*---------------------------------------------------------------------------
 *
 *      Function:     RandomSlipSystem
 *                    Returns Burgers and normal vectors corresponding to
 *                    a slip system randomly chosen
 *
 *-------------------------------------------------------------------------*/
void RandomSlipSystem(Home_t *home, real8 *bx, real8 *by, real8 *bz, 
                      real8 *nx, real8 *ny, real8 *nz) {

		int      i;
		real8    bur[3], nor[3];
		Param_t  *param;
		
		param = home->param;
		
		// Seed random number generator
		struct timeval t1;
		gettimeofday(&t1, NULL);
		srand(t1.tv_usec * t1.tv_sec);
		
		
		if (param->materialType == MAT_TYPE_FCC) {
			
			int    numSys, sn[12], sb[12], sys;
			real8  n[4][3], b[6][3];
		
			// Slip planes
			n[0][0] = -1.0; n[0][1] =  1.0; n[0][2] =  1.0; // Slip plane A(0)
			n[1][0] =  1.0; n[1][1] =  1.0; n[1][2] =  1.0; // Slip plane B(1)
			n[2][0] = -1.0; n[2][1] = -1.0; n[2][2] =  1.0; // Slip plane C(2)
			n[3][0] =  1.0; n[3][1] = -1.0; n[3][2] =  1.0; // Slip plane D(3)
			// Burgers
			b[0][0] =  0.0; b[0][1] =  1.0; b[0][2] =  1.0; // Burgers 1(0)
			b[1][0] =  0.0; b[1][1] = -1.0; b[1][2] =  1.0; // Burgers 2(1)
			b[2][0] =  1.0; b[2][1] =  0.0; b[2][2] =  1.0; // Burgers 3(2)
			b[3][0] = -1.0; b[3][1] =  0.0; b[3][2] =  1.0; // Burgers 4(3)
			b[4][0] = -1.0; b[4][1] =  1.0; b[4][2] =  0.0; // Burgers 5(4)
			b[5][0] =  1.0; b[5][1] =  1.0; b[5][2] =  0.0; // Burgers 6(5)

			// Slip systems
			numSys = 12;
			
			sn[0] = 0; sb[0] = 1; // A2 (0)
			sn[1] = 0; sb[1] = 2; // A3 (1)
			sn[2] = 0; sb[2] = 5; // A6 (2)

			sn[3] = 1; sb[3] = 1; // B2 (3)
			sn[4] = 1; sb[4] = 3; // B4 (4)
			sn[5] = 1; sb[5] = 4; // B5 (5)

			sn[6] = 2; sb[6] = 0; // C1 (6)
			sn[7] = 2; sb[7] = 2; // C3 (7)
			sn[8] = 2; sb[8] = 4; // C5 (8)

			sn[9] = 3; sb[9] = 0; // D1 (9)
			sn[10] = 3; sb[10] = 3; // D4 (10)
			sn[11] = 3; sb[11] = 5; // D6 (11)
			
			// Randomly select slip system
			sys = RandomAtMost(numSys-1);
			
			for (i = 0; i < 3; i++) {
				bur[i] = b[sb[sys]][i];
				nor[i] = n[sn[sys]][i];
			}	
				
		} else if (param->materialType == MAT_TYPE_BCC) {
				Fatal("RandomSlipSystem is not defined for BCC materials");
		} else {
			Fatal("RandomSlipSystem is not defined for this material type");
		}
		
		// Normalize vectors
		NormalizeVec(bur);
		NormalizeVec(nor);
		
		*bx = bur[0];
		*by = bur[1];
		*bz = bur[2];
		
		*nx = nor[0];
		*ny = nor[1];
		*nz = nor[2];
		
		return;
}


/*---------------------------------------------------------------------------
 *
 *      Function:     NucleateDislocation
 *
 *-------------------------------------------------------------------------*/
void NucleateDislocation(Home_t *home, int type, int numNodes, 
                         real8 *px, real8 *py, real8 *pz, 
                         real8 bx, real8 by, real8 bz, 
                         real8 nx, real8 ny, real8 nz)
{
		int     i, n1, n2, numSegs;
		Node_t	**newNodes;
		Node_t	*node1, *node2;
		
		
		// Insert new nodes
		newNodes = malloc(numNodes*sizeof(Node_t*));
		
		for (i = 0; i < numNodes; i++) {
			
			newNodes[i] = GetNewNativeNode(home);
			node1 = newNodes[i];
			FreeNodeArms(node1);
			node1->native = 1;
			
			if (type == DISLOCATION_LINE && 
			    (i == 0 || i == numNodes-1)) {
				node1->constraint = PINNED_NODE;
			} else {
				node1->constraint = UNCONSTRAINED;
			}

			node1->x = px[i];
			node1->y = py[i];
			node1->z = pz[i];
		}
		
		
		// Connect nodes to form the dislocation
		if (type == DISLOCATION_LINE) numSegs = numNodes-1;
		else numSegs = numNodes;
		
		for (i = 0; i < numSegs; i++) {
			
			n1 = i;
			n2 = (i==numNodes-1)?0:(i+1);
			node1 = newNodes[n1];
			node2 = newNodes[n2];
			
			InsertArm(home, node1, &node2->myTag,
			          -bx, -by, -bz, nx, ny, nz, 0);
			          
			InsertArm(home, node2, &node1->myTag,
			          bx, by, bz, nx, ny, nz, 0);
		}
		
		free(newNodes);
		
		return;
}

void NucleateDislocation2(Home_t *home, int type, int numNodes, 
                         real8 *px, real8 *py, real8 *pz, 
                         real8 bx, real8 by, real8 bz, 
                         real8 nx, real8 ny, real8 nz, Node_t **newNodes)
{
		int     i, n1, n2, numSegs;
		Node_t	*node1, *node2;
		
		for (i = 0; i < numNodes; i++) {
			
			newNodes[i] = GetNewNativeNode(home);
			node1 = newNodes[i];
			FreeNodeArms(node1);
			node1->native = 1;
			
			if (type == DISLOCATION_LINE && 
			    (i == 0 || i == numNodes-1)) {
				node1->constraint = PINNED_NODE;
			} else {
				node1->constraint = UNCONSTRAINED;
			}

			node1->x = px[i];
			node1->y = py[i];
			node1->z = pz[i];
		}
		
		
		// Connect nodes to form the dislocation
		if (type == DISLOCATION_LINE) numSegs = numNodes-1;
		else numSegs = numNodes;
		
		for (i = 0; i < numSegs; i++) {
			
			n1 = i;
			n2 = (i==numNodes-1)?0:(i+1);
			node1 = newNodes[n1];
			node2 = newNodes[n2];
			
			InsertArm(home, node1, &node2->myTag,
			          -bx, -by, -bz, nx, ny, nz, 0);
			          
			InsertArm(home, node2, &node1->myTag,
			          bx, by, bz, nx, ny, nz, 0);
		}
		
		return;
}


/*---------------------------------------------------------------------------
 *
 *      Function:     NucleatePrismaticLoop
 *
 *-------------------------------------------------------------------------*/
void NucleatePrismaticLoop(Home_t *home, real8 *px, real8 *py, real8 *pz, 
                           real8 bx, real8 by, real8 bz, 
                           real8 n1x, real8 n1y, real8 n1z,
                           real8 n2x, real8 n2y, real8 n2z)
{
		int     i, n1, n2, numSegs;
		Node_t	**newNodes;
		Node_t	*node1, *node2;
		
		
		// Insert new nodes
		newNodes = malloc(4*sizeof(Node_t*));
		
		for (i = 0; i < 4; i++) {
			
			//newNodes[i] = GetNewNativeNode(home);
			//node1 = newNodes[i];
			node1 = GetNewNativeNode(home);
			newNodes[i] = node1;
			FreeNodeArms(node1);
			node1->native = 1;
			node1->constraint = UNCONSTRAINED;

			node1->x = px[i];
			node1->y = py[i];
			node1->z = pz[i];
			
		}
		
		
		// Connect nodes to form the dislocation loop
		for (i = 0; i < 4; i++) {
			
			n1 = i;
			n2 = (i==3)?0:(i+1);
			node1 = newNodes[n1];
			node2 = newNodes[n2];
			
			if (i % 2 == 0) {
			
				InsertArm(home, node1, &node2->myTag,
				          -bx, -by, -bz, n1x, n1y, n1z, 0);
			          
				InsertArm(home, node2, &node1->myTag,
				          bx, by, bz, n1x, n1y, n1z, 0);
			          
			} else {
				
				InsertArm(home, node1, &node2->myTag,
				          -bx, -by, -bz, n2x, n2y, n2z, 0);
			          
				InsertArm(home, node2, &node1->myTag,
				          bx, by, bz, n2x, n2y, n2z, 0);
				
			}
		}
#if 0		
		for (i = 0; i < 4; i++) {
			node1 = newNodes[i];
			printf(" -new node id %d, numNbrs = %d\n",node1->myTag.index,node1->numNbrs);
		}
#endif		
		free(newNodes);
		
		return;
}


/*---------------------------------------------------------------------------
 *
 *      Function:     NucleateGlissileLoop
 *
 *-------------------------------------------------------------------------*/
void NucleateGlissileLoop(Home_t *home, int numNodes, real8 radius, 
                          real8 cx, real8 cy, real8 cz, 
                          real8 bx, real8 by, real8 bz, 
                          real8 nx, real8 ny, real8 nz)
{
		int        i;
		real8      normb, bnx, bny, bnz, lx, ly, lz;
		real8      xp, yp, zp, x, y, z;
		real8      *px, *py, *pz;
		Param_t    *param;
		
		param = home->param;
		
		
		printf("** Nucleate glissile loop\n");
		
		// Find orthogonal direction in the slip plane
		xvector(nx, ny, nz, bx, by, bz, &lx, &ly, &lz);
		Normalize(&lx, &ly, &lz);
		normb = sqrt(bx*bx+by*by+bz*bz);
		bnx = bx/normb;
		bny = by/normb;
		bnz = bz/normb;

#if 0		
		printf("-bur  = (%e, %e, %e)\n", bx, by, bz);
		printf("-nor  = (%e, %e, %e)\n", nx, ny, nz);
		printf("-xdir = (%e, %e, %e)\n", bnx, bny, bnz);
		printf("-ydir = (%e, %e, %e)\n", lx, ly, lz);
		printf("-ctr  = (%e, %e, %e)\n", cx, cy, cz);
		printf("-radius = %e, segsize  = %e\n", radius, radius*sqrt(2.0)*sqrt(1.0-cos(2.0*M_PI/numNodes)));
#endif
		
		// Calculate nodes position
		px = malloc(numNodes*sizeof(real8));
		py = malloc(numNodes*sizeof(real8));
		pz = malloc(numNodes*sizeof(real8));
		// numNodes and radius represent the node number in the dislocation loop and loop radius, respectively
		for (i = 0; i < numNodes; i++) {

			// in the plane
			xp = radius * cos(i * 2.0 * M_PI / numNodes);
			yp = radius * sin(i * 2.0 * M_PI / numNodes);
			zp = 0.0;

			// in global
			px[i] = xp*bnx + yp*lx + cx;
			py[i] = xp*bny + yp*ly + cy;
			pz[i] = xp*bnz + yp*lz + cz;
		}
		
		NucleateDislocation(home, DISLOCATION_LOOP, numNodes, 
                            px, py, pz, bx, by, bz, nx, ny, nz);
		
		free(px);
		free(py);
		free(pz);
		
		return;
}


/*---------------------------------------------------------------------------
 *
 *      Function:     NucleateHalfLoop
 *
 *-------------------------------------------------------------------------*/
void NucleateHalfLoop(Home_t *home, int numNodes, real8 radius, 
                      real8 cx, real8 cy, 
                      real8 bx, real8 by, real8 bz, 
                      real8 nx, real8 ny, real8 nz, HalfSpace_t *halfspace)
{
		int        i;
		real8      normb, lx, ly, lz, mx, my, mz;
		real8      xp, yp, zp, x, y, z;
		real8      *px, *py, *pz;
		Param_t    *param;
		
		param = home->param;
		
		
		printf("**NucleateHalfLoop\n");
		
		// Find surface-plane intersection direction
		xvector(nx, ny, nz, 0.0, 0.0, 1.0, &lx, &ly, &lz);
		Normalize(&lx, &ly, &lz);
		
		// Find surface-plane intersection direction
		xvector(nx, ny, nz, lx, ly, lz, &mx, &my, &mz);
		Normalize(&mx, &my, &mz);
		if (mz > 0.0) {
			mx = -mx;
			my = -my;
			mz = -mz;
		}
		
		// Calculate nodes position
		px = malloc(numNodes*sizeof(real8));
		py = malloc(numNodes*sizeof(real8));
		pz = malloc(numNodes*sizeof(real8));
		
		for (i = 0; i < numNodes; i++) {

#if 1
			if (i == 0) {
				
				px[i] = radius*lx + cx;
				py[i] = radius*ly + cy;
				pz[i] = 0.0;
				
			} else if (i == numNodes-1) {
				
				px[i] = -radius*lx + cx;
				py[i] = -radius*ly + cy;
				pz[i] = 0.0;
				
			} else {
			
				// in the plane
				xp = radius * cos(i * M_PI / (numNodes-1));
				yp = radius * sin(i * M_PI / (numNodes-1));
				zp = 0.0;

				// in global
				px[i] = xp*lx + yp*mx + cx;
				py[i] = xp*ly + yp*my + cy;
				pz[i] = xp*lz + yp*mz;
			}
#else
			
			// in the plane
			xp = radius * cos(1.0*i/(numNodes-1)*(7.0/6.0*M_PI)-M_PI/12.0);
			yp = radius * sin(1.0*i/(numNodes-1)*(7.0/6.0*M_PI)-M_PI/12.0);
			zp = 0.0;

			// in global
			px[i] = xp*lx + yp*mx + cx;
			py[i] = xp*ly + yp*my + cy;
			pz[i] = xp*lz + yp*mz;
			
#endif
			
			//printf(" node %d, pos = %e %e %e\n",i,px[i],py[i],pz[i]);
		}
		
		
		Node_t **newNodes;
		newNodes = malloc(numNodes*sizeof(Node_t*));
		
		NucleateDislocation2(home, DISLOCATION_LINE, numNodes, 
                            px, py, pz, bx, by, bz, nx, ny, nz, newNodes);

#ifdef _HALFSPACE
		newNodes[0]->constraint = HALFSPACE_SURFACE_NODE;
		newNodes[numNodes-1]->constraint = HALFSPACE_SURFACE_NODE;	
#ifdef _SURFACE_STEPS
		int newSurfSeg = CreateNewSurfaceSegment(halfspace);
		surfaceStepSeg_t * surfStepSegList = halfspace->surfStepSegList;
					
		surfStepSegList[newSurfSeg].virtual1 = 0;
		surfStepSegList[newSurfSeg].node1 = newNodes[0];
		surfStepSegList[newSurfSeg].pos1[0] = px[0];
		surfStepSegList[newSurfSeg].pos1[1] = py[0];
		surfStepSegList[newSurfSeg].pos1[2] = pz[0];
		
		surfStepSegList[newSurfSeg].virtual2 = 0;
		surfStepSegList[newSurfSeg].node2 = newNodes[numNodes-1];
		surfStepSegList[newSurfSeg].pos2[0] = px[numNodes-1];
		surfStepSegList[newSurfSeg].pos2[1] = py[numNodes-1];
		surfStepSegList[newSurfSeg].pos2[2] = pz[numNodes-1];
		
		surfStepSegList[newSurfSeg].burg[0] = bx;
		surfStepSegList[newSurfSeg].burg[1] = by;
		surfStepSegList[newSurfSeg].burg[2] = bz;
		
		surfStepSegList[newSurfSeg].norm[0] = nx;
		surfStepSegList[newSurfSeg].norm[1] = ny;
		surfStepSegList[newSurfSeg].norm[2] = nz;
#endif
#endif
		
		free(newNodes);
		
		free(px);
		free(py);
		free(pz);
		
		return;
}


/*---------------------------------------------------------------------------
 *
 *      Function:     GenerateGlissileLoop
 *
 *-------------------------------------------------------------------------*/
void GenerateGlissileLoop(Home_t *home)
{
		int        numNodes, i;
		real8      bx, by, bz, nx, ny, nz, lx, ly, lz;
		real8      center[3], radius;
		real8      xp, yp, zp, x, y, z;
		real8      *px, *py, *pz;
		Param_t    *param;
		
		param = home->param;
		
		
		printf("**GenerateGlissileLoop\n");
		
		// Number of nodes
		numNodes = 6;
		
		// Radius
		radius = 2000.0;
		
		// Select random system
		RandomSlipSystem(home, &bx, &by, &bz, &nx, &ny, &nz);
		
		// Center
		struct timeval t1;
		gettimeofday(&t1, NULL);
		srand(t1.tv_usec * t1.tv_sec);
		
		center[0] = param->minSideX + 1.0*RandomAtMost(param->Lx);
		center[1] = param->minSideY + 1.0*RandomAtMost(param->Ly);
		center[2] = param->minSideZ + 1.0*RandomAtMost(param->Lz);
		
		// Find orthogonal direction in the slip plane
		xvector(nx, ny, nz, bx, by, bz, &lx, &ly, &lz);

#if 0		
		printf("-bur  = (%e, %e, %e)\n", bx, by, bz);
		printf("-nor  = (%e, %e, %e)\n", nx, ny, nz);
		printf("-xdir = (%e, %e, %e)\n", bx, by, bz);
		printf("-ydir = (%e, %e, %e)\n", lx, ly, lz);
		printf("-ctr  = (%e, %e, %e)\n", center[0], center[1], center[2]);
#endif
		
		// Calculate new nodes position
		px = malloc(numNodes*sizeof(real8));
		py = malloc(numNodes*sizeof(real8));
		pz = malloc(numNodes*sizeof(real8));
		
		
		for (i = 0; i < numNodes; i++) {

			// in the plane
			xp = radius * cos(i * 2.0 * M_PI / numNodes);
			yp = radius * sin(i * 2.0 * M_PI / numNodes);
			zp = 0.0;

			// in global
			px[i] = xp*bx + yp*lx + center[0];
			py[i] = xp*by + yp*ly + center[1];
			pz[i] = xp*bz + yp*lz + center[2];
		}
		
		NucleateDislocation(home, DISLOCATION_LOOP, numNodes, 
                            px, py, pz, bx, by, bz, nx, ny, nz);
		
		free(px);
		free(py);
		free(pz);
		
		return;
}
