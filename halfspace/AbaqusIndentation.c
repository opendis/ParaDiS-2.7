#ifdef _ABAQUS

/*-------------------------------------------------------------------------
 * This module was only developed in an attempt to compare 
 * the spectral method with Abaqus.
 * Nicolas, 03/28/2018
 *-----------------------------------------------------------------------*/

#include "Home.h"
#include "Abaqus.h"
#include "AbaqusIndent.h"
#include <time.h>

void SolveSurfaceContactAbaqus(Home_t *home, HalfSpace_t *halfspace);
void InterpolateDisp(Home_t *home, double *pz, double pv[2], double *pznew);

/*-------------------------------------------------------------------------
 *
 *      Function:     Abaqus_Step
 * 		Description:  Function to compare the spectral method with Abaqus 
 * 					  to solve for the displacement field given the
 *                    contact pressure.
 *
 *-----------------------------------------------------------------------*/
void Abaqus_Step(Home_t *home, HalfSpace_t *halfspace)
{
		int i, j, nx, ny;
		
		nx = halfspace->nx;
		ny = halfspace->ny;


/*
 * 		Call Abaqus to solve the displacement given the 
 * 		contact pressure using FEM.
 */	

		double *pz = (double *)malloc(sizeof(double)*nx*ny);
		double *uz = (double *)malloc(sizeof(double)*nx*ny);
	
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				pz[j+i*ny] = halfspace->Tzext[j+i*ny];
			}
		}
		
		printf("F_indenter = %e\n", halfspace->F_indenter);
		
		SolveMechanicsAbaqus(home, pz, uz);
	
#if 1
/*
 * 		Output the spectral displacement and stress fields 
 * 		on a vertical slice.
 */
		FILE *fp;
		
		int N = 128;
		double r[2], p[3], u, s[3][3];
		
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				uz[j+i*ny] = halfspace->Uz_elas[j+i*ny];
			}
		}
		
		// Surface displacement along x-line
		fp = fopen("spec_disp.dat", "w");
		for (i = 0; i < N; i++) {
			r[0] = home->param->minSideX + i*1.0/(N-1)*home->param->Lx;
			r[1] = 0.0;
			InterpolateDisp(home, uz, r, &u);
			fprintf(fp, "%e %e\n", r[0], u);
		}
		fclose(fp);
		
		// Stress on xz slice
		fp = fopen("spec_stress.dat", "w");
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				p[0] = home->param->minSideX + i*1.0/(N-1)*home->param->Lx;
				p[1] = 0.0;
				p[2] = home->param->minSideZ + j*0.5/(N-1)*home->param->Lz;
				DispStress(halfspace, p, s);
				fprintf(fp, "%e %e %e %e %e %e %e %e %e\n", p[0], p[1], p[2], s[0][0], s[1][1], s[2][2], s[0][1], s[1][2], s[0][2]);
			}
		}
		fclose(fp);
#endif
	
#if 1	
/*
 * 		Time the forward calculation when using the spectral method
 */
		double *oneoverkz = (double *)malloc(sizeof(double)*nx*ny);
		fftw_complex *uztmp     = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
		fftw_complex *uzktmp    = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
		fftw_complex *pztmp     = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
		fftw_complex *pzktmp    = (COMPLEX*) malloc(sizeof(COMPLEX)*nx*ny);
		
		double *kx = halfspace->kx;
		double *ky = halfspace->ky;
		
		// ---- Allocate 1/kz ------ 
		oneoverkz[0] = 0.0;
		for (i=0; i<nx; i++) {
			for (j=0; j<ny; j++) {
				if ((i > 0) || (j > 0)){
					oneoverkz[j+i*ny] = 1.0/sqrt(kx[i]*kx[i]+ky[j]*ky[j]);
				}
				pztmp[j+i*ny] = pz[j+i*ny];
			}
		}
		
		clock_t t0 = clock();
		
		// Time for 1000 iterations.
		int iter = 0;
		for (iter = 0; iter < 1000; iter++) {
			// Fourier transform pz to 
			fourier_transform_forward(pztmp,(fftw_complex *)pzktmp,nx,ny);

			// Obtain Fourier transform of uz 
			for (i=0; i<nx; i++) {
				for (j=0; j<ny; j++) {
					
					if (i == 0 && j == 0) {
						// Specify k=0 term corresponding to the uniform displacement mode
						uzktmp[0] = 1.0*pzktmp[0];
					} else {
						uzktmp[j+i*ny] = 1.0*pzktmp[j+i*ny]*oneoverkz[j+i*ny];
					}
					
				}
			}
			
			// Inverse Fourier Transform
			fourier_transform_backward(uztmp,(fftw_complex *)uzktmp,nx,ny);
		}
		
		clock_t t1 = clock();
		double time_spent = (double)(t1 - t0) / CLOCKS_PER_SEC;
		printf("Spectral forward pass time: %e sec\n", time_spent);
		
		free(uztmp);
		free(uzktmp);
		free(pztmp);
		free(pzktmp);
		free(oneoverkz);
		
		// TIME FORWARD PASS
	
#endif

		free(pz);
		free(uz);
}

/*-------------------------------------------------------------------------
 *
 *      Function:     InterpolatePressure
 *
 *-----------------------------------------------------------------------*/
void InterpolateDisp(Home_t *home, double *pz, double r[2], double *pznew)
{
	
	int nx = home->param->hs_nx;
	int ny = home->param->hs_ny;
	
	double HSLx = home->param->Lx;
	double HSLy = home->param->Ly;
	
	// Interoplate stress on grid //
	int i, j, nxg, nyg, nzg, g[2], npd, npe;
	double p[2], q[2], xi[2];
	
	int linear = 0;
	
	if (linear) {
		// Linear interpolation
		nxg = nx;
		nyg = ny;
		npd = 2;
	} else {
		// Quadratic interpolation
		if (nx%2 !=0 || ny%2 != 0) {
			Fatal("Surface grid resolution nx and ny must be multiple of 2 "
				  "for option -D_GRID_INDENTER_STRESS");
		}
		nxg = nx/2;
		nyg = ny/2;
		npd = 3;
	}
	npe = npd*npd;
	
	p[0] = r[0] + 0.5*HSLx;
	p[1] = r[1] + 0.5*HSLy;
	
	q[0] = p[0] / (HSLx / nxg);
	q[1] = p[1] / (HSLy / nyg);
	
	g[0] = (int)floor(q[0]);
	g[1] = (int)floor(q[1]);
	
	xi[0] = 2.0*(q[0]-g[0]) - 1.0;
	xi[1] = 2.0*(q[1]-g[1]) - 1.0;
	
	// Determine elements for interpolation
	// and apply PBC in the x,y directions
	int ind1d[2][npd];
	for (i = 0; i < npd; i++) {
		ind1d[0][i] = ((npd-1)*g[0]+i)%nx;
		if (ind1d[0][i] < 0) ind1d[0][i] += nx;
		ind1d[1][i] = ((npd-1)*g[1]+i)%ny;
		if (ind1d[1][i] < 0) ind1d[1][i] += ny;
	}
	
	// 1d shape functions
	double phi1d[2][npd];
	if (linear) {
		for (i = 0; i < 2; i++) {
			phi1d[i][0] = 0.5*(1.0-xi[i]);
			phi1d[i][1] = 0.5*(1.0+xi[i]);
		}
	} else {
		for (i = 0; i < 2; i++) {
			phi1d[i][0] = 0.5*xi[i]*(xi[i]-1.0);
			phi1d[i][1] = -(xi[i]+1.0)*(xi[i]-1.0);
			phi1d[i][2] = 0.5*xi[i]*(xi[i]+1.0);
		}
	}
	
	// 2d shape functions and indices
	int ind[npe];
	double phi[npe];
	for (j = 0; j < npd; j++) {
		for (i = 0; i < npd; i++) {
			phi[i+j*npd] = phi1d[0][i]*phi1d[1][j];
			ind[i+j*npd] = ind1d[0][i] + ind1d[1][j]*nx;
		}
	}
	
	// Interpolate pressure from the grid points
	double pval = 0.0;
	for (j = 0; j < npe; j++) {
		pval += phi[j] * pz[ind[j]];
	}
	*pznew = pval;
	
	return;
}

#endif
