#ifdef _ABAQUS

#ifndef _Abaqus_h
#define _Abaqus_h

#ifdef __cplusplus

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <vector>
#include <string>

extern "C" {
	void InitAbaqus(Home_t *home);
	void SolveMechanicsAbaqus(Home_t *home, double *pz, double *uz);
}

/*------------------------------------------------------------------------
 *	DDDBoundary
 * 		Data structure to store the boundary (surface mesh) of the
 * 		simulation domain
 *----------------------------------------------------------------------*/
class DDDBoundary
{

public:
	DDDBoundary();
	//add one surface element to the list
	//input: vertex indices, boundary type
	void AddSurfaceElement(std::vector<int>&, int);
	//add one surface coordinate to the list
	//input: coordinate x,y,z
	void AddSurfaceNode(std::vector<double>&);
	
	void rescaleSurfaceMesh(double min_old[3], double min_new[3], double scale[3]);

	int getNumberOfElements() const { return bElement.size(); }
	int getElementNodeNum(const int eIndex) const { return bElement[eIndex].size(); }
	std::vector<int> getElementNodeSeq(const int eIndex) const { return bElement[eIndex]; }
	int getElementType(const int eIndex) const { return bElementType[eIndex]; }
	int getNumberOfNodes() const { return bVertex.size(); }
	std::vector<double> getNodeCoord(const int nIndex) const { return bVertex[nIndex]; }

#ifdef _ABAQUS
	// free surfaces
	bool freeBotX, freeTopX;
	bool freeBotY, freeTopY;
	bool freeBotZ, freeTopZ;
#endif

private:
	//list of surface elements
	std::vector<std::vector<int> > bElement;
	//list of boundary types in the same order
	std::vector<int> bElementType;
	//list of coordinates of surface element vertices
	std::vector<std::vector<double> > bVertex;

};

/*------------------------------------------------------------------------
 *	DDDMesh
 * 		Mesh data structure to store mesh nodes and connectivity
 * 		when a full mesh is generated or read from the input file
 *----------------------------------------------------------------------*/
class DDDMesh
{

public:
	DDDMesh();
	void setVolumeSize(double x_min, double x_max, double y_min,
	                   double y_max, double z_min, double z_max);
	void setElementType(int el_Type);
	void setMeshSize(int N_x, int N_y, int N_z);
	void setBoundary(DDDBoundary *b_Object);
	void rescaleMesh(double x_min, double x_max, double y_min,
	                 double y_max, double z_min, double z_max);
	
	int getElementsType() { return elType; }
	int getNumberOfNodes() { return nNodes; }
	int getNumberOfElmts() { return nElmts; }
	int getNumberOfElPerNodes() { return nElNodes; }
	int getConnectNode(const int nElmt, const int nNodes) { return connTable[nElmt*nElNodes + nNodes]; }
	double getNodeCoord(const int nIndex, const int nCoord) { return mNodes[nIndex][nCoord]; }
	std::vector<double> getNodeCoords(const int nIndex) { return mNodes[nIndex]; }
	int getSurfToMesh(const int nIndex) { return indSurfToMesh[nIndex]; }
	int getMeshToSurf(const int nIndex) { return indMeshToSurf[nIndex]; }
	bool isOnMeshSurface(const std::vector<double> v);
	
	void generateMesh();
	void generateLinearMesh();
	void generateQuadraticMesh();
	
	void resetNodalStresses();
	void setNodalStress(const int nIndex, double s[6]);
	std::vector<double> getNodalStress(const int nIndex) { return sNodes[nIndex]; }; //output: Sxx,Syy,Szz,Sxy,Syz,Szx
	void resetNodalDisp();
	void setNodalDisp(const int nIndex, double u[3]);
	std::vector<double> getNodalDisp(const int nIndex) { return uNodes[nIndex]; };
	
	std::vector<double> shapeFunction(const std::vector<double> xi);
	std::vector<double> interpolateStress(const std::vector<double> v);
	std::vector<double> interpolateDisp(const std::vector<double> v);
	int findElementIndex(const std::vector<double> v);
	void deleteMesh();
	
	int Nx, Ny, Nz;
	//std::string meshFile;
	int meshSize;
	
	double getMeshElArea() { return Sx*Sy; }

private:
	//physical dimensions of the mesh
	double xmin, xmax;
	double ymin, ymax;
	double zmin, zmax;
	double Lx, Ly, Lz;
	double Sx, Sy, Sz;
	
	//size and parameters of the mesh
	int elType;
	int nNodes;
	int nElNodes;
	int nSurfNodes;
	int nElmts;
	
	//connectivity tables
	int *connTable;
	int *indSurfToMesh;
	int *indMeshToSurf;
	
	//associated boundary object
	DDDBoundary *bObject;
	
	//list of coordinates of nodes
	std::vector<std::vector<double> > mNodes;
	//stresses at nodes
	std::vector<std::vector<double> > sNodes;
	std::vector<std::vector<double> > uNodes;
};

#else
void InitAbaqus(Home_t *home);
void SolveMechanicsAbaqus(Home_t *home, double *pz, double *uz);
#endif

#endif
#endif
