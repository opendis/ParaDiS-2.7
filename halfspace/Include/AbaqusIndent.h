#ifdef _ABAQUS

#ifndef _AbaqusIndent_h
#define _AbaqusIndent_h

void Abaqus_Step(Home_t *home, HalfSpace_t *halfspace);
void SolveSurfaceContactAbaqus(Home_t *home, HalfSpace_t *halfspace);

#endif
#endif
