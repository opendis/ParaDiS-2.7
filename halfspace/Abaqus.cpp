#ifdef _ABAQUS

/*-------------------------------------------------------------------------
 * This module was only developed in an attempt to compare 
 * the spectral method with Abaqus.
 * Nicolas, 03/28/2018
 *-----------------------------------------------------------------------*/

#include "Home.h"
#include "Abaqus.h"

void GenerateAbaqusInputFile(Home_t *home, double *fz, int ninc);
void RunAbaqus();
void ReadAbaqusDispResults(double *uz);
void ReadAbaqusStressResults();
void InterpolatePressure(Home_t *home, double *pz, double pv[2], double *pznew);
void SurfaceTractionForce(Home_t *home, double *pz, int surfaceNodeSize,
                          double *Xs, double *Ys, double *Zs, double *Fz);

double scalefact;
DDDMesh mesh;
DDDBoundary boundary;

/*-------------------------------------------------------------------------
 *
 *      Function:     InitAbaqus
 *
 *-----------------------------------------------------------------------*/
void InitAbaqus(Home_t *home)
{
		Param_t *param = home->param;
		DDDBoundary *bObject = &boundary;
		
		// Scaling factor for Abaqus
		scalefact = 1.e6*param->burgMag;
		
		int    N1, N2, N3;
		double xmin, xmax, ymin, ymax, zmin, zmax;
		
		N1 = param->hs_nx; //-1
		N2 = param->hs_ny; //-1
		//N3 = ceil(N1/2);
		N3 = N1;
		
		xmin = param->minSideX;
		xmax = param->maxSideX;
		ymin = param->minSideY;
		ymax = param->maxSideY;
		zmin = param->minSideZ;
		zmax = 0.0; // halfspace

		// Generate full mesh
		mesh.setMeshSize(N1, N2, N3);
		mesh.setVolumeSize(xmin, xmax, ymin, ymax, zmin, zmax);
		mesh.setElementType(1); // 0=C3D8, 1=C3D20
		mesh.setBoundary(bObject);
		mesh.generateMesh();
		
		printf("Number of mesh nodes: %d\n", mesh.getNumberOfNodes());
		printf("Number of mesh elements: %d\n", mesh.getNumberOfElmts());
		printf("Number of surface nodes: %d\n", bObject->getNumberOfNodes());
		printf("Number of surface elements: %d\n\n", bObject->getNumberOfElements());
		
		mesh.resetNodalStresses();
		mesh.resetNodalDisp();
		
		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     SolveMechanicsAbaqus
 *
 *-----------------------------------------------------------------------*/
void SolveMechanicsAbaqus(Home_t *home, double *pz, double *uz)
{
		printf("SolveMechanicsAbaqus\n");
		printf("Number of mesh nodes: %d\n", mesh.getNumberOfNodes());
		

		int i, j;

		// Interpolate pressure and compute nodal force
		double *fz = (double *)malloc(sizeof(double)*boundary.getNumberOfNodes());
		for (i = 0; i < boundary.getNumberOfNodes(); i++) {
			fz[i] = 0.0;
		}

/*
 * 		Integrate the contact pressure to compute the nodal forces 
 * 		that will be imposed as a boundary condition in the FEM calculation.
 */
		for (i = 0; i < boundary.getNumberOfElements(); i++) {
		
			// Get element nodes
			std::vector<int> elNodeSeq = boundary.getElementNodeSeq(i);
			int elNodeSize = elNodeSeq.size();
			
			double *Xs = (double *)malloc(sizeof(double)*elNodeSize);
			double *Ys = (double *)malloc(sizeof(double)*elNodeSize);
			double *Zs = (double *)malloc(sizeof(double)*elNodeSize);
			double *Fs = (double *)malloc(sizeof(double)*elNodeSize);
			for (j = 0; j < elNodeSize; j++) {
				Xs[j] = boundary.getNodeCoord(elNodeSeq[j])[0];
				Ys[j] = boundary.getNodeCoord(elNodeSeq[j])[1];
				Zs[j] = boundary.getNodeCoord(elNodeSeq[j])[2];
				Fs[j] = 0.0;
			}
			
			SurfaceTractionForce(home, pz, elNodeSize, Xs, Ys, Zs, Fs);
			
			// Assemble traction on nodes
			for (j = 0; j < elNodeSize; j++) {
				fz[elNodeSeq[j]] += Fs[j];
			}
			
			free(Xs);
			free(Ys);
			free(Zs);
			free(Fs);
		}

		double Fztot = 0.0;
		for (i = 0; i < boundary.getNumberOfNodes(); i++) {
			Fztot += fz[i];
		}
		printf("Fz_tot = %e\n", Fztot);

/*
 * 		Solve for the displacement and stress fields using Abaqus
 */
		GenerateAbaqusInputFile(home, fz, 1);
		RunAbaqus();
		
		double *uznew = (double *)malloc(sizeof(double)*boundary.getNumberOfNodes());
		ReadAbaqusDispResults(uznew);
		double minu = 0.0;
		for (i = 0; i < boundary.getNumberOfNodes(); i++) {
			minu = std::min(uznew[i], minu);
		}
		printf("min U = %e\n", minu);
		
		ReadAbaqusStressResults();
		
#if 1
/*
 * 		Output displacement and stress fields on a vertical slice
 */
		FILE *fp;
		
		int N = 128;
		
		// Surface displacement along x-line
		fp = fopen("abq_disp.dat", "w");
		for (i = 0; i < N; i++) {
			std::vector<double> p;
			p.push_back(home->param->minSideX + i*1.0/(N-1)*home->param->Lx);
			p.push_back(0.0);
			p.push_back(0.0);
			std::vector<double> u = mesh.interpolateDisp(p);
			fprintf(fp, "%e %e\n", p[0], u[2]);
		}
		fclose(fp);
		
		// Stress on xz slice
		fp = fopen("abq_stress.dat", "w");
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				std::vector<double> p;
				p.push_back(home->param->minSideX + i*1.0/(N-1)*home->param->Lx);
				p.push_back(0.0);
				p.push_back(home->param->minSideZ + j*0.5/(N-1)*home->param->Lz);
				std::vector<double> s = mesh.interpolateStress(p);
				fprintf(fp, "%e %e %e %e %e %e %e %e %e\n", p[0], p[1], p[2], s[0], s[1], s[2], s[3], s[4], s[5]);
			}
		}
		fclose(fp);
#endif
		
		
		free(uznew);
		free(fz);
		
		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     BoxOutwardNormal
 *      Description:  Returns the outward normal for a cubic simulation box
 *
 *-----------------------------------------------------------------------*/
void BoxOutwardNormal(Home_t *home, double x, double y, double z, double n[3])
{
	
	double eps_rel = 1e-5;
	double eps_abs = 1.0;
	
	Param_t *param;
	param = home->param;
	
	// Check normal
	if(fabs(fabs(n[0]) - 1.0) < eps_rel) {
		// normal in the x-direction
		if(fabs(param->maxSideX - x) < eps_abs) {
			// positive x
			n[0] = 1.0; n[1] = 0.0; n[2] = 0.0;
		} else if(fabs(param->minSideX - x) < eps_abs) {
			// negative x
			n[0] = -1.0; n[1] = 0.0; n[2] = 0.0;
		} else {
			// unknown normal...
			Fatal("Element surface does not correspond to a known facet!");
		}
	} else if(fabs(fabs(n[1]) - 1.0) < eps_rel) {
		// normal in the y-direction
		if(fabs(param->maxSideY - y) < eps_abs) {
			// positive y
			n[0] = 0.0; n[1] = 1.0; n[2] = 0.0;
		} else if(fabs(param->minSideY - y) < eps_abs) {
			// negative y
			n[0] = 0.0; n[1] = -1.0; n[2] = 0.0;
		} else {
			// unknown normal...
			Fatal("Element surface does not correspond to a known facet!");
		}
	} else if(fabs(fabs(n[2]) - 1.0) < eps_rel) {
		// normal in the z-direction
		//if(fabs(param->maxSideZ - z) < eps_abs) {
		if(fabs(z) < eps_abs) {
			// positive z
			n[0] = 0.0; n[1] = 0.0; n[2] = 1.0;
		} else if(fabs(param->minSideZ - z) < eps_abs) {
			// negative z
			n[0] = 0.0; n[1] = 0.0; n[2] = -1.0;
		} else {
			// unknown normal...
			Fatal("Element surface does not correspond to a known facet!");
		}
	} else {
		// unknown normal...
		Fatal("Surface normal does not correspond to a known facet!");
	}
}

/*-------------------------------------------------------------------------
 *
 *      Function:     NormVec
 *
 *------------------------------------------------------------------------*/
void NormVec(real8 vec[3])
{
        real8 a2, a;

        a2 = (vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);

        if (a2 > 0.0) {
            a = sqrt(a2);
            vec[0] /= a;
            vec[1] /= a;
            vec[2] /= a;
        }

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     NormCrossVector
 *
 *------------------------------------------------------------------------*/
void NormCrossVector(real8 a[3], real8 b[3], real8 c[3])
{
        real8 mag;

        c[0] = a[1]*b[2] - a[2]*b[1];
        c[1] = a[2]*b[0] - a[0]*b[2];
        c[2] = a[0]*b[1] - a[1]*b[0];

        mag = sqrt(c[0]*c[0] + c[1]*c[1] + c[2]*c[2]);

        if (mag < 1.0e-10) {
            printf("Warning! NormalizedCrossVector() All zeros.\n");
            c[0] = 0.0;
            c[1] = 0.0;
            c[2] = 0.0;
        } else {
            c[0] = c[0] / mag;
            c[1] = c[1] / mag;
            c[2] = c[2] / mag;
        }

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     SurfaceTractionForce
 *
 *-----------------------------------------------------------------------*/
void SurfaceTractionForce(Home_t *home, double *pz, int surfaceNodeSize,
                          double *Xs, double *Ys, double *Zs, double *Fz)
{
	
	//printf("enter surfaceTraction \n");
	
	int i, j, k;
	
	//for(i = 0; i < surfaceNodeSize; i++) {
		//printf("node: %d, id=%d, x=%f, y=%f, z=%f \n", i, surfaceNodeSeq[i], Xs[i], Ys[i], Zs[i]);
	//}
	
	
	// Surface normal unit vector (sign here?)
	double v1[3], v2[3], n[3];
	
	v1[0] = Xs[1] - Xs[0];
	v1[1] = Ys[1] - Ys[0];
	v1[2] = Zs[1] - Zs[0];
	
	v2[0] = Xs[2] - Xs[0];
	v2[1] = Ys[2] - Ys[0];
	v2[2] = Zs[2] - Zs[0];
	
	NormCrossVector(v1, v2, n);
	//printf("normal vector: %f %f %f \n", n[0], n[1], n[2]);
	BoxOutwardNormal(home, Xs[0], Ys[0], Zs[0], n);
	//printf("outward normal: %f %f %f \n", n[0], n[1], n[2]);
	
	
	// Nodes coordinates in 2D surface
	double nodesCoord2D[surfaceNodeSize][2];
	double x2D[3], y2D[3];
	
	x2D[0] = v1[0]; x2D[1] = v1[1]; x2D[2] = v1[2];
	NormVec(x2D);
	NormCrossVector(n, x2D, y2D);
	
	//printf("x2D: %f %f %f \n", x2D[0], x2D[1], x2D[2]);
	//printf("y2D: %f %f %f \n", y2D[0], y2D[1], y2D[2]);
	
	nodesCoord2D[0][0] = 0.0;
	nodesCoord2D[0][1] = 0.0;
	for(i = 1; i < surfaceNodeSize; i++) {
		
		double tmp[3];
		tmp[0] = Xs[i] - Xs[0];
		tmp[1] = Ys[i] - Ys[0];
		tmp[2] = Zs[i] - Zs[0];
		
		nodesCoord2D[i][0] = tmp[0] * x2D[0] + tmp[1] * x2D[1] + tmp[2] * x2D[2];
		nodesCoord2D[i][1] = tmp[0] * y2D[0] + tmp[1] * y2D[1] + tmp[2] * y2D[2];
	}
	
	//for(i = 0; i < surfaceNodeSize; i++) {
		//printf("node: %d, coords 2D: %f %f \n", i, nodesCoord2D[i][0], nodesCoord2D[i][1]);
	//}
	
	// Original frame in 2D coordinate system
	double frame3D[4][3];
	frame3D[0][0] = 0.0; frame3D[0][1] = 0.0; frame3D[0][2] = 0.0; // origin
	frame3D[1][0] = 1.0; frame3D[1][1] = 0.0; frame3D[1][2] = 0.0; // x-axis
	frame3D[2][0] = 0.0; frame3D[2][1] = 1.0; frame3D[2][2] = 0.0; // y-axis
	frame3D[3][0] = 0.0; frame3D[3][1] = 0.0; frame3D[3][2] = 1.0; // z-axis
	
	for(i = 0; i < 4; i++) {
		
		double tmp[3];
		if(i == 0) {
			tmp[0] = frame3D[i][0] - Xs[0];
			tmp[1] = frame3D[i][1] - Ys[0];
			tmp[2] = frame3D[i][2] - Zs[0];
		} else {
			tmp[0] = frame3D[i][0];
			tmp[1] = frame3D[i][1];
			tmp[2] = frame3D[i][2];
		}
		
		frame3D[i][0] = tmp[0] * x2D[0] + tmp[1] * x2D[1] + tmp[2] * x2D[2];
		frame3D[i][1] = tmp[0] * y2D[0] + tmp[1] * y2D[1] + tmp[2] * y2D[2];
		frame3D[i][2] = tmp[0] *   n[0] + tmp[1] *   n[1] + tmp[2] *   n[2];
	}
	
	//printf("X: %f %f %f \n", frame3D[1][0], frame3D[1][1], frame3D[1][2]);
	//printf("Y: %f %f %f \n", frame3D[2][0], frame3D[2][1], frame3D[2][2]);
	//printf("Z: %f %f %f \n", frame3D[3][0], frame3D[3][1], frame3D[3][2]);
	
	// back to 3D coords
	double back3D[surfaceNodeSize][3];
	for(i = 0; i < surfaceNodeSize; i++) {
		
		double tmp[3];
		tmp[0] = nodesCoord2D[i][0] - frame3D[0][0];
		tmp[1] = nodesCoord2D[i][1] - frame3D[0][1];
		tmp[2] = - frame3D[0][2];
		
		back3D[i][0] = tmp[0] * frame3D[1][0] + tmp[1] * frame3D[1][1] + tmp[2] * frame3D[1][2];
		back3D[i][1] = tmp[0] * frame3D[2][0] + tmp[1] * frame3D[2][1] + tmp[2] * frame3D[2][2];
		back3D[i][2] = tmp[0] * frame3D[3][0] + tmp[1] * frame3D[3][1] + tmp[2] * frame3D[3][2];
	}
	
	//for(i = 0; i < surfaceNodeSize; i++) {
		//printf("node: %d, back 3D: %f %f %f \n", i, back3D[i][0], back3D[i][1], back3D[i][2]);
	//}
	
	
	// Number of intergration points
	int nIntPoints;
	
	// Triangular surface elements
	if(surfaceNodeSize == 3) {
		nIntPoints = 1;
		//nIntPoints = 3;
		//nIntPoints = 4;
	}
	// Rectangular linear surface elements
	else if(surfaceNodeSize == 4) {
		nIntPoints = 4;
		//nIntPoints = 9;
	}
	// Triangular quadratic surface elements
	else if(surfaceNodeSize == 6) {
		nIntPoints = 4;
	}
	// Rectangular quadratic surface elements
	else if(surfaceNodeSize == 8) {
		nIntPoints = 9;
	}
	else {
		Fatal("Surface elements must be composed of 3, 4, 6 or 8 nodes.");
	}
	
	// Reference element properties
	double Xi[nIntPoints][2], wi[nIntPoints];
	double phi[surfaceNodeSize], dphi[surfaceNodeSize][2];
	
	// Triangular surface elements
	if(surfaceNodeSize == 3 || surfaceNodeSize == 6) {
		
		if(nIntPoints == 1) {
			
			// Coordinates of the integration point
			Xi[0][0] = 1.0/3.0; Xi[0][1] = 1.0/3.0;
			
			// Weight of the integration point
			wi[0] = 0.5;
		}
		else if(nIntPoints == 3) {
		
			// Coordinates of the integration points on reference element
			Xi[0][0] = 0.5; Xi[0][1] = 0.0;
			Xi[1][0] = 0.5; Xi[1][1] = 0.5;
			Xi[2][0] = 0.0; Xi[2][1] = 0.5;
			
			// Weights of the integration points
			wi[0] = wi[1] = wi[2] = 1.0 / 6.0;
		}
		else if(nIntPoints == 4) {
		
			// Coordinates of the integration points on reference element
			Xi[0][0] = 1.0/3.0; Xi[0][1] = 1.0/3.0;
			Xi[1][0] = 3.0/5.0; Xi[1][1] = 1.0/5.0;
			Xi[2][0] = 1.0/5.0; Xi[2][1] = 1.0/5.0;
			Xi[3][0] = 1.0/5.0; Xi[3][1] = 3.0/5.0;
			
			// Weights of the integration points
			wi[0] = -0.5 * 27.0/48.0;
			wi[1] = wi[2] = wi[3] = 0.5 * 25.0/48.0;
		}
		
		
	}
	// Rectangular surface elements
	else if(surfaceNodeSize == 4 || surfaceNodeSize == 8) {
		
		if(nIntPoints == 4) {
		
			// Coordinates of the integration points on reference element
			const double sqrt13 = 0.5773502691896257;
			Xi[0][0] = -sqrt13; Xi[0][1] = -sqrt13;
			Xi[1][0] =  sqrt13; Xi[1][1] = -sqrt13;
			Xi[2][0] =  sqrt13; Xi[2][1] =  sqrt13;
			Xi[3][0] = -sqrt13; Xi[3][1] =  sqrt13;
			
			// Weights of the integration points
			wi[0] = wi[1] = wi[2] = wi[3] = 1.0;
			
		}
		else if(nIntPoints == 9) {
			
			// Coordinates of the integration points on reference element
			const double sqrt35 = 0.7745966692414834;
			const double w0 = 0.8888888888888888;
			const double w1 = 0.5555555555555556;
			
			Xi[0][0] = -sqrt35;	Xi[0][1] = -sqrt35;
			Xi[1][0] =  0.0000; Xi[1][1] = -sqrt35;
			Xi[2][0] =  sqrt35; Xi[2][1] = -sqrt35;
			
			Xi[3][0] = -sqrt35; Xi[3][1] =  0.0000;
			Xi[4][0] =  0.0000; Xi[4][1] =  0.0000;
			Xi[5][0] =  sqrt35; Xi[5][1] =  0.0000;
			
			Xi[6][0] = -sqrt35; Xi[6][1] =  sqrt35;
			Xi[7][0] =  0.0000; Xi[7][1] =  sqrt35;
			Xi[8][0] =  sqrt35; Xi[8][1] =  sqrt35;
			
			wi[0] = wi[2] = wi[6] = wi[8] = w1 * w1;
			wi[1] = wi[3] = wi[5] = wi[7] = w0 * w1;
			wi[4] = w0 * w0;
			
		}
		
	}
	
	double area = 0.0;
	
	// Initialize surface nodal forces
	for(i = 0; i < surfaceNodeSize; i++) {
		Fz[i] = 0.0;
	}
	
	// Gaussian intergration
	int q;
	for(q = 0; q < nIntPoints; q++) {
		
		// Shape functions
		double phi[surfaceNodeSize], dphi[surfaceNodeSize][2];
		
		// Triangular surface elements
		if(surfaceNodeSize == 3) {
			
			phi[0] = 1.0 - Xi[q][0] - Xi[q][1];
			phi[1] = Xi[q][0];
			phi[2] = Xi[q][1];
			
			dphi[0][0] = -1.0;
			dphi[1][0] = 1.0;
			dphi[2][0] = 0.0;
			dphi[0][1] = -1.0;
			dphi[1][1] = 0.0;
			dphi[2][1] = 1.0;
		}
		// Rectangular linear surface elements
		else if(surfaceNodeSize == 4) {
			
			phi[0] = 0.25 * (1.0 - Xi[q][0]) * (1.0 - Xi[q][1]);
			phi[1] = 0.25 * (1.0 + Xi[q][0]) * (1.0 - Xi[q][1]);
			phi[2] = 0.25 * (1.0 + Xi[q][0]) * (1.0 + Xi[q][1]);
			phi[3] = 0.25 * (1.0 - Xi[q][0]) * (1.0 + Xi[q][1]);

			dphi[0][0] = -0.25 * (1.0 - Xi[q][1]);
			dphi[1][0] =  0.25 * (1.0 - Xi[q][1]);
			dphi[2][0] =  0.25 * (1.0 + Xi[q][1]);
			dphi[3][0] = -0.25 * (1.0 + Xi[q][1]);
			
			dphi[0][1] = -0.25 * (1.0 - Xi[q][0]);
			dphi[1][1] = -0.25 * (1.0 + Xi[q][0]);
			dphi[2][1] =  0.25 * (1.0 + Xi[q][0]);
			dphi[3][1] =  0.25 * (1.0 - Xi[q][0]);
		}
		// Triangular quadratic surface elements
		else if(surfaceNodeSize == 6) {
			
			double zeta = 1.0 - Xi[q][0] - Xi[q][1];
			phi[0] = zeta * (2.0 * zeta - 1.0);
			phi[1] = Xi[q][0] * (2.0 * Xi[q][0] - 1.0);
			phi[2] = Xi[q][1] * (2.0 * Xi[q][1] - 1.0);
			phi[3] = 4.0 * zeta * Xi[q][0];
			phi[4] = 4.0 * Xi[q][0] * Xi[q][1];
			phi[5] = 4.0 * Xi[q][1] * zeta;
			
			double a = Xi[q][0];
			double b = Xi[q][1];
			
			dphi[0][0] = 4.0*a + 4.0*b - 3.0;
			dphi[1][0] = 4.0*a - 1.0;
			dphi[2][0] = 0.0;
			dphi[3][0] = -4.0 * (2.0*a + b - 1.0);
			dphi[4][0] = 4.0*b;
			dphi[5][0] = -4.0*b;
			
			dphi[0][1] = 4.0*a + 4.0*b - 3.0;
			dphi[1][1] = 0.0;
			dphi[2][1] = 4.0*b - 1.0;
			dphi[3][1] = -4.0*a;
			dphi[4][1] = 4.0*a;
			dphi[5][1] = -4.0 * (a + 2.0*b - 1.0);
			
		}
		// Rectangular quadratic surface elements
		else if(surfaceNodeSize == 8) {
			
			phi[0] = 0.25 * (1.0 - Xi[q][0]) * (1.0 - Xi[q][1]) * (-Xi[q][0] - Xi[q][1] - 1.0);
			phi[1] = 0.25 * (1.0 + Xi[q][0]) * (1.0 - Xi[q][1]) * ( Xi[q][0] - Xi[q][1] - 1.0);
			phi[2] = 0.25 * (1.0 + Xi[q][0]) * (1.0 + Xi[q][1]) * ( Xi[q][0] + Xi[q][1] - 1.0);
			phi[3] = 0.25 * (1.0 - Xi[q][0]) * (1.0 + Xi[q][1]) * (-Xi[q][0] + Xi[q][1] - 1.0);
			
			phi[4] = 0.5 * (1.0 - Xi[q][0] * Xi[q][0]) * (1.0 - Xi[q][1]);
			phi[6] = 0.5 * (1.0 - Xi[q][0] * Xi[q][0]) * (1.0 + Xi[q][1]);
			
			phi[5] = 0.5 * (1.0 + Xi[q][0]) * (1.0 - Xi[q][1] * Xi[q][1]);
			phi[7] = 0.5 * (1.0 - Xi[q][0]) * (1.0 - Xi[q][1] * Xi[q][1]);
			
			double a = Xi[q][0];
			double b = Xi[q][1];
			
			dphi[0][0] = -0.25 * (b - 1.0) * (2 * a + b);
			dphi[1][0] = -0.25 * (b - 1.0) * (2 * a - b);
			dphi[2][0] =  0.25 * (b + 1.0) * (2 * a + b);
			dphi[3][0] =  0.25 * (b + 1.0) * (2 * a - b);
			dphi[4][0] =  a * (b - 1.0);
			dphi[5][0] = -0.5 * (b * b - 1.0);
			dphi[6][0] = -a * (b + 1.0);
			dphi[7][0] =  0.5 * (b * b - 1.0);
			
			dphi[0][1] = -0.25 * (a - 1.0) * (a + 2 * b);
			dphi[1][1] = -0.25 * (a + 1.0) * (a - 2 * b);
			dphi[2][1] =  0.25 * (a + 1.0) * (a + 2 * b);
			dphi[3][1] =  0.25 * (a - 1.0) * (a - 2 * b);
			dphi[4][1] =  0.5 * (a * a - 1.0);
			dphi[5][1] = -b * (a + 1.0);
			dphi[6][1] = -0.5 * (a * a - 1.0);
			dphi[7][1] =  b * (a - 1.0);
			
		}
		else {
			Fatal("Surface elements must be composed of 3, 4, 6 or 8 nodes.");
		}
		
		// Surface Jacobian
		double J[2][2];
		for(i = 0; i < 2; i++) {
			for(j = 0; j < 2; j++) {
				J[i][j] = 0.0;
				for(k = 0; k < surfaceNodeSize; k++) {
					J[i][j] += dphi[k][j] * nodesCoord2D[k][i];
				}
			}
		}
		double detJ = J[0][0] * J[1][1] - J[1][0] * J[0][1];
		detJ = fabs(detJ); // WARNING HERE... should always be positive? -yes
		
		area += detJ * wi[q];
		//printf("q: %d, detJ: %e \n", q, detJ);
		
		
		// Integration point position in 2D frame
		double sx2D, sy2D;
		sx2D = sy2D = 0.0;
		for(k = 0; k < surfaceNodeSize; k++) {
			sx2D += phi[k] * nodesCoord2D[k][0];
			sy2D += phi[k] * nodesCoord2D[k][1];
		}
		
		// Integration point position in 3D frame
		double sx, sy, sz;
		double tmp[3];
		tmp[0] = sx2D - frame3D[0][0];
		tmp[1] = sy2D - frame3D[0][1];
		tmp[2] = - frame3D[0][2];
		
		sx = tmp[0] * frame3D[1][0] + tmp[1] * frame3D[1][1] + tmp[2] * frame3D[1][2];
		sy = tmp[0] * frame3D[2][0] + tmp[1] * frame3D[2][1] + tmp[2] * frame3D[2][2];
		sz = tmp[0] * frame3D[3][0] + tmp[1] * frame3D[3][1] + tmp[2] * frame3D[3][2];
		
		//printf("q: %d, s2D: %f %f \n", q, sx2D, sy2D);
		//printf("q: %d, s3D: %f %f %f \n", q, sx, sy, sz);
		
		double pznew;
		double r[2] = {sx, sy};
		InterpolatePressure(home, pz, r, &pznew);
		
		// Integrate traction vectors at each surface node
		for(k = 0; k < surfaceNodeSize; k++) {
			Fz[k] += pznew * phi[k] * detJ * wi[q];
		}
		
	}

}

/*-------------------------------------------------------------------------
 *
 *      Function:     InterpolatePressure
 *
 *-----------------------------------------------------------------------*/
void InterpolatePressure(Home_t *home, double *pz, double r[2], double *pznew)
{
	
	int nx = home->param->hs_nx;
	int ny = home->param->hs_ny;
	
	double HSLx = home->param->Lx;
	double HSLy = home->param->Ly;
	
	// Interoplate stress on grid //
	int i, j, nxg, nyg, nzg, g[2], npd, npe;
	double p[2], q[2], xi[2];
	
	int linear = 0;
	
	if (linear) {
		// Linear interpolation
		nxg = nx;
		nyg = ny;
		npd = 2;
	} else {
		// Quadratic interpolation
		if (nx%2 !=0 || ny%2 != 0) {
			Fatal("Surface grid resolution nx and ny must be multiple of 2 "
				  "for option -D_GRID_INDENTER_STRESS");
		}
		nxg = nx/2;
		nyg = ny/2;
		npd = 3;
	}
	npe = npd*npd;
	
	p[0] = r[0] + 0.5*HSLx;
	p[1] = r[1] + 0.5*HSLy;
	
	q[0] = p[0] / (HSLx / nxg);
	q[1] = p[1] / (HSLy / nyg);
	
	g[0] = (int)floor(q[0]);
	g[1] = (int)floor(q[1]);
	
	xi[0] = 2.0*(q[0]-g[0]) - 1.0;
	xi[1] = 2.0*(q[1]-g[1]) - 1.0;
	
	// Determine elements for interpolation
	// and apply PBC in the x,y directions
	int ind1d[2][npd];
	for (i = 0; i < npd; i++) {
		ind1d[0][i] = ((npd-1)*g[0]+i)%nx;
		if (ind1d[0][i] < 0) ind1d[0][i] += nx;
		ind1d[1][i] = ((npd-1)*g[1]+i)%ny;
		if (ind1d[1][i] < 0) ind1d[1][i] += ny;
	}
	
	// 1d shape functions
	double phi1d[2][npd];
	if (linear) {
		for (i = 0; i < 2; i++) {
			phi1d[i][0] = 0.5*(1.0-xi[i]);
			phi1d[i][1] = 0.5*(1.0+xi[i]);
		}
	} else {
		for (i = 0; i < 2; i++) {
			phi1d[i][0] = 0.5*xi[i]*(xi[i]-1.0);
			phi1d[i][1] = -(xi[i]+1.0)*(xi[i]-1.0);
			phi1d[i][2] = 0.5*xi[i]*(xi[i]+1.0);
		}
	}
	
	// 2d shape functions and indices
	int ind[npe];
	double phi[npe];
	for (j = 0; j < npd; j++) {
		for (i = 0; i < npd; i++) {
			phi[i+j*npd] = phi1d[0][i]*phi1d[1][j];
			ind[i+j*npd] = ind1d[0][i] + ind1d[1][j]*nx;
		}
	}
	
	// Interpolate pressure from the grid points
	double pval = 0.0;
	for (j = 0; j < npe; j++) {
		pval += phi[j] * pz[ind[j]];
	}
	*pznew = pval;
	
	return;
}

/*------------------------------------------------------------------------
 * 	SPDDDInterface::generateAbaqusInputFile
 *		Generate an Abaqus input file in order to perform the mechanical 
 * 		analysis and compute the image stresses using Abaqus
 *----------------------------------------------------------------------*/
void GenerateAbaqusInputFile(Home_t *home, double *fz, int ninc)
{
	int i, j, k, rID;
	double scalefact2;
	
	
	// Scaling factor for Abaqus
	scalefact2 = scalefact*scalefact;
	
	printf("GENERATE ABAQUS INPUT FILE\n");
	
	if(mesh.getNumberOfNodes() == 0) {
		printf("Warning: The mesh has not been generated. Leaving...\n");
		return;
	}
	
	// Create directory for Abaqus simulation
	if(mkdir("ABQ", S_IRWXU) != 0) {
		if (errno == EEXIST) {
			system("exec rm -r ABQ/*");
		} else {
			printf("Error: Impossible to create directory ABQ\n");
			exit(1);
		}
	}
	
	// Write ouput file
	FILE *pFile;
	pFile = fopen("ABQ/paradis_job.inp","w");
	
	// Writing the required headers
	fprintf(pFile, "*Heading\n");
	fprintf(pFile, "Abaqus input file for SProcess / ParaDiS coupling\n");
	fprintf(pFile, "** Job name: Job-1 Model name: Model-1\n");
	fprintf(pFile, "*Preprint, echo=NO, model=NO, history=NO, contact=NO\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "** PARTS\n");
	fprintf(pFile, "**\n");
	
	// Writing nodes and their positions
	fprintf(pFile , "*Node, Nset=NAll\n");
	for (i = 0; i < mesh.getNumberOfNodes(); i++) {
		fprintf(pFile, "%7d,%20.12e,%20.12e,%20.12e\n", i+1, scalefact*mesh.getNodeCoord(i,0),
		        scalefact*mesh.getNodeCoord(i,1), scalefact*mesh.getNodeCoord(i,2));
	}
		
	// Writing elements and their connectivity
	if(mesh.getElementsType() == 0) {
		fprintf(pFile, "*Element, type=C3D8, Elset=ElAll\n");
	} else if (mesh.getElementsType() == 1) {
		fprintf(pFile, "*Element, type=C3D20, Elset=ElAll\n");
	} else if (mesh.getElementsType() == 2) {
		fprintf(pFile, "*Element, type=C3D4, Elset=ElAll\n");
	} else if (mesh.getElementsType() == 3) {
		fprintf(pFile, "*Element, type=C3D10, Elset=ElAll\n");
	}
	for (i = 0; i < mesh.getNumberOfElmts(); i++) {
		fprintf(pFile, "%7d", i + 1);
		for (j = 0; j < mesh.getNumberOfElPerNodes(); j++) {
			if(j == 14)
				fprintf(pFile, ",%7d,\n", mesh.getConnectNode(i,j) + 1);
			else if(j == 15)
				fprintf(pFile, "%7d", mesh.getConnectNode(i,j) + 1);
			else
				fprintf(pFile, ",%7d", mesh.getConnectNode(i,j) + 1);
		}
		fprintf(pFile, "\n");
	}
	
	// Assigning section to the part
	// Assigning all nodes and elements to the "ElAll" set
	fprintf(pFile, "**\n");
	fprintf(pFile, "** Section: Section-1\n");
	fprintf(pFile, "*Solid Section, elset=ElAll, material=Material-1\n");
	fprintf(pFile, ",\n");
	fprintf(pFile, "**\n");
	
	double mu, nu, E;
	mu = home->param->shearModulus;
	nu = home->param->pois;
	E = 2.0*mu*(1.0+nu);
	
	printf("param->shearModulus = %e\n",home->param->shearModulus);
	printf("param->minSideZ = %e\n",home->param->minSideZ);
	printf("param->burgMag = %e\n",home->param->burgMag);
	
	
	// Defining the material properties
	fprintf(pFile, "**\n");
	fprintf(pFile, "** MATERIALS\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Material, name=Material-1\n");
	fprintf(pFile, "*Elastic\n");
	fprintf(pFile, " %e, %f\n", E, nu);
	
	fprintf(pFile, "**\n");
	fprintf(pFile, "** ----------------------------------------------------------------\n");
		
	// Defining the loading step
	fprintf(pFile, "**\n");
	fprintf(pFile, "** STEP: LoadStep\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Step, name=LoadStep\n");
	fprintf(pFile, "*Static\n");
	fprintf(pFile, "1.0, 1.0, 1e-5, 1.0\n");
	fprintf(pFile, "**\n");
	
	// Defining the boundary conditions
	fprintf(pFile, "** BOUNDARY CONDITIONS\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Boundary\n");

	for (i = 0; i < mesh.getNumberOfNodes(); i++) {
		if(mesh.getNodeCoord(i,2) == home->param->minSideZ) {
			fprintf(pFile, "%d, 1\n", i+1);
			fprintf(pFile, "%d, 2\n", i+1);
			fprintf(pFile, "%d, 3\n", i+1);
		}
		#if 0
		if(mesh.getNodeCoord(i,0) == home->param->minSideX ||
		   mesh.getNodeCoord(i,0) == home->param->maxSideX) {
			fprintf(pFile, "%d, 1\n", i+1);
		}
		if(mesh.getNodeCoord(i,1) == home->param->minSideY ||
		   mesh.getNodeCoord(i,1) == home->param->maxSideY) {
			fprintf(pFile, "%d, 2\n", i+1);
		}
		#endif
	}
	
	// Defining the loading
	fprintf(pFile, "**\n");
	fprintf(pFile, "** LOADS\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Cload\n");
	for (i = 0; i < boundary.getNumberOfNodes(); i++) {
		if(fabs(fz[i]) > 1.0e-20) {
			fprintf(pFile, "%d, 3, %e\n", mesh.getSurfToMesh(i) + 1, scalefact2*fz[i]);
		}
	}
	
	// Defining the field and history outputs from Abaqus
	fprintf(pFile, "**\n");
	fprintf(pFile, "** OUTPUT REQUESTS\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Restart, write, frequency=0\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "** FIELD OUTPUT: F-Output-1\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Output, field\n");
	fprintf(pFile, "*Node Output\n");
	fprintf(pFile, "CF, RF, U\n");
	fprintf(pFile, "*Element Output, directions=YES\n");
	fprintf(pFile, "E, S\n");
	fprintf(pFile, "*Output, history, frequency=0\n");
		
	// Defining the desired ASCI format outputs from Abaqus
	fprintf(pFile, "**\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*Node Print\n");
	fprintf(pFile, "U\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*El Print, position=averaged at nodes\n");
	fprintf(pFile, "S\n");
	fprintf(pFile, "**\n");
	fprintf(pFile, "*End Step\n");
	
	fclose(pFile);
}

/*------------------------------------------------------------------------
 * 	SPDDDInterface::runAbaqus
 *		Perform the FEM calculation using Abaqus
 *----------------------------------------------------------------------*/
void RunAbaqus()
{
	char abq_cmd[50];
	
	printf("RUN ABAQUS\n");
	
	// run Abaqus simulation
	if(chdir("ABQ") != 0) 
		printf("Unable to cd into directory ABQ");
		
	//sprintf(abq_cmd, "abaqus job=paradis_job cpus=%d interactive", 2);
	sprintf(abq_cmd, "abaqus job=paradis_job cpus=%d interactive scratch=%s", 1, "/scratch/nbertin");
	printf("%s\n", abq_cmd);
	system(abq_cmd);
	chdir("..");
}

/*------------------------------------------------------------------------
 * 	ReadAbaqusDispResults
 *----------------------------------------------------------------------*/
void ReadAbaqusDispResults(double *uz)
{
	int i, j, ind;
	FILE * pFile;
	double u[3];
	
	int succes;
	char *line = NULL;
	char str[10];
	size_t len = 0;
	ssize_t lineLen;
	
	
	printf("READ ABAQUS OUTPUT FILE\n");
	
	// Initialize nodal displacement data
	mesh.resetNodalDisp();
	
	// Wait for Abaqus job to be completed (wait 10s max after end of the job)
	i = 0;
	succes = 0;
	while (i < 10) {
		if(access("ABQ/paradis_job.sta", F_OK ) == 0) {
			succes = 1;
			break;
		} else {
			sleep (1);
			i++;
		}
	}
	
	// If status fuile has not been generated within 10 sec of job completion
	// this means that Abaqus has terminated with errors
	if (succes == 0) {
		printf("ERROR: Abaqus terminated with errors\n");
		exit(1);
	}
	
	// Read abaqus nodal displacements
	pFile = fopen("ABQ/paradis_job.dat", "r");
	
	succes = 0;
	while ((lineLen = getline(&line, &len, pFile)) != -1) {
		std::string linestr(line);
		if(linestr.substr(0,47).compare("   THE FOLLOWING TABLE IS PRINTED FOR ALL NODES") == 0) {
			for(j = 0; j < 4; j++) lineLen = getline(&line, &len, pFile);
			succes = 1;
			break;
		}
	}
		
	if (succes == 0) {
		printf("ERROR: Nodal stresses could not be read from the Abaqus output file\n");
		exit(1);
	}
	
	// Read the nodal displacements and store them into the data structure
	while (1) {
		if (fscanf(pFile, "%d %lf %lf %lf\n", &ind, u, u+1, u+2) != 4) break;
		u[0] /= scalefact;
		u[1] /= scalefact;
		u[2] /= scalefact;
		mesh.setNodalDisp(ind-1, u);
		if (mesh.getMeshToSurf(ind-1) != -1) {
			uz[mesh.getMeshToSurf(ind-1)] = u[2];
		}
	}
		
	// Check if we have read all the data.
	// If so, close the file and return.
	fscanf(pFile, "%s", str);
	if(strcmp("MAXIMUM", str) != 0) {
		printf("Abaqus output file was not read completely\n");
		exit(1);
	}
	
	fclose(pFile);
}

/*------------------------------------------------------------------------
 * 	SPDDDInterface::readAbaqusResults
 *		Read Abaqus result and store resulting image stresses in the API
 * 		data structure
 *----------------------------------------------------------------------*/
void ReadAbaqusStressResults()
{
	int i, j, ind;
	FILE * pFile;
	double s[6];
	
	int succes;
	char *line = NULL;
	char str[10];
	size_t len = 0;
	ssize_t lineLen;
	
	
	printf("READ ABAQUS OUTPUT FILE\n");
	
	// Initialize nodal stress data
	mesh.resetNodalStresses();
	
	// Wait for Abaqus job to be completed (wait 10s max after end of the job)
	i = 0;
	succes = 0;
	while (i < 10) {
		if(access("ABQ/paradis_job.sta", F_OK ) == 0) {
			succes = 1;
			break;
		} else {
			sleep (1);
			i++;
		}
	}
	
	// If status fuile has not been generated within 10 sec of job completion
	// this means that Abaqus has terminated with errors
	if (succes == 0) {
		printf("ERROR: Abaqus terminated with errors\n");
		exit(1);
	}
	
	// Read abaqus nodal stresses
	pFile = fopen("ABQ/paradis_job.dat", "r");
	
	succes = 0;
	while ((lineLen = getline(&line, &len, pFile)) != -1) {
		std::string linestr(line);
		if(linestr.substr(0,50).compare("   THE FOLLOWING TABLE IS PRINTED FOR ALL ELEMENTS") == 0) {
			for(j = 0; j < 4; j++) lineLen = getline(&line, &len, pFile);
			succes = 1;
			break;
		}
	}
		
	if (succes == 0) {
		printf("ERROR: Nodal stresses could not be read from the Abaqus output file\n");
		exit(1);
	}
	
	// Read the nodal stresses and store them into the data structure
	while (1) {
		if (fscanf(pFile, "%d %lf %lf %lf %lf %lf %lf\n",
		&ind, s, s+1, s+2, s+3, s+5, s+4) != 7) break;
		mesh.setNodalStress(ind-1, s);
	}
		
	// Check if we have read all the data.
	// If so, close the file and return.
	fscanf(pFile, "%s", str);
	if(strcmp("MAXIMUM", str) != 0) {
		printf("Abaqus output file was not read completely\n");
		exit(1);
	}
	
	fclose(pFile);
}

/*------------------------------------------------------------------------
 *	DDDBoundary
 * 		Data structure to store the boundary (surface mesh) of the
 * 		simulation domain
 *----------------------------------------------------------------------*/
DDDBoundary::DDDBoundary() {}

/*------------------------------------------------------------------------
 *	DDDBoundary::AddSurfaceElement
 * 		Add a surface element to the surface mesh structure
 *----------------------------------------------------------------------*/
void DDDBoundary::AddSurfaceElement(std::vector<int>& eNodeSeq, int eType)
{
	bElement.push_back(eNodeSeq);
	bElementType.push_back(eType);
}

/*------------------------------------------------------------------------
 *	DDDBoundary::AddSurfaceElement
 * 		Add a surface node to the surface mesh structure
 *----------------------------------------------------------------------*/
void DDDBoundary::AddSurfaceNode(std::vector<double>& eNode)
{
	bVertex.push_back(eNode);
}

/*------------------------------------------------------------------------
 *	DDDBoundary::rescaleSurfaceMesh
 * 		Rescale the physical size of the surface mesh to match
 * 		with the ParaDiS domain
 *----------------------------------------------------------------------*/
void DDDBoundary::rescaleSurfaceMesh(double min_old[3], double min_new[3], double scale[3])
{
	int i, j;
	
	for(i = 0; i < getNumberOfNodes(); i++) for (j = 0; j < 3; j++) {
		bVertex[i][j] = scale[j] *(bVertex[i][j] - min_old[j]) + min_new[j];
	}
}

/*------------------------------------------------------------------------
 *	DDDMesh
 * 		Mesh data structure to store mesh nodes and connectivity
 * 		when a full mesh is generated or read from the input file
 *----------------------------------------------------------------------*/
DDDMesh::DDDMesh()
{
	nNodes = 0;
	meshSize = 0;
}

/*------------------------------------------------------------------------
 *	DDDMesh::setVolumeSize
 * 		Set the physical size of the simulation domain to mesh
 *----------------------------------------------------------------------*/
void DDDMesh::setVolumeSize(double x_min, double x_max, double y_min,
                            double y_max, double z_min, double z_max)
{
	xmin = x_min; xmax = x_max;
	ymin = y_min; ymax = y_max;
	zmin = z_min; zmax = z_max;
	
	Lx = fabs(xmax - xmin);
	Ly = fabs(ymax - ymin);
	Lz = fabs(zmax - zmin);
}

/*------------------------------------------------------------------------
 *	DDDMesh::rescaleMesh
 * 		Rescale the physical size of the mesh
 *----------------------------------------------------------------------*/
void DDDMesh::rescaleMesh(double x_min, double x_max, double y_min,
                          double y_max, double z_min, double z_max)
{
	int i, j;
	double scale[3], min_new[3], min_old[3];
	
	scale[0] = fabs(x_max - x_min) / Lx;
	scale[1] = fabs(y_max - y_min) / Ly;
	scale[2] = fabs(z_max - z_min) / Lz;
	
	min_old[0] = xmin;
	min_old[1] = ymin;
	min_old[2] = zmin;
	
	min_new[0] = x_min;
	min_new[1] = y_min;
	min_new[2] = z_min;
	
	// Mesh nodes
	for(i = 0; i < nNodes; i++) for (j = 0; j < 3; j++) {
		mNodes[i][j] = scale[j] *(mNodes[i][j] - min_old[j]) + min_new[j];
	}
	
	// Surface nodes
	bObject->rescaleSurfaceMesh(min_old, min_new, scale);
	
	setVolumeSize(x_min, x_max, y_min, y_max, z_min, z_max);
}

/*------------------------------------------------------------------------
 *	DDDMesh::setElementType
 * 		Set the type of element used to generate the mesh
 *----------------------------------------------------------------------*/
void DDDMesh::setElementType(int el_Type)
{
	// 0=C8D8, 1=C3D20
	if(el_Type != 0 && el_Type != 1) {
		printf("Error: Elements must be of type 0 (C3D8) or 1 (C3D20)");
		exit(1);
	}
	elType = el_Type;
}

/*------------------------------------------------------------------------
 *	DDDMesh::setMeshSize
 * 		Set the size of the regular structured mesh
 *----------------------------------------------------------------------*/
void DDDMesh::setMeshSize(int N_x, int N_y, int N_z)
{
	// Mesh size (number of elements per dimension)
	Nx = N_x;
	Ny = N_y;
	Nz = N_z;
}

/*------------------------------------------------------------------------
 *	DDDMesh::setBoundary
 * 		Set the boundary object associated with the full mesh
 *----------------------------------------------------------------------*/
void DDDMesh::setBoundary(DDDBoundary *b_Object)
{
	bObject = b_Object;
}

/*------------------------------------------------------------------------
 *	DDDMesh::generateMesh
 * 		Generate a full regular structured mesh
 *----------------------------------------------------------------------*/
void DDDMesh::generateMesh()
{
	if(elType == 0) {
		// Hexahedral linear mesh (C3D8)
		generateLinearMesh();
	} else if(elType == 1) {
		// Hexahedral quadratic mesh (C3D20)
		generateQuadraticMesh();
	}
}

/*------------------------------------------------------------------------
 *	DDDMesh::generateLinearMesh
 * 		Generate a full regular structured mesh using linear C3D8 elements
 * 		The corresponding surface mesh is stored into the associated 
 * 		boundary object
 *----------------------------------------------------------------------*/
void DDDMesh::generateLinearMesh()
{
	int i, j, k;
	double x, y, z;
	
	printf("\n GENERATE LINEAR MESH \n");
	
	// Node spacing
	Sx = Lx / Nx;
	Sy = Ly / Ny;
	Sz = Lz / Nz;
	
	nElmts = Nx * Ny * Nz;
	
	// Hexahedral linear mesh (C3D8)
	nNodes = (Nx+1)*(Ny+1)*(Nz+1);
#if 0
	nSurfNodes = 2 * (Nx*Ny + Nx*Nz + Ny*Nz + 1);
#else
	nSurfNodes = (Nx+1)*(Ny+1);
#endif
	nElNodes = 8;
	
	// Generate nodes and their coordinates
	int cNodes = 0;
	int sNodes = 0;
	int indNode[Nx+1][Ny+1][Nz+1];
	indMeshToSurf = new int[nNodes];
	indSurfToMesh = new int[nSurfNodes];
	
	for(i = 0; i < nNodes; i++) indMeshToSurf[i] = -1;
	
	for(k = 0; k < Nz + 1; k++) {
		for(j = 0; j < Ny + 1; j++) {
			for(i = 0; i < Nx + 1; i++) {
				
				x = xmin + i * Sx;
				y = ymin + j * Sy;
				z = zmin + k * Sz;
				
				std::vector<double> v;
				v.push_back(x);
				v.push_back(y);
				v.push_back(z);
				mNodes.push_back(v);
				
				// Generate surface boundary
				#if 0
				if(i == 0 || i == Nx || j == 0 || j == Ny || k == 0 || k == Nz) {
					bObject->AddSurfaceNode(v);
					indMeshToSurf[cNodes] = sNodes;
					indSurfToMesh[sNodes] = cNodes;
					sNodes++;
				}
				#else
				if(k == Nz) {
					bObject->AddSurfaceNode(v);
					indMeshToSurf[cNodes] = sNodes;
					indSurfToMesh[sNodes] = cNodes;
					sNodes++;
				}
				#endif
				
				indNode[i][j][k] = cNodes;
				cNodes++;
			}
		}
	}
	
	// Generate connectivity table
	int cElmts = 0;
	connTable = new int[nElNodes * nElmts];
	
	for(k = 0; k < Nz; k++) {
		for(j = 0; j < Ny; j++) {
			for(i = 0; i < Nx; i++) {
				connTable[cElmts*nElNodes + 0] = indNode[i][j][k];
				connTable[cElmts*nElNodes + 1] = indNode[i+1][j][k];
				connTable[cElmts*nElNodes + 2] = indNode[i+1][j+1][k];
				connTable[cElmts*nElNodes + 3] = indNode[i][j+1][k];
				connTable[cElmts*nElNodes + 4] = indNode[i][j][k+1];
				connTable[cElmts*nElNodes + 5] = indNode[i+1][j][k+1];
				connTable[cElmts*nElNodes + 6] = indNode[i+1][j+1][k+1];
				connTable[cElmts*nElNodes + 7] = indNode[i][j+1][k+1];
				cElmts++;
			}
		}
	}
#if 0	
	// Generate surface mesh (C2D4)
	for(k = 0; k < Nz; k++) {
		for(j = 0; j < Ny; j++) {
			
			std::vector<int> vx0;
			vx0.push_back(indMeshToSurf[indNode[0][j][k]]);
			vx0.push_back(indMeshToSurf[indNode[0][j+1][k]]);
			vx0.push_back(indMeshToSurf[indNode[0][j+1][k+1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j][k+1]]);
			bObject->AddSurfaceElement(vx0, 0);
			
			std::vector<int> vx1;
			vx1.push_back(indMeshToSurf[indNode[Nx][j][k]]);
			vx1.push_back(indMeshToSurf[indNode[Nx][j+1][k]]);
			vx1.push_back(indMeshToSurf[indNode[Nx][j+1][k+1]]);
			vx1.push_back(indMeshToSurf[indNode[Nx][j][k+1]]);
			bObject->AddSurfaceElement(vx1, 0);
			
		}
	}
	
	for(k = 0; k < Nz; k++) {
		for(i = 0; i < Nx; i++) {
			
			std::vector<int> vy0;
			vy0.push_back(indMeshToSurf[indNode[i][0][k]]);
			vy0.push_back(indMeshToSurf[indNode[i+1][0][k]]);
			vy0.push_back(indMeshToSurf[indNode[i+1][0][k+1]]);
			vy0.push_back(indMeshToSurf[indNode[i][0][k+1]]);
			bObject->AddSurfaceElement(vy0, 0);
			
			std::vector<int> vy1;
			vy1.push_back(indMeshToSurf[indNode[i][Ny][k]]);
			vy1.push_back(indMeshToSurf[indNode[i+1][Ny][k]]);
			vy1.push_back(indMeshToSurf[indNode[i+1][Ny][k+1]]);
			vy1.push_back(indMeshToSurf[indNode[i][Ny][k+1]]);
			bObject->AddSurfaceElement(vy1, 0);
			
		}
	}
#endif
	for(j = 0; j < Ny; j++) {
		for(i = 0; i < Nx; i++) {
#if 0			
			std::vector<int> vz0;
			vz0.push_back(indMeshToSurf[indNode[i][j][0]]);
			vz0.push_back(indMeshToSurf[indNode[i+1][j][0]]);
			vz0.push_back(indMeshToSurf[indNode[i+1][j+1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i][j+1][0]]);
			bObject->AddSurfaceElement(vz0, 0);
#endif						
			std::vector<int> vz1;
			vz1.push_back(indMeshToSurf[indNode[i][j][Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i+1][j][Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i+1][j+1][Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i][j+1][Nz]]);
			bObject->AddSurfaceElement(vz1, 0);
			
		}
	}
	
}

/*------------------------------------------------------------------------
 *	DDDMesh::generateLinearMesh
 * 		Generate a full regular structured mesh using quadratic C3D20 
 * 		elements
 *  	The corresponding surface mesh is stored into the associated 
 * 		boundary object
 *----------------------------------------------------------------------*/
void DDDMesh::generateQuadraticMesh()
{
	int i, j, k;
	double x, y, z;
	
	printf("\n GENERATE QUADRATIC MESH\n");
	
	// Node spacing
	Sx = Lx / Nx;
	Sy = Ly / Ny;
	Sz = Lz / Nz;
	
	nElmts = Nx * Ny * Nz;
	
	// Hexahedral quadratic mesh (C3D20)
	nNodes = 4*(Nx+1)*(Ny+1)*(Nz+1) -(Nx+1)*(Ny+1) -(Nx+1)*(Nz+1) -(Ny+1)*(Nz+1);
#if 0
	nSurfNodes = 2 * (3*Nx*Ny + 3*Nx*Nz + 3*Ny*Nz + 1);
#else
	nSurfNodes = (Nx+1)*(Ny+1) + Nx*(Ny+1) + (Nx+1)*Ny;
#endif
	nElNodes = 20;
	
	// Generate nodes and their coordinates
	int cNodes = 0;
	int sNodes = 0;
	int indNode[2*Nx+1][2*Ny+1][2*Nz+1];
	indMeshToSurf = new int[nNodes];
	indSurfToMesh = new int[nSurfNodes];
	
	for(i = 0; i < nNodes; i++) indMeshToSurf[i] = -1;
	
	for (i = 0; i <= 2*Nx ; i++) {
		for (j = 0; j <= 2*Ny; j++) {
			for (k = 0; k <= 2*Nz; k++) {
				indNode[i][j][k] = -1;
			}
		}
	}	
	
	for (k = 0; k <= 2*Nz; k++) {
		z = zmin + k * 0.5 * Sz;

		for (j = 0; j <= 2*Ny; j++) {
			if (k%2 == 1 && j%2 == 1) continue;
			y = ymin + j * 0.5 * Sy;
			
			for (i = 0; i <= 2*Nx; i++) {
				if ((j%2 == 1 || k%2 == 1) && (i%2 == 1)) continue;
				x = xmin + i * 0.5 * Sx;
				
				std::vector<double> v;
				v.push_back(x);
				v.push_back(y);
				v.push_back(z);
				mNodes.push_back(v);
				
				// Generate surface boundary
				#if 0
				if(i == 0 || i == 2*Nx || j == 0 || j == 2*Ny || k == 0 || k == 2*Nz) {
					bObject->AddSurfaceNode(v);
					indMeshToSurf[cNodes] = sNodes;
					indSurfToMesh[sNodes] = cNodes;
					sNodes++;
				}
				#else
				if(k == 2*Nz) {
					bObject->AddSurfaceNode(v);
					indMeshToSurf[cNodes] = sNodes;
					indSurfToMesh[sNodes] = cNodes;
					sNodes++;
				}
				#endif
				
				indNode[i][j][k] = cNodes;
				cNodes++;
			}
		}
	}
	
	// Generate connectivity table
	int cElmts = 0;
	connTable = new int[nElNodes * nElmts];
	
	for (k = 1; k < 2*Nz; k += 2) {
		for (j = 1; j < 2*Ny; j += 2) {
			for (i = 1; i < 2*Nx; i += 2) {
				
				connTable[cElmts*nElNodes + 0] = indNode[i-1][j-1][k-1];
				connTable[cElmts*nElNodes + 1] = indNode[i+1][j-1][k-1];
				connTable[cElmts*nElNodes + 2] = indNode[i+1][j+1][k-1];
				connTable[cElmts*nElNodes + 3] = indNode[i-1][j+1][k-1];
				connTable[cElmts*nElNodes + 4] = indNode[i-1][j-1][k+1];
				connTable[cElmts*nElNodes + 5] = indNode[i+1][j-1][k+1];
				connTable[cElmts*nElNodes + 6] = indNode[i+1][j+1][k+1];
				connTable[cElmts*nElNodes + 7] = indNode[i-1][j+1][k+1];
				
				connTable[cElmts*nElNodes + 8] = indNode[i][j-1][k-1];
				connTable[cElmts*nElNodes + 9] = indNode[i+1][j][k-1];
				connTable[cElmts*nElNodes + 10] = indNode[i][j+1][k-1];
				connTable[cElmts*nElNodes + 11] = indNode[i-1][j][k-1];
				connTable[cElmts*nElNodes + 12] = indNode[i][j-1][k+1];
				connTable[cElmts*nElNodes + 13] = indNode[i+1][j][k+1];
				connTable[cElmts*nElNodes + 14] = indNode[i][j+1][k+1];
				connTable[cElmts*nElNodes + 15] = indNode[i-1][j][k+1];
				connTable[cElmts*nElNodes + 16] = indNode[i-1][j-1][k];
				connTable[cElmts*nElNodes + 17] = indNode[i+1][j-1][k];
				connTable[cElmts*nElNodes + 18] = indNode[i+1][j+1][k];
				connTable[cElmts*nElNodes + 19] = indNode[i-1][j+1][k];
				
				cElmts++;
			}
		}
	}
#if 0	
	// Generate surface mesh (C2D8)
	for (k = 1; k < 2*Nz; k += 2) {
		for (j = 1; j < 2*Ny; j += 2) {
			
			std::vector<int> vx0;
			vx0.push_back(indMeshToSurf[indNode[0][j-1][k-1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j+1][k-1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j+1][k+1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j-1][k+1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j][k-1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j+1][k]]);
			vx0.push_back(indMeshToSurf[indNode[0][j][k+1]]);
			vx0.push_back(indMeshToSurf[indNode[0][j-1][k]]);
			bObject->AddSurfaceElement(vx0, 0);
			
			std::vector<int> vx1;
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j-1][k-1]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j+1][k-1]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j+1][k+1]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j-1][k+1]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j][k-1]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j+1][k]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j][k+1]]);
			vx1.push_back(indMeshToSurf[indNode[2*Nx][j-1][k]]);
			bObject->AddSurfaceElement(vx1, 0);
		}
	}
	
	for (k = 1; k < 2*Nz; k += 2) {
		for (i = 1; i < 2*Nx; i += 2) {
			
			std::vector<int> vy0;
			vy0.push_back(indMeshToSurf[indNode[i-1][0][k-1]]);
			vy0.push_back(indMeshToSurf[indNode[i+1][0][k-1]]);
			vy0.push_back(indMeshToSurf[indNode[i+1][0][k+1]]);
			vy0.push_back(indMeshToSurf[indNode[i-1][0][k+1]]);
			vy0.push_back(indMeshToSurf[indNode[i][0][k-1]]);
			vy0.push_back(indMeshToSurf[indNode[i+1][0][k]]);
			vy0.push_back(indMeshToSurf[indNode[i][0][k+1]]);
			vy0.push_back(indMeshToSurf[indNode[i-1][0][k]]);
			bObject->AddSurfaceElement(vy0, 0);
			
			std::vector<int> vy1;
			vy1.push_back(indMeshToSurf[indNode[i-1][2*Ny][k-1]]);
			vy1.push_back(indMeshToSurf[indNode[i+1][2*Ny][k-1]]);
			vy1.push_back(indMeshToSurf[indNode[i+1][2*Ny][k+1]]);
			vy1.push_back(indMeshToSurf[indNode[i-1][2*Ny][k+1]]);
			vy1.push_back(indMeshToSurf[indNode[i][2*Ny][k-1]]);
			vy1.push_back(indMeshToSurf[indNode[i+1][2*Ny][k]]);
			vy1.push_back(indMeshToSurf[indNode[i][2*Ny][k+1]]);
			vy1.push_back(indMeshToSurf[indNode[i-1][2*Ny][k]]);
			bObject->AddSurfaceElement(vy1, 0);
			
		}
	}
#endif	
	for (j = 1; j < 2*Ny; j += 2) {
		for (i = 1; i < 2*Nx; i += 2) {
#if 0			
			std::vector<int> vz0;
			vz0.push_back(indMeshToSurf[indNode[i-1][j-1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i+1][j-1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i+1][j+1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i-1][j+1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i][j-1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i+1][j][0]]);
			vz0.push_back(indMeshToSurf[indNode[i][j+1][0]]);
			vz0.push_back(indMeshToSurf[indNode[i-1][j][0]]);
			bObject->AddSurfaceElement(vz0, 0);
#endif			
			std::vector<int> vz1;
			vz1.push_back(indMeshToSurf[indNode[i-1][j-1][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i+1][j-1][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i+1][j+1][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i-1][j+1][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i][j-1][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i+1][j][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i][j+1][2*Nz]]);
			vz1.push_back(indMeshToSurf[indNode[i-1][j][2*Nz]]);
			bObject->AddSurfaceElement(vz1, 0);
			
		}
	}

}

/*------------------------------------------------------------------------
 *	DDDMesh::resetNodalStresses
 * 		Reset stress values at each mesh node
 *----------------------------------------------------------------------*/
void DDDMesh::resetNodalStresses()
{
	int i;
	
	sNodes.clear();
	for(i = 0; i < nNodes; i++) {
		std::vector<double> zero(6,0.0);
		sNodes.push_back(zero);
	}
}

/*------------------------------------------------------------------------
 *	DDDMesh::setNodalStress
 * 		Set stress tensor at a given mesh node
 *----------------------------------------------------------------------*/
void DDDMesh::setNodalStress(const int nIndex, double s[6])
{
	//components are input and stored as: Sxx,Syy,Szz,Sxy,Syz,Szx
	
	sNodes[nIndex][0] = s[0];
	sNodes[nIndex][1] = s[1];
	sNodes[nIndex][2] = s[2];
	sNodes[nIndex][3] = s[3];
	sNodes[nIndex][4] = s[4];
	sNodes[nIndex][5] = s[5];
}

/*------------------------------------------------------------------------
 *	DDDMesh::resetNodalDisp
 *----------------------------------------------------------------------*/
void DDDMesh::resetNodalDisp()
{
	int i;
	
	uNodes.clear();
	for(i = 0; i < nNodes; i++) {
		std::vector<double> zero(3,0.0);
		uNodes.push_back(zero);
	}
}

/*------------------------------------------------------------------------
 *	DDDMesh::setNodalDisp
 *----------------------------------------------------------------------*/
void DDDMesh::setNodalDisp(const int nIndex, double u[3])
{
	uNodes[nIndex][0] = u[0];
	uNodes[nIndex][1] = u[1];
	uNodes[nIndex][2] = u[2];
}

/*------------------------------------------------------------------------
 *	DDDMesh::shapeFunction
 * 		Returns the value of the interpolation function
 *----------------------------------------------------------------------*/
std::vector<double> DDDMesh::shapeFunction(const std::vector<double> xi)
{
	std::vector<double> phi(nElNodes, 0.0);
	
	if(elType == 0) {
		phi[0] = (1.0-xi[0]) * (1.0-xi[1]) * (1.0-xi[2]) / 8.0;
		phi[1] = (1.0+xi[0]) * (1.0-xi[1]) * (1.0-xi[2]) / 8.0;
		phi[2] = (1.0+xi[0]) * (1.0+xi[1]) * (1.0-xi[2]) / 8.0;
		phi[3] = (1.0-xi[0]) * (1.0+xi[1]) * (1.0-xi[2]) / 8.0;
		phi[4] = (1.0-xi[0]) * (1.0-xi[1]) * (1.0+xi[2]) / 8.0;
		phi[5] = (1.0+xi[0]) * (1.0-xi[1]) * (1.0+xi[2]) / 8.0;
		phi[6] = (1.0+xi[0]) * (1.0+xi[1]) * (1.0+xi[2]) / 8.0;
		phi[7] = (1.0-xi[0]) * (1.0+xi[1]) * (1.0+xi[2]) / 8.0;
	}
	else if(elType == 1) {
		phi[0] = (1.0-xi[0]) * (1.0-xi[1]) * (1.0-xi[2]) * (-xi[0]-xi[1]-xi[2]-2.0) / 8.0;
		phi[1] = (1.0+xi[0]) * (1.0-xi[1]) * (1.0-xi[2]) * ( xi[0]-xi[1]-xi[2]-2.0) / 8.0;
		phi[2] = (1.0+xi[0]) * (1.0+xi[1]) * (1.0-xi[2]) * ( xi[0]+xi[1]-xi[2]-2.0) / 8.0;
		phi[3] = (1.0-xi[0]) * (1.0+xi[1]) * (1.0-xi[2]) * (-xi[0]+xi[1]-xi[2]-2.0) / 8.0;
		phi[4] = (1.0-xi[0]) * (1.0-xi[1]) * (1.0+xi[2]) * (-xi[0]-xi[1]+xi[2]-2.0) / 8.0;
		phi[5] = (1.0+xi[0]) * (1.0-xi[1]) * (1.0+xi[2]) * ( xi[0]-xi[1]+xi[2]-2.0) / 8.0;
		phi[6] = (1.0+xi[0]) * (1.0+xi[1]) * (1.0+xi[2]) * ( xi[0]+xi[1]+xi[2]-2.0) / 8.0;
		phi[7] = (1.0-xi[0]) * (1.0+xi[1]) * (1.0+xi[2]) * (-xi[0]+xi[1]+xi[2]-2.0) / 8.0;
		
		phi[8]  = (1.0-xi[0]*xi[0]) * (1.0-xi[1]) * (1.0-xi[2]) / 4.0;
		phi[9]  = (1.0+xi[0]) * (1.0-xi[1]*xi[1]) * (1.0-xi[2]) / 4.0;
		phi[10] = (1.0-xi[0]*xi[0]) * (1.0+xi[1]) * (1.0-xi[2]) / 4.0;
		phi[11] = (1.0-xi[0]) * (1.0-xi[1]*xi[1]) * (1.0-xi[2]) / 4.0;
		
		phi[12] = (1.0-xi[0]*xi[0]) * (1.0-xi[1]) * (1.0+xi[2]) / 4.0;
		phi[13] = (1.0+xi[0]) * (1.0-xi[1]*xi[1]) * (1.0+xi[2]) / 4.0;
		phi[14] = (1.0-xi[0]*xi[0]) * (1.0+xi[1]) * (1.0+xi[2]) / 4.0;
		phi[15] = (1.0-xi[0]) * (1.0-xi[1]*xi[1]) * (1.0+xi[2]) / 4.0;
		
		phi[16] = (1.0-xi[0]) * (1.0-xi[1]) * (1.0-xi[2]*xi[2]) / 4.0;
		phi[17] = (1.0+xi[0]) * (1.0-xi[1]) * (1.0-xi[2]*xi[2]) / 4.0;
		phi[18] = (1.0+xi[0]) * (1.0+xi[1]) * (1.0-xi[2]*xi[2]) / 4.0;
		phi[19] = (1.0-xi[0]) * (1.0+xi[1]) * (1.0-xi[2]*xi[2]) / 4.0;
	}
	else {
		printf("ERROR: Shape functions for element type %d have not been implemented yet.\n", elType);
		exit(1);
	}
	
	return phi;
}

/*------------------------------------------------------------------------
 *	DDDMesh::findElementIndex
 * 		Finds the element in which a point resides
 *----------------------------------------------------------------------*/
int DDDMesh::findElementIndex(const std::vector<double> v)
{
	int i, k[3], eIndex;
	double p[3], r[3], sp[3];
	
	if(elType == 0 || elType == 1) {
	
		sp[0] = Sx;
		sp[1] = Sy;
		sp[2] = Sz;
		
		p[0] = v[0] - xmin;
		p[1] = v[1] - ymin;
		p[2] = v[2] - zmin;
		
		for(i = 0; i < 3; i++) {
			r[i] = p[i] / sp[i];
			k[i] = (int)floor(r[i]);
		}
		eIndex = k[2]*Nx*Ny + k[1]*Nx + k[0];
	}
	else {
		printf("ERROR: findElementIndex for element type %d has not been implemented yet.\n", elType);
		exit(1);
	}
	
	return eIndex;
}

/*------------------------------------------------------------------------
 *	DDDMesh::interpolateStress
 * 		Returns the stress tensor at a given point in the domain by
 * 		interpolation of nodal stress values
 * 		output: Sxx,Syy,Szz,Sxy,Syz,Szx
 *----------------------------------------------------------------------*/
std::vector<double> DDDMesh::interpolateStress(const std::vector<double> v)
{
	int i, j, k[3], eIndex;
	double p[3], r[3], sp[3];
	std::vector<double> xi;
	std::vector<double> phi;
	std::vector<double> svec(6, 0.0);
	
	// Find element in which v resides
	if(elType == 0 || elType == 1) {
		
		sp[0] = Sx;
		sp[1] = Sy;
		sp[2] = Sz;
		
		p[0] = v[0] - xmin;
		p[1] = v[1] - ymin;
		p[2] = v[2] - zmin;
		
		for(i = 0; i < 3; i++) {
			r[i] = p[i] / sp[i];
			k[i] = (int)floor(r[i]);
			xi.push_back(2.0 * (r[i] - k[i]) - 1.0); 
		}
		
		// Edge exceptions (only matters if no PBC)
		if(k[0] == Nx) { k[0]--; xi[0] = 1.0; }
		if(k[1] == Ny) { k[1]--; xi[1] = 1.0; }
		if(k[2] == Nz) { k[2]--; xi[2] = 1.0; }
		
		eIndex = k[2]*Nx*Ny + k[1]*Nx + k[0];
	}
	else {
		printf("ERROR: findElementIndex for element type %d has not been implemented yet.\n", elType);
		exit(1);
	}
	
	// Shape functions
	phi = shapeFunction(xi);
	
	// Interpolate stress between mesh nodes
	for(i = 0; i < 6; i++) {
		svec[i] = 0.0;
		for(j = 0; j < nElNodes; j++) {
			svec[i] += phi[j] * getNodalStress(getConnectNode(eIndex, j))[i];
		}
	}
	
	return svec;
}

/*------------------------------------------------------------------------
 *	DDDMesh::interpolateDisp
 *----------------------------------------------------------------------*/
std::vector<double> DDDMesh::interpolateDisp(const std::vector<double> v)
{
	int i, j, k[3], eIndex;
	double p[3], r[3], sp[3];
	std::vector<double> xi;
	std::vector<double> phi;
	std::vector<double> uvec(3, 0.0);
	
	// Find element in which v resides
	if(elType == 0 || elType == 1) {
		
		sp[0] = Sx;
		sp[1] = Sy;
		sp[2] = Sz;
		
		p[0] = v[0] - xmin;
		p[1] = v[1] - ymin;
		p[2] = v[2] - zmin;
		
		for(i = 0; i < 3; i++) {
			r[i] = p[i] / sp[i];
			k[i] = (int)floor(r[i]);
			xi.push_back(2.0 * (r[i] - k[i]) - 1.0); 
		}
		
		// Edge exceptions (only matters if no PBC)
		if(k[0] == Nx) { k[0]--; xi[0] = 1.0; }
		if(k[1] == Ny) { k[1]--; xi[1] = 1.0; }
		if(k[2] == Nz) { k[2]--; xi[2] = 1.0; }
		
		eIndex = k[2]*Nx*Ny + k[1]*Nx + k[0];
	}
	else {
		printf("ERROR: findElementIndex for element type %d has not been implemented yet.\n", elType);
		exit(1);
	}
	
	// Shape functions
	phi = shapeFunction(xi);
	
	// Interpolate stress between mesh nodes
	for(i = 0; i < 3; i++) {
		uvec[i] = 0.0;
		for(j = 0; j < nElNodes; j++) {
			uvec[i] += phi[j] * getNodalDisp(getConnectNode(eIndex, j))[i];
		}
	}
	
	return uvec;
}

/*------------------------------------------------------------------------
 *	DDDMesh::deleteMesh
 * 		Deallocate mesh structure
 *----------------------------------------------------------------------*/
void DDDMesh::deleteMesh()
{
	if (connTable != NULL) delete[] connTable;
	if (indSurfToMesh != NULL) delete[] indSurfToMesh;
}

/*------------------------------------------------------------------------
 *	DDDMesh::isOnMeshSurface
 * 		Query if an arbitrary coordinate is on the surface of the mesh
 *----------------------------------------------------------------------*/
bool DDDMesh::isOnMeshSurface(const std::vector<double> v)
{
	// check with bounding box
	if (((v[0] == xmin) || (v[0] == xmax)) &&
	    (v[1] >= ymin) && (v[1] <= ymax) &&
	    (v[2] >= zmin) && (v[2] <= zmax) )
		return true;
	if (((v[1] == ymin) || (v[1] == ymax)) &&
	    (v[0] >= xmin) && (v[0] <= xmax) &&
	    (v[2] >= zmin) && (v[2] <= zmax) )
		return true;
	if (((v[2] == zmin) || (v[2] == zmax)) &&
	    (v[0] >= xmin) && (v[0] <= xmax) &&
	    (v[1] >= ymin) && (v[1] <= ymax) )
		return true;
	return false;
}

#endif
