from ctypes import *
import numpy as np
import sys, os

try: from mpl_toolkits.mplot3d import Axes3D
except ImportError: print("cannot import Axes3D")

try: from mpl_toolkits.mplot3d.art3d import Line3DCollection
except ImportError: print("cannot import Line3DCollection")

import matplotlib.pyplot as plt
import re
from ctypes import c_double
real8 = c_double

class constraints:
    UNCONSTRAINED         = 0
    SURFACE_NODE          = 1
    THINFILM_SURFACE_NODE = 6
    CYLINDER_SURFACE_NODE = 6
    PINNED_NODE           = 7

class stages:
    STAGE_INIT  = 1
    STAGE_CYCLE = 2
    STAGE_TERM  = 3

class optype:
    CHANGE_CONNECTION = 0
    INSERT_ARM        = 1
    REMOVE_NODE       = 2
    CHANGE_ARM_BURG   = 3
    SPLIT_NODE        = 4
    RESET_COORD       = 5
    RESET_SEG_FORCES  = 6
    RESET_SEG_FORCES2 = 7
    MARK_FORCES_OBSOLETE = 8
    RESET_GLIDE_PLANE = 9
    START_STEP        = 10
    TIME_INTEGRATION  = 11
    START_SPLIT_MULTI = 12
    START_CROSS_SLIP  = 13
    START_COLLISION   = 14
    START_REMESH      = 15

# from include/Constants.h
class gids:
    FULL   = 2
    GROUP0 = 10
    GROUP1 = 11
    GROUP2 = 12
    GROUP3 = 13
    GROUP4 = 14

class bcolors:
    RED = '\033[31m'
    GRN = '\033[32m'
    YEL = '\033[33m'
    BLU = '\033[34m'
    MAG = '\033[35m'
    CYN = '\033[36m'
    BOLD = '\033[1m'
    RESET = '\033[0m'
    UNDERLINE = '\033[4m'

class mat_type:
    FCC = 0
    BCC = 1
    HCP = 2

class ParaDiS():
    # inherit all contents in home_lib
    def __init__(self, home_lib):
        for item in home_lib.__dict__:
            self.__dict__[item] = home_lib.__dict__[item]

    # this function has the limit that the data file and control file must have the same name.
    def paradis_init(self, taskname):
        workdir = os.getcwd()
        paradis_ctrlfile =  os.path.join(workdir, taskname + '.ctrl')
        paradis_datafile =  os.path.join(workdir, taskname + '.data')

        if not os.path.exists(paradis_ctrlfile) or not os.path.exists(paradis_datafile):
            print("Paradis input files missing.")
            exit(0)

        paradis_bin = 'paradis'
        self.home = POINTER(self.Home_t)()
        # for preparing argv see https://stackoverflow.com/questions/58598012/ctypes-errors-with-argv
        LP_c_char = POINTER(c_char)
        LP_LP_c_char = POINTER(LP_c_char)
        argv = (LP_c_char*2)(create_string_buffer(paradis_bin.encode('utf-8')), create_string_buffer(paradis_ctrlfile.encode('utf-8')))
        self.ParadisInit(len(argv),cast(argv,LP_LP_c_char),byref(self.home))

        p_memSize = POINTER(c_int)
        p_param = self.home.contents.param
        self.home.contents.cycle = p_param.contents.cycleStart
        self.cycleEnd = p_param.contents.cycleStart + p_param.contents.maxstep
        self.initialDLBCycles = p_param.contents.numDLBCycles

        return self.home

    def paradis_init_argv(self, argv):
        argv = ['paradis'] + argv

        self.home = POINTER(self.Home_t)()
        LP_c_char = POINTER(c_char)
        LP_LP_c_char = POINTER(LP_c_char)
        p = (LP_c_char*len(argv))()
        for i, arg in enumerate(argv):
            enc_arg = arg.encode('utf-8')
            p[i] = self.ctypes.create_string_buffer(enc_arg)
        na = self.ctypes.cast(p, LP_LP_c_char)

        self.ParadisInit(len(argv),cast(na,LP_LP_c_char),byref(self.home))

        p_memSize = POINTER(c_int)
        p_param = self.home.contents.param
        self.home.contents.cycle = p_param.contents.cycleStart
        cycleEnd = p_param.contents.cycleStart + p_param.contents.maxstep
        initialDLBCycles = p_param.contents.numDLBCycles

        return self.home

    def hs_init(self, home):
        halfspace = POINTER(self.HalfSpace_t)()
        self.HS_Init(home,byref(halfspace))
        return halfspace

    def extract_nodal_force_vel(self, home):
        force_vel_array = np.empty((0,7), dtype=np.double)
        for key in range(home.contents.newNodeKeyPtr):
            node = home.contents.nodeKeys[key]
            if node: # node is not NULL
                force_vel_array = np.append(force_vel_array, np.array([[node.contents.myTag.index,
                                        node.contents.fX,node.contents.fY,node.contents.fZ,
                                        node.contents.vX,node.contents.vY,node.contents.vZ]]),axis=0)
        return force_vel_array


    def write_force_vel_array_to_file(self, data_fname, force_vel_array):
        with open(data_fname, 'w') as f:
            for i in range(force_vel_array.shape[0]):
                f.write("%3d %19.12e %19.12e %19.12e %19.12e %19.12e %19.12e\n"%
                        (int(force_vel_array[i][0]),
                         force_vel_array[i][1], force_vel_array[i][2], force_vel_array[i][3],
                         force_vel_array[i][4], force_vel_array[i][5], force_vel_array[i][6] ))

    def compute_seg_stress_coord_dep(self, p1, p2, b, x, mu, nu, a):
        """
        dislocation segment from p1 to p2 with Burgers vector b
        field point at x
        """
        sigma=np.ctypeslib.as_ctypes(np.zeros((3,3), dtype=real8))
        self.SegmentStress(
            *(mu, nu),
            *(b[0], b[1], b[2]),
            *(p1[0], p1[1], p1[2]),
            *(p2[0], p2[1], p2[2]),
            *(x[0], x[1], x[2]),
            a,
            sigma
        )
        return np.array(sigma)

    def compute_seg_stress_coord_indep(self, p1, p2, b, x, mu, nu, a):
        """
        dislocation segment from p1 to p2 with Burgers vector b
        field point at x
        """
        sigma_vec=np.ctypeslib.as_ctypes(np.zeros((6), dtype=real8))
        self.StressDueToSeg(
            *(x[0], x[1], x[2]),
            *(p1[0], p1[1], p1[2]),
            *(p2[0], p2[1], p2[2]),
            *(b[0], b[1], b[2]),
            *(a, mu, nu),
            sigma_vec
        )
        sigma = np.array([ [sigma_vec[0], sigma_vec[3], sigma_vec[5]],
                           [sigma_vec[3], sigma_vec[1], sigma_vec[4]],
                           [sigma_vec[5], sigma_vec[4], sigma_vec[2]] ])
        return np.array(sigma)

    def compute_segseg_force_SBA(self, p1, p2, p3, p4, b1, b2, mu, nu, a, seg12local=1, seg34local=1):
        """
        dislocation segment from p1 to p2 with Burgers vector b1
        dislocation segment from p3 to p4 with Burgers vector b2
        """
        f1x, f1y, f1z = real8(), real8(), real8()
        f2x, f2y, f2z = real8(), real8(), real8()
        f3x, f3y, f3z = real8(), real8(), real8()
        f4x, f4y, f4z = real8(), real8(), real8()
        self.SegSegForceIsotropic(
            *(p1[0], p1[1], p1[2]),
            *(p2[0], p2[1], p2[2]),
            *(p3[0], p3[1], p3[2]),
            *(p4[0], p4[1], p4[2]),
            *(b1[0], b1[1], b1[2]),
            *(b2[0], b2[1], b2[2]),
            *(a, mu, nu),
            *(seg12local, seg34local),
            *(f1x, f1y, f1z),
            *(f2x, f2y, f2z),
            *(f3x, f3y, f3z),
            *(f4x, f4y, f4z),
        )
        f1 = np.array([f1x.value, f1y.value, f1z.value])
        f2 = np.array([f2x.value, f2y.value, f2z.value])
        f3 = np.array([f3x.value, f3y.value, f3z.value])
        f4 = np.array([f4x.value, f4y.value, f4z.value])
        return f1, f2, f3, f4

    def compute_segseg_force_SBA_vec(self, p1_list, p2_list, p3_list, p4_list,
                                  b1_list, b2_list, mu, nu, a, seg12local=1, seg34local=1 ):
        # to do: seg12local and seg34local should be arrays
        f1_list = np.empty_like(p1_list)
        f2_list = np.empty_like(p2_list)
        f3_list = np.empty_like(p3_list)
        f4_list = np.empty_like(p4_list)

        for ii, (p1, p2, p3, p4, b1, b2) in enumerate( zip(p1_list, p2_list, p3_list, p4_list, b1_list, b2_list) ):
            f1_list[ii], f2_list[ii], f3_list[ii], f4_list[ii] = self.compute_segseg_force_SBA(
                           p1, p2, p3, p4, b1, b2, mu, nu, a, seg12local, seg34local )

        return f1_list, f2_list, f3_list, f4_list

    def compute_segseg_force_SBN1_SBA(self, p1, p2, p3, p4, b1, b2, mu, nu, a, Nint, seg12local=1, seg34local=1):
        """
        dislocation segment from p1 to p2 with Burgers vector b1
        dislocation segment from p3 to p4 with Burgers vector b2
        """
        f1x, f1y, f1z = real8(), real8(), real8()
        f2x, f2y, f2z = real8(), real8(), real8()
        f3x, f3y, f3z = real8(), real8(), real8()
        f4x, f4y, f4z = real8(), real8(), real8()
        self.SegSegForce_SBN1_SBA(
            *(p1[0], p1[1], p1[2]),
            *(p2[0], p2[1], p2[2]),
            *(p3[0], p3[1], p3[2]),
            *(p4[0], p4[1], p4[2]),
            *(b1[0], b1[1], b1[2]),
            *(b2[0], b2[1], b2[2]),
            *(a, mu, nu, Nint),
            *(seg12local, seg34local),
            *(f1x, f1y, f1z),
            *(f2x, f2y, f2z),
            *(f3x, f3y, f3z),
            *(f4x, f4y, f4z),
        )
        f1 = np.array([f1x.value, f1y.value, f1z.value])
        f2 = np.array([f2x.value, f2y.value, f2z.value])
        f3 = np.array([f3x.value, f3y.value, f3z.value])
        f4 = np.array([f4x.value, f4y.value, f4z.value])
        return f1, f2, f3, f4

    def compute_segseg_force_SBN1_SBA_vec(self, p1_list, p2_list, p3_list, p4_list,
                                  b1_list, b2_list, mu, nu, a, Nint, seg12local=1, seg34local=1 ):
        # to do: seg12local and seg34local should be arrays
        f1_list = np.empty_like(p1_list)
        f2_list = np.empty_like(p2_list)
        f3_list = np.empty_like(p3_list)
        f4_list = np.empty_like(p4_list)

        for ii, (p1, p2, p3, p4, b1, b2) in enumerate( zip(p1_list, p2_list, p3_list, p4_list, b1_list, b2_list) ):
            f1_list[ii], f2_list[ii], f3_list[ii], f4_list[ii] = self.compute_segseg_force_SBN1_SBA(
                           p1, p2, p3, p4, b1, b2, mu, nu, a, Nint, seg12local, seg34local )

        return f1_list, f2_list, f3_list, f4_list






# this function will be removed later, use class ParaDiS instead
# this function has the limit that the data file and control file must have the same name.
def paradis_init(taskname, Home_t, ParadisInit):
    workdir = os.getcwd() 
    paradis_ctrlfile =  os.path.join(workdir, taskname + '.ctrl')
    paradis_datafile =  os.path.join(workdir, taskname + '.data')

    if not os.path.exists(paradis_ctrlfile) or not os.path.exists(paradis_datafile):    
        print("Paradis input files missing.")
        exit(0)

    paradis_bin = 'paradis'
    home = POINTER(Home_t)()
    # for preparing argv see https://stackoverflow.com/questions/58598012/ctypes-errors-with-argv
    LP_c_char = POINTER(c_char)
    LP_LP_c_char = POINTER(LP_c_char) 
    argv = (LP_c_char*2)(create_string_buffer(paradis_bin.encode('utf-8')), create_string_buffer(paradis_ctrlfile.encode('utf-8')))
    ParadisInit(len(argv),cast(argv,LP_LP_c_char),byref(home))

    p_memSize = POINTER(c_int)
    p_param = home.contents.param
    home.contents.cycle = p_param.contents.cycleStart
    cycleEnd = p_param.contents.cycleStart + p_param.contents.maxstep
    initialDLBCycles = p_param.contents.numDLBCycles

    return home

# this function will be removed later, use class ParaDiS instead
def paradis_init_argv(argv, Home_t, ParadisInit):
    argv = ['paradis'] + argv

    home = POINTER(Home_t)()
    LP_c_char = POINTER(c_char)
    LP_LP_c_char = POINTER(LP_c_char)
    p = (LP_c_char*len(argv))()
    for i, arg in enumerate(argv):
        enc_arg = arg.encode('utf-8')
        p[i] = ctypes.create_string_buffer(enc_arg)
    na = ctypes.cast(p, LP_LP_c_char)

    ParadisInit(len(argv),cast(na,LP_LP_c_char),byref(home))

    p_memSize = POINTER(c_int)
    p_param = home.contents.param
    home.contents.cycle = p_param.contents.cycleStart
    cycleEnd = p_param.contents.cycleStart + p_param.contents.maxstep
    initialDLBCycles = p_param.contents.numDLBCycles

    return home

def convert_nodes_to_dict(home, material_type=mat_type.FCC):
    print("convert nodes in current configuration to dictionary format")

    nodes = {} # initialize an empty dictionary
    for key in range(home.contents.newNodeKeyPtr):
        node = home.contents.nodeKeys[key]
        if node: # node is not NULL
            numNbrs = node.contents.numNbrs
            my_tag = tuple([node.contents.myTag.domainID, node.contents.myTag.index])
            my_coord = np.array([node.contents.x, node.contents.y, node.contents.z, node.contents.constraint])
            arms_dict = {}
            for i in range(numNbrs):
                nbr_tag = tuple([node.contents.nbrTag[i].domainID, node.contents.nbrTag[i].index])
                # Oct 2nd, 2022, Jae-Hoon Choi: added slip system in arm_data to match the format of node dictionary with read_nodes_to_dict()
                armsysID = which_slipSystem( np.array( [ node.contents.burgX[i], node.contents.burgY[i], node.contents.burgZ[i] ] ) ,
                                             np.array( [ node.contents.nx[i],    node.contents.ny[i],    node.contents.nz[i]    ] ) ,
                                             material_type )[0]
                arm_data = np.array([node.contents.burgX[i], node.contents.burgY[i], node.contents.burgZ[i], 
                                     node.contents.nx[i],    node.contents.ny[i],    node.contents.nz[i],    armsysID   ])
                arms_dict[nbr_tag] = arm_data
            nodes[my_tag] = [my_coord, arms_dict]

    return nodes

def find_bounds_from_param(param):
    bounds = np.array([[param.contents.minCoordinates[0],
                        param.contents.minCoordinates[1],
                        param.contents.minCoordinates[2]],
                       [param.contents.maxCoordinates[0],
                        param.contents.maxCoordinates[1],
                        param.contents.maxCoordinates[2]]])
    return bounds


def pbc_position_param(param, r1, r2, PBCPOSITION):
    c_x2, c_y2, c_z2 = c_double(r2[0]), c_double(r2[1]), c_double(r2[2])
    PBCPOSITION(param, r1[0], r1[1], r1[2], c_x2, c_y2, c_z2)
    return np.array([c_x2.value, c_y2.value, c_z2.value])

def pbc_position_L(r1, r2, L):
    ds = (r2-r1)/L
    ds = ds - ds.round()
    return r1 + ds*L

def h_matrix_from_bounds(bounds):
    return np.diag([bounds[1][0]-bounds[0][0], bounds[1][1]-bounds[0][1], bounds[1][2]-bounds[0][2]])

def pbc_dr_bounds(r1, r2, bounds):
    dr = r2-r1
    h = h_matrix_from_bounds(bounds)
    ds = np.linalg.inv(h) @ dr
    ds = ds - ds.round()
    dr = h @ ds
    return dr

# copied from NetworCh.git/python/ddd/ddd_util.py
def plot_nodes_dict(nodes, bounds, plot_links=True, trim=False, fig=None, ax=None, block=False, pause_seconds=0.01):
    if fig==None:
        fig = plt.figure(figsize=(8,8))
    if ax==None:
        ax = plt.axes(projection='3d')

    # to do: extend to non-cubic box
    L = bounds[1][0] - bounds[0][0]

    rn = np.array([nodes[i][0].tolist() for i in list(nodes.keys())])
    p_link = np.empty((0,6))

    plt.cla()
    if plot_links:
        for my_tag in list(nodes.keys()):
            my_coords = nodes[my_tag][0][0:3]
            arms_dict = nodes[my_tag][1]
            for nbr_tag in list(arms_dict.keys()):
                if my_tag < nbr_tag:
                    r_link = np.zeros((2,3))
                    if not nbr_tag in nodes.keys():
                        print("plot_nodes_dict(): cannot find neighbor")
                        print([my_tag, nbr_tag])
                        continue
                    nbr_coords = nodes[nbr_tag][0][0:3]
                    r_link[0,:] = my_coords
                    r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                    if (not trim) or np.max(np.absolute(r_link)) <= L/2:
                        p_link = np.append(p_link, [r_link[0,:], r_link[1,:]])

    ls = p_link.reshape((-1,2,3))
    lc = Line3DCollection(ls, linewidths=0.5, colors='b')
    ax.add_collection(lc)

    ax.scatter(rn[:,0], rn[:,1], rn[:,2], c='r', s=4)
    ax.set_xlim(bounds[0][0], bounds[1][0])
    ax.set_ylim(bounds[0][1], bounds[1][1])
    ax.set_zlim(bounds[0][2], bounds[1][2])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_box_aspect([1,1,1])

    plt.draw()
    plt.show(block=block)
    plt.pause(pause_seconds)

    return fig, ax

'''
get_plane_from_three_points: output the normal vector of the plane
'''
def get_plane_from_points(p1, p2, p3):
    
    plane = np.zeros(3,)
    for i in range(3):
        A = np.vstack((p1,p2,p3))
        A[:,i] = 1.0
        plane[i] = np.linalg.det(A)
    plane = plane/np.linalg.norm(plane)

    return plane

'''
calculate the normal vector for various glide planes in HCP materials
'''
def calculate_glide_plane_HCP(cOa):
    plane = np.zeros((16,3))

    # Points used to calculate Burgers vectors or plane normal vectors
    # Refer to Figure 1 in Aubry et al. JMPS 94 (2016) 105-126
    A = np.array([0.0, 0.0, 0.0])
    B = np.array([-0.5, 0.5*np.sqrt(3.0), 0.0])
    C = np.array([0.5, 0.5*np.sqrt(3.0), 0.0])
    delta = np.array([0.0, np.sqrt(3.0)/3.0, 0.5*cOa])
    Ap = np.array([0.0 , 0.0, cOa])
    Bp = np.array([-0.5, 0.5*np.sqrt(3.0), cOa])
    Cp = np.array([0.5, 0.5*np.sqrt(3.0), cOa])

    plane[0] = get_plane_from_points(C, A, B) # basal

    plane[1] = get_plane_from_points(A, B, Bp) # prismatic
    plane[2] = get_plane_from_points(A, C, Ap) # prismatic
    plane[3] = get_plane_from_points(C, B, Cp) # prismatic

    plane[4] = get_plane_from_points(Cp, A, B)  # 1st pyramidal
    plane[5] = get_plane_from_points(C, Bp, Ap) # 1st pyramidal
    plane[6] = get_plane_from_points(A, Bp, Cp) # 1st pyramidal
    plane[7] = get_plane_from_points(Ap, C, B)  # 1st pyramidal
    plane[8] = get_plane_from_points(Bp, A, C)  # 1st pyramidal
    plane[9] = get_plane_from_points(B, Cp, Ap) # 1st pyramidal

    plane[10] = get_plane_from_points(delta, A, Bp) # 2nd pyramidal
    plane[11] = get_plane_from_points(delta, B, Cp) # 2nd pyramidal
    plane[12] = get_plane_from_points(C, delta, Ap) # 2nd pyramidal
    plane[13] = get_plane_from_points(Ap, delta, B) # 2nd pyramidal
    plane[14] = get_plane_from_points(Bp, delta, C) # 2nd pyramidal
    plane[15] = get_plane_from_points(Cp, A, delta) # 2nd pyramidal

    return plane

def burgID_from_sysID(sysID, material_type=mat_type.FCC):

    if material_type == mat_type.FCC:
        if sysID == 6 or sysID == 9:
            bid = 0 # [ 0, 1, 1]
        elif sysID == 0 or sysID == 3:
            bid = 1 # [ 0,-1, 1]
        elif sysID == 1 or sysID == 7:
            bid = 2 # [ 1, 0, 1]
        elif sysID == 4 or sysID == 10:
            bid = 3 # [-1, 0, 1]
        elif sysID == 5 or sysID == 8:
            bid = 4 # [-1, 1, 0]
        elif sysID == 2 or sysID == 11:
            bid = 5 # [ 1, 1, 0]
        else:
            bid = -1

    elif material_type == mat_type.HCP:
        if sysID == 0 or sysID == 1 or sysID == 2 or sysID == 3:
            bid = 0 # AB == [-0.5, 0.5*np.sqrt(3.0), 0.0]
        elif sysID == 4 or sysID == 5 or sysID == 6 or sysID == 7:
            bid = 1 # BC == [1, 0, 0]
        elif sysID == 8 or sysID == 9 or sysID == 10 or sysID == 11:
            bid = 2 # CA == [-0.5, -0.5*np.sqrt(3.0), 0.0]
        elif sysID == 12 or sysID == 13 or sysID == 14 or sysID == 15:
            bid = 3 # ABp == [-0.5, 0.5*np.sqrt(3.0), cOa]
        elif sysID == 16 or sysID == 17 or sysID == 18 or sysID == 19:
            bid = 4 # BCp == [1, 0, cOa]
        elif sysID == 20 or sysID == 21 or sysID == 22 or sysID == 23:
            bid = 5 # CAp == [-0.5, -0.5*np.sqrt(3.0), cOa]
        elif sysID == 24 or sysID == 25 or sysID == 26 or sysID == 27:
            bid = 6 # ApB == [-0.5, 0.5*np.sqrt(3.0), -cOa]
        elif sysID == 28 or sysID == 29 or sysID == 30 or sysID == 31:
            bid = 7 # BpC == [1, 0, -cOa]
        elif sysID == 32 or sysID == 33 or sysID == 34 or sysID == 35:
            bid = 8 # CpA == [-0.5, -0.5*np.sqrt(3.0), -cOa]
        elif sysID == 36 or sysID == 37 or sysID == 38:
            bid = 9 # AAp == [0, 0, cOa]
        else:
            bid = -1

    return bid


def planeID_from_sysID(sysID, material_type=mat_type.FCC):

    if material_type == mat_type.FCC:
        if sysID == 0 or sysID == 1 or sysID == 2:
            nid = 0 # (-1,  1,  1)
        elif sysID == 3 or sysID == 4 or sysID == 5:
            nid = 1 # ( 1,  1,  1)
        elif sysID == 6 or sysID == 7 or sysID == 8:
            nid = 2 # (-1, -1,  1)
        elif sysID == 9 or sysID == 10 or sysID == 11:
            nid = 3 # ( 1, -1,  1)
        else:
            nid = -1

    elif material_type == mat_type.HCP:
        if sysID == 0 or sysID == 4 or sysID == 8:
            nid = 0 # basal
        elif sysID == 2 or sysID == 14 or sysID == 26 or sysID == 38:
            nid = 1 # prismatic
        elif sysID == 10 or sysID == 22 or sysID == 34 or sysID == 36:
            nid = 2 # prismatic        
        elif sysID == 6 or sysID == 16 or sysID == 30 or sysID == 37:
            nid = 3 # prismatic
        elif sysID == 1 or sysID == 17 or sysID == 35:
            nid = 4 # first pyramidal
        elif sysID == 3 or sysID == 23 or sysID == 29:
            nid = 5 # first pyramidal
        elif sysID == 5 or sysID == 13 or sysID == 33:
            nid = 6 # first pyramidal
        elif sysID == 7 or sysID == 21 or sysID == 25:
            nid = 7 # first pyramidal
        elif sysID == 9 or sysID == 15 or sysID == 31:
            nid = 8 # first pyramidal
        elif sysID == 11 or sysID == 19 or sysID == 27:
            nid = 9 # first pyramidal
        elif sysID == 12:
            nid = 10 # second pyramidal
        elif sysID == 18:
            nid = 11 # second pyramidal
        elif sysID == 20:
            nid = 12 # second pyramidal
        elif sysID == 24:
            nid = 13 # second pyramidal
        elif sysID == 28:
            nid = 14 # second pyramidal
        elif sysID == 32:
            nid = 15 # second pyramidal
        else:
            nid = -1

    return nid


def burgID_from_burgers_vector(burg, material_type=mat_type.FCC, cOa=None):

    bX, bY, bZ = burg
    if material_type == mat_type.FCC:
        if   (isCollinear(bX, bY, bZ, 0.0, 1.0, 1.0)):
            bid = 0
        elif (isCollinear(bX, bY, bZ, 0.0, -1.0, 1.0)):
            bid = 1
        elif (isCollinear(bX, bY, bZ, 1.0, 0.0, 1.0)):
            bid = 2
        elif (isCollinear(bX, bY, bZ, -1.0, 0.0, 1.0)):
            bid = 3
        elif (isCollinear(bX, bY, bZ, -1.0, 1.0, 0.0)):
            bid = 4
        elif (isCollinear(bX, bY, bZ, 1.0, 1.0, 0.0)):
            bid = 5
        else:
            bid = -1

    elif material_type == mat_type.HCP:
        if   (isCollinear(bX, bY, bZ, -0.5, 0.5*np.sqrt(3.0), 0.0)):
            bid = 0 # AB == [-0.5, 0.5*np.sqrt(3.0), 0.0]
        elif (isCollinear(bX, bY, bZ, 1.0, 0.0, 0.0)):
            bid = 1 # BC == [1, 0, 0]
        elif (isCollinear(bX, bY, bZ, -0.5, -0.5*np.sqrt(3.0), 0.0)):
            bid = 2 # CA == [-0.5, -0.5*np.sqrt(3.0), 0.0]
        elif (isCollinear(bX, bY, bZ, -0.5, 0.5*np.sqrt(3.0), cOa)):
            bid = 3 # ABp == [-0.5, 0.5*np.sqrt(3.0), cOa]
        elif (isCollinear(bX, bY, bZ, 1.0, 0.0, cOa)):
            bid = 4 # BCp == [1, 0, cOa]
        elif (isCollinear(bX, bY, bZ, -0.5, -0.5*np.sqrt(3.0), cOa)):
            bid = 5 # CAp == [-0.5, -0.5*np.sqrt(3.0), cOa]
        elif (isCollinear(bX, bY, bZ, -0.5, 0.5*np.sqrt(3.0), -cOa)):
            bid = 6 # ApB == [-0.5, 0.5*np.sqrt(3.0), -cOa]
        elif (isCollinear(bX, bY, bZ, 1.0, 0.0, -cOa)):
            bid = 7 # BpC == [1, 0, -cOa]
        elif (isCollinear(bX, bY, bZ, -0.5, -0.5*np.sqrt(3.0), -cOa)):
            bid = 8 # CpA == [-0.5, -0.5*np.sqrt(3.0), -cOa]
        elif (isCollinear(bX, bY, bZ, 0.0, 0.0, cOa)):
            bid = 9 # AAp == [0, 0, cOa]
        else:
            bid = -1

    return bid


def planeID_from_plane_normal_vector(nPlane, material_type=mat_type.FCC, cOa=None):

    nX, nY, nZ = nPlane
    if material_type == mat_type.FCC:
        if  (isCollinear(nX, nY, nZ, -1.0, 1.0, 1.0)):
            nid = 0
        elif (isCollinear(nX, nY, nZ, 1.0, 1.0, 1.0)):
            nid = 1
        elif (isCollinear(nX, nY, nZ, -1.0, -1.0, 1.0)):
            nid = 2
        elif (isCollinear(nX, nY, nZ, 1.0, -1.0, 1.0)):
            nid = 3
        else:
            nid = -1

    elif material_type == mat_type.HCP:
        # define reference norms
        plane = calculate_glide_plane_HCP(cOa)
        if  (isCollinear(nX, nY, nZ, plane[0][0], plane[0][1], plane[0][2])):
            nid = 0 # basal
        elif (isCollinear(nX, nY, nZ, plane[1][0], plane[1][1], plane[1][2])):
            nid = 1 # prismatic
        elif (isCollinear(nX, nY, nZ, plane[2][0], plane[2][1], plane[2][2])):
            nid = 2 # prismatic
        elif (isCollinear(nX, nY, nZ, plane[3][0], plane[3][1], plane[3][2])):
            nid = 3 # prismatic
        elif (isCollinear(nX, nY, nZ, plane[4][0], plane[4][1], plane[4][2])):
            nid = 4 # first pyramidal
        elif (isCollinear(nX, nY, nZ, plane[5][0], plane[5][1], plane[5][2])):
            nid = 5 # first pyramidal
        elif (isCollinear(nX, nY, nZ, plane[6][0], plane[6][1], plane[6][2])):
            nid = 6 # first pyramidal
        elif (isCollinear(nX, nY, nZ, plane[7][0], plane[7][1], plane[7][2])):
            nid = 7 # first pyramidal
        elif (isCollinear(nX, nY, nZ, plane[8][0], plane[8][1], plane[8][2])):
            nid = 8 # first pyramidal
        elif (isCollinear(nX, nY, nZ, plane[9][0], plane[9][1], plane[9][2])):
            nid = 9 # first pyramidal
        elif (isCollinear(nX, nY, nZ, plane[10][0], plane[10][1], plane[10][2])):
            nid = 10 # second pyramidal
        elif (isCollinear(nX, nY, nZ, plane[11][0], plane[11][1], plane[11][2])):
            nid = 11 # second pyramidal
        elif (isCollinear(nX, nY, nZ, plane[12][0], plane[12][1], plane[12][2])):
            nid = 12 # second pyramidal
        elif (isCollinear(nX, nY, nZ, plane[13][0], plane[13][1], plane[13][2])):
            nid = 13 # second pyramidal
        elif (isCollinear(nX, nY, nZ, plane[14][0], plane[14][1], plane[14][2])):
            nid = 14 # second pyramidal
        elif (isCollinear(nX, nY, nZ, plane[15][0], plane[15][1], plane[15][2])):
            nid = 15 # second pyramidal
        else:
            nid = -1

    return nid


def plane_normal_vector_from_planeID(nid, material_type=mat_type.FCC, cOa=None):

    if material_type == mat_type.FCC:
        f = math.sqrt(3)
        if nid == 0:
            nPlane = np.array([-1.0, 1.0, 1.0]) / f
        elif nid == 1:
            nPlane = np.array([1.0, 1.0, 1.0]) / f
        elif nid == 2:
            nPlane = np.array([-1.0, -1.0, 1.0]) / f
        elif nid == 3:
            nPlane = np.array([1.0, -1.0, 1.0]) / f

    elif material_type == mat_type.HCP:
        # define reference norms
        plane = calculate_glide_plane_HCP(cOa)
        if nid == 0:
            nPlane = plane[0]/np.linalg.norm(plane[0]) # basal
        elif nid == 1:
            nPlane = plane[1]/np.linalg.norm(plane[1]) # prismatic
        elif nid == 2:
            nPlane = plane[2]/np.linalg.norm(plane[2]) # prismatic
        elif nid == 3:
            nPlane = plane[3]/np.linalg.norm(plane[3]) # prismatic
        elif nid == 4:
            nPlane = plane[4]/np.linalg.norm(plane[4]) # first pyramidal
        elif nid == 5:
            nPlane = plane[5]/np.linalg.norm(plane[5]) # first pyramidal
        elif nid == 6:
            nPlane = plane[6]/np.linalg.norm(plane[6]) # first pyramidal
        elif nid == 7:
            nPlane = plane[7]/np.linalg.norm(plane[7]) # first pyramidal
        elif nid == 8:
            nPlane = plane[8]/np.linalg.norm(plane[8]) # first pyramidal
        elif nid == 9:
            nPlane = plane[9]/np.linalg.norm(plane[9]) # first pyramidal
        elif nid == 10:
            nPlane = plane[10]/np.linalg.norm(plane[10]) # second pyramidal
        elif nid == 11:
            nPlane = plane[11]/np.linalg.norm(plane[11]) # second pyramidal
        elif nid == 12:
            nPlane = plane[12]/np.linalg.norm(plane[12]) # second pyramidal
        elif nid == 13:
            nPlane = plane[13]/np.linalg.norm(plane[13]) # second pyramidal
        elif nid == 14:
            nPlane = plane[14]/np.linalg.norm(plane[14]) # second pyramidal
        elif nid == 15:
            nPlane = plane[15]/np.linalg.norm(plane[15]) # second pyramidal

    return nPlane


def sysID_from_nid_and_bid(nid, bid, material_type=mat_type.FCC):

    if material_type == mat_type.FCC:
        if (nid == 0):
            sys = {bid==1: 0, bid==2: 1, bid==5: 2}.get(True, 12)
        elif (nid == 1):
            sys = {bid==1: 3, bid==3: 4, bid==4: 5}.get(True, 12)
        elif (nid == 2):
            sys = {bid==0: 6, bid==2: 7, bid==4: 8}.get(True, 12)
        elif (nid == 3):
            sys = {bid==0: 9, bid==3: 10, bid==5: 11}.get(True, 12)
        else:
            sys = 12; #Junction or non-native FCC system 

    elif material_type == mat_type.HCP:
        if (bid == 0):
            sys = {nid==0: 0, nid==4: 1, nid==1: 2, nid==5: 3}.get(True, 39)
        elif (bid == 1):
            sys = {nid==0: 4, nid==6: 5, nid==3: 6, nid==7: 7}.get(True, 39)
        elif (bid == 2):
            sys = {nid==0: 8, nid==8: 9, nid==2: 10, nid==9: 11}.get(True, 39)
        elif (bid == 3):
            sys = {nid==10: 12, nid==6: 13, nid==1: 14, nid==8: 15}.get(True, 39)
        elif (bid == 4):
            sys = {nid==3: 16, nid==4: 17, nid==11: 18, nid==9: 19}.get(True, 39)
        elif (bid == 5):
            sys = {nid==12: 20, nid==7: 21, nid==2: 22, nid==5: 23}.get(True, 39)
        elif (bid == 6):
            sys = {nid==13: 24, nid==7: 25, nid==1: 26, nid==9: 27}.get(True, 39)
        elif (bid == 7):
            sys = {nid==14: 28, nid==5: 29, nid==3: 30, nid==8: 31}.get(True, 39)
        elif (bid == 8):
            sys = {nid==15: 32, nid==6: 33, nid==2: 34, nid==4: 35}.get(True, 39)
        elif (bid == 9):
            sys = {nid==2: 36, nid==3: 37, nid==1: 38}.get(True, 39)
        else:
            sys = 39; #Junction or non-native HCP system

    return sys


def sysTypeID_from_nid_and_bid(nid, bid, material_type=mat_type.HCP):

    if material_type == mat_type.HCP:
        if (bid == 0):
            sysType = {nid==0: 0, nid==4: 2, nid==1: 1, nid==5: 2}.get(True, 7)
        elif (bid == 1):
            sysType = {nid==0: 0, nid==6: 2, nid==3: 1, nid==7: 2}.get(True, 7)
        elif (bid == 2):
            sysType = {nid==0: 0, nid==8: 2, nid==2: 1, nid==9: 2}.get(True, 7)
        elif (bid == 3):
            sysType = {nid==10: 5, nid==6: 4, nid==1: 3, nid==8: 4}.get(True, 7)
        elif (bid == 4):
            sysType = {nid==3: 3, nid==4: 4, nid==11: 5, nid==9: 4}.get(True, 7)
        elif (bid == 5):
            sysType = {nid==12: 5, nid==7: 4, nid==2: 3, nid==5: 4}.get(True, 7)
        elif (bid == 6):
            sysType = {nid==13: 5, nid==7: 4, nid==1: 3, nid==9: 4}.get(True, 7)
        elif (bid == 7):
            sysType = {nid==14: 5, nid==5: 4, nid==3: 3, nid==8: 4}.get(True, 7)
        elif (bid == 8):
            sysType = {nid==15: 5, nid==6: 4, nid==2: 3, nid==4: 4}.get(True, 7)
        elif (bid == 9):
            sysType = {nid==2: 6, nid==3: 6, nid==1: 6}.get(True, 7)
        else:
            sysType = 7; #Junction or non-native HCP system type
    
    return sysType


def which_slipSystem(burg, nPlane, material_type=mat_type.FCC, cOa=None):

    burg = burg.reshape(-1,3)
    nPlane = nPlane.reshape(-1,3 )
    sysID = np.zeros(( burg.shape[0] ))
    for i in range( burg.shape[0] ):

        if material_type == mat_type.FCC:
            nid = planeID_from_plane_normal_vector(nPlane[i,:], material_type)
            bid = burgID_from_burgers_vector(burg[i,:], material_type)

        elif material_type == mat_type.HCP:
            nid = planeID_from_plane_normal_vector(nPlane[i,:], material_type, cOa)
            bid = burgID_from_burgers_vector(burg[i,:], material_type, cOa)
        
        sysID[i] = sysID_from_nid_and_bid(nid, bid, material_type)

    return sysID

def isCollinear(X, Y, Z, X2, Y2, Z2):
#Based on isCollinear function in WriteParaview.c
#Hamedakh
    norm = np.sqrt(X**2+Y**2+Z**2);
    X=X/norm;
    Y=Y/norm;
    Z=Z/norm;

    norm2 = np.sqrt(X2**2+Y2**2+Z2**2);
    X2=X2/norm2;
    Y2=Y2/norm2;
    Z2=Z2/norm2;

    res=0;
    if (abs( abs(X*X2+Y*Y2+Z*Z2)-1 )<1e-4) :
        res=1
    return res

def read_vel_to_dict(velfile):
# Read nodal velocities from ParaDiS velocity file into a python dictionary
# Works for velXXXX files generated from parallel runs by using "velfile = 1" in the ctrl file
# 
# Syntax
# nodes_vel = read_vel_to_dict(velfile)
#
# Input
# velfile: path to the ParaDiS velocity file
#
# Outputs
# nodes_vel: dictionary indexed by tag
#            each value of the dictionary is a numpy array [my_vel]

    try:
        with open(velfile,'r') as fid:
            # initialize an empty dictionary
            nodes_vel = {}
            while True:
                line = fid.readline(); vel_list = line.split()
                if len(vel_list) == 0:
                    break
                if len(vel_list) != 5:
                    print('Unknow velocity file format')
                    return nodes_vel
                my_tag = tuple([int(i) for i in re.split('[( , )]', vel_list[-1]) if i.isdigit()])
                my_vel = np.array([float(vel_list[0]), float(vel_list[1]), float(vel_list[2])])
                nodes_vel[my_tag] = my_vel  
            fid.close()
    except:
        print('Cannot open velocity file: ' + velfile)
        raise

    return nodes_vel

def read_force_to_dict(forcefile):
# Read nodal forces from ParaDiS force file into a python dictionary
# Works for forceXXXX files generated from parallel runs by using "writeForce = 1" in the ctrl file
# 
# Syntax
# nodes_force = read_force_to_dict(forcefile)
#
# Input
# forcefile: path to the ParaDiS force file
#
# Outputs
# nodes_force: dictionary indexed by tag
#            each value of the dictionary is a numpy array [my_force]

    try:
        with open(forcefile,'r') as fid:
            # initialize an empty dictionary
            nodes_force = {}
            while True:
                line = fid.readline(); force_list = line.split()
                if len(force_list) == 0:
                    break
                if len(force_list) != 4:
                    print('Unknow force file format')
                    return nodes_force
                my_tag = tuple([int(i) for i in re.split('[( , )]', force_list[-1]) if i.isdigit()])
                my_force = np.array([float(force_list[0]), float(force_list[1]), float(force_list[2])])
                nodes_force[my_tag] = my_force  
            fid.close()
    except:
        print('Cannot open force file: ' + forcefile)
        raise

    return nodes_force

def read_nodes_to_dict(datafile, need_aux_data=False):
# Read dislocation nodes from ParaDiS data file into a python dictionary
# Works for restart files generated from parallel runs
#
# Syntax
# nodes, bounds = read_nodes_to_dict(datafile)
#
# Input
#  datafile: path to the ParaDiS data file
#
# Outputs
#  nodes: dictionary indexed by tag
#         each value of the dictionary is a list [ my_coord, arms_dict ]
#         where my_coord is numpy array, arms_dict is a dictionary indexed by tag whose values are numpy arrays
#  bounds: [[xMin,yMin,zMin],[xMax,yMax,zMax]]

    try:
        with open(datafile,'r') as fid:
            line = fid.readline(); dataFileVersion = int(line.split()[2])
            print("dataFileVersion = %d"%(dataFileVersion))
            line = fid.readline(); numFileSegments = int(line.split()[2])
            print("numFileSegments = %d"%(numFileSegments))

            line = fid.readline()
            line = fid.readline(); xMin = float(line.split()[0])
            line = fid.readline(); yMin = float(line.split()[0])
            line = fid.readline(); zMin = float(line.split()[0])
            line = fid.readline()
            line = fid.readline()
            line = fid.readline(); xMax = float(line.split()[0])
            line = fid.readline(); yMax = float(line.split()[0])
            line = fid.readline(); zMax = float(line.split()[0])
            line = fid.readline()

            bounds = np.array([[xMin,yMin,zMin], [xMax,yMax,zMax]])

            line = fid.readline(); nodeCount = int(line.split()[2])
            print("nodeCount = %d"%(nodeCount))
            line = fid.readline(); dataDecompType = int(line.split()[2])
            print("dataDecompType = %d"%(dataDecompType))

            # initialize an empty dictionary
            nodes = {}
            while True:
                line = fid.readline()
                if line.find('nodalData =') != -1:
                    line = fid.readline(); skiptwo = line.find('#') != -1
                    break
                if not line:
                    print("Cannot find nodalData in" + datafile)
                    return
            print("skiptwo = %r"%(skiptwo))

            if skiptwo:
                line = fid.readline()
            while True:
                if not skiptwo and len(nodes) == 0:
                    data_list = line.split()
                else:
                    line = fid.readline(); data_list = line.split()
                if len(data_list) == 0:
                    break
                if len(data_list) != 6:
                    print('Unknown data file format')
                    return nodes, bounds

                my_tag = tuple([ int(i) for i in data_list[0].split(',') ])
                my_coord = np.array([float(data_list[1]), float(data_list[2]), float(data_list[3]), float(data_list[5])])

                num_arms = int(data_list[4])
                arms_dict = {}
                for i in range(num_arms):
                    line = fid.readline(); arm_data_list = line.split()
                    if len(arm_data_list) != 4:
                        print('Unknown data file format')
                        return nodes, bounds
                    nbr_tag = tuple([ int(i) for i in arm_data_list[0].split(',')])
                    line = fid.readline(); plane_data_list = line.split()
                    if len(plane_data_list) != 3:
                        print('Unknown data file format')
                        return nodes, bounds
                    armsysID = which_slipSystem( np.array([ float(arm_data_list[1]),float(arm_data_list[2]),float(arm_data_list[3]) ] ) ,
                                                 np.array([ float(plane_data_list[0]),float(plane_data_list[1]),float(plane_data_list[2])] ) )[0]
                    arm_data = np.array([ float(arm_data_list[1]),float(arm_data_list[2]),float(arm_data_list[3]),
                                          float(plane_data_list[0]),float(plane_data_list[1]),float(plane_data_list[2]), armsysID])
                    arms_dict[nbr_tag] = arm_data
                nodes[my_tag] = [my_coord, arms_dict]
            fid.close()
    except:
        print('Cannot open data file ' + datafile)
        raise

    if need_aux_data:
        aux_data = [dataFileVersion, numFileSegments, dataDecompType]
        return nodes, bounds, aux_data
    else:
        return nodes, bounds

# Jae-Hoon (Aug,18,2022): copied from MLmat.git/DislocationDynamics/utilities/python/disl_analysis_util.py
def oprec_player_on_dict(orig_nodes, bounds, oprecfile, istart=None, iend=None, stress_strain=False, print_freq=1, fig=None, ax=None, plot_freq=100, plot_physical_structure=False, plot_links=True, trim=False):
# Replay the oprecfile and update the DD structure stored in the nodes dictionary
#
# Syntax
#  nodes = oprec_player_on_dict(orig_nodes, bounds, oprecfile)
#
# Inputs
#  nodes: dictionary of dislocation network structure organized by nodes
#  bounds: bounds of the primary network
#  oprecfile: path of the oprecfile
#
# Outputs
#  nodes: new dictionary of dislocation network after evolution
#
# to do: also include stress, strain, time in the input/output, declare simulation data structure
# to do: update code to use full bounds instead of L
# to do: clarify with Nicolas on the purpose of io_inc = 4 lines in optype == 0, optype == 3 (done)
# to do: implement istart, skip lines until step == istart

    if fig==None:
        fig = plt.figure(figsize=(8,8))
    if ax==None:
        ax = plt.axes(projection='3d')

    nodes = orig_nodes.copy()

    step = 0
    io = 0
    print("reading oprecfile (" + oprecfile + ") line by line")
    with open(oprecfile) as f:
      while True:
        oprec_line = f.readline()
        if oprec_line == '':
            print('End of oprec file')
            break

        optype = int(oprec_line)

        #if optype != 5:
        #    print("io = %d  optype = %d"%(io,optype))

        if optype == 10:
            # START_STEP
            purge_network_dict(nodes)

            if stress_strain:
                nodes_old = nodes.deepcopy()

            io_inc = 1

        elif optype == 5:
            # RESET_COORD
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]
            my_tag = tuple(np.array(opdata)[0:2].astype(int))
            nodes[my_tag][0][0:3] = np.array(opdata[2:5])

            io_inc = 2

        elif optype == 11:
            # TIME_INTEGRATION
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]
            dt     = opdata[0]
            #cycle[step] = step
            #dens[step] = 1.0/(burgMag**2)/V*np.sum(all_segments_length(rn,links,L))

            if print_freq > 0:
                if step % print_freq == 0:
                    print('Step %d, dt = %e' % (step+1, dt))

            if stress_strain:    #Following lines to calcualte dpstn were copied from oprec_player_dict_gammaidot but was not tested

                nNodes = len(nodes)
                list_keys = list( nodes.keys() )

                #Alternatively, we can convert nodes to rn_links and use the matlab version of oprec_player for this section
                for counter_my_node, my_node_tag in enumerate(list_keys):
                    arms_dict = nodes[my_node_tag][1]
                    nArms = len(arms_dict)
                    for counter, nbr_tag in enumerate(arms_dict.keys()):
                        link = arms_dict[nbr_tag]
                        sysID = int(link[6])

                        if( OrderNodes(nbr_tag,my_node_tag)==1 ):
                            continue

                        r1 = nodes[my_node_tag][0]  #TODO: check if this has 3 components of coor
                        r2 = pbc_position(r1, nodes[nbr_tag][0], L)
                        r3 = pbc_position(r1, nodes_old[my_node_tag][0], L)
                        r4 = pbc_position(r3, nodes_old[my_node_tag][0], L)
                        n = 0.5*np.cross(r2-r3, r1-r4)
                        P = np.multiply(n[ [0,0,0,1,1,1,2,2,2]], links[ [2,3,4,2,3,4,2,3,4]])
                        dstn_eachLink = 0.5/V* ( P[ [0,4,8,1,2,5]]+P[ [0,4,8,3,6,7]] )
                        dpstn_eachLink = np.sum(np.multiply(dstn_eachLink, tmp),1).reshape(-1)
                        dstn[sysID,:] += dstn_eachLink
                        dspn += 0.5/V*(P[ [1,2,5]] - P[ [3,6,7]] )


                phispn = np.array([-dspn[2], dspn[1], -dspn[0]])
                Rspin = spin_matrix(phispn)
                edir = np.matmul(Rspin, edir)
                tmp = np.array([edir[0]**2, edir[1]**2, edir[2]**2, 2*edir[0]*edir[1], 2*edir[0]*edir[2], 2*edir[1]*edir[2]])
                for sysID in range(13):
                    dpstn_sys[sysID] = np.sum(np.multiply(dstn[sysID,:], tmp))

                dpstn = np.sum(dpstn_sys)
                dstrain = dt[step]*eRate
                dstress = 2*mu*(1+nu)*(dstrain-dpstn)


            if plot_freq > 0:
                if step % plot_freq == 0:
                    if plot_physical_structure:
                        phys_nodes = physical_structure(nodes, bounds)
                        fig, ax = plot_nodes_dict(phys_nodes, bounds, fig=fig, ax=ax, plot_links=plot_links, trim=trim)
                    else:
                        fig, ax = plot_nodes_dict(nodes, bounds, fig=fig, ax=ax, plot_links=plot_links, trim=trim)

            #if write_movie
            # to be written later

            step = step+1;
            io_inc = 2

            if iend != None:
                if step >= iend:
                    print('End of step ' + str(step))
                    break

        elif optype == 4:
            # SPLIT_NODE
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]
            my_tag = tuple(np.array(opdata)[0:2].astype(int))

            # create new node with empty arms dictionary
            nodes[my_tag] = [np.concatenate([np.array(opdata)[2:5],[0]]), {}]

            io_inc = 2

        elif optype == 0:
            # CHANGE_CONNECTION
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]

            tag_1 = tuple(np.array(opdata)[0:2].astype(int))
            tag_2 = tuple(np.array(opdata)[2:4].astype(int))
            tag_3 = tuple(np.array(opdata)[4:6].astype(int))

            arms_dict = nodes[tag_1][1]
            if not tag_2 in list(arms_dict.keys()):
                print("oprec_player_on_dict(): cannot find tag_2 in nodes(tag_1)")
                return None
            if tag_3 in list(arms_dict.keys()):
                # tag_3 already exist as a neighbor for tag_1
                # merge double links
                arms_dict[tag_3][0:3] += arms_dict[tag_2][0:3]
                arms_dict.pop(tag_2)
            else:
                arms_dict[tag_3] = arms_dict.pop(tag_2)

            io_inc = 2

        elif optype == 1:
            # INSERT_ARM
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]

            tag_1 = tuple(np.array(opdata)[0:2].astype(int))
            tag_2 = tuple(np.array(opdata)[2:4].astype(int))

            arms_dict = nodes[tag_1][1]
            burg_vec = np.array(opdata)[4:7]
            if tag_2 in list(arms_dict.keys()):
                # tag_2 is already a neighbor of tag_1, merge double links
                arms_dict[tag_2][0:3] += np.array(opdata)[4:7]
            else:
                arms_dict[tag_2] = np.array(opdata)[4:10]

            io_inc = 2

        elif optype == 3:
            # CHANGE_ARM_BURG
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]

            tag_1 = tuple(np.array(opdata)[0:2].astype(int))
            tag_2 = tuple(np.array(opdata)[2:4].astype(int))
            #print([tag_1,tag_2])

            arms_dict = nodes[tag_1][1]
            burg_vec = np.array(opdata)[4:7]
            if not tag_2 in list(arms_dict.keys()):
                if np.linalg.norm(burg_vec) > 0:
                    print("oprec_player_on_dict(): [3] tag_2 not a neighbor of nodes(tag_1)")
                    print(tag_1); print(tag_2); print(nodes[tag_1]); print(nodes[tag_2])
                    return None
            else:
                arms_dict[tag_2][0:6] = np.array(opdata)[4:10]
                burg_vec = arms_dict[tag_2][0:3]
                if np.linalg.norm(burg_vec) > 0:
                    pass
                else:
                    arms_dict.pop(tag_2)

            io_inc = 2

        elif optype == 9:
            # RESET_GLIDE_PLANE
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]

            tag_1 = tuple(np.array(opdata)[0:2].astype(int))
            tag_2 = tuple(np.array(opdata)[2:4].astype(int))

            arms_dict = nodes[tag_1][1]
            if not tag_2 in list(arms_dict.keys()):
                print("oprec_player_on_dict(): [9] tag_2 not a neighbor of nodes(tag_1)")
                return None

            arms_dict[tag_2][3:6] = np.array(opdata)[4:7]

            io_inc = 2

        elif optype == 2:
            # REMOVE_NODE
            oprec_line = f.readline()
            opdata = [float(e) for e in oprec_line.split()]

            tag_1 = tuple(np.array(opdata)[0:2].astype(int))

            nodes.pop(tag_1)

            io_inc = 2

        elif optype >= 12 and optype <= 15:
            # START_*: start of stages log, just skip it
            io_inc = 1

        else:
            print('WARNING: Unknown operation type ' + str(optype) + '. Skipping.')

        io = io + io_inc
        # End of while loop

    return nodes

def paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec):
    paradis_oprec = OperateRec_t(i1=0,i2=0,i3=0,i4=0,i5=0,i6=0,d1=0.0,d2=0.0,d3=0.0,d4=0.0,d5=0.0,d6=0.0,d7=0.0,d8=0.0,d9=0.0)

    my_optype = op['optype']
    paradis_oprec.type = my_optype

    #print('paradis_execute_oprec: optype = %d' % paradis_oprec.type)

    if my_optype == optype.CHANGE_CONNECTION: # optype=0
        # format: { 'optype':optype.CHANGE_CONNECTION, 'tag':start_tag, 'from_node':from_node, 'to_node':to_node }
        paradis_oprec.i1 = op['tag'][0]
        paradis_oprec.i2 = op['tag'][1]
        paradis_oprec.i3 = op['from_node'][0]
        paradis_oprec.i4 = op['from_node'][1]
        paradis_oprec.i5 = op['to_node'][0]
        paradis_oprec.i6 = op['to_node'][1]

    elif my_optype == optype.INSERT_ARM: # optype=1
        # format: { 'optype':optype.INSERT_ARM, 'tag':new_tag, 'arm_tag':node1, 'arm_data':arm_data }
        paradis_oprec.i1 = op['tag'][0]
        paradis_oprec.i2 = op['tag'][1]
        paradis_oprec.i3 = op['arm_tag'][0]
        paradis_oprec.i4 = op['arm_tag'][1]
        paradis_oprec.d1 = op['arm_data'][0]
        paradis_oprec.d2 = op['arm_data'][1]
        paradis_oprec.d3 = op['arm_data'][2]
        paradis_oprec.d7 = op['arm_data'][3]
        paradis_oprec.d8 = op['arm_data'][4]
        paradis_oprec.d9 = op['arm_data'][5]

    elif my_optype == optype.REMOVE_NODE: # optype=2
        # format: { 'optype':optype.REMOVE_NODE, 'tag':tag }
        paradis_oprec.i1 = op['tag'][0]
        paradis_oprec.i2 = op['tag'][1]

    elif my_optype == optype.CHANGE_ARM_BURG: # optype=3
        # format: { 'optype':optype.CHANGE_ARM_BURG, 'tag':new_tag, 'arm_tag':node1, 'arm_data':arm_data }
        paradis_oprec.i1 = op['tag'][0]
        paradis_oprec.i2 = op['tag'][1]
        paradis_oprec.i3 = op['arm_tag'][0]
        paradis_oprec.i4 = op['arm_tag'][1]
        paradis_oprec.d1 = op['arm_data'][0]
        paradis_oprec.d2 = op['arm_data'][1]
        paradis_oprec.d3 = op['arm_data'][2]
        paradis_oprec.d7 = op['arm_data'][3]
        paradis_oprec.d8 = op['arm_data'][4]
        paradis_oprec.d9 = op['arm_data'][5]

    elif my_optype == optype.SPLIT_NODE: # optype=4
        # format: { 'optype':optype.SPLIT_NODE, 'tag':new_tag, 'new_pos':new_node_position[0:3] }
        paradis_oprec.i3 = op['tag'][0]
        paradis_oprec.i4 = op['tag'][1]
        paradis_oprec.d4 = op['new_pos'][0]
        paradis_oprec.d5 = op['new_pos'][1]
        paradis_oprec.d6 = op['new_pos'][2]

    elif my_optype == optype.RESET_COORD: # optype=5
        # format: { 'optype':optype.RESET_COORD, 'tag':tag, 'new_pos':new_pos }
        paradis_oprec.i1 = op['tag'][0]
        paradis_oprec.i2 = op['tag'][1]
        paradis_oprec.d4 = op['new_pos'][0]
        paradis_oprec.d5 = op['new_pos'][1]
        paradis_oprec.d6 = op['new_pos'][2]

    elif my_optype == optype.START_STEP: # optype=10
        # format: { 'optype':optype.START_STEP }
        pass

    else:
        raise ValueError('unknown optype: ' + my_optype)

    '''
    print(paradis_oprec.type)
    print(" %d %d %d %d %d %d" % (paradis_oprec.i1, paradis_oprec.i2, paradis_oprec.i3, paradis_oprec.i4, paradis_oprec.i5, paradis_oprec.i6))
    print(" %e %e %e %e %e %e %e %e %e" % (paradis_oprec.d1, paradis_oprec.d2, paradis_oprec.d3, paradis_oprec.d4, paradis_oprec.d5, paradis_oprec.d6,
                                           paradis_oprec.d7, paradis_oprec.d8, paradis_oprec.d9))
    '''
    ExecuteOpRec(home, byref(paradis_oprec))


def paradis_apply_oprec_list(home, oprec_list, OperateRec_t, ExecuteOpRec, SortNativeNodes):
 
    oprec_list.insert(0, {'optype':optype.START_STEP})
    for op in oprec_list:
        paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)
    SortNativeNodes(home)

# Jae-Hoon (Aug,30,2022): copied from MLmat.git/DislocationDynamics/utilities/python/disl_analysis_util.py
def purge_network_dict(nodes):
# Purge the network by removing nodes that are unconnected to the network
# Note: changes the input nodes dictionary

    #nodesid = np.unique([links[:,0], links[:,1]])
    #nodesid = np.setxor1d(np.arange(rn.shape[0]), nodesid)
    #rn, links, offset = remove_nodes(rn, links, nodesid)
    return None



