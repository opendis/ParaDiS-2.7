
import numpy as np
import os, copy, math, random
from paradis_util import *

class ParaDiS_Topology(ParaDiS):
    def remove_link(self, nodes, bounds, link_list, link_id_to_remove, oprec_list=None, home=None):
        link_list = remove_link(nodes, bounds, link_list, link_id_to_remove, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
        return link_list
    def dissociate_link_by_change_arm(self, nodes, bounds, link_list, link_id_to_dissociate, oprec_list=None, home=None):
        link_list = dissociate_link_by_change_arm(nodes, bounds, link_list, link_id_to_dissociate, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
        return link_list
    def dissociate_link_by_split_node(self, nodes, bounds, link_list, link_id_to_dissociate, oprec_list=None, home=None):
        link_list = dissociate_link_by_split_node(nodes, bounds, link_list, link_id_to_dissociate, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
        return link_list
    def generate_jog_on_link(self, nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list=None, home=None, material_type=mat_type.FCC):
        link_list = generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home, material_type)
        return link_list
    def generate_jog_on_node(self, nodes, bounds, tag, length, dir, oprec_list=None, home=None):
        generate_jog_on_node(nodes, bounds, tag, length, dir, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
    def generate_jog_on_datafile(self, datafile, crit, probability, jog_length_ratio, jog_height, save_dir=None, oprec_list=None, home=None, material_type=mat_type.FCC):
        save_filename = generate_jog_on_datafile(datafile, crit, probability, jog_length_ratio, jog_height, self.OperateRec_t, self.ExecuteOpRec, save_dir, oprec_list, home, material_type)
        return save_filename
    def remove_link_from_nodes_dict(self, nodes, link_list, link_id, oprec_list=None, home=None):
        link_list = remove_link_from_nodes_dict(nodes, link_list, link_id, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
        return link_list
    def remove_2_arm_node(self, nodes, tag, oprec_list=None, home=None):
        remove_2_arm_node(nodes, tag, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
    def add_2_arm_node(self, nodes, node1, node2, new_tag, new_node_position, oprec_list=None, home=None):
        add_2_arm_node(nodes, node1, node2, new_tag, new_node_position, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
    def change_arm(self, nodes, tag, from_node, to_node, oprec_list=None, home=None):
        change_arm(nodes, tag, from_node, to_node, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
    def merge_node(self, nodes, from_node, to_node, oprec_list=None, home=None):
        merge_node(nodes, from_node, to_node, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
    def split_node(self, nodes, bounds, tag, new_tag, pos, new_pos, new_arm_tags, oprec_list=None, home=None):
        split_node(nodes, bounds, tag, new_tag, pos, new_pos, new_arm_tags, self.OperateRec_t, self.ExecuteOpRec, oprec_list, home)
    def add_node_on_opposite_side(self, nodes, link_list, link_id, from_node, to_node, new_tag, oprec_list=None, normal=None, offset=None, home=None):
        link_list = add_node_on_opposite_side(nodes, link_list, link_id, from_node, to_node, new_tag, self.OperateRec_t, self.ExecuteOpRec, oprec_list, normal, offset, home)
        return link_list
    def remove_node(self, nodes, tag, arm_tag=None, oprec_list=None, home=None):
        remove_node(nodes, self.OperateRec_t, self.ExecuteOpRec, tag, arm_tag, oprec_list, home)
    def paradis_apply_oprec_list(self, home, oprec_list):
        paradis_apply_oprec_list(home, oprec_list, self.OperateRec_t, self.ExecuteOpRec, self.SortNativeNodes)
    def paradis_execute_oprec(self, home, op):
        paradis_execute_oprec(home, op, self.OperateRec_t, self.ExecuteOpRec)


################################################################################
#
#    make_link_list():
#        Calculate link lengths and make a node list consisting of each link
#
#    Arguments:
#        nodes: node dictionary in Python
#        bounds: an 2x3 array for domain boundary
#            [[xMin,yMin,zMin],[xMax,yMax,zMax]]
#
#    Return:
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]]
#
#    Auxiliary functions:
#        find_link_from_node(): Calculate contour length by summing over arm length
#
################################################################################

def make_link_list(nodes, bounds, check_slip_system=False):
   
    # flag to check whether an arm is used or not 
    # same form with the 'nodes' structure, but the contensts is 1 (used) or 0 (not used)
    node_arm_flag = {}
    for tag in nodes.keys():
        num_arm = len(nodes[tag][1])
        arm_dict = {}
        for arm_tag in nodes[tag][1].keys():
            arm_dict[arm_tag] = 0
        node_arm_flag[tag] = arm_dict
    
    link_list = []
    for tag_i in nodes.keys(): # loop for all nodes

        num_nbr = len(nodes[tag_i][1]) # the number of arms

        # if num_nbr!=2: link finding is performed in only one direction.
        if num_nbr!=2:
            for tag_j in nodes[tag_i][1].keys():
                if node_arm_flag[tag_i][tag_j]==0:
                    contour_length, link_nodes, armsysID = find_link_from_node(nodes,bounds,node_arm_flag,tag_i,tag_j,check_slip_system,order=1)
                    local_link_list = [ contour_length, link_nodes ]
                    if check_slip_system:
                        local_link_list.append(armsysID)
                    link_list.append(local_link_list)

        # if num_nbr==2: link finding is performed in two directions.
        else:
            tag_j = list(nodes[tag_i][1].keys())[0]; flag0 = 0
            if node_arm_flag[tag_i][tag_j]==0:
                flag0 = 1
                contour_length0, link_nodes0, armsysID0 = find_link_from_node(nodes,bounds,node_arm_flag,tag_i,tag_j,check_slip_system,order=-1)
                
            tag_j = list(nodes[tag_i][1].keys())[1]; flag1 = 0
            if node_arm_flag[tag_i][tag_j]==0:
                flag1 = 1
                contour_length1, link_nodes1, armsysID1 = find_link_from_node(nodes,bounds,node_arm_flag,tag_i,tag_j,check_slip_system,order=1)

            if flag0 == 1 and flag1 == 1:
                if check_slip_system:
                    if armsysID0 == armsysID1:
                        link_list.append( [ contour_length0+contour_length1, link_nodes0+link_nodes1[1:], armsysID0 ] )
                    else:
                        link_list.append( [ contour_length0, link_nodes0, armsysID0 ] )
                        link_list.append( [ contour_length1, link_nodes1, armsysID1 ] )
                else:
                    link_list.append( [ contour_length0+contour_length1, link_nodes0+link_nodes1[1:] ] )
            elif flag0 == 1:
                local_link_list = [ contour_length0, link_nodes0 ]
                if check_slip_system:
                    local_link_list.append(armsysID0)
                link_list.append(local_link_list)
            elif flag1 == 1:
                local_link_list = [ contour_length1, link_nodes1 ]
                if check_slip_system:
                    local_link_list.append(armsysID1)
                link_list.append(local_link_list)

    return link_list


################################################################################
#
#    find_link_from_node():
#        find a node list with starting an arbitrary node to neighbor nodes. If
#        we meet a physical node, this function is ends.
#
#    Arguments:
#        nodes: node dictionary in Python
#        bounds: an 2x3 array for domain boundary
#            [[xMin,yMin,zMin],[xMax,yMax,zMax]]
#        node_arm_flag: flag to check whether an arm is used (1) or not (0)
#        tag_i: start node
#        tag_j: neighbor node of the start node (tag_i)
#        order: if 1:    save tags right-hand direction as [tag_i, tag_j, ...]
#               elif -1: save tags left-hand direction as [..., tag_j, tag_i]
#
#    Return:
#        contour_length: summation of arm lengths from start node (input tag_i)
#            to end node
#        link_nodes: node list from start node to end node
#
################################################################################

def find_link_from_node(nodes, bounds, node_arm_flag, tag_i, tag_j, check_slip_system=False, order=1):

    xi = copy.deepcopy(nodes[tag_i][0][0:3])
    xj = copy.deepcopy(nodes[tag_j][0][0:3])
    V = pbc_dr_bounds(xi, xj, bounds)
    contour_length = np.linalg.norm(V)

    armsysID_ij = -1
    if check_slip_system:
        armsysID_ij = copy.deepcopy(nodes[tag_i][1][tag_j][6])

    node_arm_flag[tag_i][tag_j] = 1
    node_arm_flag[tag_j][tag_i] = 1

    # Node tag is saved in the right-hand direction.
    if order==1: link_nodes = [tag_i, tag_j]
    # Node tag is saved in the left-hand direction.
    elif order==-1: link_nodes = [tag_j, tag_i]
    else: raise ValueError('order ('+str(order)+') has to be 1 or -1')

    # This while loop is designed to start with an arbitrary node and end
    #     when the node tag meets a node that has more than 3 arms.
    while True:
        arm_tag_list = list(nodes[tag_j][1].keys())
        if len(arm_tag_list)!=2: break

        # Lenghts are calculated only if the arm is not used yet.
        tag_k = arm_tag_list[0] if arm_tag_list[0] != tag_i else arm_tag_list[1]

        if check_slip_system:
            armsysID_jk = copy.deepcopy(nodes[tag_j][1][tag_k][6])
            if armsysID_ij != armsysID_jk:
                break

        if node_arm_flag[tag_j][tag_k] == 0:
            xk = copy.deepcopy(nodes[tag_k][0][0:3])
            V = pbc_dr_bounds(xj, xk, bounds)
            contour_length += np.linalg.norm(V)
            
            node_arm_flag[tag_j][tag_k] = 1
            node_arm_flag[tag_k][tag_j] = 1
            if order==1: link_nodes.append(tag_k)
            elif order==-1: link_nodes.insert(0,tag_k)
            else: raise ValueError('order ('+str(order)+') has to be 1 or -1')
            
            num_nbr = len(nodes[tag_k][1])
            if num_nbr>2: break
            
            tag_i = tag_j; xi = xj
            tag_j = tag_k; xj = xk

        else: break

    return contour_length, link_nodes, armsysID_ij
    
	
################################################################################
#
#    write_link_list():
#        Wirte node lists in each link
#
#    Arguments:
#        taskname: task name
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]]
#        suffix: suffix of the linkData file written by this function
#
################################################################################

def write_link_list(taskname, link_list, link_ids=None, suffix='.linkData', check_slip_system=False):
	
    pathname, extension = os.path.splitext(taskname)
    FileName = pathname + suffix

    fileID = open(FileName,'w')

    if link_ids != None:
        for link_id in link_ids:
            fileID.write("%d "%(link_id))
        fileID.write("\n\n")

    fileID.write(" Total number of links:    %d\n\n"%len(link_list))
    if check_slip_system:
        fileID.write("# link id (slip system id), link length\n")
    else:
        fileID.write("# link id, link length\n")
    fileID.write("# nodal lists\n")

    if check_slip_system:
        fileID.write("\n#  slip system id\n")
        fileID.write("#  0: glide plane = (-1, 1, 1), burgers vector = [ 0,-1, 1]\n")
        fileID.write("#  1: glide plane = (-1, 1, 1), burgers vector = [ 1, 0, 1]\n")
        fileID.write("#  2: glide plane = (-1, 1, 1), burgers vector = [ 1, 1, 0]\n")
        fileID.write("#  3: glide plane = ( 1, 1, 1), burgers vector = [ 0,-1, 1]\n")
        fileID.write("#  4: glide plane = ( 1, 1, 1), burgers vector = [-1, 0, 1]\n")
        fileID.write("#  5: glide plane = ( 1, 1, 1), burgers vector = [-1, 1, 0]\n")
        fileID.write("#  6: glide plane = ( 1, 1,-1), burgers vector = [ 0, 1, 1]\n")
        fileID.write("#  7: glide plane = ( 1, 1,-1), burgers vector = [ 1, 0, 1]\n")
        fileID.write("#  8: glide plane = ( 1, 1,-1), burgers vector = [-1, 1, 0]\n")
        fileID.write("#  9: glide plane = ( 1,-1, 1), burgers vector = [ 0, 1, 1]\n")
        fileID.write("# 10: glide plane = ( 1,-1, 1), burgers vector = [-1, 0, 1]\n")
        fileID.write("# 11: glide plane = ( 1,-1, 1), burgers vector = [ 1, 1, 0]\n")
        fileID.write("# 12: others\n\n")

    for link_id, my_link in enumerate(link_list):
        if check_slip_system:
            link_sys_id = my_link[2]
            fileID.write(" %d (%d):"%(link_id,link_sys_id))
        else:
            fileID.write(" %d:"%link_id)
        fileID.write(" %f\n"%my_link[0])
	
        for tag in my_link[1]:
            fileID.write(" (%d,%d)"%(tag[0],tag[1]))
        fileID.write("\n")

    fileID.close

 
################################################################################
#
#    find_links_to_be_operated():
#         Find link IDs to be operated. If a link is shorter than 'crit', then
#         the link is saved in 'supp_link_ids'.
#
#    Arguments:
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]] 
#        crit_length: criteria based on length
#        crit_num: criteria based on the number of nodes on links
#
#    Return:
#        supp_link_ids: list of link id to be suppressed
#
################################################################################

def find_links_to_be_operated(link_list, crit_length, crit_num=None):

    if not isinstance(crit_length, list):
        lower_bound = 0
        upper_bound = crit_length
    else:
        lower_bound = crit_length[0]
        upper_bound = crit_length[1]

    if crit_num==None:
        crit_num = 10000000

    supp_link_ids = []
    contour_lengths = []
    for link_id, my_link in enumerate(link_list):
        if lower_bound<my_link[0] and my_link[0]<upper_bound and len(my_link[1])<=crit_num:
            supp_link_ids.append(link_id)
            contour_lengths.append(my_link[0])

    contour_lengths = np.array(contour_lengths)
    sorting_index = contour_lengths.argsort()
    sorted_link_ids = [ supp_link_ids[index] for index in sorting_index ]

    return sorted_link_ids


def find_junctions_to_be_removed(nodes, bounds, link_list, crit):

    r2 = math.sqrt(2)
    r3 = math.sqrt(3)
    b_FCC = np.array([[1,1,0], [1,-1,0], [0,1,1], [0,1,-1], [1,0,1], [-1,0,1]])/r2
    n_FCC = np.array([[1,1,1], [-1,1,1], [1,-1,1], [1,1,-1]])/r3

    tol = 1e-6

    if not isinstance(crit, list):
        lower_bound = 0
        upper_bound = crit
    else:
        lower_bound = crit[0]
        upper_bound = crit[1]

    supp_link_ids = []
    contour_lengths = []
    for link_id, my_link in enumerate(link_list):
        #if lower_bound<my_link[0] and my_link[0]<upper_bound:
        if lower_bound<my_link[0] and my_link[0]<upper_bound and len(my_link[1])<3:
            tag1 = my_link[1][0]; x1 = copy.deepcopy(nodes[tag1][0][0:3])
            tag2 = my_link[1][1]; x2 = copy.deepcopy(nodes[tag2][0][0:3])
            direction = pbc_dr_bounds(x1, x2, bounds) 
            direction = direction/np.linalg.norm(direction)
            burg = copy.deepcopy(nodes[tag1][1][tag2][0:3])
            burg = burg/np.linalg.norm(burg)
            normal = copy.deepcopy(nodes[tag1][1][tag2][3:6])
            normal = normal/np.linalg.norm(normal)

            dot_dir_burg = np.abs(np.dot(direction,burg))
            check_glissile = np.abs(dot_dir_burg-0.5)<tol
            check_collinear = np.abs(dot_dir_burg)<tol

            dot_b_FCC = np.abs([ np.dot(burg,b) for b in b_FCC ])
            check_b_FCC = any([ np.abs(val-1)<tol for val in dot_b_FCC ])
            dot_b_1 = np.abs(burg)
            check_b_1 = any([ np.abs(val-1)<tol for val in dot_b_1 ])

            dot_n_FCC = np.abs([ np.dot(normal,n) for n in n_FCC ])
            check_n_FCC = any([ np.abs(val-1)<tol for val in dot_n_FCC ])
            dot_n_1 = np.abs(normal)
            check_n_1 = any([ np.abs(val-1)<tol for val in dot_n_1 ])

            #if check_n_FCC: # on glide plane
            #if check_b_FCC and check_n_1: # Lomer juntion
            #if check_b_1 and check_n_1: # Hirth juntion
            #if check_b_FCC and check_n_FCC: # Glissile dislocation
            #if check_b_FCC and check_n_FCC and check_glissile: # Glissile junction
            if check_b_FCC and check_n_FCC and not check_glissile: # Coplanar(?) junction
            #if check_b_FCC and check_n_FCC and check_collinear: # Collinear junction
                supp_link_ids.append(link_id)
                contour_lengths.append(my_link[0])

    contour_lengths = np.array(contour_lengths)
    sorting_index = contour_lengths.argsort()
    sorted_link_ids = [ supp_link_ids[index] for index in sorting_index ]

    return sorted_link_ids


################################################################################
#
#    find_connected_link_ids():
#        find two link IDs attached to two end nodes of a link
#
#    Arguments:
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]] 
#        link_id: the target link to find the attaced link IDs
#    
################################################################################

def find_connected_link_ids(link_list, link_id):
    my_link = link_list[link_id]
    end_nodes = [ my_link[1][0], my_link[1][-1] ]

    # find all links connected to end_nodes
    nbr_link_ids = [[], []]
    for nbr_id, nbr_link in enumerate(link_list):
        if nbr_id == link_id or len(nbr_link[1])==0:
            continue

        nbr_link_node = [ nbr_link[1][0], nbr_link[1][-1] ]

        if nbr_link_node[0] == end_nodes[0] or nbr_link_node[1] == end_nodes[0]:
            nbr_link_ids[0].append(nbr_id)

        if nbr_link_node[0] == end_nodes[1] or nbr_link_node[1] == end_nodes[1]:
            nbr_link_ids[1].append(nbr_id)

    return nbr_link_ids


################################################################################
#
#    remove_link_from_nodes_dict():
#        remove node tags of a link from the node dictionary
#
#    Arguments:
#        nodes: a node dictionary in Python
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]] 
#        link_id: the target link to be removed
#    
################################################################################

def remove_link_from_nodes_dict(nodes, link_list, link_id, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    my_link = link_list[link_id]
    first_node = my_link[1][0]   # first node in the link
    last_node  = my_link[1][-1]  # last node in the link
    start_tag  = my_link[1][1]   # second node in the link
    end_tag    = my_link[1][-2]  # second to last node in the link
    
    # remove arms pointing to nodes in this link
    remove_node(nodes, OperateRec_t, ExecuteOpRec, first_node, start_tag, oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, last_node, end_tag, oprec_list, home)

    # Delete discretization nodes
    for tag in my_link[1][1:-1]:
        arm_tag_keys = list(nodes[tag][1].keys())
        for arm_tag in arm_tag_keys:
            remove_node(nodes, OperateRec_t, ExecuteOpRec, tag, arm_tag, oprec_list, home)
        remove_node(nodes, OperateRec_t, ExecuteOpRec, tag, None, oprec_list, home)

    link_list[link_id][1] = []
    return link_list


################################################################################
#
#    get_new_node_tag_from_home():
#        find and return a new node tag
#
#    Arguments:
#        home: a ParaDiS home structure pointer
#
#    Return:
#        new_tag: a new node tag
#    
################################################################################

def get_new_node_tag_from_home(home):

    return (0, home.contents.newNodeKeyPtr+1)

################################################################################
#
#    get_new_node_tag_from_nodes():
#        find and return a new node tag
#
#    Arguments:
#        nodes: a node dictionary in Python
#
#    Return:
#        new_tag: a new node tag
#    
################################################################################

def get_new_node_tag_from_nodes(nodes):

    num_nodes = len(nodes)
    node_key_list = list(nodes.keys())

    new_tag = (0, max([tag[1] for tag in nodes.keys()])+1)
    #max_tag = (0, max([tag[1] for tag in nodes.keys()]))
    #test_tag = (0, num_nodes-1)
    #if max_tag==test_tag: # there is no tag for recycle
    #    new_tag = (0, num_nodes)
    #else: # there is a tag for recycle
    #    for i in range(num_nodes):
    #        test_tag = (0, i)
    #        if not test_tag in nodes:
    #            new_tag = test_tag
    #            break

    return new_tag


################################################################################
#
#    remove_2_arm_node():
#        remove 2-arm node and change connectivity
#
#    Arguments:
#        nodes: a node dictionary in Python
#        tag: a node to be removed
#    
################################################################################

def remove_2_arm_node(nodes, tag, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    arm_tag = list(nodes[tag][1].keys())
    if len(arm_tag) != 2:
        print("error in remove_2_arm_node(): given nodes[(%d,%d)] dosen't have 2 arms !"%(tag[0],tag[1]))
        return

    change_arm(nodes, arm_tag[0], tag, arm_tag[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

    remove_node(nodes, OperateRec_t, ExecuteOpRec, arm_tag[1], tag, oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, tag, arm_tag[1], oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, tag, None, oprec_list, home)


##############################################################################
#
#    add_2_arm_node():
#        Insert a new node between node1 and node2
#        * Origianal connectivity:     node1 - node2
#        * After inserting a new node: node1 - new_tag - node2
#
#    Argument:
#        nodes: node dictionary in Python
#        node1 & node2: node tag which already exist
#        new_tag: new node tag which is newly added
#        new_node_position: nodal position and constraint for the new node
#
################################################################################

def add_2_arm_node(nodes, node1, node2, new_tag, new_node_position, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    add_1_node(nodes, new_tag, new_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)
    change_arm(nodes, node1, node2, new_tag, OperateRec_t, ExecuteOpRec, oprec_list, home)

    arm_data = copy.deepcopy(nodes[node1][1][new_tag])
    add_1_arm(nodes, new_tag, node2, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)

    arm_data = copy.deepcopy(nodes[new_tag][1][node1])
    add_1_arm(nodes, node2, new_tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)


def add_1_node(nodes, new_tag, new_node_position, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):
    nodes[new_tag] = [new_node_position, {}]
    op = {'optype':optype.SPLIT_NODE, 'tag':new_tag, 'new_pos':new_node_position[0:3]}
    if oprec_list != None: oprec_list.append(op)
    if home != None: paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)
    

################################################################################
#
#    change_arm():
#        change arm of nodes[tag] from from_node to to_node two nodes
#
#    Arguments:
#        nodes: a node dictionary in Python
#        tag: target node to be changed arm
#        from_node: originally connected arm tag
#        to_node: newly connected arm tag
#    
################################################################################

def change_arm(nodes, tag, from_node, to_node, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    arm_data = copy.deepcopy(nodes[tag][1][from_node])
    add_1_arm(nodes, tag, to_node, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, tag, from_node, oprec_list, home)

    arm_data = copy.deepcopy(nodes[from_node][1][tag])
    add_1_arm(nodes, to_node, tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, from_node, tag, oprec_list, home)


def add_1_arm(nodes, tag, arm_tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):
    nodes[tag][1][arm_tag] = arm_data
    op = {'optype':optype.INSERT_ARM, 'tag':tag, 'arm_tag':arm_tag, 'arm_data':arm_data}
    if oprec_list != None: oprec_list.append(op)
    if home != None: paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)


################################################################################
#
#    merge_node():
#        merge two nodes
#
#    Arguments:
#        nodes: a node dictionary in Python
#        from_node: originally connected node of link_id
#        to_node: target node to move of link_id
#    
################################################################################

def merge_node(nodes, from_node, to_node, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    arm_tag_list = list(nodes[from_node][1].keys())
    for arm_tag in arm_tag_list:
        if arm_tag != to_node:
            change_arm(nodes, arm_tag, from_node, to_node, OperateRec_t, ExecuteOpRec, oprec_list, home)

    remove_node(nodes, OperateRec_t, ExecuteOpRec, to_node, from_node, oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, from_node, to_node, oprec_list, home)
    remove_node(nodes, OperateRec_t, ExecuteOpRec, from_node, None, oprec_list, home)


################################################################################
#
#    split_node():
#        split a node to two nodes
#
#    Arguments:
#        nodes: a node dictionary in Python
#        tag: a node tag to be splitted off
#        new_tag: new node tag newly generated through splitting
#        pos: position of nodes[tag]
#        new_pas: position of nodes[new_tag]
#        new_arm_tags: arm tags to be attaced to nodes[new_tag]
#    
################################################################################

def split_node(nodes, bounds, tag, new_tag, pos, new_pos, new_arm_tags, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    # set node positions
    reset_position(nodes, tag, pos, OperateRec_t, ExecuteOpRec, oprec_list, home)

    new_node_position = np.append(new_pos, np.array([0]))
    add_1_node(nodes, new_tag, new_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # transfer a part of arms (new_arm_tags) of nodes[tag] to arms of nodes[new_tag]
    burg = np.zeros(3)
    for arm_tag in new_arm_tags:
        burg += copy.deepcopy(nodes[tag][1][arm_tag][0:3])
        change_arm(nodes, arm_tag, tag, new_tag, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # connect nodes[tag] to nodes[new_tag]
    dir = pbc_dr_bounds(pos, new_pos, bounds) 
    normal = np.cross(burg, dir)
    if all(np.abs(val)<1e-10 for val in normal): # screw dislocation
        normal = pick_screw_glide_plane(burg)
    #normal = calc_glide_plane(burg, dir)

    arm_data = np.append(burg, normal)
    add_1_arm(nodes, tag, new_tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)

    arm_data = np.append(-burg, normal)
    add_1_arm(nodes, new_tag, tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)


def reset_position(nodes, tag, new_pos, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):
    nodes[tag][0][0:3] = new_pos
    op = {'optype':optype.RESET_COORD, 'tag':tag, 'new_pos':new_pos}
    if oprec_list != None: oprec_list.append(op)
    if home != None: paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)


################################################################################
#
#    pick_screw_glide_plane():
#        pick a normal vector in case of screw segment
#        The alorighm of this function is based on PickScrewGlidePlane.c. So,
#            if we can call the function PickFCCScrewGlidePlane(), this
#            function can be replaced
#
#    Arguments:
#        burg: Burgers vector
#    
################################################################################

def pick_screw_glide_plane(burg):

    normal = np.zeros(3)
    f = 1/math.sqrt(3)
    randVal = random.random()

    tol = 1e-10
    if np.abs(burg[0])<tol:
        normal = np.array([f, f, -np.sign(burg[1]*burg[2])*f])
        if randVal<0.5:
            normal[0] = -f
    elif np.abs(burg[1])<tol:
        normal = np.array([f, f, -np.sign(burg[0]*burg[2])*f])
        if randVal<0.5:
            normal[1] = -f
    else:
        normal = np.array([f, -np.sign(burg[0]*burg[1])*f, f])
        if randVal<0.5:
            normal[2] = -f

    return normal


def calc_glide_plane(b, offset):

    # There are 4 methods to specify glide plane of new dislocation

    # 3. glidePlane is calculated by the cross-product of director and burgers vectors (crossNormal)
    #n = np.cross(offset,b);
    #if abs(n[0])<1e-10 and abs(n[1])<1e-10 and abs(n[2])<1e-10:
    #    normal = pick_screw_glide_plane(b)
    #else:
    #    normal = n/np.linalg.norm(n)

    # 4. glidePlane is calculated from ParaDiS function (FindFCC)
    normal = FindFCCGlidePlane_python(b, offset)
   
    return normal


def FindFCCGlidePlane_python(burg, dir):

    burgRef = [ # [21][3]
         # glide and junction burgers vectors 
       [ 7.0710678118e-01,  7.0710678118e-01,           0.0e+00],
       [ 7.0710678118e-01, -7.0710678118e-01,           0.0e+00],
       [ 7.0710678118e-01,           0.0e+00,  7.0710678118e-01],
       [ 7.0710678118e-01,           0.0e+00, -7.0710678118e-01],
       [          0.0e+00,  7.0710678118e-01,  7.0710678118e-01],
       [          0.0e+00,  7.0710678118e-01, -7.0710678118e-01],
         # other likely junction burgers vectors 
       [ 1.0000000000e+00,           0.0e+00,           0.0e+00],
       [          0.0e+00,  1.0000000000e+00,           0.0e+00],
       [          0.0e+00,           0.0e+00,  1.0000000000e+00],
         # unlikely junction burgers vectors 
       [ 8.1649658092e-01,  4.0824829046e-01,  4.0824829046e-01],
       [-8.1649658092e-01,  4.0824829046e-01,  4.0824829046e-01],
       [ 8.1649658092e-01, -4.0824829046e-01,  4.0824829046e-01],
       [ 8.1649658092e-01,  4.0824829046e-01, -4.0824829046e-01],
       [ 4.0824829046e-01,  8.1649658092e-01,  4.0824829046e-01],
       [-4.0824829046e-01,  8.1649658092e-01,  4.0824829046e-01],
       [ 4.0824829046e-01, -8.1649658092e-01,  4.0824829046e-01],
       [ 4.0824829046e-01,  8.1649658092e-01, -4.0824829046e-01],
       [ 4.0824829046e-01,  4.0824829046e-01,  8.1649658092e-01],
       [-4.0824829046e-01,  4.0824829046e-01,  8.1649658092e-01],
       [ 4.0824829046e-01, -4.0824829046e-01,  8.1649658092e-01],
       [ 4.0824829046e-01,  4.0824829046e-01, -8.1649658092e-01]]
    
    planesPerBurg = [ 3, 3, 3, 3, 3, 3, 4, 4, 4, 2, 2,
                      2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ];
    burgPlaneIndex = [ 0, 3, 6, 9, 12, 15, 18, 22, 26, 30,
                       32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52 ];
    glideRef = [ # [54][3]
         # for burgRef[0][*] 
       [ 5.7735026919e-01, -5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [-5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [                0,                 0,                 1], # junction plane 
         # for burgRef[1][*] 
       [ 5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [ 5.7735026919e-01,  5.7735026919e-01, -5.7735026919e-01], # common glide plane 
       [                0,                 0,                 1], # junction plane 
         # for burgRef[2][*] 
       [ 5.7735026919e-01,  5.7735026919e-01, -5.7735026919e-01], # common glide plane 
       [-5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [                0,                 1,                 0], # junction plane 
         # for burgRef[3][*] 
       [ 5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [ 5.7735026919e-01, -5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [                0,                 1,                 0], # junction plane 
         # for burgRef[4][*] 
       [ 5.7735026919e-01,  5.7735026919e-01, -5.7735026919e-01], # common glide plane 
       [ 5.7735026919e-01, -5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [                1,                 0,                 0], # junction plane 
         # for burgRef[5][*] 
       [ 5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [-5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01], # common glide plane 
       [                1,                 0,                 0], # junction plane 
         # for burgRef[6][*] 
       [                0,  7.0710678118e-01,  7.0710678118e-01], # junction plane 
       [                0,  7.0710678118e-01, -7.0710678118e-01], # junction plane 
       [                0,                 1,                 0], # junction plane 
       [                0,                 0,                 1], # junction plane 
         # for burgRef[7][*] 
       [ 7.0710678118e-01,                  0,  7.0710678118e-01], # junction plane 
       [ 7.0710678118e-01,                  0, -7.0710678118e-01], # junction plane 
       [                1,                  0,                 0], # junction plane 
       [                0,                  0,                  1], # junction plane 
         # for burgRef[8][*] 
       [ 7.0710678118e-01,  7.0710678118e-01,                 0], # junction plane 
       [ 7.0710678118e-01, -7.0710678118e-01,                 0], # junction plane 
       [                0,                 1,                 0], # junction plane 
       [                1,                 0,                 0], # junction plane 
         # for burgRef[9][*] 
       [                0,  7.0710678118e-01, -7.0710678118e-01],
       [-5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[10][*] 
       [                0,  7.0710678118e-01, -7.0710678118e-01],
       [ 5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[11][*] 
       [                0,  7.0710678118e-01,  7.0710678118e-01],
       [ 5.7735026919e-01,  5.7735026919e-01, -5.7735026919e-01],
         # for burgRef[12][*] 
       [                0,  7.0710678118e-01,  7.0710678118e-01],
       [ 5.7735026919e-01, -5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[13][*] 
       [ 7.0710678118e-01,                 0, -7.0710678118e-01],
       [ 5.7735026919e-01, -5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[14][*] 
       [ 7.0710678118e-01,                 0,  7.0710678118e-01],
       [ 5.7735026919e-01,  5.7735026919e-01, -5.7735026919e-01],
         # for burgRef[15][*] 
       [ 7.0710678118e-01,                 0, -7.0710678118e-01],
       [ 5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[16][*] 
       [ 7.0710678118e-01,                 0,  7.0710678118e-01],
       [-5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[17][*] 
       [ 7.0710678118e-01, -7.0710678118e-01,                 0],
       [ 5.7735026919e-01,  5.7735026919e-01, -5.7735026919e-01],
         # for burgRef[18][*] 
       [ 7.0710678118e-01,  7.0710678118e-01,                 0],
       [ 5.7735026919e-01, -5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[19][*] 
       [ 7.0710678118e-01,  7.0710678118e-01,                 0],
       [-5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01],
         # for burgRef[20][*] 
       [ 7.0710678118e-01, -7.0710678118e-01,                 0],
       [ 5.7735026919e-01,  5.7735026919e-01,  5.7735026919e-01]]
    
    lDotb = np.dot(dir, burg)
    lMag = math.sqrt(np.dot(dir, dir))
    bMag = math.sqrt(np.dot(burg, burg))
    lMagbMag = lMag * bMag
    
    if ((lDotb/lMagbMag) * (lDotb/lMagbMag) > 0.9995):
        glidePlane = pick_screw_glide_plane(burg)
        return glidePlane
    
    glidePlane = np.cross(burg, dir)
    glidePlane = glidePlane/np.linalg.norm(glidePlane)
    
    burgRefXburg = [np.dot(val,burg) for val in burgRef]
    burgIndex = np.argmax(np.abs(burgRefXburg))
    
    numGlidePlanes  = planesPerBurg[burgIndex]
    firstGlidePlaneIndex = burgPlaneIndex[burgIndex]
    
    glideRefXglide = np.zeros(numGlidePlanes)
    for i in range(numGlidePlanes):
        glideRefXglide[i] = np.dot(glidePlane,glideRef[firstGlidePlaneIndex+i])
    glidePlaneIndex = firstGlidePlaneIndex + np.argmax(np.abs(glideRefXglide))
    
    return glideRef[glidePlaneIndex]


################################################################################
#
#    add_node_on_opposite_side():
#        move a link to a new node
#
#    Arguments:
#        nodes: a node dictionary in Python
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]] 
#        link_id: the target link to be moved
#        from_node: originally connected node of link_id
#    
################################################################################

def add_node_on_opposite_side(nodes, link_list, link_id, from_node, to_node, new_tag, OperateRec_t, ExecuteOpRec, oprec_list=None, normal=None, offset=None, home=None):

    my_link = link_list[link_id]
    first_node = my_link[1][0]   # first node in the link

    if first_node == from_node:
        my_tag  = my_link[1][-1] # last node in the link
        arm_tag = my_link[1][-2] # second to last node in the link
        new_tag_id = -1
        to_node_id = 0
    else:
        my_tag  = first_node
        arm_tag = my_link[1][1]  # second node in the link
        new_tag_id = 1
        to_node_id = -1

    new_node_position = copy.deepcopy(nodes[my_tag][0])
    new_node_position[3] = 0
    add_2_arm_node(nodes, my_tag, arm_tag, new_tag, new_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)
    #b_nbr = copy.deepcopy(nodes[new_tag][1][my_tag][0:3])
    #normal = calc_glide_plane(b_nbr, offset)
    if normal is not None:
        arm_data = np.append(copy.deepcopy(nodes[my_tag][1][new_tag][0:3]), normal)
        change_arm_data(nodes, my_tag, new_tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)

        arm_data = np.append(copy.deepcopy(nodes[new_tag][1][my_tag][0:3]), normal)
        change_arm_data(nodes, new_tag, my_tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)

    link_list[link_id][1].insert(new_tag_id,new_tag)
    link_list[link_id][1][to_node_id] = to_node

    return link_list


def change_arm_data(nodes, tag, arm_tag, arm_data, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):
    nodes[tag][1][arm_tag] = arm_data
    op = {'optype':optype.CHANGE_ARM_BURG, 'tag':tag, 'arm_tag':arm_tag, 'arm_data':arm_data}
    if oprec_list != None: oprec_list.append(op)
    if home != None: paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)


def enforce_glide_plane(nodes, bounds, tag1, tag2, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    x1 = copy.deepcopy(nodes[tag1][0][0:3])
    x2 = copy.deepcopy(nodes[tag2][0][0:3])
    V = pbc_dr_bounds(x1, x2, bounds)

    burg = copy.deepcopy(nodes[tag1][1][tag2][0:3])
    normal = FindFCCGlidePlane_python(burg, V)
    armsysID = which_slipSystem( np.array(burg), np.array(normal) )[0]

    arm_data = np.append(np.append(burg, normal), armsysID)
    change_arm_data(nodes, tag1, tag2, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)
    arm_data = np.append(np.append(-burg, normal), armsysID)
    change_arm_data(nodes, tag2, tag1, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)


def generate_jog_on_node(nodes, bounds, tag, length, dir, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    x = copy.deepcopy(nodes[tag][0][0:3])
    arm_tag_list = list(nodes[tag][1].keys())
    new_tag = None
    for arm_tag in arm_tag_list:

        # add new nodes on original link
        x_arm = copy.deepcopy(nodes[arm_tag][0][0:3])
        V = pbc_dr_bounds(x, x_arm, bounds)
        V =  V/np.linalg.norm(V)
        x_orig = x + V*length/2
        orig_node_position = np.append(x_orig, 0)
        orig_tag = get_new_node_tag(nodes, new_tag, home)
        add_2_arm_node(nodes, tag, arm_tag, orig_tag, orig_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)

        # add new nodes on jog's position
        x_jog = x_orig + dir
        jog_node_position = np.append(x_jog, 0)
        jog_tag = get_new_node_tag(nodes, new_tag, home)
        add_2_arm_node(nodes, tag, orig_tag, jog_tag, jog_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)
        
        # change glide plane of jog
        enforce_glide_plane(nodes, bounds, orig_tag, jog_tag, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # translate the target node
    new_pos = x + dir
    reset_position(nodes, tag, new_pos, OperateRec_t, ExecuteOpRec, oprec_list, home)


def generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None, material_type=mat_type.FCC):

    # Information of the target link
    my_link = link_list[link_id]
    link_sysID = my_link[2]
    burg = copy.deepcopy(nodes[my_link[1][0]][1][my_link[1][1]][0:3])
    bid = burgID_from_sysID(link_sysID, material_type)
    link_nid = planeID_from_sysID(link_sysID, material_type)

    # only some planes are active
    #if link_sysID != 8 and link_sysID != 11:
    #    return link_list

    # if a new jog cannot glide, skip the jog generation
    if bid == -1:
        return link_list

    # Calculate lengths
    contour_length = my_link[0]
    jog_length = contour_length * jog_length_ratio
    first_length = (contour_length - jog_length) / 2
    second_length = first_length + jog_length

    # Find the position of jog
    link_length = 0
    node_tags_between_jogs = []
    jog_start = False
    for i in range(len(my_link[1])-1):
        tag_i = my_link[1][i]
        tag_j = my_link[1][i+1]
        xi = copy.deepcopy(nodes[tag_i][0][0:3])
        xj = copy.deepcopy(nodes[tag_j][0][0:3])
        V = pbc_dr_bounds(xi, xj, bounds)
        length_ij = np.linalg.norm(V)
        V = V / length_ij

        if link_length < first_length and first_length < link_length + length_ij:
            jog1_pos = xi + (first_length - link_length)*V
            arm_tag_of_jog1 = [tag_i, tag_j]
            jog1_index = i
            jog_start = True

        if link_length < second_length and second_length < link_length + length_ij:
            jog2_pos = xi + (second_length - link_length)*V
            arm_tag_of_jog2 = [tag_i, tag_j]
            jog2_index = i + 1
            break

        link_length += length_ij
        if jog_start:
            node_tags_between_jogs.append(tag_j)

    # Create nodes on jog's position
    jog1_tag1 = get_new_node_tag(nodes, None, home)
    jog1_node_position = np.append(jog1_pos,0)
    add_2_arm_node(nodes, arm_tag_of_jog1[0], arm_tag_of_jog1[1], jog1_tag1, jog1_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)

    jog1_tag2 = get_new_node_tag(nodes, jog1_tag1, home)
    jog1_node_position = np.append(jog1_pos,0)
    add_2_arm_node(nodes, jog1_tag1, arm_tag_of_jog1[1], jog1_tag2, jog1_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)
    node_tags_between_jogs.insert(0,jog1_tag2)

    if set(arm_tag_of_jog1) == set(arm_tag_of_jog2):
        arm_tag_of_jog2[0] = jog1_tag2

    jog2_tag1 = get_new_node_tag(nodes, jog1_tag2, home)
    jog2_node_position = np.append(jog2_pos,0)
    add_2_arm_node(nodes, arm_tag_of_jog2[0], arm_tag_of_jog2[1], jog2_tag1, jog2_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)

    jog2_tag2 = get_new_node_tag(nodes, jog2_tag1, home)
    jog2_node_position = np.append(jog2_pos,0)
    add_2_arm_node(nodes, jog2_tag1, arm_tag_of_jog2[0], jog2_tag2, jog2_node_position, OperateRec_t, ExecuteOpRec, oprec_list, home)
    node_tags_between_jogs.append(jog2_tag2)

    # Find jog's glide plane
    if material_type == mat_type.FCC:
        jog_nid = 1
        jog_glide_plane = np.array([1, 1, 1])/math.sqrt(3)
        if link_sysID == 2 or link_sysID == 4 or link_sysID == 6 or ( link_sysID == 12 and bid == 5 ):
            jog_nid = 3
            jog_glide_plane[1] = -jog_glide_plane[1]
        elif link_sysID == 3 or link_sysID == 7 or link_sysID == 11 or ( link_sysID == 12 and bid == 2 ):
            jog_nid = 0
            jog_glide_plane[0] = -jog_glide_plane[0]
        elif link_sysID == 1 or link_sysID == 5 or link_sysID == 9 or ( link_sysID == 12 and bid == 0 ):
            jog_nid = 2
            jog_glide_plane[2] = -jog_glide_plane[2]
        elif link_sysID == 12 or bid == -1 or link_nid == -1:
            print("  generate_jog_on_link(): You're not supposed to be here!")

    elif material_type == mat_type.HCP:
        if link_sysID < 3:
            jog_nid = bid + 3
        elif link_sysID < 6:
            jog_nid = bid - 3
        elif link_sysID < 12:
            jog_nid = 0
        jog_glide_plane = plane_normal_vector_from_planeID(jog_nid, material_type)

    # Calculate jog direction vector
    link_dir = pbc_dr_bounds(jog1_pos, jog2_pos, bounds)
    jog_dir = np.cross( link_dir, jog_glide_plane )
    jog_dir = jog_dir/np.linalg.norm(jog_dir) * jog_height

    # Translate node to make jog
    for node_tag in node_tags_between_jogs:
        pos = copy.deepcopy(nodes[node_tag][0][0:3])
        new_pos = pos + jog_dir
        reset_position(nodes, node_tag, new_pos, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # Change the glide plane of jogs
    jog_sysID = sysID_from_nid_and_bid(jog_nid, bid, material_type)
    arm_data = np.append(np.append( burg, jog_glide_plane ), jog_sysID)
    change_arm_data(nodes, jog1_tag1, jog1_tag2, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)
    change_arm_data(nodes, jog2_tag2, jog2_tag1, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)
    arm_data = np.append(np.append( -burg, jog_glide_plane ), jog_sysID)
    change_arm_data(nodes, jog1_tag2, jog1_tag1, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)
    change_arm_data(nodes, jog2_tag1, jog2_tag2, arm_data, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # Tear the target link into 5 links
    link1_node_tags = my_link[1][0:jog1_index+1]
    link1_node_tags.append(jog1_tag1)
    link_list[link_id] = [first_length, link1_node_tags, link_sysID] # 1st link

    link2_node_tags = my_link[1][jog2_index:len(my_link[1])+1]
    link2_node_tags.insert(0,jog2_tag1)
    link_list.append([first_length, link2_node_tags, link_sysID]) # last link

    link_list.append([jog_length, node_tags_between_jogs, link_sysID]) # center of jog
    link_list.append([jog_height, [jog1_tag1, jog1_tag2], jog_sysID]) # jog1
    link_list.append([jog_height, [jog2_tag1, jog2_tag2], jog_sysID]) # jog2

    return link_list


def generate_jog_on_datafile(datafile, crit, probability, jog_length_ratio, jog_height, OperateRec_t, ExecuteOpRec, save_dir=None, oprec_list=None, home=None, material_type=mat_type.FCC):

    filename, extension = os.path.splitext(datafile)
    if save_dir != None:
        dirname, basename = os.path.split(filename)
        save_filename = save_dir + basename
    else:
        save_filename = filename

    nodes, bounds, aux_data = read_nodes_to_dict(datafile, need_aux_data=True)
    num_nodes_before_jog = len(nodes)
    link_list = make_link_list(nodes, bounds, check_slip_system=True)
    write_link_list(save_filename, link_list, None, suffix='_before_generation.linkData')
    target_link_ids = find_links_to_be_operated(link_list, crit)
    
    num = 0
    jog_link_ids = []

    # jog generations on multiple links
    for link_id in target_link_ids:
        f = random.random()
        if f < probability and crit[0]<link_list[link_id][0]:
            num += 1
            link_list = generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, OperateRec_t, ExecuteOpRec, oprec_list, home, material_type)
            jog_link_ids.append(link_id)

    print('    The number of link to generate jogs: %d'%num)
    print('    Total number of nodes is change from %d to %d.'%(num_nodes_before_jog,len(nodes)))
    if home != None:
        SortNativeNodes(home)

    link_list.clear()
    link_list = make_link_list(nodes, bounds, check_slip_system=True)
    write_link_list(save_filename, link_list, None, suffix='_after_generation.linkData')

    jog_filename = save_filename + '_jog'
    write_jog_generation_information(crit, probability, jog_length_ratio, jog_height, num, jog_link_ids, save_filename, suffix='.jog_info')
    write_data_file_with_aux_data(nodes, bounds, aux_data, jog_filename)

    return save_filename


def write_jog_generation_information(crit, probability, jog_length_ratio, jog_height, num, link_ids, taskname, suffix='.jog_info'):
 
    pathname, extension = os.path.splitext(taskname)
    FileName = pathname + suffix
    
    fileID = open(FileName,'w')
    fileID.write("# Information about jog generations\n\n")
    fileID.write("  crit_length = %d < l < %d  \n"%(crit[0],crit[1]))
    fileID.write("  probability = %.2f  \n"%probability)
    fileID.write("  jog_length_ratio = %.6f  \n"%jog_length_ratio)
    fileID.write("  jog_height = %d  \n"%jog_height)

    fileID.write("  link_ids = \n")
    for link_id in link_ids:
        fileID.write(" %d"%link_id)

    fileID.close

################################################################################
#
#    remove_link():
#        remove dislocation link and translate nodes in the attached links
#
#    Arguments:
#        nodes: node dictionary in Python
#        bounds: an 2x3 array for domain boundary
#            [[xMin,yMin,zMin],[xMax,yMax,zMax]]
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]]
#        link_id_to_remove: link id to be removed
#
#    Return:
#        link_list: link list after suppressing junction
#
################################################################################

def remove_link(nodes, bounds, link_list, link_id_to_remove, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    link_to_remove = link_list[link_id_to_remove]

    # if the target link is already removed, do nothing
    if len(link_to_remove[1])==0:
        return link_list

    end_nodes = [ link_to_remove[1][0], link_to_remove[1][-1] ]
    nbr_link_ids = find_connected_link_ids(link_list, link_id_to_remove)
    sum_contour_lengths = [ np.array([link_list[ link_id ][0] for link_id in nbr_link_ids[0]]).sum(),
                            np.array([link_list[ link_id ][0] for link_id in nbr_link_ids[1]]).sum() ]
    end_to_move = 0 if sum_contour_lengths[0] < sum_contour_lengths[1] else 1
    end_to_stay = 1 - end_to_move

    # if the target link to be removed is a loop, this link is removed without
    #     changing of neighbor links
    if end_nodes[0] == end_nodes[1]:
        link_list = remove_link_from_nodes_dict(nodes, link_list, link_id_to_remove, OperateRec_t, ExecuteOpRec, oprec_list, home)
        return link_list

    x0 = copy.deepcopy(nodes[end_nodes[end_to_move]][0][0:3])
    x1 = copy.deepcopy(nodes[end_nodes[end_to_stay]][0][0:3])
    offset = pbc_dr_bounds(x0, x1, bounds) 

    # There are 4 methods to specify glide plane of new dislocation. Other 2
    #     methods are in add_node_on_opposite_side().

    # 1. glidePlane is refered from the neighbor dislocation
    normal = None

    # 2. glidePlane is refered from that of junction to be removed (normal)
    #normal = np.array(nodes[link_to_remove[1][0]][1][link_to_remove[1][1]][3:6])

    new_tag = None
    # modifying neighbor links
    for nbr_link_id in nbr_link_ids[end_to_move]:
        nbr_link = link_list[nbr_link_id]

        # Case I: this link is a loop, just shift it to the other node
        if nbr_link[1][0] == nbr_link[1][-1]:
            link_list[nbr_link_id][1][ 0] = end_nodes[end_to_stay]
            link_list[nbr_link_id][1][-1] = end_nodes[end_to_stay]
        # Case II: this link connects two end points, will become a loop, here we will remove it
        elif set([ nbr_link[1][0], nbr_link[1][-1] ]) == set([ end_nodes[0], end_nodes[1] ]):
            link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id, OperateRec_t, ExecuteOpRec, oprec_list, home)
            continue
        # Case III: most general links
        else:
            new_tag = get_new_node_tag(nodes, new_tag, home)
            link_list = add_node_on_opposite_side(nodes, link_list, nbr_link_id,
                                                  end_nodes[end_to_move], end_nodes[end_to_stay],
                                                  new_tag, OperateRec_t, ExecuteOpRec, oprec_list, normal, offset, home)
            nbr_link = link_list[nbr_link_id]

        # move all nodes except the end nodes
        for tag in nbr_link[1][1:-1]:
            new_pos = copy.deepcopy(nodes[tag][0][0:3]) + offset
            reset_position(nodes, tag, new_pos, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # Delete discretization nodes
    for tag in link_to_remove[1][1:-1]:
        remove_2_arm_node(nodes, tag, OperateRec_t, ExecuteOpRec, oprec_list, home)

    # remove the dangling node at end_to_move
    merge_node(nodes, end_nodes[end_to_move], end_nodes[end_to_stay], OperateRec_t, ExecuteOpRec, oprec_list, home)
    link_list[link_id_to_remove][1] = []

    # append two-link lists if the number of arms of the end node is 2
    tag = end_nodes[end_to_stay]
    if len(nodes[tag][1])==2:
        link_list = append_2_link_lists(link_list, nbr_link_ids, tag)

    return link_list


def append_2_link_lists(link_list, link_ids, tag):

    # find link ids including the node
    for link_id in link_ids[0]:
        if tag in link_list[link_id][1]: id0 = link_id
    for link_id in link_ids[1]:
        if tag in link_list[link_id][1]: id1 = link_id
    
    # reverse the node list
    if tag == link_list[id0][1][0]:
        link_list[id0][1].reverse()
        if tag == link_list[id1][1][-1]:
            link_list[id1][1].reverse()
    elif tag == link_list[id0][1][-1] and tag == link_list[id1][1][-1]:
        link_list[id1][1].reverse()

    # append two link lists
    link_list[id0][0] += link_list[id1][0]
    link_list[id0][1] += link_list[id1][1][1:]
    link_list[id1][1] = []

    return link_list


def check_dissociate_possible(nodes, end_nodes, link_list, nbr_link_ids):

    match_ids = -np.ones(len(nbr_link_ids[0]), dtype=int)
    if len(nbr_link_ids[0]) != 2 or len(nbr_link_ids[0]) != len(nbr_link_ids[1]):
        return False, match_ids, -1

    burg = [[],[]]
    adj_node_tags = [[],[]]
    for end_id in range(2):
        for link_id in nbr_link_ids[end_id]:
            my_link = link_list[link_id]

            # find end_nodes[end_id]'s arm
            arm_tag = my_link[1][1] if my_link[1][0] == end_nodes[end_id] else my_link[1][-2]

            burg[end_id].append(copy.deepcopy(nodes[end_nodes[end_id]][1][arm_tag][0:3]))
            adj_node_tags[end_id].append(arm_tag)

    for burg0_id, burg0 in enumerate(burg[0]):
        for burg1_id, burg1 in enumerate(burg[1]):
            if not any(burg0 + burg1):
                match_ids[burg0_id] = burg1_id

    if all(match_ids!=-1):
        return True, match_ids, adj_node_tags
    else: 
        return False, match_ids, -1


def find_link_id_from_end_nodes(nodes, link_list, end_nodes):

    for link_id, my_link in enumerate(link_list):
        if len(my_link[1])<2:
            continue
        first_node = my_link[1][0]   # first node in the link
        last_node  = my_link[1][-1]  # last node in the link
        if set([first_node, last_node]) == set([end_nodes[0], end_nodes[1]]):
            return True, link_id

    return False, -1


def change_link_burg(nodes, my_link, burg_ij, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    for id in range(len(my_link[1])-1):
        tag_i = my_link[1][id]
        tag_j = my_link[1][id+1]
        normal = copy.deepcopy(nodes[tag_i][1][tag_j][3:6])
        arm_data_ij = np.append( burg_ij, normal)
        arm_data_ji = np.append(-burg_ij, normal)
        change_arm_data(nodes, tag_i, tag_j, arm_data_ij, OperateRec_t, ExecuteOpRec, oprec_list, home)
        change_arm_data(nodes, tag_j, tag_i, arm_data_ji, OperateRec_t, ExecuteOpRec, oprec_list, home)


################################################################################
#
#    dissociate_link():
#        remove dislocation link and translate nodes in the attached links
#
#    Arguments:
#        nodes: node dictionary in Python
#        bounds: an 2x3 array for domain boundary
#            [[xMin,yMin,zMin],[xMax,yMax,zMax]]
#        link_list: a list including contour lengths and node lists. Each link
#            is defined by dislocations between two physical nodes. In the node
#            tag list, start and end nodes denotes the two pysical nodes.
#            [contour_length, [node_tag1, node_tag2, ...]]
#        link_id_to_remove: link id to be removed
#
#    Return:
#        link_list: link list after suppressing junction
#
################################################################################

def dissociate_link_by_change_arm(nodes, bounds, link_list, link_id_to_dissociate, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    link_to_dissociate = link_list[link_id_to_dissociate]

    # if the target link is already removed, do nothing
    if len(link_to_dissociate[1]) == 0:
        return link_list, False

    end_nodes = [ link_to_dissociate[1][0], link_to_dissociate[1][-1] ]
    nbr_link_ids = find_connected_link_ids(link_list, link_id_to_dissociate)

    # if the target link to be removed is a loop, this link is removed without
    #     changing of neighbor links
    if end_nodes[0] == end_nodes[1]:
        link_list = remove_link_from_nodes_dict(nodes, link_list, link_id_to_dissociate, OperateRec_t, ExecuteOpRec, oprec_list, home)
        return link_list, False

    ispossible, match_ids, adj_node_tags = check_dissociate_possible(nodes, end_nodes, link_list, nbr_link_ids)

    if ispossible:
        is_target_remove = True
        for arm_id in range(2):
            nbr_link_id  = [ nbr_link_ids[0][arm_id]    , nbr_link_ids[1][ match_ids[arm_id] ] ]
            nbr_link     = [ link_list[ nbr_link_id[0] ], link_list[ nbr_link_id[1] ]          ]

            opposite_id = match_ids[arm_id]
            if len(nbr_link[1][1])==0:
                opposite_id = 1 if match_ids[arm_id] == 0 else 0 
                nbr_link_id  = [ nbr_link_ids[0][arm_id]    , nbr_link_ids[1][ opposite_id ] ]
                nbr_link     = [ link_list[ nbr_link_id[0] ], link_list[ nbr_link_id[1] ]    ]
            adj_node_tag = [ adj_node_tags[0][arm_id]   , adj_node_tags[1][ opposite_id ] ]
            first_node   = [ nbr_link[0][1][0]               , nbr_link[1][1][0]                ] # first node in the link
            num_adj_nbr  = [ len(nodes[ adj_node_tag[0] ][1]), len(nodes[ adj_node_tag[1] ][1]) ]
        
            # this link connects two end points, here we remove it
            if set([ nbr_link[0][1][0], nbr_link[0][1][-1] ]) == set([ end_nodes[0], end_nodes[1] ]):
                link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
                continue

            # Triangular case
            opposite_node_id = [ -1 if first_node[0] == end_nodes[0] else 0,
                                 -1 if first_node[1] == end_nodes[1] else 0  ]
            opposite_tag = [ nbr_link[0][1][ opposite_node_id[0] ],
                             nbr_link[1][1][ opposite_node_id[1] ] ]
            if opposite_tag[0] == opposite_tag[1] and ( num_adj_nbr[0]>2 or num_adj_nbr[1]>2 ):
                if len(nodes[opposite_tag[0]][1].keys())<4: # triangle is not removed
                    if first_node[0] == end_nodes[0]:
                        my_tag  = first_node[0]
                        arm_tag = my_link[0][1][1]   # second node in the link
                    else:
                        my_tag  = my_link[0][1][-1]  # last node in the link
                        arm_tag = my_link[0][1][-2]  # second to last node in the link

                    burg_ij = -copy.deepcopy(nodes[my_tag][1][arm_tag][0:3])
                    change_link_burg(nodes, link_to_dissociate, burg_ij, OperateRec_t, ExecuteOpRec, oprec_list, home)
                    is_target_remove = False

                    # Append 3 link lists
                    for nbr_id in range(2):
                        append_link_ids = [ [ link_id_to_dissociate ], [ nbr_link_id[nbr_id] ]]
                        link_list = append_2_link_lists(link_list, append_link_ids, end_nodes[nbr_id])
                        link_list[ nbr_link_id[nbr_id] ][1] = []

                else: # remove triangle
                    link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
                    link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

                    if len(nodes[opposite_tag[0]][1].keys())==2: # append link list
                        append_link_ids = []
                        for link_id, my_link in enumerate(link_list):
                            if len(my_link[1])<2:
                                continue
                            if my_link[1][0]==opposite_tag[0] or my_link[1][-1]==opposite_tag[0]:
                                append_link_ids.append([link_id])
                        if len(append_link_ids)==2:
                            link_list = append_2_link_lists(link_list, append_link_ids, opposite_tag[0])

                continue
                
            # Rectangular case
            if num_adj_nbr[0]>2 or num_adj_nbr[1]>2:
                there_is_link, center_link_id = find_link_id_from_end_nodes(nodes, link_list, adj_node_tag)
                if there_is_link:
                    my_link = link_list[center_link_id]
                    first_node = my_link[1][0]   # first node in the link
                    start_tag  = my_link[1][1]   # second node in the link
                    last_node  = my_link[1][-1]  # last node in the link

                    burg = copy.deepcopy(nodes[adj_node_tag[0]][1][end_nodes[0]][0:3])
                    if first_node == adj_node_tag[1]: burg = -burg 
                    burg_ij = burg + copy.deepcopy(nodes[first_node][1][start_tag][0:3])
                    change_link_burg(nodes, my_link, burg_ij, OperateRec_t, ExecuteOpRec, oprec_list, home)

                    link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
                    link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

                    # Append link lists
                    nbr_center_link_ids = find_connected_link_ids(link_list, center_link_id)
                    center_end_nodes = [ first_node, last_node ]
                    if nbr_center_link_ids[0][0] != nbr_center_link_ids[1][0]:
                        for nbr_id in range(2):
                            if len(nbr_center_link_ids[nbr_id]) == 1:
                                append_link_ids = [ [ center_link_id ], [ nbr_center_link_ids[nbr_id][0] ] ]
                                link_list = append_2_link_lists(link_list, append_link_ids, center_end_nodes[nbr_id])
                                link_list[ nbr_center_link_ids[nbr_id][0] ][1] = []

                    continue

            # Change arms attached to end_nodes[0]
            change_arm(nodes, adj_node_tag[0], end_nodes[0], adj_node_tag[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

            # Change arm data
            enforce_glide_plane(nodes, bounds, adj_node_tag[0], adj_node_tag[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

            # Detach arm end_nodes[1] from arms attached to end_nodes[1]
            remove_node(nodes, OperateRec_t, ExecuteOpRec, adj_node_tag[1], end_nodes[1], oprec_list, home)
            remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[1], adj_node_tag[1], oprec_list, home)

            # Match end nodes in link_list before appending two link lists
            end_node_id = 0 if first_node[0] == end_nodes[0] else -1
            link_list[ nbr_link_id[0] ][1][end_node_id] = end_nodes[1]

            # Append two link lists
            append_link_ids = [ [ nbr_link_id[0] ], [ nbr_link_id[1] ] ]
            link_list = append_2_link_lists(link_list, append_link_ids, end_nodes[1])
            del link_list[ nbr_link_id[0] ][1][ nbr_link[0][1].index(end_nodes[1]) ]

        # Remove the target link
        if is_target_remove:
            link_list = remove_link_from_nodes_dict(nodes, link_list, link_id_to_dissociate, OperateRec_t, ExecuteOpRec, oprec_list, home)
            remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[0], None, oprec_list, home)
            remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[1], None, oprec_list, home)

        return link_list, True

    else:
        return link_list, False


def dissociate_link_by_split_node(nodes, bounds, link_list, link_id_to_dissociate, OperateRec_t, ExecuteOpRec, oprec_list=None, home=None):

    link_to_dissociate = link_list[link_id_to_dissociate]

    # if the target link is already removed, do nothing
    if len(link_to_dissociate[1]) == 0:
        return link_list, False

    end_nodes = [ link_to_dissociate[1][0], link_to_dissociate[1][-1] ]
    nbr_link_ids = find_connected_link_ids(link_list, link_id_to_dissociate)

    # if the target link to be removed is a loop, this link is removed without
    #     changing of neighbor links
    if end_nodes[0] == end_nodes[1]:
        link_list = remove_link_from_nodes_dict(nodes, link_list, link_id_to_dissociate, OperateRec_t, ExecuteOpRec, oprec_list, home)
        return link_list, False

    ispossible, match_ids, adj_node_tags = check_dissociate_possible(nodes, end_nodes, link_list, nbr_link_ids)

    if ispossible:

        # Delete discretization nodes
        for tag in link_to_dissociate[1][1:-1]:
            remove_2_arm_node(nodes, tag, OperateRec_t, ExecuteOpRec, oprec_list, home)

        # merge node
        merge_node(nodes, end_nodes[0], end_nodes[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        # split node: recycle end_nodes[0]
        pos = copy.deepcopy(nodes[end_nodes[1]][0][0:3])
        arm_tag_list = [ [ adj_node_tags[0][0], adj_node_tags[1][ match_ids[0] ] ],
                         [ adj_node_tags[0][1], adj_node_tags[1][ match_ids[1] ] ] ]
        split_node(nodes, bounds, end_nodes[1], end_nodes[0], pos, pos, arm_tag_list[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        # remove arm
        remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[1], end_nodes[0], oprec_list, home)
        remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[0], end_nodes[1], oprec_list, home)
        remove_2_arm_node(nodes, end_nodes[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
        remove_2_arm_node(nodes, end_nodes[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        # modify link_list
        link_list[link_id_to_dissociate][1] = []

        for i, link_id in enumerate(nbr_link_ids[0]):
            my_link = link_list[link_id]
            first_node = my_link[1][0]   # first node in the link

            end_node_id = 0 if first_node == end_nodes[0] else -1
            link_list[link_id][1][end_node_id] = end_nodes[1]

            append_link_ids = [ [ link_id ], [ nbr_link_ids[1][ match_ids[i] ] ] ]
            link_list = append_2_link_lists(link_list, append_link_ids, end_nodes[1])
            del link_list[ link_id ][1][ link_list[link_id][1].index(end_nodes[1]) ]

        #is_target_remove = True
        #for arm_id in range(2):
        #    nbr_link_id  = [ nbr_link_ids[0][arm_id]         , nbr_link_ids[1][ match_ids[arm_id] ]  ]
        #    nbr_link     = [ link_list[ nbr_link_id[0] ]     , link_list[ nbr_link_id[1] ]           ]
        #    first_node   = [ nbr_link[0][1][0]               , nbr_link[1][1][0]                     ] # first node in the link
        #    adj_node_tag = [ adj_node_tags[0][arm_id]        , adj_node_tags[1][ match_ids[arm_id] ] ]
        #    num_adj_nbr  = [ len(nodes[ adj_node_tag[0] ][1]), len(nodes[ adj_node_tag[1] ][1])      ]
        #
        #    # this link connects two end points, here we remove it
        #    if set([ nbr_link[0][1][0], nbr_link[0][1][-1] ]) == set([ end_nodes[0], end_nodes[1] ]):
        #        link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
        #        continue

        #    # Triangular case
        #    opposite_node_id = [ -1 if first_node[0] == end_nodes[0] else 0,
        #                         -1 if first_node[1] == end_nodes[1] else 0  ]
        #    opposite_tag = [ nbr_link[0][1][ opposite_node_id[0] ],
        #                     nbr_link[1][1][ opposite_node_id[1] ] ]
        #    if opposite_tag[0] == opposite_tag[1] and ( num_adj_nbr[0]>2 or num_adj_nbr[1]>2 ):
        #        if len(nodes[opposite_tag[0]][1].keys())<4:
        #            if first_node[0] == end_nodes[0]:
        #                my_tag  = first_node[0]
        #                arm_tag = my_link[0][1][1]   # second node in the link
        #            else:
        #                my_tag  = my_link[0][1][-1]  # last node in the link
        #                arm_tag = my_link[0][1][-2]  # second to last node in the link

        #            burg_ij = -copy.deepcopy(nodes[my_tag][1][arm_tag][0:3])
        #            change_link_burg(nodes, link_to_dissociate, burg_ij, OperateRec_t, ExecuteOpRec, oprec_list, home)
        #            is_target_remove = False

        #            # Append 3 link lists
        #            for nbr_id in range(2):
        #                append_link_ids = [ [ link_id_to_dissociate ], [ nbr_link_id[nbr_id] ]]
        #                link_list = append_2_link_lists(link_list, append_link_ids, end_nodes[nbr_id])
        #                link_list[ nbr_link_id[nbr_id] ][1] = []

        #        else: 
        #            link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
        #            link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        #        continue
        #        
        #    # Rectangular case
        #    if num_adj_nbr[0]>2 or num_adj_nbr[1]>2:
        #        there_is_link, center_link_id = find_link_id_from_end_nodes(nodes, link_list, adj_node_tag)
        #        if there_is_link:
        #            my_link = link_list[center_link_id]
        #            first_node = my_link[1][0]   # first node in the link
        #            start_tag  = my_link[1][1]   # second node in the link
        #            last_node  = my_link[1][-1]  # last node in the link

        #            burg = copy.deepcopy(nodes[adj_node_tag[0]][1][end_nodes[0]][0:3])
        #            if first_node == adj_node_tag[1]: burg = -burg 
        #            burg_ij = burg + copy.deepcopy(nodes[first_node][1][start_tag][0:3])
        #            change_link_burg(nodes, my_link, burg_ij, OperateRec_t, ExecuteOpRec, oprec_list, home)

        #            link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[0], OperateRec_t, ExecuteOpRec, oprec_list, home)
        #            link_list = remove_link_from_nodes_dict(nodes, link_list, nbr_link_id[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        #            # Append link lists
        #            nbr_center_link_ids = find_connected_link_ids(link_list, center_link_id)
        #            center_end_nodes = [ first_node, last_node ]
        #            if nbr_center_link_ids[0][0] != nbr_center_link_ids[1][0]:
        #                for nbr_id in range(2):
        #                    if len(nbr_center_link_ids[nbr_id]) == 1:
        #                        append_link_ids = [ [ center_link_id ], [ nbr_center_link_ids[nbr_id][0] ] ]
        #                        link_list = append_2_link_lists(link_list, append_link_ids, center_end_nodes[nbr_id])
        #                        link_list[ nbr_center_link_ids[nbr_id][0] ][1] = []

        #            continue

        #    # Change arms attached to end_nodes[0]
        #    change_arm(nodes, adj_node_tag[0], end_nodes[0], adj_node_tag[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        #    # Change arm data
        #    #enforce_glide_plane(nodes, bounds, adj_node_tag[0], adj_node_tag[1], OperateRec_t, ExecuteOpRec, oprec_list, home)

        #    # Detach arm end_nodes[1] from arms attached to end_nodes[1]
        #    remove_node(nodes, OperateRec_t, ExecuteOpRec, adj_node_tag[1], end_nodes[1], oprec_list, home)
        #    remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[1], adj_node_tag[1], oprec_list, home)

        #    # Match end nodes in link_list before appending two link lists
        #    end_node_id = 0 if first_node[0] == end_nodes[0] else -1
        #    link_list[ nbr_link_id[0] ][1][end_node_id] = end_nodes[1]

        #    # Append two link lists
        #    append_link_ids = [ [ nbr_link_id[0] ], [ nbr_link_id[1] ] ]
        #    link_list = append_2_link_lists(link_list, append_link_ids, end_nodes[1])
        #    del link_list[ nbr_link_id[0] ][1][ nbr_link[0][1].index(end_nodes[1]) ]

        ## Remove the target link
        #if is_target_remove:
        #    link_list = remove_link_from_nodes_dict(nodes, link_list, link_id_to_dissociate, OperateRec_t, ExecuteOpRec, oprec_list, home)
        #    remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[0], None, oprec_list, home)
        #    remove_node(nodes, OperateRec_t, ExecuteOpRec, end_nodes[1], None, oprec_list, home)

        return link_list, True

    else:
        return link_list, False


def get_new_node_tag(nodes, new_tag, home=None):

    if home != None:
        if new_tag == None:
            return get_new_node_tag_from_home(home)
        else:
            return (0, new_tag[1]+1)
    else:
        return get_new_node_tag_from_nodes(nodes)


################################################################################
#
#    remove_node():
#        remove a node in node dictionary
#
#    Arguments:
#        nodes: a node dictionary in Python
#        tag: a node to be removed or designated arm
#        arm_tag: an arm tag of node to be removed
#    
################################################################################

def remove_node(nodes, OperateRec_t, ExecuteOpRec, tag, arm_tag=None, oprec_list=None, home=None):
    if tag in nodes:
        if arm_tag==None:
            del nodes[tag]
            op = {'optype':optype.REMOVE_NODE, 'tag':tag}
            if oprec_list != None: oprec_list.append(op)
            if home != None: paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)

        elif arm_tag in nodes[tag][1]:
            del nodes[tag][1][arm_tag]
            op = {'optype':optype.CHANGE_ARM_BURG, 'tag':tag, 'arm_tag':arm_tag, 'arm_data':np.array([0, 0, 0, 0, 0, 0])}
            if oprec_list != None: oprec_list.append(op)
            if home != None: paradis_execute_oprec(home, op, OperateRec_t, ExecuteOpRec)


################################################################################
#
#    compare_data_files():
#        comparisons of two data files
#
#    Arguments:
#        datafile1: data file name
#        datafile2: data file name
#
################################################################################

def compare_data_files(datafile1, datafile2):

    print('===== ',datafile1)
    nodes1, bounds1 = read_nodes_to_dict(datafile1)

    print('===== ',datafile2)
    nodes2, bounds1 = read_nodes_to_dict(datafile2)

    return compare_nodes_dict(nodes1,nodes2)


################################################################################
#
#    compare_nodes_dict():
#        comparisons of two nodes dictionaries 
#
#    Arguments:
#        nodes1: node dictionary in Python
#        nodes2: node dictionary in Python
#
################################################################################

def compare_nodes_dict(nodes1, nodes2):

    tol = 1e-10 # tolerance for comparing burgers vector and glide plane normal vector

    nodes1_node_tag_list = sorted(nodes1.keys())
    nodes2_node_tag_list = sorted(nodes2.keys())
    all_node_tag_list = set( nodes1_node_tag_list + nodes2_node_tag_list )

    nodes_are_same = True
    for tag in sorted(all_node_tag_list):

        if not tag in nodes1_node_tag_list:
            nodes_are_same = False
            print('> (%d,%d):'%(tag[0],tag[1]), end='')
            for arm_tag in nodes2[tag][1]:
                print(' (%d,%d)'%(arm_tag[0],arm_tag[1]), end='')
            print()

        elif not tag in nodes2_node_tag_list:
            nodes_are_same = False
            print('< (%d,%d):'%(tag[0],tag[1]), end='')
            for arm_tag in sorted(nodes1[tag][1].keys()):
                print(' (%d,%d)'%(arm_tag[0],arm_tag[1]), end='')
            print()

        else:
            nodes1_arm_tag_list = sorted(nodes1[tag][1].keys())
            nodes2_arm_tag_list = sorted(nodes2[tag][1].keys())
            all_arm_tag_list = set( nodes1_arm_tag_list + nodes2_arm_tag_list ) 

            node_tag_flag = 1
            for arm_tag in all_arm_tag_list:

                if node_tag_flag==1 and ( (not arm_tag in nodes1_arm_tag_list) or (not arm_tag in nodes2_arm_tag_list) ):
                    nodes_are_same = False
                    node_tag_flag = 0
                    print(' (%d,%d)'%(tag[0],tag[1]))
                 
                if not arm_tag in nodes1_arm_tag_list:
                    nodes_are_same = False
                    print('> (%d,%d)'%(arm_tag[0],arm_tag[1]))
        
                elif not arm_tag in nodes2_arm_tag_list:
                    nodes_are_same = False
                    print('< (%d,%d)'%(arm_tag[0],arm_tag[1]))

                else:
                    nodes1_arm_data = nodes1[tag][1][arm_tag]
                    nodes2_arm_data = nodes2[tag][1][arm_tag]
                    
                    arm_data_are_same = 1
                    for i in range(6):
                        if abs(nodes1_arm_data[i]-nodes2_arm_data[i])>tol:
                            arm_data_are_same = 0
                            break
                    
                    if arm_data_are_same == 0:
                        nodes_are_same = False
                        print(' (%d,%d)'%(tag[0],tag[1]))
                        print('< (%d,%d):'%(arm_tag[0],arm_tag[1]), end='')
                        for i in range(6):
                            print(' %1.10e'%nodes1_arm_data[i], end='')
                        print('\n> (%d,%d):'%(arm_tag[0],arm_tag[1]), end='')
                        for i in range(6):
                            print(' %1.10e'%nodes2_arm_data[i], end='')
                        print()

    return nodes_are_same


def write_data_file_with_aux_data(nodes, bounds, aux_data, taskname, suffix='.data'):
 
    pathname, extension = os.path.splitext(taskname)
    FileName = pathname + suffix
    
    fileID = open(FileName,'w')
    fileID.write("dataFileVersion =   %d  \n"%aux_data[0])
    fileID.write("numFileSegments =   %d  \n"%aux_data[1])
    fileID.write("minCoordinates = [\n")
    fileID.write("  %1.6e\n"%bounds[0][0])
    fileID.write("  %1.6e\n"%bounds[0][1])
    fileID.write("  %1.6e\n"%bounds[0][2])
    fileID.write("  ]\n")
    fileID.write("maxCoordinates = [\n")
    fileID.write("  %1.6e\n"%bounds[1][0])
    fileID.write("  %1.6e\n"%bounds[1][1])
    fileID.write("  %1.6e\n"%bounds[1][2])
    fileID.write("  ]\n")
    fileID.write("nodeCount =   %d  \n"%len(nodes))
    fileID.write("dataDecompType =   %d  \n"%aux_data[2])
    fileID.write("dataDecompGeometry = [\n")
    fileID.write("  1\n")
    fileID.write("  1\n")
    fileID.write("  1\n")
    fileID.write("  ]\n\n")
    
    fileID.write("#\n")
    fileID.write("#  END OF DATA FILE PARAMETERS\n")
    fileID.write("#\n\n")
    
    fileID.write("domainDecomposition = \n")
    if aux_data[2] == 1:
        fileID.write(" %5.4f\n"%bounds[0][0])
        fileID.write("     %5.4f\n"%bounds[0][1])
        fileID.write("         %5.4f\n"%bounds[0][2])
        fileID.write("          %5.4f\n"%bounds[1][0])
        fileID.write("      %5.4f\n"%bounds[1][1])
        fileID.write("  %5.4f\n\n"%bounds[1][2])
    else:
        fileID.write("# Dom_ID  Minimum XYZ bounds   Maximum XYZ bounds\n")
        fileID.write("  0")
        fileID.write("  %5.4f"%bounds[0][0])
        fileID.write(" %5.4f"%bounds[0][1])
        fileID.write(" %5.4f"%bounds[0][2])
        fileID.write("   %5.4f"%bounds[1][0])
        fileID.write(" %5.4f"%bounds[1][1])
        fileID.write(" %5.4f\n"%bounds[1][2])

    fileID.write("nodalData = \n")
    fileID.write("#  Primary lines: node_tag, x, y, z, num_arms, constraint\n")
    fileID.write("#  Secondary lines: arm_tag, burgx, burgy, burgz, nx, ny, nz\n")
    
    for my_tag in sorted(nodes.keys()):
        my_coord = copy.deepcopy(nodes[my_tag][0])
        arms_dict = copy.deepcopy(nodes[my_tag][1])
        num_arm = len(arms_dict)
		
        fileID.write(" %d,%d"%(my_tag[0],my_tag[1])) # node tag
        fileID.write(" %4.8f %4.8f %4.8f"%(my_coord[0],my_coord[1],my_coord[2])) # coordinates
        fileID.write(" %d %d\n"%(num_arm,my_coord[3])) # number of amrs, constraint type
        
        b = np.zeros(3)
        for arm_tag in arms_dict.keys():
            arm_data = arms_dict[arm_tag]
            b += arm_data[0:3]
                
            fileID.write("   %d,%d"%(arm_tag[0],arm_tag[1])) # arm tag
            fileID.write(" %1.10e %1.10e %1.10e\n"%(arm_data[0],arm_data[1],arm_data[2])) # Burgers vector
            fileID.write("       %1.10e %1.10e %1.10e\n"%(arm_data[3],arm_data[4],arm_data[5])) # plane normal vector
            
        if np.linalg.norm(b)>1e-9 and my_coord[3]!=7:
            print("   Burgers vectors are not conserved at node (%d,%d)"%(my_tag[0],my_tag[1]))
        if num_arm==1:
            print("   There is 1-arm node: (%d,%d)"%(my_tag[0],my_tag[1]))
			
    fileID.close


################################################################################
#
#    write_data_file():
#        write a restart data file. Data in 'nodes' dictionary is written as a
#        data file format.
#
#    Arguments:
#        param: data structure in C
#        nodes: node dictionary in Python
#        taskname: to designate the restart file name
#        suffix: suffix of the restart file written by this function
#
################################################################################

def write_data_file(param, nodes, taskname, suffix='.data'):
 
    pathname, extension = os.path.splitext(taskname)
    FileName = pathname + suffix
    
    fileID = open(FileName,'w')
    fileID.write("dataFileVersion =   %d  \n"%param.contents.dataFileVersion)
    fileID.write("numFileSegments =   %d  \n"%param.contents.numFileSegments)
    fileID.write("minCoordinates = [\n")
    fileID.write("  %1.6e\n"%param.contents.minCoordinates[0])
    fileID.write("  %1.6e\n"%param.contents.minCoordinates[1])
    fileID.write("  %1.6e\n"%param.contents.minCoordinates[2])
    fileID.write("  ]\n")
    fileID.write("maxCoordinates = [\n")
    fileID.write("  %1.6e\n"%param.contents.maxCoordinates[0])
    fileID.write("  %1.6e\n"%param.contents.maxCoordinates[1])
    fileID.write("  %1.6e\n"%param.contents.maxCoordinates[2])
    fileID.write("  ]\n")
    fileID.write("nodeCount =   %d  \n"%len(nodes))
    fileID.write("dataDecompType =   %d  \n"%param.contents.dataDecompType)
    fileID.write("dataDecompGeometry = [\n")
    fileID.write("  %d\n"%param.contents.dataDecompGeometry[0])
    fileID.write("  %d\n"%param.contents.dataDecompGeometry[1])
    fileID.write("  %d\n"%param.contents.dataDecompGeometry[2])
    fileID.write("  ]\n\n")
    
    fileID.write("#\n")
    fileID.write("#  END OF DATA FILE PARAMETERS\n")
    fileID.write("#\n\n")
    
    fileID.write("domainDecomposition = \n")
    if param.contents.dataDecompType==1:
        fileID.write(" %5.4f\n"%param.contents.minCoordinates[0])
        fileID.write("     %5.4f\n"%param.contents.minCoordinates[1])
        fileID.write("         %5.4f\n"%param.contents.minCoordinates[2])
        fileID.write("          %5.4f\n"%param.contents.maxCoordinates[0])
        fileID.write("      %5.4f\n"%param.contents.maxCoordinates[1])
        fileID.write("  %5.4f\n\n"%param.contents.maxCoordinates[2])
    else:
        fileID.write("# Dom_ID  Minimum XYZ bounds   Maximum XYZ bounds\n")
        fileID.write("  0")
        fileID.write("  %5.4f"%param.contents.minCoordinates[0])
        fileID.write(" %5.4f"%param.contents.minCoordinates[1])
        fileID.write(" %5.4f"%param.contents.minCoordinates[2])
        fileID.write("   %5.4f"%param.contents.maxCoordinates[0])
        fileID.write(" %5.4f"%param.contents.maxCoordinates[1])
        fileID.write(" %5.4f\n"%param.contents.maxCoordinates[2])

    fileID.write("nodalData = \n")
    fileID.write("#  Primary lines: node_tag, x, y, z, num_arms, constraint\n")
    fileID.write("#  Secondary lines: arm_tag, burgx, burgy, burgz, nx, ny, nz\n")
    
    for my_tag in sorted(nodes.keys()):
        my_coord = copy.deepcopy(nodes[my_tag][0])
        arms_dict = copy.deepcopy(nodes[my_tag][1])
        num_arm = len(arms_dict)
		
        fileID.write(" %d,%d"%(my_tag[0],my_tag[1])) # node tag
        fileID.write(" %4.8f %4.8f %4.8f"%(my_coord[0],my_coord[1],my_coord[2])) # coordinates
        fileID.write(" %d %d\n"%(num_arm,my_coord[3])) # number of amrs, constraint type
        
        b = np.zeros(3)
        for arm_tag in arms_dict.keys():
            arm_data = arms_dict[arm_tag]
            b += arm_data[0:3]
                
            fileID.write("   %d,%d"%(arm_tag[0],arm_tag[1])) # arm tag
            fileID.write(" %1.10e %1.10e %1.10e\n"%(arm_data[0],arm_data[1],arm_data[2])) # Burgers vector
            fileID.write("       %1.10e %1.10e %1.10e\n"%(arm_data[3],arm_data[4],arm_data[5])) # plane normal vector
            
        if np.linalg.norm(b)>1e-9 and my_coord[3]!=7:
            print("   Burgers vectors are not conserved at node (%d,%d)"%(my_tag[0],my_tag[1]))
        if num_arm==1:
            print("   There is 1-arm node: (%d,%d)"%(my_tag[0],my_tag[1]))
			
    fileID.close
	

def sanity_check_in_nodes_dict(nodes, bounds):

    sum_burg_tol = 1e-6
    plane_tol = 1e-6

    total_contour_length = 0
    error_contour_length = 0
    for node_tag in nodes.keys():
        node_pos = nodes[node_tag][0][0:3]
        nc = nodes[node_tag][0][3]
        arms_dict = nodes[node_tag][1]
        num_arm = len(arms_dict)
        
        sum_burg_on_node = np.zeros(3)
        for arm_tag in arms_dict.keys():
            burg = arms_dict[arm_tag][0:3]
            plane = arms_dict[arm_tag][3:6]
            plane = plane / np.linalg.norm(plane)
            sum_burg_on_node += burg[0:3]

            if node_tag[0] < arm_tag[0] or node_tag[1] < arm_tag[1]:

                # check Burgers vector conservation on each arm
                sum_burg_on_arm = burg + nodes[arm_tag][1][node_tag][0:3]
                if any(sum_burg_on_arm > sum_burg_tol) and \
                   nc != 7 and nodes[arm_tag][0][3] != 7 and \
                   num_arm != 1 and len(nodes[arm_tag][1]) != 1:
                    print("    Burgers vectors are not conserved at arm (%d,%d)-(%d,%d)"%(node_tag[0],node_tag[1],arm_tag[0],arm_tag[1]))
                
                # check whether glide planes are same on each arm
                if np.abs( np.dot( plane, nodes[arm_tag][1][node_tag][3:6] ) - 1 ) > plane_tol:
                    print("    Glide planes are different on arm (%d,%d)-(%d,%d)"%(node_tag[0],node_tag[1],arm_tag[0],arm_tag[1]))

                # check whether the dislocation is on the glide plane or not
                arm_pos = nodes[arm_tag][0][0:3]
                line_dir = pbc_dr_bounds(node_pos, arm_pos, bounds)
                contour_length = np.linalg.norm(line_dir)
                total_contour_length += contour_length
                line_dir = line_dir / contour_length
                dir_dot_plane = np.abs( np.dot( plane, line_dir) )
                if dir_dot_plane > plane_tol:
                    error_contour_length += contour_length
                    #print("    Dislocation (%d,%d)-(%d,%d) is not on the glide plane"%(node_tag[0],node_tag[1],arm_tag[0],arm_tag[1]))
                    plane = plane / max(np.abs(plane))
                    #print("      current plane = (%.1f, %.1f, %.1f) with error %.3f"%(plane[0],plane[1],plane[2],dir_dot_plane))
                    closest_plane = FindFCCGlidePlane_python(burg, line_dir)
                    closest_plane = closest_plane / np.linalg.norm(closest_plane)
                    dir_dot_closest_plane = np.abs( np.dot( closest_plane, line_dir) )
                    closest_plane = closest_plane / max(np.abs(closest_plane))
                    #print("      closest plane = (%.1f, %.1f, %.1f) with error %.3f"%(closest_plane[0],closest_plane[1],closest_plane[2],dir_dot_closest_plane))
            
        # check Burgers vector conservation on each node
        if any(sum_burg_on_node > sum_burg_tol) and nc != 7:
            print("  Burgers vectors are not conserved at node (%d,%d)"%(node_tag[0],node_tag[1]))

        if num_arm == 1:
            print("  There is 1-arm node: (%d,%d)"%(node_tag[0],node_tag[1]))

    print("sum(dislocation length on error plane) / sum(all dislocation length) = %.4f"%(error_contour_length/total_contour_length))
        

################################################################################
#
#    write_oprec_file():
#        write a operec file. Data in 'oprec_list' list is written.
#
#    Arguments:
#        home: data structure in C
#        nodes: node dictionary in Python
#        taskname: to designate the restart file name
#        suffix: suffix of the restart file written by this function
#
################################################################################

def write_oprec_file(oprec_list, taskname, suffix='.oprec'):
 
    pathname, extension = os.path.splitext(taskname)
    FileName = pathname + suffix
    
    fileID = open(FileName,'w')
    for my_dict in oprec_list:
        my_optype = my_dict['optype']
        fileID.write("%d\n"%my_optype)

        if my_optype == optype.CHANGE_CONNECTION: # optype=0
            # format: { 'optype':optype.CHANGE_CONNECTION, 'tag':start_tag, 'from_node':from_node, 'to_node':to_node }
            fileID.write("%d %d "%(my_dict['tag'][0], my_dict['tag'][1]))
            fileID.write("%d %d "%(my_dict['from_node'][0], my_dict['from_node'][1]))
            fileID.write("%d %d\n"%(my_dict['to_node'][0], my_dict['to_node'][1]))

        elif my_optype == optype.INSERT_ARM: # optype=1
            # format: { 'optype':optype.INSERT_ARM, 'tag':new_tag, 'arm_tag':node1, 'arm_data':arm_data }
            fileID.write("%d %d "%(my_dict['tag'][0], my_dict['tag'][1]))
            fileID.write("%d %d "%(my_dict['arm_tag'][0], my_dict['arm_tag'][1]))
            fileID.write("%1.10e %1.10e %1.10e "%(my_dict['arm_data'][0], my_dict['arm_data'][1], my_dict['arm_data'][2])) # burgers vector
            fileID.write("%1.10e %1.10e %1.10e\n"%(my_dict['arm_data'][3], my_dict['arm_data'][4], my_dict['arm_data'][5])) # glide plane
        
        elif my_optype == optype.REMOVE_NODE: # optype=2
            # format: { 'optype':optype.REMOVE_NODE, 'tag':tag }
            fileID.write("%d %d\n"%(my_dict['tag'][0], my_dict['tag'][1]))
        
        elif my_optype == optype.CHANGE_ARM_BURG: # optype=3
            # format: { 'optype':optype.CHANGE_ARM_BURG, 'tag':new_tag, 'arm_tag':node1, 'arm_data':arm_data }
            fileID.write("%d %d "%(my_dict['tag'][0], my_dict['tag'][1]))
            fileID.write("%d %d "%(my_dict['arm_tag'][0], my_dict['arm_tag'][1]))
            fileID.write("%1.10e %1.10e %1.10e "%(my_dict['arm_data'][0], my_dict['arm_data'][1], my_dict['arm_data'][2])) # burgers vector
            fileID.write("%1.10e %1.10e %1.10e\n"%(my_dict['arm_data'][3], my_dict['arm_data'][4], my_dict['arm_data'][5])) # glide plane
        
        elif my_optype == optype.SPLIT_NODE: # optype=4
            # format: { 'optype':optype.SPLIT_NODE, 'tag':new_tag, 'new_pos':new_node_position[0:3] }
            fileID.write("%d %d "%(my_dict['tag'][0], my_dict['tag'][1]))
            fileID.write("%4.8f %4.8f %4.8f\n"%(my_dict['new_pos'][0], my_dict['new_pos'][1], my_dict['new_pos'][2]))
        
        elif my_optype == optype.RESET_COORD: # optype=5
            # format: { 'optype':optype.RESET_COORD, 'tag':tag, 'new_pos':new_pos }
            fileID.write("%d %d "%(my_dict['tag'][0], my_dict['tag'][1]))
            fileID.write("%4.8f %4.8f %4.8f\n"%(my_dict['new_pos'][0], my_dict['new_pos'][1], my_dict['new_pos'][2]))

        elif my_optype == optype.START_STEP: # optype=10
            # format: { 'optype':optype.START_STEP }
            fileID.write("10\n")
        
        #elif my_optype == optype.RESET_GLIDE_PLANE: # optype=9
        else:
            raise ValueError('unknown optype: ' + my_optype)

    fileID.close


def save_plot_nodes_dict(nodes, bounds, plot_links=True, trim=False, fig=None, ax=None, block=False, pause_seconds=0.01, filename='plot'):

    if fig==None:
        fig = plt.figure(figsize=(8,8))
    if ax==None:
        ax = plt.axes(projection='3d')

    # to do: extend to non-cubic box
    L = bounds[1][0] - bounds[0][0]

    rn = np.array([nodes[i][0].tolist() for i in list(nodes.keys())])
    p_link = np.empty((0,6))

    plt.cla()
    if plot_links:
        for my_tag in list(nodes.keys()):
            my_coords = nodes[my_tag][0][0:3]
            arms_dict = nodes[my_tag][1]
            for nbr_tag in list(arms_dict.keys()):
                if my_tag < nbr_tag:
                    r_link = np.zeros((2,3))
                    if not nbr_tag in nodes.keys():
                        print("plot_nodes_dict(): cannot find neighbor")
                        print([my_tag, nbr_tag])
                        continue
                    nbr_coords = nodes[nbr_tag][0][0:3]
                    r_link[0,:] = my_coords
                    r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                    if (not trim) or np.max(np.absolute(r_link)) <= L/2:
                        p_link = np.append(p_link, [r_link[0,:], r_link[1,:]])

    ls = p_link.reshape((-1,2,3))
    lc = Line3DCollection(ls, linewidths=0.5, colors='b')
    ax.add_collection(lc)

    ax.scatter(rn[:,0], rn[:,1], rn[:,2], c='r', s=4)
    ax.set_xlim(bounds[0][0], bounds[1][0])
    ax.set_ylim(bounds[0][1], bounds[1][1])
    ax.set_zlim(bounds[0][2], bounds[1][2])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_box_aspect([1,1,1])

    plt.savefig(filename + '.png')

    return fig, ax
