# Python wrapper file for libparadis.so created by ctypesgen

  Home.py file needs to be generated again if any header files in ../include folder changes.  

  `Important`: if compiler options changes, Home.py file needs to be generated again.  
  
## Download ctypesgen

  cd $HOME/Codes  
  git clone https://github.com/davidjamesca/ctypesgen.git Ctypesgen.git  

## Create python wrapper file (on MC2)

  cd $HOME/Codes/ParaDiS.git/python  
  make  
  


