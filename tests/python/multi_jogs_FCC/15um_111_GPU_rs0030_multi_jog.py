
import numpy as np
import sys, os
from random import *
from paradis_util import *
from disl_link_util import *

home_lib = __import__('Home_gpu')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

########## Input data

ctrlfile = '15um_111_GPU_rs0030_multi_jog'
datafile = '15um_111_GPU_rs0030.data'
results_dir = ctrlfile + '_results/'

savecncounter = 30

Num_Iter = 600

crit = [3000, 1000000]
#jog_step = 1 # period for jog generation

probability = 0.1
jog_length_ratio = 1/3
jog_height = 15

########## Generate jogs on initial configuration

os.system('mkdir ' + results_dir)

save_filename = paradis.generate_jog_on_datafile(datafile, crit, probability, jog_length_ratio, jog_height, results_dir)

########## multi-step jog generation

os.system('../../../bin/paradis_gpu -d ' + save_filename  + '_jog.data ' + ctrlfile)

restart_dir = results_dir + 'restart/'

for counter in range(savecncounter+1, Num_Iter+1):
    ctrlfile = restart_dir + 'rs%04d'%counter
    datafile = ctrlfile + '.data'
    save_filename = paradis.generate_jog_on_datafile(datafile, crit, probability, jog_length_ratio, jog_height)
    os.system('../../../bin/paradis_gpu -d ' + save_filename  + '_jog.data ' + ctrlfile)






