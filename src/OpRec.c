#ifdef _OP_REC

/*-------------------------------------------------------------------------
 *
 *      Module:      OpRec.c
 *      Description: This module implements functions to record and output
 *                   all topolgical operations that are performed during
 *                   the course of a simulation. It also implements a
 *                   player than can re-run a simulation from previously
 *                   saved oprec files.
 *
 *                   Note: The oprec output option uses two frequencies:
 *                   * oprecwritefreq: frequency at which the list of
 *                     toplogical operations is appended to the current
 *                     oprec file (oprecXXXX).
 *                   * oprecfilefreq: frequency at which new oprec files
 *                     are being created.
 *
 *                   Nicolas Bertin
 *
 *------------------------------------------------------------------------*/

#if PARALLEL
#error Cannot compile the OP_REC option in parallel yet
#endif

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include "Home.h"
#include "OpList.h"
#include "Tag.h"
#include "QueueOps.h"

/*-------------------------------------------------------------------------
 *
 *      Function:      AddOpRec
 *      Description:   Add a topological operation to the list to
 *                     be recorded and outputed in a file.
 *
 *      Arguments:
 *          type        Type of operation to be performed
 *          dom1, idx1  Tag information for the 1st node involved
 *                      in the operation.
 *          dom2, idx2  Tag information for the 2nd node involved
 *                      in the operation.  (If no second node is
 *                      involved in this operation, these values
 *                      should be set to -1)
 *          dom3, idx3  Tag information for the 3rd node involved
 *                      in the operation.  (If no third node is
 *                      involved in this operation, these values
 *                      should be set to -1)
 *          bx, by, bz  Components of the burgers vector for the
 *                      operation.  (If not applicable to the
 *                      operation, these should be zeroes)
 *          x, y, z     Components of node's position (If not
 *                      applicable to the operation, should be zero'ed)
 *          nx, ny, nz  Components of glide plane normal. (If not
 *                      applicable to the operation, should be zero'ed)
 *
 *------------------------------------------------------------------------*/
void AddOpRec (Home_t *home,
            OpType_t type, int dom1, int idx1,
            int dom2, int idx2, int dom3, int idx3,
            real8 bx, real8 by, real8 bz,
            real8 x, real8 y, real8 z,
            real8 nx, real8 ny, real8 nz)
{
	if (home->param->oprec){
        OperateRec_t *op;

/*
 *      Make sure the buffer allocated for the operation list is
 *      large enough to contain another operation.  If not, increase
 *      the size of the buffer.
 */
        if (home->OpRecCount >= home->OpRecListLen) {
            ExtendOpRecList (home);
        }

        op = &home->opRecList[home->OpRecCount];

        op->type = type;
        op->i1 = dom1;
        op->i2 = idx1;
        op->i3 = dom2;
        op->i4 = idx2;
        op->i5 = dom3;
        op->i6 = idx3;
        op->d1 = bx;
        op->d2 = by;
        op->d3 = bz;
        op->d4 = x;
        op->d5 = y;
        op->d6 = z;
        op->d7 = nx;
        op->d8 = ny;
        op->d9 = nz;
        home->OpRecCount++;
    }
    return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:      StartStepOpRecList
 *      Description:   Zero's both the list of operations which is
 *                     communicated to remote domains during various
 *                     stages of topological changes and the count
 *                     of operations on the list .
 *
 *------------------------------------------------------------------------*/
void StartStepOpRecList (Home_t *home, int step)
{
	if (home->param->oprec){
        AddOpRec (home, step, home->cycle, 0,
                  0, 0, 0, 0, 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}
    return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:      NodeMoveOpRecList
 *      Description:   Log nodes motion that occured during time-integration
 *
 *------------------------------------------------------------------------*/
void NodeMoveOpRecList (Home_t *home)
{
	if (home->param->oprec){
        int i;
        Node_t *node;

        if (home->param->oprecmove) {

            for (i = 0; i < home->newNodeKeyPtr; i++) {
    			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;

                AddOpRec(home, RESET_COORD,
                    node->myTag.domainID,
                    node->myTag.index,
                    -1, -1,                 /* 2nd tag unneeded */
                    -1, -1,                 /* 3rd node tag unneeded */
                    0.0, 0.0, 0.0,          /* burgers vector unneeded */
                    node->x, node->y, node->z,
                    0.0, 0.0, 0.0);         /* plane normal not needed */
            }

        }

        AddOpRec(home, TIME_INTEGRATION, home->cycle,
                 0, 0, 0, 0, 0,
                 home->param->realdt,
                 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}
    return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:      ExtendOpRecList
 *      Description:   Increases the buffer size dedicated to the
 *                     list of operations sent to remote domains
 *                     after topological changes.
 *
 *------------------------------------------------------------------------*/
void ExtendOpRecList (Home_t *home)
{
        int newSize;

        newSize = (home->OpRecListLen+OpBlock_Count) * sizeof(OperateRec_t);
        OperateRec_t *opRecList_tmp;
        opRecList_tmp = (OperateRec_t *) realloc(home->opRecList, newSize);
         
		if (opRecList_tmp != NULL) {
			home->opRecList= opRecList_tmp;
		} else {
			Fatal("Error (re)allocating memory for opRecList in ExtendOpRecList. Please decrease the oprecfreq in ctrl file.");
		}
		
        home->OpRecListLen += OpBlock_Count;

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:      InitOpRecList
 *      Description:   Allocates an initial block of memory for the
 *                     list of operations this domain will send to
 *                     remote domains for processing during the
 *                     various stages of topological changes.
 *
 *                     This function should only need to be called
 *                     one time during initialization of the application.
 *                     Adding operations to the list as well as altering
 *                     the list size will be handled dynamically as
 *                     necessary
 *
 *------------------------------------------------------------------------*/
void InitOpRecList (Home_t *home)
{
	if (home->param->oprec){
        home->opRecList = (OperateRec_t *) malloc(sizeof(OperateRec_t)*OpBlock_Count);
        home->OpRecCount = 0;
        home->OpRecListLen = OpBlock_Count;
        //home->rcvOpCount = 0;
        //home->rcvOpList = 0;
	}
    return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:      WriteOpRecList
 *
 *------------------------------------------------------------------------*/
void WriteOpRecList(Home_t *home, char *baseFileName)
{
        int i;
        OperateRec_t *op;
        char fileName[256], buff[128];
        FILE *fp;


#if 0
/*
 *      Raw format
 */
        snprintf(fileName, sizeof(fileName), "%s/%s.dat",
                 DIR_OPREC, baseFileName);

        fp = fopen(fileName, "a");

        for (i = 0; i < home->OpRecCount; i++) {
			op = &home->opRecList[i];

            switch (op->type) {
                case START_STEP:
                    sprintf(buff, "**START_STEP"); break;
                case TIME_INTEGRATION:
                    sprintf(buff, "**TIME_INTEGRATION"); break;
                case START_SPLIT_MULTI:
                    sprintf(buff, "**START_SPLIT_MULTI"); break;
                case START_CROSS_SLIP:
                    sprintf(buff, "**START_CROSS_SLIP"); break;
                case START_COLLISION:
                    sprintf(buff, "**START_COLLISION"); break;
                case START_REMESH:
                    sprintf(buff, "**START_REMESH"); break;

                case CHANGE_CONNECTION:
                    sprintf(buff, "CHANGE_CONNECTION"); break;
                case INSERT_ARM:
                    sprintf(buff, "INSERT_ARM"); break;
                case REMOVE_NODE:
                    sprintf(buff, "REMOVE_NODE"); break;
                case CHANGE_ARM_BURG:
                    sprintf(buff, "CHANGE_ARM_BURG"); break;
                case SPLIT_NODE:
                    sprintf(buff, "SPLIT_NODE"); break;
                case RESET_COORD:
                    sprintf(buff, "RESET_COORD"); break;
                case RESET_GLIDE_PLANE:
                    sprintf(buff, "RESET_GLIDE_PLANE"); break;
                default:
                    sprintf(buff, "UNKNOWN"); break;
            }

			fprintf(fp, "%s %d %d %d %d %d %d %d %e %e %e %e %e %e %e %e %e\n",
                    buff, op->type,
                    op->i1, op->i2,
                    op->i3, op->i4,
                    op->i5, op->i6,
                    op->d1, op->d2, op->d3,
                    op->d4, op->d5, op->d6,
                    op->d7, op->d8, op->d9);
		}

        fclose(fp);
#endif

#if 1
/*
 *      Compressed format
 */
        snprintf(fileName, sizeof(fileName), "%s/%s",
                 DIR_OPREC, baseFileName);

        fp = fopen(fileName, "a");

        for (i = 0; i < home->OpRecCount; i++) {
			op = &home->opRecList[i];

            fprintf(fp, "%d\n", op->type);

            switch (op->type) {

                case TIME_INTEGRATION:
                    fprintf(fp, "%e\n", op->d1);
                    break;

                case CHANGE_CONNECTION:
                    fprintf(fp, "%d %d %d %d %d %d\n",
                            op->i1, op->i2, op->i3, op->i4, op->i5, op->i6);
                    break;

                case INSERT_ARM:
                    fprintf(fp, "%d %d %d %d %e %e %e %e %e %e\n",
                            op->i1, op->i2, op->i3, op->i4,
                            op->d1, op->d2, op->d3, op->d7, op->d8, op->d9);
                    break;

                case REMOVE_NODE:
                    fprintf(fp, "%d %d\n", op->i1, op->i2);
                    break;

                case CHANGE_ARM_BURG:
                    fprintf(fp, "%d %d %d %d %e %e %e %e %e %e\n",
                            op->i1, op->i2, op->i3, op->i4,
                            op->d1, op->d2, op->d3, op->d7, op->d8, op->d9);
                    break;

                case SPLIT_NODE:
                    fprintf(fp, "%d %d %e %e %e\n",
                            op->i3, op->i4, op->d4, op->d5, op->d6);
                    break;

                case RESET_COORD:
                    fprintf(fp, "%d %d %e %e %e\n",
                            op->i1, op->i2, op->d4, op->d5, op->d6);
                    break;

                case RESET_GLIDE_PLANE:
                    fprintf(fp, "%d %d %d %d %e %e %e\n",
                            op->i1, op->i2, op->i3, op->i4,
                            op->d7, op->d8, op->d9);
                    break;

                default:
                    break;
            }
		}

        fclose(fp);
#endif

/*
 *      Reset oprec list
 */
        memset(home->opRecList, 0, sizeof(OperateRec_t)*home->OpRecListLen);
        home->OpRecCount = 0;

        return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:    PrintOpRec
 *
 *-------------------------------------------------------------------------*/
void PrintOpRec(OperateRec_t *op)
{
        char buff[128];

        switch (op->type) {
            case START_STEP:
                sprintf(buff, "START_STEP"); break;
            case TIME_INTEGRATION:
                sprintf(buff, "TIME_INTEGRATION"); break;
            case START_SPLIT_MULTI:
                sprintf(buff, "START_SPLIT_MULTI"); break;
            case START_CROSS_SLIP:
                sprintf(buff, "START_CROSS_SLIP"); break;
            case START_COLLISION:
                sprintf(buff, "START_COLLISION"); break;
            case START_REMESH:
                sprintf(buff, "START_REMESH"); break;

            case CHANGE_CONNECTION:
                sprintf(buff, "CHANGE_CONNECTION"); break;
            case INSERT_ARM:
                sprintf(buff, "INSERT_ARM"); break;
            case REMOVE_NODE:
                sprintf(buff, "REMOVE_NODE"); break;
            case CHANGE_ARM_BURG:
                sprintf(buff, "CHANGE_ARM_BURG"); break;
            case SPLIT_NODE:
                sprintf(buff, "SPLIT_NODE"); break;
            case RESET_COORD:
                sprintf(buff, "RESET_COORD"); break;
            case RESET_GLIDE_PLANE:
                sprintf(buff, "RESET_GLIDE_PLANE"); break;
            default:
                sprintf(buff, "UNKNOWN"); break;
        }

        printf("%s %d %d %d %d %d %d %d %e %e %e %e %e %e %e %e %e\n",
                buff, op->type,
                op->i1, op->i2,
                op->i3, op->i4,
                op->i5, op->i6,
                op->d1, op->d2, op->d3,
                op->d4, op->d5, op->d6,
                op->d7, op->d8, op->d9);

        return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:    ReadOpRecList
 *
 *-------------------------------------------------------------------------*/
void ReadOpRecList(Home_t *home, char *oprecFile)
{

/*
 *      Reset oprec list
 */
        memset(home->opRecList, 0, sizeof(OperateRec_t)*home->OpRecListLen);
        home->OpRecCount = 0;

/*
 *		Only processor zero reads in the file.  It will broadcast the
 *		table (and supporting data) to all other processors (TBD).
 */
		if (home->myDomain == 0) {

            FILE *fp;
            int ibuf[7];
            double dbuf[9];
            char sbuf[100];

			printf("Reading oprec file %s ...\n", oprecFile);

            if ((fp = fopen(oprecFile, "r")) == (FILE *)NULL) {
                Fatal("Error %d opening file %s to read oprec data",
                      errno, oprecFile);
            }

            while (1) {

                if (fscanf(fp, "%s %d %d %d %d %d %d %d %le %le %le %le %le %le %le %le %le\n",
                    sbuf, &ibuf[0], &ibuf[1], &ibuf[2], &ibuf[3], &ibuf[4], &ibuf[5], &ibuf[6],
                    &dbuf[0], &dbuf[1], &dbuf[2], &dbuf[3], &dbuf[4], &dbuf[5],
                    &dbuf[6], &dbuf[7], &dbuf[8]) != 17) break;

                    AddOpRec(home, ibuf[0], ibuf[1], ibuf[2],
                             ibuf[3], ibuf[4], ibuf[5], ibuf[6],
                             dbuf[0], dbuf[1], dbuf[2],
                             dbuf[3], dbuf[4], dbuf[5],
                             dbuf[6], dbuf[7], dbuf[8]);
            }

			fclose(fp);

			printf("done.\n");
		}

        return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:    ReadOpRecListCmp
 *
 *-------------------------------------------------------------------------*/
void ReadOpRecListCmp(Home_t *home, char *oprecFile)
{

/*
 *      Reset oprec list
 */
        memset(home->opRecList, 0, sizeof(OperateRec_t)*home->OpRecListLen);
        home->OpRecCount = 0;

/*
 *		Only processor zero reads in the file.  It will broadcast the
 *		table (and supporting data) to all other processors (TBD).
 */
		if (home->myDomain == 0) {

            FILE *fp;
            int optype, ibuf[6];
            double dbuf[9];
            char sbuf[100];

			printf("Reading oprec file %s ...\n", oprecFile);

            if ((fp = fopen(oprecFile, "r")) == (FILE *)NULL) {
                Fatal("Error %d opening file %s to read oprec data",
                      errno, oprecFile);
            }

            while (1) {

                if (fscanf(fp, "%d\n", &optype) != 1) break;

                ibuf[0] = ibuf[1] = ibuf[2] = 0;
                ibuf[3] = ibuf[4] = ibuf[5] = 0;
                dbuf[0] = dbuf[1] = dbuf[2] = 0.0;
                dbuf[3] = dbuf[4] = dbuf[5] = 0.0;
                dbuf[6] = dbuf[7] = dbuf[8] = 0.0;

                switch (optype) {

                    case TIME_INTEGRATION:
                        if (fscanf(fp, "%le\n", &dbuf[0]) != 1)
                            Fatal("ReadOpRecListCmp error in reading TIME_INTEGRATION event");
                        break;

                    case CHANGE_CONNECTION:
                        if (fscanf(fp, "%d %d %d %d %d %d\n",
                                &ibuf[0], &ibuf[1], &ibuf[2], &ibuf[3], &ibuf[4], &ibuf[5]) != 6)
                            Fatal("ReadOpRecListCmp error in reading CHANGE_CONNECTION event");
                        break;

                    case INSERT_ARM:
                        if (fscanf(fp, "%d %d %d %d %le %le %le %le %le %le\n",
                                &ibuf[0], &ibuf[1], &ibuf[2], &ibuf[3],
                                &dbuf[0], &dbuf[1], &dbuf[2], &dbuf[6], &dbuf[7], &dbuf[8]) != 10)
                            Fatal("ReadOpRecListCmp error in reading INSERT_ARM event");
                        break;

                    case REMOVE_NODE:
                        if (fscanf(fp, "%d %d\n", &ibuf[0], &ibuf[1]) != 2)
                            Fatal("ReadOpRecListCmp error in reading REMOVE_NODE event");
                        break;

                    case CHANGE_ARM_BURG:
                        if (fscanf(fp, "%d %d %d %d %le %le %le %le %le %le\n",
                                &ibuf[0], &ibuf[1], &ibuf[2], &ibuf[3],
                                &dbuf[0], &dbuf[1], &dbuf[2], &dbuf[6], &dbuf[7], &dbuf[8]) != 10)
                            Fatal("ReadOpRecListCmp error in reading CHANGE_ARM_BURG event");
                        break;

                    case SPLIT_NODE:
                        if (fscanf(fp, "%d %d %le %le %le\n",
                                &ibuf[2], &ibuf[3], &dbuf[3], &dbuf[4], &dbuf[5]) != 5)
                            Fatal("ReadOpRecListCmp error in reading SPLIT_NODE event");
                        break;

                    case RESET_COORD:
                        if (fscanf(fp, "%d %d %le %le %le\n",
                                &ibuf[0], &ibuf[1], &dbuf[3], &dbuf[4], &dbuf[5]) != 5)
                            Fatal("ReadOpRecListCmp error in reading RESET_COORD event");
                        break;

                    case RESET_GLIDE_PLANE:
                        if (fscanf(fp, "%d %d %d %d %le %le %le\n",
                                &ibuf[0], &ibuf[1], &ibuf[2], &ibuf[3],
                                &dbuf[6], &dbuf[7], &dbuf[8]) != 7)
                            Fatal("ReadOpRecListCmp error in reading RESET_GLIDE_PLANE event");
                        break;

                    default:
                        break;
                }

                AddOpRec(home, optype, ibuf[0], ibuf[1],
                         ibuf[2], ibuf[3], ibuf[4], ibuf[5],
                         dbuf[0], dbuf[1], dbuf[2],
                         dbuf[3], dbuf[4], dbuf[5],
                         dbuf[6], dbuf[7], dbuf[8]);
            }

			fclose(fp);

			printf("done.\n");
		}

        return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     RequestNodeTag
 *
 *------------------------------------------------------------------------*/
int RequestNodeTag(Home_t *home, Tag_t *tag)
{
        if (tag->index >= home->newNodeKeyMax) {
/*
 *          The node index is higher than the current size
 *          of the node list. Reallocate the node list with
 *          the new size to allow the node index to be requested.
 */
            int NUM_INC = ceil((tag->index-home->newNodeKeyMax-1)/NEW_NODEKEY_INC);

            home->newNodeKeyMax += NUM_INC*NEW_NODEKEY_INC;
            home->nodeKeys = (Node_t **) realloc(home->nodeKeys,
            home->newNodeKeyMax * sizeof(Node_t *));

            home->newNodeKeyPtr = tag->index+1;

            return tag->index;

        } else {
/*
 *          Check if the requested node index is already used. If not
 *          make sure to update the newNodeKeyPtr variable if needed.
 */
            if (home->nodeKeys[tag->index] == (Node_t *)NULL) {

                if (tag->index >= home->newNodeKeyPtr) {
                    home->newNodeKeyPtr = tag->index+1;
                }
                return tag->index;

            } else {
                return -1;
            }
        }
}

/*-------------------------------------------------------------------------
 *
 *      Function:     RequestNewNativeNodeTag
 *
 *------------------------------------------------------------------------*/
Node_t *RequestNewNativeNodeTag(Home_t *home, Tag_t *tag)
{
        int     newIdx;
        Node_t *newNode;

        newNode = PopFreeNodeQ(home);
        newIdx = RequestNodeTag(home, tag);

        if (newIdx < 0) {
            Fatal("Cannot request node index (%d,%d)",
            tag->domainID, tag->index);
        }

        home->nodeKeys[newIdx] = newNode;

        newNode->myTag.domainID = home->myDomain;
        newNode->myTag.index    = newIdx;
        newNode->cellIdx        = -1;
        newNode->cell2Idx       = -1;
        newNode->cell2QentIdx   = -1;

/*
 *      Explicitly zero out velocity so we don't end up
 *      with garbage when we are calculating the velocity
 *      delta between timesteps.
 */
        newNode->vX = 0.0;
        newNode->vY = 0.0;
        newNode->vZ = 0.0;

        newNode->oldvX = 0.0;
        newNode->oldvY = 0.0;
        newNode->oldvZ = 0.0;

        newNode->flags = 0;

#ifdef DEBUG_LOG_MULTI_NODE_SPLITS
        newNode->multiNodeLife = 0;
#endif

#ifdef _FEM
        newNode->fem_Surface[0] = 0;
        newNode->fem_Surface[1] = 0;

        newNode->fem_Surface_Norm[0] = 0.0;
        newNode->fem_Surface_Norm[1] = 0.0;
        newNode->fem_Surface_Norm[2] = 0.0;
#endif

        return(newNode);
}

/*---------------------------------------------------------------------------
 *
 *      Function:    RunFromOpRecFile
 *
 *-------------------------------------------------------------------------*/
void RunFromOpRecFile(Home_t *home)
{
/*
 *		Read list of topological operations from file
 */
        //ReadOpRecList(home, home->param->oprecfile);
        ReadOpRecListCmp(home, home->param->oprecfile);

        RunFromOpRecList(home);
}

/*---------------------------------------------------------------------------
 *
 *      Function:    RunFromOpRecList
 *
 *-------------------------------------------------------------------------*/
void RunFromOpRecList(Home_t *home)
{
/*
 *		Apply operations to the current configuration
 */
    int k;
    OperateRec_t *op;
    for (k = 0; k < home->OpRecCount; k++) {
        op = &home->opRecList[k];
        ExecuteOpRec(home, op);
    }
    return;
}

/*---------------------------------------------------------------------------
 *
 *      Function:    ExecuteOpRec
 *
 *-------------------------------------------------------------------------*/
void ExecuteOpRec(Home_t *home, OperateRec_t *op)
{
/*
 *		Apply operations to the current configuration
 */
        int i;
        Node_t *node;
        Tag_t tag, tag2, tag3;
        Param_t *param = home->param;

        switch (op->type) {

                case START_STEP:
/*
 *                  Store the current positions.
 */
            		for (i = 0; i < home->newNodeKeyPtr; i++) {
            			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
            			node->oldx = node->x;  node->olderx = node->x;
            			node->oldy = node->y;  node->oldery = node->y;
            			node->oldz = node->z;  node->olderz = node->z;
            		}
            		node = home->ghostNodeQ;
            		while (node) {
            			node->oldx = node->x;  node->olderx = node->x;
            			node->oldy = node->y;  node->oldery = node->y;
            			node->oldz = node->z;  node->olderz = node->z;
            			node = node->next;
            		}

                    break;

                case RESET_COORD:
/*
 *                  Reposition nodes that have moved
 */
                    tag.domainID = op->i1;
                    tag.index = op->i2;

                    double newPos[3] = {op->d4, op->d5, op->d6};

                    RepositionNode(home, newPos, &tag, 0);

                    break;

                case TIME_INTEGRATION:

                    param->realdt = op->d1;
                    param->timeStart = param->timeNow;
/*
 *                  Calculate the new plastic strain.
 */
                    DeltaPlasticStrain(home);
/*
 *                  The call to GenerateOutput will update the time and cycle counters,
 *                  determine if any output needs to be generated at this stage, and
 *                  call the appropriate I/O functions if necessary.
 */
                    GenerateOutput(home, STAGE_CYCLE);
/*
 *                  Define load curve and calculate change in applied stress this cycle
 */
                    real8 deltaStress[3][3];
                    LoadCurve(home, deltaStress);

                    break;

                case SPLIT_NODE:
/*
 *                  Insert new node resulting from the split operation
 */
                    tag.domainID = op->i3;
                    tag.index = op->i4;

                    node = RequestNewNativeNodeTag(home, &tag);
                    FreeNodeArms(node);

                    if (node->myTag.domainID != tag.domainID) {
                        Fatal("New node with different domainID\n");
                    }
                    if (node->myTag.index != tag.index) {
                        Fatal("New node with different index\n");
                    }

                    node->x = op->d4;
                    node->y = op->d5;
                    node->z = op->d6;

                    break;

                case CHANGE_CONNECTION:

                    tag.domainID = op->i1;
                    tag.index = op->i2;
                    tag2.domainID = op->i3;
                    tag2.index = op->i4;
                    tag3.domainID = op->i5;
                    tag3.index = op->i6;

                    if ((node = GetNodeFromTag(home, tag)) == (Node_t *)NULL) {
                        Fatal("ChangeConnection node could not be found");
                    }

                    if (ChangeConnection(home, node, &tag2, &tag3, 0) == -1) {
                        Fatal("ChangeConnection could not be applied");
                    }

                    break;

                case INSERT_ARM:

                    tag.domainID = op->i1;
                    tag.index = op->i2;
                    tag2.domainID = op->i3;
                    tag2.index = op->i4;

                    if ((node = GetNodeFromTag(home, tag)) == (Node_t *)NULL) {
                        Fatal("InsertArm node could not be found");
                    }

                    InsertArm(home, node, &tag2, op->d1, op->d2, op->d3,
                              op->d7, op->d8, op->d9, 0);

                    break;

                case CHANGE_ARM_BURG:

                    tag.domainID = op->i1;
                    tag.index = op->i2;
                    tag2.domainID = op->i3;
                    tag2.index = op->i4;

                    if ((node = GetNodeFromTag(home, tag)) == (Node_t *)NULL) {
                        Fatal("ChangeArmBurg node could not be found");
                    }

                    ChangeArmBurg(home, node, &tag2, op->d1, op->d2, op->d3,
                                  op->d7, op->d8, op->d9, 0, 0.0);

                    break;

                case RESET_GLIDE_PLANE:

                    tag.domainID = op->i1;
                    tag.index = op->i2;
                    tag2.domainID = op->i3;
                    tag2.index = op->i4;

                    double newPlane[3] = {op->d7, op->d8, op->d9};

                    ResetGlidePlane(home, newPlane, &tag, &tag2, 0);

                    break;

                case REMOVE_NODE:

                    tag.domainID = op->i1;
                    tag.index = op->i2;

                    if ((node = GetNodeFromTag(home, tag)) == (Node_t *)NULL) {
                        Fatal("RemoveNode node could not be found");
                    }

                    RemoveNode(home, node, 0);

                    break;


                case START_SPLIT_MULTI:
                    break;
                case START_CROSS_SLIP:
                    break;
                case START_COLLISION:
                    break;
                case START_REMESH:
                    break;

                default:
                    printf("WARNING: Unknown operation type %d. Skipping.\n", op->type);
                    break;
            }

        return;
}

#endif
