/***************************************************************************
 *
 *  Function    : Main
 *  Description : main routine for ParaDiS simulation
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"
#include "Init.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef _GPU_SUBCYCLE
#include "SubcycleGPU.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif


int main (int argc, char *argv[])
{
        int     cycleEnd, memSize, initialDLBCycles;
        time_t  tp;
        Home_t  *home;
        Param_t *param;

/*
 *      On some systems, the getrusage() call made by Meminfo() to get
 *      the memory resident set size does not work properly.  In those
 *      cases, the function will try to return the current heap size
 *      instead.  This initial call allows meminfo() to get a copy of
 *      the original heap pointer so subsequent calls can calculate the
 *      heap size by taking the diference of the original and current
 *      heap pointers.
 */
        Meminfo(&memSize);

/*
 *      on linux systems (e.g. MCR) if built to have floating point exceptions
 *      turned on, invoke macro to do so
 */

#ifdef FPES_ON
        unmask_std_fpes();
#endif


        ParadisInit(argc, argv, &home);

#ifdef _GPU_SUBCYCLE
        InitializeParadisGPU(home);
#endif

        home->cycle      = home->param->cycleStart;

        param            = home->param;
        cycleEnd         = param->cycleStart + param->maxstep;
        initialDLBCycles = param->numDLBCycles;

/*
 *      Perform the needed number (if any) of load-balance-only
 *      steps before doing the main processing loop.  These steps
 *      perform only the minimal amount of stuff needed to
 *      estimate per-process load, move boundaries and migrate
 *      nodes among processsors to get a good initial balance.
 */
        TimerStart(home, INITIALIZE);

        if ((home->myDomain == 0) && (initialDLBCycles != 0)) {
            time(&tp);
            printf("  +++ Beginning %d load-balancing steps at %s",
                   initialDLBCycles, asctime(localtime(&tp)));
        }

        while (param->numDLBCycles > 0) {
            ParadisStep(home);
            home->cycle++;
            param->numDLBCycles--;
        }

        if ((home->myDomain == 0) && (initialDLBCycles != 0)) {
            time(&tp);
            printf("  +++ Completed load-balancing steps at %s",
                   asctime(localtime(&tp)));
        }

        TimerStop(home, INITIALIZE);

/*
 *      Any time spent doing the initial DLB-only steps should
 *      just be attributed to initialization time, so be sure to
 *      reset the other timers before going into the main
 *      computational loop
 */
        TimerInitDLBReset(home);

#ifdef _THERMAL_ACTIVATED_CROSSSLIP
/*
 *      Thermal activation requires that the random number generater is
 *      initialized.
 */
        srand((long)time(NULL));
#endif

#ifdef _NOBUF_STDOUT
/*
 *      Force the stdout to flush.
 */
        setbuf(stdout, NULL);
#endif

/*
 *      The cycle number may have been incremented during the initial
 *      load-balance steps, so reset it to the proper starting
 *      value before entering the main processing loop.
 */
        home->cycle = home->param->cycleStart;

#ifdef _WRITE_INIT_FORCEVEL_EXIT
        CellCharge(home);
        SegSegListMaker(home, FULL);
        int doAll_ref = 1;
        int zeroOnErr_ref = 1;
        // NodeForce(home, FULL) should be in front of NodeForce(home, i)
        // so that ComputeSegSigbRem() function is called (needed for fmEnabled = 0)
        NodeForce(home, FULL);
        CalcNodeVelocities(home, zeroOnErr_ref, doAll_ref);
        Write_Node_Force_Vel(home,"before cycles",FULL);

#if defined _SUBCYCLING && defined _WRITE_INIT_GROUPS_FORCEVEL_EXIT
        int i;
        int i_max = GROUP4+1;
        for (i = GROUP0; i < i_max; i++){
                FlagSubcycleNodes(home, i);
                NodeForce(home, i);
                CalcNodeVelocities(home, zeroOnErr_ref, doAll_ref);
                Write_Node_Force_Vel(home,"before cycles",i);
        }
#endif
        exit(0);
#endif

#ifdef _OP_REC
        if (home->param->runfromoprec) {
            RunFromOpRecFile(home);
        } else
#endif
        {
            while (home->cycle < cycleEnd) {
                ParadisStep(home);
                TimerClearAll(home);
            }
            Write_Node_Force_Vel(home,"after cycles",FULL);
        }

        ParadisFinish(home);

        exit(0);
}
