/***************************************************************************
 *
 *  Function    : Main
 *  Description : main routine for ParaDiS simulation
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"
#include "Init.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef _GPU_SUBCYCLE
#include "SubcycleGPU.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif





int main (int argc, char *argv[])
{
        int     cycleEnd, memSize, initialDLBCycles;
        time_t  tp;
        Home_t  *home;
        Param_t *param;

/*
 *      On some systems, the getrusage() call made by Meminfo() to get
 *      the memory resident set size does not work properly.  In those
 *      cases, the function will try to return the current heap size 
 *      instead.  This initial call allows meminfo() to get a copy of
 *      the original heap pointer so subsequent calls can calculate the
 *      heap size by taking the diference of the original and current
 *      heap pointers.
 */
        Meminfo(&memSize);

/*
 *      on linux systems (e.g. MCR) if built to have floating point exceptions
 *      turned on, invoke macro to do so
 */
   
#ifdef FPES_ON
        unmask_std_fpes();
#endif


        ParadisInit(argc, argv, &home);
        home->cycle      = home->param->cycleStart;
   
#ifdef _GPU_SUBCYCLE
        InitializeParadisGPU(home);
#endif

        param            = home->param;
        cycleEnd         = param->cycleStart + param->maxstep;

#ifdef _GPU_SUBCYCLE
        sprintf(param->subInteg0Integ1, "GPU");
#else
        sprintf(param->subInteg0Integ1, "RKF-RKF");
#endif

        home->cycle = home->param->cycleStart;

        while (home->cycle < cycleEnd) {
            ParadisStep(home);
            TimerClearAll(home);
        }

        ParadisFinish(home);

        exit(0);
}
