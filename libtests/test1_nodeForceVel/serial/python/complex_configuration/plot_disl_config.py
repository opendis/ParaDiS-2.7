from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, nodes

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)

    if use_GPU: paradis.InitializeParadisGPU(home)

    t_begin = time.time()  

    paradis.CellCharge(home)
    paradis.SegSegListMaker(home, gids.FULL)

    print(' convert network to nodes dictionary')
    nodes = convert_nodes_to_dict(home)

    print(" plot dislocation structure")
    bounds = np.array([[home.contents.domXmin, home.contents.domYmin, home.contents.domZmin],
                       [home.contents.domXmax, home.contents.domYmax, home.contents.domZmax]])
    fig = plt.figure(figsize=(8,8))
    plot_nodes_dict(nodes, bounds, fig=fig, block=False)
    plt.pause(0.5)
    print('')
    print("If window closed to quickly, run interactive session with  " + bcolors.MAG + "python3 -i" + bcolors.RESET)
    print('')
    print("In python interactive session, try")
    print("    " + bcolors.BLU + "home.contents.nodeKeys[10].contents.myTag.index" + bcolors.RESET)
    print("    " + bcolors.BLU + "nodes[(0,10)]" + bcolors.RESET)

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))


if __name__ == "__main__":
     main()
