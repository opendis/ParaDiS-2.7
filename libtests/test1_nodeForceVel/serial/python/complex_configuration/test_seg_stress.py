from ctypes import *
import numpy as np
import pickle
import time, sys, os

sys.path.extend([os.path.abspath('../../../../../python'),os.path.abspath('../../../../../lib')])

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import * 

paradis = ParaDiS(home_lib)

mu = 1000.0
nu = 0.3
a = 0.01

atol = 1e-10

seg_data = np.load("seg_data.npy")
p1_list = seg_data[:, 0:3]
p2_list = seg_data[:, 3:6]
b12_list = seg_data[:, 6:9]
x_list = seg_data[:, 9:12]

ref_stress = np.load("ref_seg_stress.npy")
seg_stress_A = np.zeros_like(ref_stress)
seg_stress_B = np.zeros_like(ref_stress)

# Test A: use ParaDiS library (stress dependent form)
for ii, (p1, p2, b12, x) in enumerate(zip(p1_list, p2_list, b12_list, x_list)):
    seg_stress_A[ii] = paradis.compute_seg_stress_coord_dep(p1, p2, b12, x, mu, nu, a)

print(f"max error: {np.max(np.abs(seg_stress_A - ref_stress)): .6e}")
isclose_seg_stress_A = np.allclose(seg_stress_A, ref_stress, rtol=0.0, atol=atol)
if isclose_seg_stress_A:
    print("\033[92mTest_A Passed!\033[0m")
else:
    print("\033[91mTest_A Failed!\033[0m")

# Test B: use ParaDiS library (stress dependent form)
for ii, (p1, p2, b12, x) in enumerate(zip(p1_list, p2_list, b12_list, x_list)):
    seg_stress_B[ii] = paradis.compute_seg_stress_coord_indep(p1, p2, b12, x, mu, nu, a)

print(f"max error: {np.max(np.abs(seg_stress_B - ref_stress)): .6e}")
isclose_seg_stress_B = np.allclose(seg_stress_B, ref_stress, rtol=0.0, atol=atol)
if isclose_seg_stress_B:
    print("\033[92mTest_B Passed!\033[0m")
else:
    print("\033[91mTest_B Failed!\033[0m")

sys.exit(0 if isclose_seg_stress_A and isclose_seg_stress_B else 1)