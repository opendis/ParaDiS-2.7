from ctypes import *
import numpy as np
import pickle
import time, sys, os

sys.path.extend([os.path.abspath('../../../../../python'),os.path.abspath('../../../../../lib')])

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import * 

paradis = ParaDiS(home_lib)


tolA, tolB = 2e4, 1e-4

segseg_data_SBA = np.loadtxt("segsegforce_180chains_SBA_1000.dat")
segseg_data_SBN1 = np.loadtxt("segsegforce_180chains_SBN1_10000.dat")

#segseg_data = segseg_data_SBA
segseg_data = segseg_data_SBN1
print("segseg_data.shape = ", segseg_data.shape)

npairs = segseg_data.shape[0]

p1 = segseg_data[:npairs, 0:3]
p2 = segseg_data[:npairs, 3:6]
p3 = segseg_data[:npairs, 6:9]
p4 = segseg_data[:npairs, 9:12]

b12 = segseg_data[:npairs, 12:15]
b34 = segseg_data[:npairs, 15:18]
f1234_ref = segseg_data[:npairs, 18:30]

mu = segseg_data[0,30]
nu = segseg_data[0,31]
a = segseg_data[0,32]

seg12local = segseg_data[:npairs, 33].astype(int)
seg34local = segseg_data[:npairs, 34].astype(int)

force_nint = 3

f1_ref = f1234_ref[:,0:3]
f2_ref = f1234_ref[:,3:6]
f3_ref = f1234_ref[:,6:9]
f4_ref = f1234_ref[:,9:12]

# Test A: use ParaDiS library (SBA)
f1A, f2A, f3A, f4A = paradis.compute_segseg_force_SBA_vec(p1, p2, p3, p4, b12, b34, mu, nu, a)

for i in range(npairs):
    if seg12local[i] == 0:
        f1A[i,:] = 0.0; f2A[i,:] = 0.0
    if seg34local[i] == 0:
        f3A[i,:] = 0.0; f4A[i,:] = 0.0 

max_err_A = np.max(np.abs(np.concatenate((f1A, f2A, f3A, f4A), axis=1) - f1234_ref))
print(f"\nmax error = {max_err_A:.6e}")

testA_PASSED = max_err_A < tolA
print("\033[92mTestA Passed!\033[0m" if testA_PASSED else "\033[91mTestA Failed!\033[0m")

# Test B: use ParaDiS library (SBN1_SBA)
Nint = 3
f1B, f2B, f3B, f4B = paradis.compute_segseg_force_SBN1_SBA_vec(p1, p2, p3, p4, b12, b34, mu, nu, a, Nint)

for i in range(npairs):
    if seg12local[i] == 0:
        f1B[i,:] = 0.0; f2B[i,:] = 0.0
    if seg34local[i] == 0:
        f3B[i,:] = 0.0; f4B[i,:] = 0.0 

max_err_B = np.max(np.abs(np.concatenate((f1B, f2B, f3B, f4B), axis=1) - f1234_ref))
print(f"\nmax error = {max_err_B:.6e}")

testB_PASSED = max_err_B < tolB
print("\033[92mTestB Passed!\033[0m" if testB_PASSED else "\033[91mTestB Failed!\033[0m")

sys.exit(0 if (testA_PASSED and testB_PASSED) else 1)
