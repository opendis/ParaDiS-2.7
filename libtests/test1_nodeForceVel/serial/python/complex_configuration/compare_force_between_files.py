import sys, os
import numpy as np
import matplotlib.pyplot as plt

file1 = sys.argv[1]
file2 = sys.argv[2]

data1=np.loadtxt(file1)
data2=np.loadtxt(file2)

force1 = data1[:,1:4]
force2 = data2[:,1:4]

f_diff = force1 - force2

plt.plot(f_diff[:,0])
plt.plot(f_diff[:,1])
plt.plot(f_diff[:,2])

plt.show()