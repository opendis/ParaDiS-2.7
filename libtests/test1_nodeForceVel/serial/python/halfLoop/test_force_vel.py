from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import * 

paradis = ParaDiS(home_lib)

class dpaths:
    REFDATADIR = "../ref_results"
    USRDATADIR = "properties"
    FULLDATAFILE  = "NodeForce_FULL.out"
    GROUPDATAFILE = "NodeForce_GROUP%d.out"
    PKLFILE = "%s_nodes.pkl"

class tols:
    TESTA = 3e-4     # sqrt(1e-7)
    TESTB_F = 3e-4     # sqrt(1e-9)
    TESTB_V = 3e-3     # sqrt(1e-9)
    TESTC_F = 3e-4     # sqrt(1e-9)
    TESTC_V = 3e-4     # sqrt(1e-9)
    MIN_V_MAG = 1e-1 # values below this threshold are considered as zero
    MIN_F_MAG = 1e-5

def cal_nodal_force_vel(home):
    paradis.CellCharge(home)
    paradis.SegSegListMaker(home, gids.FULL)

    zeroOnErr = 1
    doAll = 1
    forces_velocities = {}
    # NodeForce(home, gids.FULL) should be in front of NodeForce(home, j)
    # so that ComputeSegSigbRem() function is called (needed for fmEnabled = 0)
    paradis.NodeForce(home, gids.FULL)
    paradis.CalcNodeVelocities(home, zeroOnErr, doAll)
    forces_velocities[gids.FULL] = paradis.extract_nodal_force_vel(home)

    for j in range(gids.GROUP0, gids.GROUP4+1):
        paradis.FlagSubcycleNodes(home, j)
        paradis.NodeForce(home, j)
        paradis.CalcNodeVelocities(home, zeroOnErr, doAll)
        forces_velocities[j] = paradis.extract_nodal_force_vel(home)

    return forces_velocities

def write_force_vel_to_file(forces_velocities):
    for j in range(gids.GROUP0, gids.GROUP4+1):
        paradis.write_force_vel_array_to_file(os.path.join(dpaths.USRDATADIR,dpaths.GROUPDATAFILE%(j-gids.GROUP0)), forces_velocities[j])
    paradis.write_force_vel_array_to_file(os.path.join(dpaths.USRDATADIR,dpaths.FULLDATAFILE), forces_velocities[gids.FULL])

def read_force_vel_from_file(datadir):
    ref_forces_velocities = {}
    for j in range(gids.GROUP0, gids.GROUP4+1):
        ref_forces_velocities[j] = np.loadtxt(os.path.join(datadir,dpaths.GROUPDATAFILE%(j-gids.GROUP0)))
    ref_forces_velocities[gids.FULL] = np.loadtxt(os.path.join(datadir,dpaths.FULLDATAFILE))
    return ref_forces_velocities

def compare_force_vel(forces_velocities, ref_forces_velocities, do_testB_VEL=True, do_testC=True):
    print()
    print("*****************************************************************")
    print("testA checks if the sum of subgroup forces equal to total forces.")
    print("*****************************************************************")
    force_full = forces_velocities[gids.FULL][:,1:4]
    force_diff = force_full.copy()
    for j in range(gids.GROUP0, gids.GROUP4+1):
        force_diff -= forces_velocities[j][:,1:4]
    force_diff_norm = np.linalg.norm(force_diff,axis=1)
    force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
    force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(force_full,axis=1), tols.MIN_F_MAG) )
    print("max(force_rel_error) = %e"%np.amax(force_rel_err))

    if np.amax(force_rel_err) < tols.TESTA:
        print("testA" + bcolors.GRN + " PASSED" + bcolors.RESET)
        testA_PASSED = True
    else:
        print("testA" + bcolors.RED + " FAILED" + bcolors.RESET)
        testA_PASSED = False
        max_err_id = np.argmax(force_rel_err)
        node_id = forces_velocities[gids.FULL][max_err_id,0]
        print("Forces_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,force_full[max_err_id][0],force_full[max_err_id][1],force_full[max_err_id][2]))
        for j in range(gids.GROUP0, gids.GROUP4+1):
            force_group = forces_velocities[j][:,1:4]
            print("Forces_GROUP%d_user(%d): %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,force_group[max_err_id][0],force_group[max_err_id][1],force_group[max_err_id][2]))
        print("Forces_diff_max(%d):    %13.6e %13.6e %13.6e "%(node_id,force_diff[max_err_id][0],force_diff[max_err_id][1],force_diff[max_err_id][2]))


    print()
    print("***********************************************************************")
    print("testB compares the FULL forces and velocities against reference values.")
    print("***********************************************************************")
    ref_force_full = ref_forces_velocities[gids.FULL][:,1:4]
    force_diff = force_full - ref_force_full
    force_diff_norm = np.linalg.norm(force_diff,axis=1)
    force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
    force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(ref_force_full,axis=1), tols.MIN_F_MAG) )
    print("max(force_rel_error) = %e"%np.amax(force_rel_err))
    if np.amax(force_rel_err) < tols.TESTB_F:
        print("testB (force)" + bcolors.GRN + " PASSED" + bcolors.RESET)
        testB_FORCE_PASSED = True
    else:
        print("testB (force)" + bcolors.RED + " FAILED" + bcolors.RESET)
        testB_FORCE_PASSED = False
        max_err_id = np.argmax(force_rel_err)
        node_id = forces_velocities[gids.FULL][max_err_id,0]
        print("Forces_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,force_full[max_err_id][0],force_full[max_err_id][1],force_full[max_err_id][2]))
        print("Forces_full_ref (%d):   %13.6e %13.6e %13.6e "%(node_id,ref_force_full[max_err_id][0],ref_force_full[max_err_id][1],ref_force_full[max_err_id][2]))
        print("Forces_diff_max (%d):   %13.6e %13.6e %13.6e "%(node_id,force_diff[max_err_id][0],force_diff[max_err_id][1],force_diff[max_err_id][2]))

    testAll_PASSED = testA_PASSED and testB_FORCE_PASSED

    if do_testB_VEL:
        vel_full = forces_velocities[gids.FULL][:,4:7]
        ref_vel_full = ref_forces_velocities[gids.FULL][:,4:7]
        vel_diff = vel_full - ref_vel_full
        vel_diff_norm = np.linalg.norm(vel_diff,axis=1)
        vel_diff_norm[ vel_diff_norm < tols.MIN_V_MAG ] = 0
        vel_rel_err = np.divide( vel_diff_norm, np.maximum(np.linalg.norm(ref_vel_full,axis=1), tols.MIN_V_MAG) )
        print("max(vel_rel_error) = %e"%np.amax(vel_rel_err))
        if np.amax(vel_rel_err) < tols.TESTB_V:
            print("testB (velocity)" + bcolors.GRN + " PASSED" + bcolors.RESET)
            testB_VEL_PASSED = True
        else:
            print("testB (velocity)" + bcolors.RED + " FAILED" + bcolors.RESET)
            testB_VEL_PASSED = False
            max_err_id = np.argmax(vel_rel_err)
            node_id = forces_velocities[gids.FULL][max_err_id,0]
            print("Velocities_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,vel_full[max_err_id][0],vel_full[max_err_id][1],vel_full[max_err_id][2]))
            print("Velocities_full_ref (%d):   %13.6e %13.6e %13.6e "%(node_id,ref_vel_full[max_err_id][0],ref_vel_full[max_err_id][1],ref_vel_full[max_err_id][2]))
            print("Velocities_diff_max (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_diff[max_err_id][0],vel_diff[max_err_id][1],vel_diff[max_err_id][2]))

        testAll_PASSED = testAll_PASSED and testB_VEL_PASSED

    if do_testC:
        print()
        print("************************************************************************")
        print("testC compares the GROUP forces and velocities against reference values.")
        print("************************************************************************")
        testC_FORCE_PASSED = True
        testC_VEL_PASSED = True
        for j in range(gids.GROUP0, gids.GROUP4+1):
            force_group = forces_velocities[j][:,1:4]
            ref_force_group = ref_forces_velocities[j][:,1:4]
            force_diff = force_group - ref_force_group
            force_diff_norm = np.linalg.norm(force_diff,axis=1)
            force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
            force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(ref_force_group,axis=1), tols.MIN_F_MAG) )
            print("max(force_rel_error for group%d) = %e"%(j-gids.GROUP0,np.amax(force_rel_err)))
            if np.amax(force_rel_err) < tols.TESTC_F:
                print("testC (force for group%d)"%(j-gids.GROUP0) + bcolors.GRN + " PASSED" + bcolors.RESET)
            else:
                print("testC (force for group%d)"%(j-gids.GROUP0) + bcolors.RED + " FAILED" + bcolors.RESET)
                testC_FORCE_PASSED = False
                max_err_id = np.argmax(force_rel_err)
                node_id = forces_velocities[gids.FULL][max_err_id,0]
                print("Forces_group%d_user(%d):     %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,force_group[max_err_id][0],force_group[max_err_id][1],force_group[max_err_id][2]))
                print("Forces_group%d_ref (%d):     %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,ref_force_group[max_err_id][0],ref_force_group[max_err_id][1],ref_force_group[max_err_id][2]))
                print("Forces_group%d_diff_max(%d): %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,force_diff[max_err_id][0],force_diff[max_err_id][1],force_diff[max_err_id][2]))
    
            vel_group = forces_velocities[j][:,4:7]
            ref_vel_group = ref_forces_velocities[j][:,4:7]
            vel_diff = vel_group - ref_vel_group
            vel_diff_norm = np.linalg.norm(vel_diff,axis=1)
            vel_diff_norm[ vel_diff_norm < tols.MIN_V_MAG ] = 0
            vel_rel_err = np.divide( vel_diff_norm, np.maximum(np.linalg.norm(ref_vel_group,axis=1), tols.MIN_V_MAG) )
            print("max(vel_rel_error for group%d) = %e"%(j-gids.GROUP0,np.amax(vel_rel_err)))
            if np.amax(vel_rel_err) < tols.TESTC_V:
                print("testC (velocity for group%d)"%(j-gids.GROUP0) + bcolors.GRN + " PASSED" + bcolors.RESET)
            else:
                print("testC (velocity for group%d)"%(j-gids.GROUP0) + bcolors.RED + " FAILED" + bcolors.RESET)
                testC_VEL_PASSED = False
                max_err_id = np.argmax(vel_rel_err)
                node_id = forces_velocities[gids.FULL][max_err_id,0]
                print("Velocities_group%d_user(%d):     %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,vel_group[max_err_id][0],vel_group[max_err_id][1],vel_group[max_err_id][2]))
                print("Velocities_group%d_ref (%d):     %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,ref_vel_group[max_err_id][0],ref_vel_group[max_err_id][1],ref_vel_group[max_err_id][2]))
                print("Velocities_group%d_diff_max(%d): %13.6e %13.6e %13.6e "%(j-gids.GROUP0,node_id,vel_diff[max_err_id][0],vel_diff[max_err_id][1],vel_diff[max_err_id][2]))
            print()

        testC_PASSED = testC_FORCE_PASSED and testC_VEL_PASSED
        testAll_PASSED = testAll_PASSED and testC_PASSED

    return testAll_PASSED

'''
Main Program Starts Here
'''
def main():
    global home, nodes

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)

    if use_GPU: paradis.InitializeParadisGPU(home)

    t_begin = time.time()  

    print('  convert network to nodes dictionary')
    nodes = convert_nodes_to_dict(home)
    output_filename = dpaths.PKLFILE%(taskname)
    print('  save nodes dictionary to %s'%(output_filename))
    with open(output_filename, 'wb') as f:
        pickle.dump(nodes, f)

    print("calculate nodal forces and velocities")
    forces_velocities = cal_nodal_force_vel(home)

    write_force_vel_to_file(forces_velocities)
    ref_forces_velocities = read_force_vel_from_file(dpaths.REFDATADIR)
    testAll_PASSED = compare_force_vel(forces_velocities, ref_forces_velocities, do_testB_VEL=(not use_GPU), do_testC=(not use_GPU))

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0 if testAll_PASSED else 1

if __name__ == "__main__":
    sys.exit( main() )
