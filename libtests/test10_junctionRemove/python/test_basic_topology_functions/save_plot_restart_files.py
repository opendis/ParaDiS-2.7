
from ctypes import *
import numpy as np
import pickle
import time, sys, os
import glob

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *
from disl_link_util import save_plot_nodes_dict


'''
Main Program Starts Here
'''
def main():
    global home, nodes

    # can plot one restart (*rs*.data) file or all restart files in a folder
    if len(sys.argv) > 1:
        fpath = sys.argv[1]
    else:
        fpath = '.'

    if not os.path.exists(fpath):
        print(fpath + ' does not exist')
        return

    if os.path.isfile(fpath):
        datafile = fpath
        nodes, bounds = read_nodes_to_dict(datafile)
        save_plot_nodes_dict(nodes, bounds, block=False, filename='binary_junction')
        #plt.pause(0.5)
    else:
        fig = plt.figure(figsize=(8,8))
        for datafile in sorted(glob.glob(os.path.join(fpath,'*rs*.data'))):
            nodes, bounds = read_nodes_to_dict(datafile)
            save_plot_nodes_dict(nodes, bounds, fig=fig, block=False, filename='binary_junction')
            plt.title(os.path.basename(datafile))
            #plt.pause(0.5)

if __name__ == "__main__":
     main()
