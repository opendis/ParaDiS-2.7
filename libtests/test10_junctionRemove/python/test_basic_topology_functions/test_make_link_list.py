
from ctypes import *
import numpy as np
import sys, random, filecmp

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    nodes = convert_nodes_to_dict(home)
    bounds = find_bounds_from_param(param)

    # Make and write link data
    link_list_without_slip_system = make_link_list(nodes, bounds)
    write_link_list(taskname, link_list_without_slip_system, None, suffix='_without_slip_system.linkData')

    link_list_with_slip_system = make_link_list(nodes, bounds, check_slip_system=True)
    write_link_list(taskname, link_list_with_slip_system, None, suffix='_with_slip_system.linkData', check_slip_system=True)

    # Compare files against reference
    fname = 'without_slip_system.linkData'
    if filecmp.cmp(taskname+'_'+fname, '../ref_results/'+taskname+'_'+fname, shallow=False):
        print("test (" + fname + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    fname = 'with_slip_system.linkData'
    if filecmp.cmp(taskname+'_'+fname, '../ref_results/'+taskname+'_'+fname, shallow=False):
        print("test (" + fname + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
