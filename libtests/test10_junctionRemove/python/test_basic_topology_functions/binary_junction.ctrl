########################################
###                                  ###
###  ParaDiS control parameter file  ###
###                                  ###
########################################

#
#  Simulation cell and processor setup
#  
numXdoms =   1  
numYdoms =   1  
numZdoms =   1  
numXcells =   4  
numYcells =   4  
numZcells =   4  
xBoundType =   0  
yBoundType =   0  
zBoundType =   0  
DLBfreq =   0  
#
#  Simulation time and timestepping controls
#  
cycleStart =   50  
maxstep =   50  
timeNow =   3.765263e-06  
timeStart =   3.665263e-06  
timestepIntegrator =   "forceBsubcycle"  
subInteg0Integ1 =   "GPU"  
deltaTT =   1.000000e-07  
maxDT =   1.000000e-07  
nextDT =   1.000000e-07  
dtIncrementFact =   1.200000e+00  
dtDecrementFact =   5.000000e-01  
dtExponent =   4.000000e+00  
dtVariableAdjustment =   0  
rTol =   2.500000e-02  
rTolrel =   1.000000e-02  
rTolth =   1.000000e-01  
renh =   0.000000e+00  
rg1 =   0.000000e+00  
rg2 =   0.000000e+00  
rg3 =   0.000000e+00  
rg4 =   0.000000e+00  
nTry =   0  
sendSubGroupForc =   0  
#
#  Discretization and topological change controls
#  
maxSeg =   2.000000e+03  
minSeg =   5.000000e+02  
remeshRule =   2  
splitMultiNodeFreq =   1  
collisionMethod =   4  
rann =   5.000000e-02  
#
#  Fast Multipole Method controls
#  
fmEnabled =   1  
fmMPOrder =   2  
fmTaylorOrder =   5  
fmCorrectionTbl =   "../../../../inputs/fm-ctab.Ta.600K.0GPa.m2.t5.dat"  
#
#  Tables for non-FMM far-field force calcs
#  
#
#  Loading conditions
#  
loadType =   1  
appliedStress = [
  4.411030e-15
  1.928842e-14
  6.338506e+05
  8.639629e-05
  4.051941e-05
  9.221262e-15
  ]
eRate =   1.000000e+00  
indxErate =   1  
edotdir = [
  1.788233e-10
  3.688575e-10
  1.000000e+00
  ]
useLabFrame =   0  
#
#  Material and mobility parameters
#  
mobilityLaw =   "FCC_0"  
shearModulus =   6.488424e+10  
pois =   3.327533e-01  
burgMag =   2.875401e-10  
YoungModulus =   1.729494e+11  
rc =   1.000000e-01  
Ecore =   0.000000e+00  
MobScrew =   1.000000e+01  
MobEdge =   1.000000e+01  
MobClimb =   1.000000e-04  
FricStress =   0.000000e+00  
includeInertia =   0  
#
#  Flux decomposition
#  
totstraintensor = [
  2.550888e-26
  1.115443e-25
  3.665263e-06
  4.996166e-16
  2.343182e-16
  5.332630e-26
  ]
totpStn = [
  -1.295683e-11
  -3.016911e-10
  3.146479e-10
  -6.478416e-12
  -1.638024e-10
  -1.573240e-10
  ]
totpSpn = [
  0.000000e+00
  0.000000e+00
  0.000000e+00
  3.688575e-10
  1.788233e-10
  2.124557e-10
  ]
FCC_Ltot = [
  1.573211e+09
  2.724882e+09
  0.000000e+00
  2.250902e-01
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  2.257226e+07
  0.000000e+00
  3.909631e+07
  3.229575e-03
  1.640928e+09
  2.763978e+09
  0.000000e+00
  2.283198e-01
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  ]
FCC_fluxtot = [
  -8.288574e-17
  -2.912393e-05
  0.000000e+00
  2.405840e-15
  2.009690e-05
  0.000000e+00
  -5.647187e-16
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  1.175169e-27
  0.000000e+00
  2.821290e-06
  2.330542e-16
  0.000000e+00
  -2.821290e-06
  -8.780692e-17
  1.148138e-17
  -3.734686e-04
  0.000000e+00
  3.085079e-14
  -1.152085e-04
  0.000000e+00
  3.576663e-15
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  0.000000e+00
  ]
#
#  Total density. Informational only; ignored on input
#  
disloDensity =   6.409352e+09  
#
#  Velocity statistics
#  
vAverage =   0.000000e+00  
vStDev =   0.000000e+00  
#
#  I/O controls and parameters
#  
dirname =   "binary_junction_results"  
writeBinRestart =   0  
skipIO =   0  
numIOGroups =   1  
armfile =   0  
fluxfile =   0  
fragfile =   0  
gnuplot =   0  
polefigfile =   0  
povray =   0  
povrayfreq =   100  
povraycounter =   0  
atomeye =   0  
atomeyefreq =   0  
atomeyedt =   0.000000e+00  
atomeyetime =   0.000000e+00  
atomeyesegradius =   5.000000e+02  
atomeyecounter =   0  
psfile =   0  
savecn =   1  
savecnfreq =   1  
savecncounter =   50  
saveprop =   1  
savepropfreq =   1  
savetimers =   0  
tecplot =   0  
paraview =   0  
paraviewfreq =   100  
paraviewdt =   -1.000000e+00  
paraviewtime =   0.000000e+00  
paraviewcounter =   0  
velfile =   0  
writeForce =   0  
writeVisit =   0  
winDefaultsFile =   "inputs/paradis.xdefaults"  
#
#  Miscellaneous parameters
#  
enforceGlidePlanes =   1  
enableCrossSlip =   1  
TensionFactor =   1.000000e+00  
elasticinteraction =   1  
