
from ctypes import *
import numpy as np
import sys, random

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    cycleStart = param.contents.cycleStart
    cycleEnd = 200
    bounds = find_bounds_from_param(param)

    while home.contents.cycle<cycleEnd:
        paradis.ParadisStep(home)

        # add a 2-arm node
        #if home.contents.cycle == 50:
        if home.contents.cycle % 10 == 0:
            nodes = convert_nodes_to_dict(home)

            tag_list = list(nodes.keys())
            random.shuffle(tag_list)
            oprec_list = []
            new_tag = None
            for tag in tag_list[:10]:
                arm_tag_list = list(nodes[tag][1].keys())
                num_nbr = len(arm_tag_list)
                nbr_tag = arm_tag_list[random.randint(0,num_nbr-1)]
                new_tag = get_new_node_tag(nodes, new_tag, home)
                
                x0 = nodes[tag][0][0:3]
                x1 = nodes[nbr_tag][0][0:3]
                dx = pbc_dr_bounds(x0, x1, bounds)
                x1 = x0 + dx
                new_node_position = (x0 + x1)/2
                new_node_position = np.append(new_node_position, np.array([0]))
                print('add a new node (%d,%d) between nodes[(%d,%d)] to nodes[(%d,%d)]'%\
                      (new_tag[0],new_tag[1],tag[0],tag[1],nbr_tag[0],nbr_tag[1]))
                paradis.add_2_arm_node(nodes, tag, nbr_tag, new_tag, new_node_position, oprec_list, home)
            paradis.SortNativeNodes(home)
            write_oprec_file(oprec_list, taskname)
            
            supp_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_add_2_arm_node'
            write_data_file(param, nodes, supp_restart_file_name)

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
