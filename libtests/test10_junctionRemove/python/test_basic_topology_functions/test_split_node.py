
from ctypes import *
import numpy as np
import time, sys, os
import random

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    # Make and write link data
    nodes = convert_nodes_to_dict(home)
    bounds = find_bounds_from_param(param)

    link_list = make_link_list(nodes, bounds)
    write_link_list(taskname, link_list, None, suffix='_before_splitting.linkData')

    cycleStart = param.contents.cycleStart
    cycleEnd  = 200
    bounds = find_bounds_from_param(param)

    while home.contents.cycle<cycleEnd:
        # split nodes
        #if home.contents.cycle == 50:
        if home.contents.cycle % 10 == 0:
            nodes = convert_nodes_to_dict(home)

            oprec_list = []
            multi_arm_nodes = [tag for tag in nodes if len(nodes[tag][1])>3]
            new_tag = None
            for tag in multi_arm_nodes:

                new_tag = get_new_node_tag(nodes, new_tag, home)
                x0 = nodes[tag][0][0:3]
                dx = np.array([0.0, 70.0, 70.0])
                pos = x0 - dx
                new_pos = x0 + dx

                arm_tag_list = list(nodes[tag][1].keys())
                random.shuffle(arm_tag_list)
                new_arm_tags = arm_tag_list[:2]

                print('split node nodes[(%d,%d)] to nodes[(%d,%d)]'%\
                      (tag[0],tag[1],new_tag[0],new_tag[1]))
                paradis.split_node(nodes, bounds, tag, new_tag, pos, new_pos, new_arm_tags, oprec_list, home)
            paradis.SortNativeNodes(home)
            write_oprec_file(oprec_list, taskname)
            
            supp_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_merge_node'
            write_data_file(param, nodes, supp_restart_file_name)

        paradis.ParadisStep(home)

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
