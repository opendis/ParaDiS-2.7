
from ctypes import *
import numpy as np
import sys

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    cycleStart = param.contents.cycleStart
    cycleEnd  = 200
    bounds = find_bounds_from_param(param)

    while home.contents.cycle<cycleEnd:
        paradis.ParadisStep(home)

        # remove all discretization nodes
        #if home.contents.cycle == 50:
        if home.contents.cycle % 10 == 0:
            nodes = convert_nodes_to_dict(home)

            print('remove all discretization nodes')

            oprec_list = []
            node_key_list = list(nodes.keys())
            for tag in node_key_list:
                if len(nodes[tag][1]) == 2:
                    paradis.remove_2_arm_node(nodes, tag, oprec_list, home)
            paradis.SortNativeNodes(home)
            write_oprec_file(oprec_list, taskname)
            
            supp_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_remove_2_arm_node'
            write_data_file(param, nodes, supp_restart_file_name)

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
