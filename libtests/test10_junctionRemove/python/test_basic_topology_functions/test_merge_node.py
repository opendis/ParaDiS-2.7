
from ctypes import *
import numpy as np
import sys, random

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    cycleStart = param.contents.cycleStart
    cycleEnd  = 200
    bounds = find_bounds_from_param(param)

    while home.contents.cycle<cycleEnd:
        paradis.ParadisStep(home)

        # merge nodes
        #if home.contents.cycle == 50:
        if home.contents.cycle % 10 == 0:
            nodes = convert_nodes_to_dict(home)

            multi_arm_nodes = [tag for tag in nodes if len(nodes[tag][1])>2]
            tag_list = list(multi_arm_nodes)
            random.shuffle(tag_list)
            tag_list = tag_list[:10]

            oprec_list = []
            while len(tag_list)>0:
                to_node = tag_list[0]
                arm_tag_list = list(nodes[to_node][1].keys())
                num_nbr = len(arm_tag_list)
                from_node = arm_tag_list[random.randint(0,num_nbr-1)]

                print('merge nodes[(%d,%d)] to nodes[(%d,%d)]'%(from_node[0],from_node[1],to_node[0],to_node[1]))
                paradis.merge_node(nodes, from_node, to_node, oprec_list, home)
                tag_list.remove(to_node)
                if from_node in tag_list:
                    tag_list.remove(from_node)
            paradis.SortNativeNodes(home)
            write_oprec_file(oprec_list, taskname)
            
            supp_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_merge_node'
            write_data_file(param, nodes, supp_restart_file_name)

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
