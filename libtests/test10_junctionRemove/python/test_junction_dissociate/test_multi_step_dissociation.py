
from ctypes import *
import numpy as np
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Input variables
    crit = 300 # Critical length for junction removal
    diss_step = 10 # period for junction dissociation

    cycleStart = param.contents.cycleStart
    #cycleEnd = cycleStart + param.contents.maxstep
    cycleEnd = cycleStart + 100
    bounds = find_bounds_from_param(param)

    param.contents.savecnfreq = diss_step

    t_begin = time.time()  

    while home.contents.cycle<cycleEnd:
        t0 = time.time()  
        paradis.ParadisStep(home)

        t1 = time.time()
        #print('step = %d/%d  time used = %g s'%(home.contents.cycle, cycleEnd, t1-t0))

        # Remove links
        if home.contents.cycle % diss_step == 0:
            nodes = convert_nodes_to_dict(home)
            num_nodes_before_diss = len(nodes)
            link_list = make_link_list(nodes, bounds)
            diss_link_ids = find_links_to_be_operated(link_list, crit)

            oprec_list = []
            num = 0
            for link_id_to_dissociate in diss_link_ids:
                link_list, issucceed = paradis.dissociate_link_by_change_arm(nodes, bounds, link_list, link_id_to_dissociate, oprec_list)

                if issucceed:
                    num += 1

            paradis.SortNativeNodes(home)
            print('    The number of dissociated links: %d'%num)
            print('    Total number of node changed from %d to %d'%(num_nodes_before_diss,len(nodes)))
            
            diss_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_diss'
            write_data_file(param, nodes, diss_restart_file_name)

        t_end = time.time()  
        #print('Total time used = %g s'%(t_end-t_begin))

    paradis.ParadisFinish(home)

if __name__ == "__main__":
     main()
