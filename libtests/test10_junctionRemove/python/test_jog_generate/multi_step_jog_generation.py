
from ctypes import *
import numpy as np
import sys, os
from random import *

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Input variables
    crit = [3000, 1000000]
    jog_step = 10 # period for jog generation
    param.contents.savecnfreq = jog_step

    cycleStart = param.contents.cycleStart
    cycleEnd = cycleStart + param.contents.maxstep
    bounds = find_bounds_from_param(param)

    probability = 0.1
    jog_length_ratio = 1/3
    jog_height = 15

    is_initial_cycle = True
    while home.contents.cycle<cycleEnd:
        paradis.ParadisStep(home)

        # Generate jogs
        if home.contents.cycle % jog_step == 0 or is_initial_cycle:
            nodes = convert_nodes_to_dict(home)
            num_nodes_before_jog = len(nodes)
            link_list = make_link_list(nodes, bounds, check_slip_system=True)
            target_link_ids = find_links_to_be_operated(link_list, crit)

            if is_initial_cycle:
                is_initial_cycle = False
                restart_filename = 'restart/rs0030'
            else:
                restart_filename = 'restart/rs%04d'%param.contents.savecncounter
            write_link_list(restart_filename, link_list, None, suffix='_before_generation.linkData')
            
            num = 0
            jog_link_ids = []
            oprec_list = []

            # jog generations on multiple links
            #for link_id in target_link_ids:
            #    f = random.random()
            #    if f < probability and crit[0]<link_list[link_id][0]:
            #        num += 1
            #        #link_list = paradis.generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list)
            #        link_list = paradis.generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list, home)
            #        jog_link_ids.append(link_id)

            # jog generations on 1 link
            while True:
                link_id = random.randint(0,len(link_list)-1)
                if crit[0] < link_list[link_id][0]:
                    num = 1
                    #link_list = paradis.generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list)
                    link_list = paradis.generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list, home)
                    jog_link_ids.append(link_id)
                    break

            print('    The number of link to generate jogs: %d'%num)
            print('    Total number of nodes is change from %d to %d.'%(num_nodes_before_jog,len(nodes)))
            #paradis.paradis_apply_oprec_list(home, oprec_list)
            paradis.SortNativeNodes(home)

            link_list.clear()
            link_list = make_link_list(nodes, bounds)
            write_link_list(restart_filename, link_list, None, suffix='_after_generation.linkData')

            jog_restart_filename = restart_filename + '_jog'
            write_jog_generation_information(crit, probability, jog_length_ratio, jog_height, num, jog_link_ids, restart_filename, suffix='.jog_info')
            write_data_file(param, nodes, jog_restart_filename)

    paradis.ParadisFinish(home)

if __name__ == "__main__":
     main()
