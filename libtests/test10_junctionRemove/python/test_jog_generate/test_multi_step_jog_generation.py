
from ctypes import *
import numpy as np
import sys, os
from random import *

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Input variables
    jog_step = 10 # period for jog generation
    param.contents.maxstep = 100
    param.contents.savecnfreq = jog_step

    cycleStart = param.contents.cycleStart
    cycleEnd = cycleStart + param.contents.maxstep
    bounds = find_bounds_from_param(param)

    jog_length_ratio = 1/3
    jog_height = 15

    while home.contents.cycle < cycleEnd:
        paradis.ParadisStep(home)

        # Generate jogs
        if home.contents.cycle % jog_step == 0:
            nodes = convert_nodes_to_dict(home)
            num_nodes_before_jog = len(nodes)
            link_list = make_link_list(nodes, bounds, check_slip_system=True)

            success_to_find_link_id = False
            while not success_to_find_link_id:
                link_id = random.randint(0,len(link_list)-1)
                if jog_height < link_list[link_id][0]:
                    success_to_find_link_id = True

            oprec_list = []

            # version 1. make oprec_list and apply to home structure
            #link_list = paradis.generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list)
            #paradis.paradis_apply_oprec_list(home, oprec_list)

            # verstion 2. modify home structures simultaneously
            link_list = paradis.generate_jog_on_link(nodes, bounds, link_list, link_id, jog_length_ratio, jog_height, oprec_list, home)

            print('    Total number of nodes is change from %d to %d.'%(num_nodes_before_jog,len(nodes)))
            paradis.SortNativeNodes(home)
            
            jog_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_jog'
            write_data_file(param, nodes, jog_restart_file_name)

    paradis.ParadisFinish(home)

if __name__ == "__main__":
     main()
