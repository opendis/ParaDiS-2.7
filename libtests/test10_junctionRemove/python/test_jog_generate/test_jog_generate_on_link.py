
from ctypes import *
import numpy as np
import time, sys, os
import filecmp

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Make and write link data
    nodes = convert_nodes_to_dict(home)
    bounds = find_bounds_from_param(param)

    link_list = make_link_list(nodes, bounds, check_slip_system=True)
    write_link_list(taskname, link_list, None, suffix='_before_jog_generation_on_link.linkData', check_slip_system=True)

    # Generate jog
    new_nodes = copy.deepcopy(nodes)

    #updated_link_list = link_list
    updated_link_list = copy.deepcopy(link_list)
    link_id = 0
    jog_length_ratio = 1/3
    jog_height = 15
    oprec_list = []
    updated_link_list = paradis.generate_jog_on_link(new_nodes, bounds, updated_link_list, link_id, jog_length_ratio, jog_height, oprec_list)
    #updated_link_list = paradis.generate_jog_on_link(new_nodes, bounds, tag, length, dir, oprec_list, home)

    new_link_list = make_link_list(new_nodes, bounds, check_slip_system=True)

    # Write data after generating jog
    write_link_list(taskname, new_link_list, None, suffix='_after_jog_generation_on_link.linkData', check_slip_system=True)
    write_data_file(param, new_nodes, taskname, suffix="_after_jog_generation_on_link.data")
    write_oprec_file(oprec_list, taskname)

    # Compare files against reference
    fname = 'before_jog_generation_on_link.linkData'
    if filecmp.cmp(taskname+'_'+fname, '../ref_results/'+taskname+'_'+fname, shallow=False):
        print("test (" + fname + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    fname = 'after_jog_generation_on_link.linkData'
    if filecmp.cmp(taskname+'_'+fname, '../ref_results/'+taskname+'_'+fname, shallow=False):
        print("test (" + fname + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname + ")" + bcolors.RED + " FAILED" + bcolors.RESET)
        
    fname = 'after_jog_generation_on_link.data'
    if compare_data_files(taskname+'_'+fname, '../ref_results/'+taskname+'_'+fname):
        print("test (" + fname + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
