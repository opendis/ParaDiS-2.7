########################################
###                                  ###
###  ParaDiS control parameter file  ###
###                                  ###
########################################
#
# Copper_GPU_1e4_10000step_rs0100
#
#  Simulation cell and processor setup
#  
numXdoms =   1  
numYdoms =   1  
numZdoms =   1  
numXcells =   4  
numYcells =   4  
numZcells =   4  
xBoundType =   0  
yBoundType =   0  
zBoundType =   0  
decompType =   2  
DLBfreq =   0  
#
#  Simulation time and timestepping controls
#  
cycleStart =   10000  
maxstep =   10000  
timeNow =   1.550000e-07  
timeStart =   1.549964e-07  
timestepIntegrator =   "forceBsubcycle"  
subInteg0Integ1 =   "GPU"  
deltaTT =   3.653582e-12  
maxDT =   1.000000e-07  
nextDT =   3.653582e-12  
dtIncrementFact =   1.200000e+00  
dtDecrementFact =   5.000000e-01  
dtExponent =   4.000000e+00  
dtVariableAdjustment =   0  
rTol =   1.000000e+01  
rTolrel =   1.000000e-02  
rTolth =   1.000000e-01  
renh =   0.000000e+00  
rg1 =   0.000000e+00  
rg2 =   0.000000e+00  
rg3 =   0.000000e+00  
rg4 =   0.000000e+00  
nTry =   0  
sendSubGroupForc =   0  
#
#  Discretization and topological change controls
#  
maxSeg =   4.000000e+02  
minSeg =   1.359235e+02  
remeshRule =   2  
splitMultiNodeFreq =   1  
collisionMethod =   4  
rann =   2.000000e+01  
#
#  Fast Multipole Method controls
#  
fmEnabled =   1  
fmMPOrder =   2  
fmTaylorOrder =   5  
fmCorrectionTbl =   "../../../../inputs/fm-ctab.Cu.m2.t5.dat"  
#
#  Tables for non-FMM far-field force calcs
#  
#
#  Loading conditions
#  
loadType =   1  
appliedStress = [
  1.171464e-02
  -1.458627e-02
  7.518246e+07
  3.716554e+02
  -5.297973e+02
  -3.336470e-03
  ]
eRate =   1.000000e+04  
indxErate =   1  
edotdir = [
  -4.077200e-05
  1.295203e-04
  1.000000e+00
  ]
useLabFrame =   0  
#
#  Material and mobility parameters
#  
mobilityLaw =   "FCC_0"  
shearModulus =   5.460000e+10  
pois =   3.240000e-01  
burgMag =   2.556000e-10  
YoungModulus =   1.445808e+11  
rc =   5.000000e+00  
Ecore =   1.699747e+10  
MobScrew =   1.000000e+04  
MobEdge =   1.000000e+04  
FricStress =   0.000000e+00  
includeInertia =   0  
#
#  Flux decomposition
#  
totstraintensor = [
  1.453362e-12
  5.417853e-12
  1.549964e-03
  6.503180e-08
  -3.825272e-08
  -2.622716e-12
  ]
totpStn = [
  -5.401603e-04
  -4.897807e-04
  1.029963e-03
  1.178297e-05
  5.496176e-05
  -2.723499e-05
  ]
totpSpn = [
  0.000000e+00
  0.000000e+00
  0.000000e+00
  1.295181e-04
  -4.077629e-05
  6.185133e-05
  ]
FCC_Ltot = [
  6.349548e+10
  3.342933e+10
  3.971408e+10
  1.130804e+10
  5.060191e+10
  4.838836e+10
  2.299524e+10
  8.564484e+09
  3.519647e+11
  1.597247e+11
  2.084234e+11
  3.320866e+09
  2.937801e+11
  1.489156e+11
  1.796105e+11
  4.480550e+09
  2.928290e+11
  2.005611e+11
  2.045013e+11
  4.544701e+10
  2.480800e+11
  1.684156e+11
  6.842200e+10
  2.874680e+10
  ]
FCC_fluxtot = [
  -1.030574e-04
  2.614248e+01
  -4.531125e+01
  7.226461e-05
  -3.984918e+01
  -7.282620e+00
  -6.367185e-06
  8.313080e-03
  -4.536866e+01
  -2.573284e+01
  -1.953890e-02
  3.809820e+01
  -1.108020e+01
  -1.525998e-09
  -9.760279e-02
  -1.589226e+03
  2.630644e+03
  1.775218e-01
  -8.882915e+02
  2.347898e+03
  -4.543175e-08
  2.163437e-01
  -1.859132e+03
  -1.987529e+03
  5.526088e-02
  -1.069950e+03
  -1.303791e+03
  -1.124504e-03
  -4.086982e-02
  -2.860292e+03
  2.535130e+03
  -1.041069e-01
  -1.047168e+03
  1.092441e+03
  -1.637199e-03
  2.233637e-03
  -2.147517e+03
  -3.008137e+02
  -4.673098e-02
  -1.141682e+03
  -5.235003e+02
  -6.331511e-05
  ]
#
#  Total density. Informational only; ignored on input
#  
disloDensity =   2.276092e+12  
#
#  Velocity statistics
#  
vAverage =   0.000000e+00  
vStDev =   0.000000e+00  
#
#  I/O controls and parameters
#  
dirname =   "Copper_rs0100_results"  
writeBinRestart =   0  
skipIO =   0  
numIOGroups =   1  
armfile =   0  
fluxfile =   0  
fragfile =   0  
gnuplot =   0  
polefigfile =   0  
povray =   0  
atomeye =   0  
atomeyefreq =   0  
atomeyedt =   0.000000e+00  
atomeyetime =   0.000000e+00  
atomeyesegradius =   5.000000e+02  
atomeyecounter =   0  
psfile =   0  
savecn =   1  
savecnfreq =   100  
savecncounter =   100  
saveprop =   1  
savepropfreq =   100  
savetimers =   1  
savetimersfreq =   100  
savetimerscounter =   100  
tecplot =   0  
paraview =   0  
paraviewfreq =   100  
paraviewdt =   -1.000000e+00  
paraviewtime =   0.000000e+00  
paraviewcounter =   0  
velfile =   0  
writeForce =   0  
writeVisit =   0  
winDefaultsFile =   "../../../inputs/paradis.xdefaults"  
#
#  Miscellaneous parameters
#  
enforceGlidePlanes =   1  
enableCrossSlip =   1  
TensionFactor =   1.000000e+00  
elasticinteraction =   1  
