
from ctypes import *
import numpy as np
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Critical length for junction removal
    crit = 200

    # Make and write link data
    nodes = convert_nodes_to_dict(home)
    bounds = find_bounds_from_param(param)
    link_list = make_link_list(nodes, bounds)

    # Remove links
    supp_link_ids = find_links_to_be_operated(link_list, crit)

    oprec_list = []
    new_nodes = copy.deepcopy(nodes)
    updated_link_list = link_list
    for i in range(len(supp_link_ids)):
        link_id_to_remove = supp_link_ids[i]
        updated_link_list = paradis.remove_link(new_nodes, bounds, updated_link_list, link_id_to_remove, oprec_list, home)

    # Write link list after suppressing junction
    write_data_file(param, new_nodes, taskname, suffix="_after_suppression.data")

    paradis.ParadisFinish(home)

    # Compare two nodes dictionaries
    fname1 = taskname + '_after_suppression.data'
    #fname1 = '../' + taskname + '.data' # to test FAILED case
    fname2 = 'restart/restart.data'
    PASSED = compare_data_files(fname1,fname2)

    # Compare files against reference
    fname = 'remove_link() vs ExecuteOpRec()'
    if PASSED:
        print("test (" + fname + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname + ")" + bcolors.RED + " FAILED" + bcolors.RESET)


if __name__ == "__main__":
     main()
