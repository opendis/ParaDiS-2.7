
from ctypes import *
import numpy as np
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Input variables
    crit = 200 # Critical length for junction removal
    supp_step = 10 # period for junction suppression

    cycleStart = param.contents.cycleStart
    #cycleEnd  = cycleStart + param.contents.maxstep
    cycleEnd = cycleStart + 1000
    bounds = find_bounds_from_param(param)

    t_begin = time.time()  

    while home.contents.cycle<cycleEnd:
        t0 = time.time()  
        paradis.ParadisStep(home)

        t1 = time.time()
        #print('step = %d/%d  time used = %g s'%(home.contents.cycle, cycleEnd, t1-t0))

        # Remove links
        if home.contents.cycle % supp_step == 0:
            nodes = convert_nodes_to_dict(home)
            num_nodes_before_supp = len(nodes)
            link_list = make_link_list(nodes, bounds)
            supp_link_ids = find_links_to_be_operated(link_list, crit)

            oprec_list = []
            updated_link_list = link_list
            for i in range(len(supp_link_ids)):
                link_id_to_remove = supp_link_ids[i]
                updated_link_list = paradis.remove_link(nodes, bounds, updated_link_list, link_id_to_remove, oprec_list, home)

            paradis.SortNativeNodes(home)
            print('====Total number of node changed from %d to %d'%(num_nodes_before_supp,len(nodes)))
            
            supp_restart_file_name = 'restart/rs%04d'%param.contents.savecncounter + '_supp'
            write_data_file(param, nodes, supp_restart_file_name)

        t_end = time.time()  
        #print('Total time used = %g s'%(t_end-t_begin))

    paradis.ParadisFinish(home)


if __name__ == "__main__":
     main()
