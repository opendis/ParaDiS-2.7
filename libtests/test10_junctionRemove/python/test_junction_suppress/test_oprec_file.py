
from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

from disl_link_util import *
paradis = ParaDiS_Topology(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    global nodes, link_list

    # Critical length for junction removal
    crit = 200

    # Make and write link data
    nodes = convert_nodes_to_dict(home)
    bounds = find_bounds_from_param(param)

    link_list = make_link_list(nodes, bounds)

    # Remove links
    new_nodes = copy.deepcopy(nodes)
    supp_link_ids = find_links_to_be_operated(link_list, crit)

    oprec_list = []
    updated_link_list = link_list
    for i in range(len(supp_link_ids)):
        link_id_to_remove = supp_link_ids[i]
        updated_link_list = paradis.remove_link(new_nodes, bounds, updated_link_list, link_id_to_remove, oprec_list)
    new_link_list = make_link_list(new_nodes, bounds)

    # Write data & oprec files after suppressing junction
    write_data_file(param, new_nodes, taskname, suffix="_after_suppression.data")
    write_oprec_file(oprec_list, taskname)

    oprecfile = taskname + ".oprec"
    new_nodes_by_oprec = oprec_player_on_dict(nodes, bounds, oprecfile, istart=None, iend=None, stress_strain=False, print_freq=1, fig=None, ax=None, plot_freq=100, plot_physical_structure=False, plot_links=True, trim=False)
    write_data_file(param, new_nodes_by_oprec, taskname, suffix="_after_suppression_by_oprec.data")

    paradis.paradis_apply_oprec_list(home, oprec_list)
    new_nodes_by_ExecuteOpRec = convert_nodes_to_dict(home)
    write_data_file(param, new_nodes_by_ExecuteOpRec, taskname, suffix="_after_suppression_by_ExecuteOpRec.data")

    # Compare files against reference
    fname = 'after_suppression.data'
    fname_by_oprec = 'after_suppression_by_oprec.data'
    if compare_data_files(taskname+'_'+fname, taskname+'_'+fname_by_oprec):
        print("test (" + fname_by_oprec + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname_by_oprec + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    fname_by_paradis_oprec = 'binary_junction_run_from_paradis_oprec.data'
    if compare_data_files(taskname+'_'+fname_by_oprec, '../ref_results/'+fname_by_paradis_oprec):
        print("test (" + fname_by_paradis_oprec + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname_by_paradis_oprec + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    fname_by_ExecuteOpRec = 'after_suppression_by_ExecuteOpRec.data'
    if compare_data_files(taskname+'_'+fname_by_ExecuteOpRec, '../ref_results/'+fname_by_paradis_oprec):
        print("test (" + fname_by_ExecuteOpRec + ")" + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("test (" + fname_by_ExecuteOpRec + ")" + bcolors.RED + " FAILED" + bcolors.RESET)

    paradis.ParadisFinish(home)

if __name__ == "__main__":
     main()

