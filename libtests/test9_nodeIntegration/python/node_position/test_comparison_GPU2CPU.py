from ctypes import *
import numpy as np
import time, sys, os
import glob
import math
import copy
from collections import defaultdict
from paradis_util import * 

class dpaths:
    CPUDATADIR = "./test_CPU_results"
    GPUDATADIR = "./test_GPU_results"
    RESTARTFILE  = "restart/rs0001.data"

class tols:
    TEST_P = 1e-4   

def get_key_by_value(dictionary, search_value):
    for key, value in dictionary.items():
        if value == search_value:
            return key
    return None  # Return None if the value is not found in the dictionary

def compare_GPU2CPU_coord_integration(data_dir_CPU, data_dir_GPU):
    print()
    print("***************************************************************************************************************************")
    print("test compares the nodal positions between CPU and GPU results after integration but before topological changes.")
    print("***************************************************************************************************************************")
    test_nodes_cpu, test_bounds_cpu = read_nodes_to_dict(os.path.join(data_dir_CPU,dpaths.RESTARTFILE))
    test_nodes_gpu, test_bounds_gpu = read_nodes_to_dict(os.path.join(data_dir_GPU,dpaths.RESTARTFILE))
    test_nodes_gpu_copy = copy.deepcopy(test_nodes_gpu)
    L = test_bounds_cpu[1][0] - test_bounds_cpu[0][0]
    
    for key in test_nodes_cpu.keys():
        node_CPU_coords = test_nodes_cpu[key][0][0:3]
        node_GPU_coords = test_nodes_gpu_copy[key][0][0:3]
        node_GPU_coords_pbc = pbc_position_L(node_CPU_coords, node_GPU_coords, L)
        test_nodes_gpu_copy[key][0][0:3] = node_GPU_coords_pbc

        if not np.array_equal(test_nodes_gpu[key][0][0:3], test_nodes_gpu_copy[key][0][0:3]):
            print("Node ID: %d" %key[1])
            print("GPU node:")
            print(test_nodes_gpu[key][0][0:3])
            print("GPU node for pbc:")
            print(test_nodes_gpu_copy[key][0][0:3])
    
    coord_diff = {key: test_nodes_cpu[key][0] - test_nodes_gpu_copy[key][0] for key in test_nodes_cpu.keys()}
    coord_diff_norm = {key: math.sqrt(coord_diff[key][0]**2+coord_diff[key][1]**2+coord_diff[key][2]**2) for key in coord_diff.keys()}
    test_PASSED = True
    coord_diff_norm_list = []
    for key in coord_diff.keys():
        diff = math.sqrt(coord_diff[key][0]**2+coord_diff[key][1]**2+coord_diff[key][2]**2)
        coord_diff_norm_list.append(diff)
        if diff > tols.TEST_P:
            test_PASSED = False
    maximum_diff = max(coord_diff_norm_list)
    node_ID = get_key_by_value(coord_diff_norm, maximum_diff)

    print("maximum difference is %e for node %d between (%13.6e %13.6e %13.6e) from CPU calculation and (%13.6e %13.6e %13.6e) from GPU calculation" 
          %(maximum_diff, node_ID[1], test_nodes_cpu[node_ID][0][0], test_nodes_cpu[node_ID][0][1], test_nodes_cpu[node_ID][0][2], test_nodes_gpu[node_ID][0][0], test_nodes_gpu[node_ID][0][1], test_nodes_gpu[node_ID][0][2]))

    if test_PASSED == True:
        print("max(position_diff) = %e < %e," %(maximum_diff,tols.TEST_P) + bcolors.GRN + " PASSED" + bcolors.RESET)
    else:
        print("max(position_diff) = %e > %e," %(maximum_diff,tols.TEST_P) + bcolors.RED + " FAILED" + bcolors.RESET)
    return test_PASSED

'''
Main Program Starts Here
'''
def main():
    test_PASSED = compare_GPU2CPU_coord_integration(dpaths.CPUDATADIR, dpaths.GPUDATADIR)
    return 0 if test_PASSED else 1

if __name__ == "__main__":
    sys.exit( main() )
