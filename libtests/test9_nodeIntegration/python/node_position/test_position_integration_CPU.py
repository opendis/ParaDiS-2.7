from ctypes import *
import numpy as np
import time, sys, os
import glob
from collections import defaultdict 

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import * 

paradis = ParaDiS(home_lib)

class dpaths:
    FULLDATAFILE  = "./NodeForceVel_FULL_CPU.txt"

def run_paradis_output(home,data_dir):
    param   = home.contents.param
    cycleStart = param.contents.cycleStart
    cycleEnd   = param.contents.cycleStart + param.contents.maxstep
    forces_velocities = {}

    for home.contents.cycle in range(cycleStart, cycleEnd):
        paradis.ParadisStep(home)

    forces_velocities[gids.FULL] = paradis.extract_nodal_force_vel(home)

    np.savetxt(data_dir, forces_velocities[gids.FULL], delimiter=',')

    paradis.ParadisFinish(home)

    return

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    print("use_GPU = ", use_GPU)

    param.contents.timestepIntegrator = b'forceBsubcycle'
    param.contents.maxstep = 1
    param.contents.maxDT = 1.0e-11
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    t_begin = time.time()

    run_paradis_output(home,dpaths.FULLDATAFILE)

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0

if __name__ == "__main__":
    sys.exit( main() )
