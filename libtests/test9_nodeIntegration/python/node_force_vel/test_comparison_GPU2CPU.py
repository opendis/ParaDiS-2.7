from ctypes import *
import numpy as np
import time, sys, os
import glob
from collections import defaultdict
from paradis_util import * 

class dpaths:
    CPUDATADIR = "./test_CPU_results"
    GPUDATADIR = "./test_GPU_results"
    CPUFULLDATAFILE  = "NodeForceVel_FULL_CPU.txt"
    GPUFULLDATAFILE  = "NodeForceVel_FULL_GPU.txt"
    FORCEDATAFILE  = "force/force0001"
    VELDATAFILE  = "velocity/vel0001"

class tols:
    TESTA_F = 3e-4     # sqrt(1e-9)
    TESTA_V = 3e-3     # sqrt(1e-9)
    TESTB_F = 3e-4     # sqrt(1e-9)
    TESTB_V = 3e-3     # sqrt(1e-9)
    MIN_V_MAG = 1e-1 # values below this threshold are considered as zero
    MIN_F_MAG = 1e-5

def compare_GPU2CPU_force_vel_integration(data_dir_CPU, data_dir_GPU, do_testA_VEL=True, do_testB=True, do_testB_VEL=True):
    print()
    print("***************************************************************************************************************************")
    print("testA compares the FULL forces and velocities between CPU and GPU results after integration but before topological changes.")
    print("***************************************************************************************************************************")
    test_nodes_force_cpu = read_force_to_dict(os.path.join(data_dir_CPU,dpaths.FORCEDATAFILE))
    test_nodes_force_gpu = read_force_to_dict(os.path.join(data_dir_GPU,dpaths.FORCEDATAFILE))
    force_diff = {key: test_nodes_force_cpu[key] - test_nodes_force_gpu.get(key,0) for key in test_nodes_force_cpu.keys()}
    test_nodes_force_cpu_array = np.column_stack((list(test_nodes_force_cpu.keys()), list(test_nodes_force_cpu.values())))
    np.savetxt("./test_nodes_force_cpu.txt", test_nodes_force_cpu_array, delimiter=',')
    test_nodes_force_gpu_array = np.column_stack((list(test_nodes_force_gpu.keys()), list(test_nodes_force_gpu.values())))
    np.savetxt("./test_nodes_force_gpu.txt", test_nodes_force_gpu_array, delimiter=',')
    force_diff_array = np.column_stack((list(force_diff.keys()), list(force_diff.values())))
    np.savetxt("./force_diff.txt", force_diff_array, delimiter=',')
    force_diff_norm = np.linalg.norm(np.array(list(force_diff.values())),axis=1)
    force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
    force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(np.array(list(test_nodes_force_cpu.values())),axis=1), tols.MIN_F_MAG) )
    print("max(force_rel_error) = %e"%np.amax(force_rel_err))
    if np.amax(force_rel_err) < tols.TESTA_F:
        print("testA (force)" + bcolors.GRN + " PASSED" + bcolors.RESET)
        testA_FORCE_PASSED = True
    else:
        print("testA (force)" + bcolors.RED + " FAILED" + bcolors.RESET)
        testA_FORCE_PASSED = False

        max_err_id = np.argmax(force_rel_err)
        node_id = force_diff_array[max_err_id,1]
        print("Forces_full_gpu before topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,test_nodes_force_gpu_array[max_err_id][2],test_nodes_force_gpu_array[max_err_id][3],test_nodes_force_gpu_array[max_err_id][4]))
        print("Forces_full_cpu before topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,test_nodes_force_cpu_array[max_err_id][2],test_nodes_force_cpu_array[max_err_id][3],test_nodes_force_cpu_array[max_err_id][4]))
        print("Forces_diff_max before topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,force_diff_array[max_err_id][2],force_diff_array[max_err_id][3],force_diff_array[max_err_id][4]))

    testAll_PASSED = testA_FORCE_PASSED

    if do_testA_VEL:
        test_nodes_vel_cpu = read_vel_to_dict(os.path.join(data_dir_CPU,dpaths.VELDATAFILE))
        test_nodes_vel_gpu = read_vel_to_dict(os.path.join(data_dir_GPU,dpaths.VELDATAFILE))
        vel_diff = {key: test_nodes_vel_cpu[key] - test_nodes_vel_gpu.get(key,0) for key in test_nodes_vel_cpu.keys()}
        test_nodes_vel_cpu_array = np.column_stack((list(test_nodes_vel_cpu.keys()), list(test_nodes_vel_cpu.values())))
        np.savetxt("./test_nodes_vel_cpu.txt", test_nodes_vel_cpu_array, delimiter=',')
        test_nodes_vel_gpu_array = np.column_stack((list(test_nodes_vel_gpu.keys()), list(test_nodes_vel_gpu.values())))
        np.savetxt("./test_nodes_vel_gpu.txt", test_nodes_vel_gpu_array, delimiter=',')
        vel_diff_array = np.column_stack((list(vel_diff.keys()), list(vel_diff.values())))
        np.savetxt("./vel_diff.txt", vel_diff_array, delimiter=',')
        vel_diff_norm = np.linalg.norm(np.array(list(vel_diff.values())),axis=1)
        vel_diff_norm[ vel_diff_norm < tols.MIN_V_MAG ] = 0
        vel_rel_err = np.divide( vel_diff_norm, np.maximum(np.linalg.norm(np.array(list(test_nodes_vel_cpu.values())),axis=1), tols.MIN_V_MAG) )
        print("max(vel_rel_error) = %e"%np.amax(vel_rel_err))
        if np.amax(vel_rel_err) < tols.TESTA_V:
            print("testA (velocity)" + bcolors.GRN + " PASSED" + bcolors.RESET)
            testA_VEL_PASSED = True
        else:
            print("testA (velocity)" + bcolors.RED + " FAILED" + bcolors.RESET)
            testA_VEL_PASSED = False
            
            max_err_id = np.argmax(vel_rel_err)
            node_id = vel_diff_array[max_err_id,1]
            print("Velocities_full_gpu before topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,test_nodes_vel_gpu_array[max_err_id][2],test_nodes_vel_gpu_array[max_err_id][3],test_nodes_vel_gpu_array[max_err_id][4]))
            print("Velocities_full_cpu before topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,test_nodes_vel_cpu_array[max_err_id][2],test_nodes_vel_cpu_array[max_err_id][3],test_nodes_vel_cpu_array[max_err_id][4]))
            print("Velocities_diff_max before topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_diff_array[max_err_id][2],vel_diff_array[max_err_id][3],vel_diff_array[max_err_id][4]))   
            
        testAll_PASSED = testAll_PASSED and testA_VEL_PASSED

    if do_testB:
        print()
        print("********************************************************************************************************************")
        print("testB compares the FULL forces and velocities between CPU and GPU results after integration and topological changes.")
        print("********************************************************************************************************************")
        forces_velocities_CPU = np.loadtxt(os.path.join(data_dir_CPU,dpaths.CPUFULLDATAFILE), delimiter=',')
        forces_velocities_GPU = np.loadtxt(os.path.join(data_dir_GPU,dpaths.GPUFULLDATAFILE), delimiter=',')
        force_full_CPU = forces_velocities_CPU[:,1:4]
        force_full_GPU = forces_velocities_GPU[:,1:4]
        print("The node number after topological changes in the CPU test: %d"%len(forces_velocities_CPU))
        print("The node number after topological changes in the GPU test: %d"%len(forces_velocities_GPU))
        if len(forces_velocities_CPU) != len(forces_velocities_GPU):
            print("The node number after topological changes in the GPU test is not equal to the CPU test.")
            print("testB" + bcolors.RED + " FAILED" + bcolors.RESET)
            testB_PASSED = False
            testAll_PASSED = testAll_PASSED and testB_PASSED
        else:
            force_diff = force_full_GPU - force_full_CPU
            force_diff_norm = np.linalg.norm(force_diff,axis=1)
            force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
            force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(force_full_CPU,axis=1), tols.MIN_F_MAG) )
            print("max(force_rel_error) = %e"%np.amax(force_rel_err))
            if np.amax(force_rel_err) < tols.TESTB_F:
                print("testB (force)" + bcolors.GRN + " PASSED" + bcolors.RESET)
                testB_FORCE_PASSED = True
            else:
                print("testB (force)" + bcolors.RED + " FAILED" + bcolors.RESET)
                testB_FORCE_PASSED = False
                
                max_err_id = np.argmax(force_rel_err)
                node_id = forces_velocities_GPU[max_err_id,0]
                print("Forces_full_gpu after topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,force_full_GPU[max_err_id][0],force_full_GPU[max_err_id][1],force_full_GPU[max_err_id][2]))
                print("Forces_full_cpu after topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,force_full_CPU[max_err_id][0],force_full_CPU[max_err_id][1],force_full_CPU[max_err_id][2]))
                print("Forces_diff_max after topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,force_diff[max_err_id][0],force_diff[max_err_id][1],force_diff[max_err_id][2]))
                
            testAll_PASSED = testAll_PASSED and testB_FORCE_PASSED

            if do_testB_VEL:
                vel_full_CPU = forces_velocities_CPU[:,4:7]
                vel_full_GPU = forces_velocities_GPU[:,4:7]
                vel_diff = vel_full_GPU - vel_full_CPU
                vel_diff_norm = np.linalg.norm(vel_diff,axis=1)
                vel_diff_norm[ vel_diff_norm < tols.MIN_V_MAG ] = 0
                vel_rel_err = np.divide( vel_diff_norm, np.maximum(np.linalg.norm(vel_full_CPU,axis=1), tols.MIN_V_MAG) )
                print("max(vel_rel_error) = %e"%np.amax(vel_rel_err))
                if np.amax(vel_rel_err) < tols.TESTB_V:
                    print("testB (velocity)" + bcolors.GRN + " PASSED" + bcolors.RESET)
                    testB_VEL_PASSED = True
                else:
                    print("testB (velocity)" + bcolors.RED + " FAILED" + bcolors.RESET)
                    testB_VEL_PASSED = False
                    max_err_id = np.argmax(vel_rel_err)
                    node_id = forces_velocities_GPU[max_err_id,0]
                    print("Velocities_full_gpu after topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_full_GPU[max_err_id][0],vel_full_GPU[max_err_id][1],vel_full_GPU[max_err_id][2]))
                    print("Velocities_full_cpu after topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_full_CPU[max_err_id][0],vel_full_CPU[max_err_id][1],vel_full_CPU[max_err_id][2]))
                    print("Velocities_diff_max after topological changes (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_diff[max_err_id][0],vel_diff[max_err_id][1],vel_diff[max_err_id][2]))

                testAll_PASSED = testAll_PASSED and testB_VEL_PASSED

    return testAll_PASSED

'''
Main Program Starts Here
'''
def main():
    testAll_PASSED = compare_GPU2CPU_force_vel_integration(dpaths.CPUDATADIR, dpaths.GPUDATADIR, do_testA_VEL=True, do_testB=False, do_testB_VEL=True)
    return 0 if testAll_PASSED else 1

if __name__ == "__main__":
    sys.exit( main() )
