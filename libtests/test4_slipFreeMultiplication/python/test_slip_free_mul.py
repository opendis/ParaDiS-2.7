from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

class settings:
    A2FORMATIONSTEP = 50  # number of steps for A2 formation (on MC2)

def paradis_run(home):
    param   = home.contents.param
    cycleStart = param.contents.cycleStart
    cycleEnd   = cycleStart + settings.A2FORMATIONSTEP
    for tstep in range(cycleStart, cycleEnd):
        paradis.ParadisStep(home)

def detect_collinear_arm(home):
    burg_A2 = np.array([ 0, -1,  1], dtype=np.double)
    nPln_A2 = np.array([-1,  1,  1], dtype=np.double)
    collinearFormed = False
    for key in range(home.contents.newNodeKeyPtr):
        node = home.contents.nodeKeys[key]
        if node: # node is not NULL
            numNbrs = node.contents.numNbrs
            for i in range(numNbrs):
                nbr_tag = tuple([node.contents.nbrTag[i].domainID, node.contents.nbrTag[i].index])
                burg_arm = np.array([node.contents.burgX[i], node.contents.burgY[i], node.contents.burgZ[i] ])
                nPln_arm = np.array([node.contents.nx[i],    node.contents.ny[i],    node.contents.nz[i]    ])
                if (isCollinear(burg_arm[0], burg_arm[1], burg_arm[2], burg_A2[0], burg_A2[1], burg_A2[2] ) and
                    isCollinear(nPln_arm[0], nPln_arm[1], nPln_arm[2], nPln_A2[0], nPln_A2[1], nPln_A2[2] ) ):
                    collinearFormed = True
                    break

    if collinearFormed:
        print("collinear dislocation formation" + bcolors.GRN + " SUCCEEDED" + bcolors.RESET)
    else:
        print("collinear dislocation formation" + bcolors.RED + " FAILED" + bcolors.RESET)

    return collinearFormed


'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.contents.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    t_begin = time.time() 

    paradis_run(home)
    collinearFormed = detect_collinear_arm(home)
    paradis.ParadisFinish(home)
    os.chdir('..')

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0 if collinearFormed else 1

if __name__ == "__main__":
     sys.exit( main() )
