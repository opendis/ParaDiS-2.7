/***************************************************************************
 *
 *  Function    : Main
 *  Description : main routine for ParaDiS simulation
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"
#include "Init.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif



void SegNodeFind(Home_t *home) {
	Node_t *NODE;
	Node_t *nbr;

	int j,m,NextNbr,k;
	int StartNode=0;
	int EndNode=1;
	int NeighborNode=0;
	int Nodes[1000]={0};
	int Length ;

	static int NODE_Count_prev=0;
	Param_t *param;
	param = home->param;
	char     fileName[256];
	FILE     *fp;

	int Count=0;
	Nodes[Count]=StartNode;

	// In the .data file provided nodes with IDs of 0 and 1, are two ends of FR source.
	// Counting number of discretization nodes, going from one end node to the other
	while (NeighborNode!= EndNode) {
		NODE = home->nodeKeys[NeighborNode];


		if (NODE->numNbrs>2) {
			printf("Node with 4 arms, time=%e \n",param->timeNow);
			Count=NODE_Count_prev;
			break;
		}

		 for (j = 0; j < NODE->numNbrs; j++) {
			nbr = GetNeighborNode(home, NODE, j);
			  
			NextNbr=0;
			for (m=0; m<1000; m++){
				if (nbr->myTag.index==Nodes[m]){
					NextNbr=1;	 
					break;	
				} else if (m>0 && Nodes[m]==0){
					break;
				}
			}


			 if (NextNbr==1) continue;
			 
			 NeighborNode=nbr->myTag.index;
			 Count+=1;
			 Nodes[Count]=NeighborNode;

			 break;

		}
	}

	printf("Node count is =%i  \n",Count);

	//Criteria for loop nucleation: if number of discretization nodes is 10 nodes less than previous time step's node count
	if (Count<NODE_Count_prev-10){
	   
		printf("Loop is formed, time=%e \n",param->timeNow);
		snprintf(fileName, sizeof(fileName), "NucleationTime.txt");
		fp = fopen(fileName, "a");
		printf("%s \n",fileName);
		printf("%e %e  \n", param->appliedStress[5], param->timeNow);
		
		fprintf(fp, "%e %e  \n", param->appliedStress[5], param->timeNow);
		fclose(fp);
		exit(0);
	}
	NODE_Count_prev=Count;
}




int main (int argc, char *argv[])
{
        int     cycleEnd, memSize, initialDLBCycles;
        time_t  tp;
        Home_t  *home;
        Param_t *param;

/*
 *      On some systems, the getrusage() call made by Meminfo() to get
 *      the memory resident set size does not work properly.  In those
 *      cases, the function will try to return the current heap size 
 *      instead.  This initial call allows meminfo() to get a copy of
 *      the original heap pointer so subsequent calls can calculate the
 *      heap size by taking the diference of the original and current
 *      heap pointers.
 */
        Meminfo(&memSize);

/*
 *      on linux systems (e.g. MCR) if built to have floating point exceptions
 *      turned on, invoke macro to do so
 */
   
#ifdef FPES_ON
        unmask_std_fpes();
#endif


        ParadisInit(argc, argv, &home);
        home->cycle      = home->param->cycleStart;

#ifdef _GPU_SUBCYCLE
        InitializeParadisGPU(home);
#endif

        param            = home->param;
        cycleEnd         = param->cycleStart + param->maxstep;

#ifdef _GPU_SUBCYCLE
        sprintf(param->subInteg0Integ1, "GPU");
#else
        sprintf(param->subInteg0Integ1, "RKF-RKF");
#endif

        home->cycle = home->param->cycleStart;

        while (home->cycle < cycleEnd) {
            ParadisStep(home);
            SegNodeFind(home);
            TimerClearAll(home);
        }

        ParadisFinish(home);

        exit(0);
}
