clear;clc; close all;

%Following value has to be consistent with .ctrl file
B = 15.6e-12;      
burgMag=255e-6;
ShearModulus = 54.6e3 ;

FR_Length = 4.5; %in units of um, has to be consistent with values in .data file
SourceTau_c = 5.9; % in units of MPa;
X = [1.05, 0.1, 10];
Stress_ref = (X(1)*SourceTau_c):(X(2)*SourceTau_c):(X(3)*SourceTau_c)
 
%Calculate the loop nucleation time for the given material properties and
%FR source length:
for i=1:length(Stress_ref)
    [tNuc_ref(i), tAct_ref(i)] = tNucDeter (Stress_ref(i),FR_Length,B, ...
                    burgMag,ShearModulus,SourceTau_c);
end
figure(1)
plot( X(1):X(2):X(3) , tNuc_ref, 'LineWidth',2);
hold on

figure(2)
plot( X(1):X(2):X(3) , tNuc_ref./tAct_ref, 'LineWidth',2);
hold on

% Read ParaDis calculated values, which are stored in the following .txt file
fid=fopen('NucleationTime.txt','r');
Data=textscan(fid,'%f%f'); 
fclose(fid);
Stress_ParaDiS = Data{1}/1e6;
tNuc_ParaDiS = Data{2};
nPoints = size(Stress_ParaDiS,1);

tAct_ParaDiS = zeros( size(Stress_ParaDiS) );
for i=1:length(Stress_ParaDiS)
    [Null, tAct_ParaDiS(i)] = tNucDeter (Stress_ParaDiS(i),FR_Length,B, ...
                    burgMag,ShearModulus,SourceTau_c);
end

figure(1)
plot(Stress_ParaDiS/SourceTau_c , tNuc_ParaDiS, 'x', 'MarkerSize', 12, 'LineWidth',1.5)
xlim([1, X(3)]);
xlabel('$\zeta = \tau / \tau_{act}$','interpreter','latex')
ylabel('$t_{nuc}$','interpreter','latex')
legend('ref vals (Tpz)','subcycling scheme')
set(gca, 'FontSize',15)
title({ 'Loop nucleation time of FR source ';strcat('$ L_{FR} = ',num2str(FR_Length),' \mu m$')},'interpreter','latex') 

figure(2)
plot(Stress_ParaDiS/SourceTau_c , tNuc_ParaDiS./tAct_ParaDiS, 'x', 'MarkerSize', 12, 'LineWidth',1.5)
xlim([1, X(3)]);
xlabel('$\zeta = \tau / \tau_{act}$','interpreter','latex')
ylabel('$ t_{nuc} / t_{act} $','interpreter','latex')
legend('ref vals (Tpz)','subcycling scheme')
set(gca, 'FontSize',15)
title({ 'Loop nucleation time of FR source ';strcat('$ L_{FR} = ',num2str(FR_Length),' \mu m$')},'interpreter','latex') 

