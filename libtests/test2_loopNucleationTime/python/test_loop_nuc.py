from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import * 

paradis = ParaDiS(home_lib)

class fnames:
    OUTPUT = "NucleationTime.txt"

def Find_FR_Link_Length(home, NODE_Count_prev): # find the number of nodes along FR link
    StartNode, EndNode = 0, 1 # two end nodes of the FR source

    NextNode, NodeList, Count = StartNode, [StartNode], 1
    while NextNode != EndNode:
        NODE = home.contents.nodeKeys[NextNode]

        if NODE.contents.numNbrs > 2:
            print("encounter node with %d arms, time = %e"%(NODE.contents.numNbrs,home.contents.param.contents.timeNow))
            Count = NODE_Count_prev
            break

        for j in range(0, NODE.contents.numNbrs):
            nbr = paradis.GetNeighborNode(home, NODE, j)
            if nbr.contents.myTag.index in NodeList: # nbr is already contained in NodeList
                continue
            else:
                NextNode = nbr.contents.myTag.index
                NodeList.append(NextNode)
                Count = len(NodeList)
                break

    print("Node count = %d"%Count)

    return Count

def detect_loop_formation(home, Count, NODE_Count_prev):
    if (Count < NODE_Count_prev - 10):
        loopFormed = True
        loop_form_time = home.contents.param.contents.timeNow
    else:
        loopFormed = False
        loop_form_time = None

    return loopFormed, loop_form_time

def cal_loop_nuc_time(home, sigma12):
    param   = home.contents.param
    cycleStart = param.contents.cycleStart
    cycleEnd   = param.contents.cycleStart + param.contents.maxstep

    print("set appliedStress[5] = %e"%sigma12)
    param.contents.appliedStress[5] = sigma12

    NODE_Count_prev = 0
    for home.contents.cycle in range(cycleStart, cycleEnd):
        paradis.ParadisStep(home)
        Count = Find_FR_Link_Length(home, NODE_Count_prev)
        loopFormed, loop_form_time = detect_loop_formation(home, Count, NODE_Count_prev)
        NODE_Count_prev = Count
        if loopFormed:
            print("Loop is formed, time = %e"%(loop_form_time))
            with open(fnames.OUTPUT, 'a') as f:
                print("%e %e"%(param.contents.appliedStress[5], loop_form_time),file=f)
            break
        paradis.TimerClearAll(home)

    paradis.ParadisFinish(home)

    if loopFormed:
        print("dislocation loop nucleation" + bcolors.GRN + " SUCCEEDED" + bcolors.RESET)
    else:
        print("dislocation loop nucleation" + bcolors.RED + " FAILED" + bcolors.RESET)

    return loopFormed, loop_form_time


'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]

    t_begin = time.time() 

    # compute loop nucleation time from FR source under different applied stress
    ref_loop_form_time = [1.72e-07, 1.17e-07, 8.70e-8]
    test_passed = [False, False, False]
    for idx, sigma12 in enumerate([10e6, 12e6, 14e6]):
        home = paradis.paradis_init(taskname)
        param = home.contents.param

        if use_GPU and param.contents.useLabFrame:
            print("test" + bcolors.RED + " FAILED" + bcolors.RESET + " : useLabFrame not implemented for GPU")
            os.chdir('..')
            continue

        param.contents.timestepIntegrator = b'forceBsubcycle'
        if use_GPU:
            param.contents.subInteg0Integ1 = b'GPU'
        else:
            param.contents.subInteg0Integ1 = b'RKF-RKF'

        if idx > 0:
            if use_GPU: paradis.InitializeParadisGPU(home)

        loopFormed, loop_form_time = cal_loop_nuc_time(home, sigma12)
        test_passed[idx] = loopFormed and np.abs(loop_form_time - ref_loop_form_time[idx]) < 1.1e-9
        print('loopFormed = ', loopFormed)
        print('loop_form_time = ', loop_form_time)

        os.chdir('..')

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0 if all(test_passed) else 1

if __name__ == "__main__":
     sys.exit( main() )
