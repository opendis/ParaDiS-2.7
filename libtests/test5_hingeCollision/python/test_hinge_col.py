from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

def collision_criterion(mindist, r1t, r1tau, r2t, r2tau, r3t, r3tau, r4t, r4tau):
    c_dist2, c_L1_ratio, c_L2_ratio = c_double(0.0), c_double(0.0), c_double(0.0)
    LP_c_double = POINTER(c_double)
    collisionDetected = paradis.CollisionCriterion(c_dist2, c_L1_ratio, c_L2_ratio, mindist, 
                                           cast(r1t.ctypes, LP_c_double), cast(r1tau.ctypes, LP_c_double), 
                                           cast(r2t.ctypes, LP_c_double), cast(r2tau.ctypes, LP_c_double),
                                           cast(r3t.ctypes, LP_c_double), cast(r3tau.ctypes, LP_c_double), 
                                           cast(r4t.ctypes, LP_c_double), cast(r4tau.ctypes, LP_c_double))
    return collisionDetected, c_dist2.value, c_L1_ratio.value, c_L2_ratio.value

def hinge_collision_criterion(vec1, vec2):
    c_L1_ratio = c_double(0.0)
    LP_c_double = POINTER(c_double)
    collisionDetected = paradis.HingeCollisionCriterion(c_L1_ratio, cast(vec1.ctypes, LP_c_double),
                                                            cast(vec2.ctypes, LP_c_double))
    return collisionDetected, c_L1_ratio.value

def detect_collisions(home):
    param   = home.contents.param
    mindist = param.contents.rann

    node1 = home.contents.nodeKeys[0]
    node3 = paradis.GetNodeFromTag(home, node1.contents.nbrTag[1])
    node4 = paradis.GetNodeFromTag(home, node1.contents.nbrTag[2])

    print("node1: (%d, %d)"%(node1.contents.myTag.domainID, node1.contents.myTag.index))
    print("node3: (%d, %d)"%(node3.contents.myTag.domainID, node3.contents.myTag.index))
    print("node4: (%d, %d)"%(node4.contents.myTag.domainID, node4.contents.myTag.index))

    r1 = np.array([node1.contents.x, node1.contents.y, node1.contents.z])
    r3 = np.array([node3.contents.x, node3.contents.y, node3.contents.z])
    r4 = np.array([node4.contents.x, node4.contents.y, node4.contents.z])

    print("r1 = "+str(r1))
    print("r3 = "+str(r3))
    print("r4 = "+str(r4))

    r3 = pbc_position_param(param, r1, r3, paradis.PBCPOSITION)
    r4 = pbc_position_param(param, r3, r4, paradis.PBCPOSITION)

    print("after PBCPOSITION")
    print("r3 = "+str(r3))
    print("r4 = "+str(r4))

    collisionDetected, dist2, L1ratio, L2ratio = collision_criterion(mindist, r1, r1, r3, r3, r4, r4, r4, r4)

    if not collisionDetected:
        print("test (collision detection)      " + bcolors.GRN + " PASSED" + bcolors.RESET + " (does not detect)")
    else:
        print("test (collision detection)      " + bcolors.RED + " FAILED" + bcolors.RESET + " (detects when shouldn't)")

    hingeCollisionDetected, L0ratio = hinge_collision_criterion(r3-r1, r4-r1)

    if hingeCollisionDetected:
        print("test (hinge collision detection)" + bcolors.GRN + " PASSED" + bcolors.RESET + " (detects collision)")
    else:
        print("test (hinge collision detection)" + bcolors.RED + " FAILED" + bcolors.RESET + " (does not detect)")

    success = (not collisionDetected) and hingeCollisionDetected

    return success


'''
Main Program Starts Here
'''
def main():
    taskname = sys.argv[1]

    t_begin = time.time() 
    home = paradis.paradis_init(taskname)
    success = detect_collisions(home)
    paradis.ParadisFinish(home)
    os.chdir('..')

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0 if success else 1

if __name__ == "__main__":
     sys.exit( main() )
