/***************************************************************************
 *
 *  Function    : Main
 *  Description : main routine for ParaDiS simulation
 *
 **************************************************************************/
#include <stdio.h>
#include <time.h>
#include "Home.h"
#include "Init.h"

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef _HALFSPACE
#include "HS.h"
#endif

#ifdef _GPU_SUBCYCLE
#include "SubcycleGPU.h"
#endif

#ifdef FPES_ON
#include <fpcontrol.h>
#endif 



int main (int argc, char *argv[])
{
        int     cycleEnd, memSize, initialDLBCycles;
        time_t  tp;
        Home_t  *home;
        Param_t *param;
        Node_t *node;
#ifdef _HALFSPACE
	HalfSpace_t *halfspace;
#endif

/*
 *      On some systems, the getrusage() call made by Meminfo() to get
 *      the memory resident set size does not work properly.  In those
 *      cases, the function will try to return the current heap size 
 *      instead.  This initial call allows meminfo() to get a copy of
 *      the original heap pointer so subsequent calls can calculate the
 *      heap size by taking the diference of the original and current
 *      heap pointers.
 */
        Meminfo(&memSize);

/*
 *      on linux systems (e.g. MCR) if built to have floating point exceptions
 *      turned on, invoke macro to do so
 */
   
#ifdef FPES_ON
        unmask_std_fpes();
#endif

        ParadisInit(argc, argv, &home);
        home->cycle      = home->param->cycleStart;

#ifdef _HALFSPACE    
        HS_Init(home,&halfspace);
#ifdef _GPU_SUBCYCLE
        InitializeParadisGPU(home, halfspace);
#endif
#endif
   
#ifndef _HALFSPACE    
#ifdef _GPU_SUBCYCLE
        InitializeParadisGPU(home);
#endif
#endif

        param            = home->param;
        cycleEnd         = param->cycleStart + param->maxstep;

#ifdef _GPU_SUBCYCLE
        sprintf(param->subInteg0Integ1, "GPU");
#else
        sprintf(param->subInteg0Integ1, "RKF-RKF");
#endif

        home->cycle = home->param->cycleStart;

        while (home->cycle < cycleEnd) {
	    SortNativeNodes(home);
	    CellCharge(home);
	    HS_Step(home,halfspace);
	    PrintSurfaceDisplacement(home,halfspace);
	    PrintSurfaceSegments(home,halfspace);

	    BuildSurfaceSegList(home, halfspace);
	    ParadisStep(home,halfspace);

            //ParadisStep(home);
            TimerClearAll(home);

#ifdef _HALFSPACE
            if (node->constraint == HALFSPACE_SURFACE_NODE)
            {
            real8 vmag;
                vmag = sqrt(node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ);
                if (fabs(node->vZ) > (vmag*1e-4))
                if (fabs(node->vZ) > 0.0)
                {
                    printf("surface node vX = %e vY = %e vZ = %e vmag = %e\n", node->vX,node->vY,node->vZ,vmag);
                    Fatal("MobilityLaw_FCC_0: surface node vZ non-zero!");
                }
            }
#endif
        }

        ParadisFinish(home, halfspace);
        //ParadisFinish(home);

        HS_Finish(halfspace);
	FreeCellCters();

        exit(0);
}
