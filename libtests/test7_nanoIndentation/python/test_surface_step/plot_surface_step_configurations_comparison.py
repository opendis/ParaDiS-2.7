from ctypes import *
import numpy as np
import time, sys, os
import glob
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import matplotlib.pyplot as plt
from paradis_util import *

class bcolors:
    RED = '\033[31m'
    GRN = '\033[32m'
    YEL = '\033[33m'
    BLU = '\033[34m'
    MAG = '\033[35m'
    CYN = '\033[36m'
    BOLD = '\033[1m'
    RESET = '\033[0m'
    UNDERLINE = '\033[4m'

class tols:
    MIN_V_MAG = 1e-1 # values below this threshold are considered as zero
    MIN_F_MAG = 1e-5

'''
Main Program Starts Here
'''
def main():
    # plot the restart files in a folder
    fpath1 = 'test_CPU_results'
    fpath2 = 'test_GPU_results'
    fpath3 = 'test_old_CPU_results'

    data1=[]
    data2=[]
    data3=[]

    vel1=[]
    vel2=[]
    vel3=[]

    force1=[]
    force2=[]
    force3=[]

    for CPU_datafile in sorted(glob.glob(os.path.join(fpath1,'restart/*rs*.data'))):
        nodes, bounds = read_nodes_to_dict(CPU_datafile)
        data1.append([nodes, bounds])

    for GPU_datafile in sorted(glob.glob(os.path.join(fpath2,'restart/*rs*.data'))):
        nodes, bounds = read_nodes_to_dict(GPU_datafile)
        data2.append([nodes, bounds])

    for old_CPU_datafile in sorted(glob.glob(os.path.join(fpath3,'restart/*rs*.data'))):
        nodes, bounds = read_nodes_to_dict(old_CPU_datafile)
        data3.append([nodes, bounds])

    for CPU_velfile in sorted(glob.glob(os.path.join(fpath1,'velocity/vel*'))):
        nodes_vel = read_vel_to_dict(CPU_velfile)
        vel1.append(nodes_vel)

    for GPU_velfile in sorted(glob.glob(os.path.join(fpath2,'velocity/vel*'))):
        nodes_vel = read_vel_to_dict(GPU_velfile)
        vel2.append(nodes_vel)

    for old_CPU_velfile in sorted(glob.glob(os.path.join(fpath3,'velocity/vel*'))):
        nodes_vel = read_vel_to_dict(old_CPU_velfile)
        vel3.append(nodes_vel)

    for CPU_forcefile in sorted(glob.glob(os.path.join(fpath1,'force/force*'))):
        nodes_force = read_force_to_dict(CPU_forcefile)
        force1.append(nodes_force)

    for GPU_forcefile in sorted(glob.glob(os.path.join(fpath2,'force/force*'))):
        nodes_force = read_force_to_dict(GPU_forcefile)
        force2.append(nodes_force)

    for old_CPU_forcefile in sorted(glob.glob(os.path.join(fpath3,'force/force*'))):
        nodes_force = read_force_to_dict(old_CPU_forcefile)
        force3.append(nodes_force)

    Nfile=len(data1)
    
    fig = plt.figure(figsize=(8,8))
    ax = plt.axes(projection='3d')
    plot_links = True

    for i in range(24):
        tmp_nodes1 = data1[i][0]
        tmp_bounds1 = data1[i][1]
        tmp_nodes2 = data2[i][0]
        tmp_bounds2 = data2[i][1]

        tmp_nodes_vel1 = vel1[i]
        tmp_nodes_vel2 = vel2[i]

        tmp_nodes_force1 = force1[i]
        tmp_nodes_force2 = force2[i]

        L = tmp_bounds1[1][0] - tmp_bounds1[0][0]

        rn1 = np.array([tmp_nodes1[j][0].tolist() for j in list(tmp_nodes1.keys())])        
        rn2 = np.array([tmp_nodes2[j][0].tolist() for j in list(tmp_nodes2.keys())])        
        p_link1 = np.empty((0,6))
        p_link2 = np.empty((0,6))        

        res_vel = {key: tmp_nodes_vel1[key] - tmp_nodes_vel2.get(key,0) for key in tmp_nodes_vel1.keys()}
        for key in res_vel.keys():
             if res_vel[key][0]<1e-1 and res_vel[key][1]<1e-1 and res_vel[key][2]<1e-1:
                 print('The comparison between the CPU and GPU versions about Node'+str(key)+' in the file vel'+str(i+1).zfill(4)+':'+bcolors.GRN+'PASSED'+bcolors.RESET)
             else:
                 print('The comparison between the CPU and GPU versions about Node'+str(key)+' in the file vel'+str(i+1).zfill(4)+':'+bcolors.RED+'FAILED'+bcolors.RESET)

        res_force = {key: tmp_nodes_force1[key] - tmp_nodes_force2.get(key,0) for key in tmp_nodes_force1.keys()}
        for key in res_force.keys():
             if res_force[key][0]<1e-5 and res_force[key][1]<1e-5 and res_force[key][2]<1e-5:
                 print('The comparison between the CPU and GPU versions about Node'+str(key)+' in the file force'+str(i+1).zfill(4)+':'+bcolors.GRN+'PASSED'+bcolors.RESET)
             else:
                 print('The comparison between the CPU and GPU versions about Node'+str(key)+' in the file force'+str(i+1).zfill(4)+':'+bcolors.RED+'FAILED'+bcolors.RESET)

        plt.cla()
        if plot_links:
            for my_tag in list(tmp_nodes1.keys()):
                my_coords = tmp_nodes1[my_tag][0][0:3]
                arms_dict = tmp_nodes1[my_tag][1]
                for nbr_tag in list(arms_dict.keys()):
                    if my_tag < nbr_tag:
                        r_link = np.zeros((2,3))
                        if not nbr_tag in tmp_nodes1.keys():
                            print("cannot find neighbor")
                            print([my_tag, nbr_tag])
                            continue
                        nbr_coords = tmp_nodes1[nbr_tag][0][0:3]
                        r_link[0,:] = my_coords
                        r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                        if (not False) or np.max(np.absolute(r_link)) <= L/2:
                            p_link1 = np.append(p_link1, [r_link[0,:], r_link[1,:]])

            for my_tag in list(tmp_nodes2.keys()):
                my_coords = tmp_nodes2[my_tag][0][0:3]
                arms_dict = tmp_nodes2[my_tag][1]
                for nbr_tag in list(arms_dict.keys()):
                    if my_tag < nbr_tag:
                        r_link = np.zeros((2,3))
                        if not nbr_tag in tmp_nodes2.keys():
                            print("cannot find neighbor")
                            print([my_tag, nbr_tag])
                            continue
                        nbr_coords = tmp_nodes2[nbr_tag][0][0:3]
                        r_link[0,:] = my_coords
                        r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                        if (not False) or np.max(np.absolute(r_link)) <= L/2:
                            p_link2 = np.append(p_link2, [r_link[0,:], r_link[1,:]])

        ls1 = p_link1.reshape((-1,2,3))
        ls2 = p_link2.reshape((-1,2,3))

        lc1 = Line3DCollection(ls1, linewidths=0.5, colors='b')
        lc2 = Line3DCollection(ls2, linewidths=0.5, colors='r')

        ax.add_collection(lc1)
        ax.add_collection(lc2)

        ax.scatter(rn1[:,0], rn1[:,1], rn1[:,2], c='b', s=10, marker='o', label='CPU')
        ax.scatter(rn2[:,0], rn2[:,1], rn2[:,2], c='r', s=10, marker='v', label='GPU')
        ax.legend()
        ax.set_xlim(tmp_bounds1[0][0]/3, tmp_bounds1[1][0]/3)
        ax.set_ylim(tmp_bounds1[0][1]/3, tmp_bounds1[1][1]/3)
        ax.set_zlim(tmp_bounds1[0][2]/3, 0)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_box_aspect([1,1,1])
        
        plt.draw()
        plt.show(block=False)
        plt.title('rs'+str(i+1).zfill(4)+'.data')
        plt.pause(0.5)

    for i in range(24):
        tmp_nodes1 = data1[i][0]
        tmp_bounds1 = data1[i][1]
        tmp_nodes3 = data3[i][0]
        tmp_bounds3 = data3[i][1]

        tmp_nodes_vel1 = vel1[i]
        tmp_nodes_vel3 = vel3[i]

        tmp_nodes_force1 = force1[i]
        tmp_nodes_force3 = force3[i]

        L = tmp_bounds1[1][0] - tmp_bounds1[0][0]

        rn1 = np.array([tmp_nodes1[j][0].tolist() for j in list(tmp_nodes1.keys())])        
        rn3 = np.array([tmp_nodes3[j][0].tolist() for j in list(tmp_nodes3.keys())])        
        p_link1 = np.empty((0,6))
        p_link3 = np.empty((0,6))

        res_vel = {key: tmp_nodes_vel1[key] - tmp_nodes_vel3.get(key,0) for key in tmp_nodes_vel1.keys()}
        for key in res_vel.keys():
             if res_vel[key][0]<1e-1 and res_vel[key][1]<1e-1 and res_vel[key][2]<1e-1:
                 print('The comparison between the current and old CPU versions about Node'+str(key)+' in the file vel'+str(i+1).zfill(4)+':'+bcolors.GRN+'PASSED'+bcolors.RESET)
             else:
                 print('The comparison between the current and old CPU versions about Node'+str(key)+' in the file vel'+str(i+1).zfill(4)+':'+bcolors.RED+'FAILED'+bcolors.RESET)

        res_force = {key: tmp_nodes_force1[key] - tmp_nodes_force3.get(key,0) for key in tmp_nodes_force1.keys()}
        for key in res_force.keys():
             if res_force[key][0]<1e-5 and res_force[key][1]<1e-5 and res_force[key][2]<1e-5:
                 print('The comparison between the current and old CPU versions about Node'+str(key)+' in the file force'+str(i+1).zfill(4)+':'+bcolors.GRN+'PASSED'+bcolors.RESET)
             else:
                 print('The comparison between the current and old CPU versions about Node'+str(key)+' in the file force'+str(i+1).zfill(4)+':'+bcolors.RED+'FAILED'+bcolors.RESET)

        plt.cla()
        if plot_links:
            for my_tag in list(tmp_nodes1.keys()):
                my_coords = tmp_nodes1[my_tag][0][0:3]
                arms_dict = tmp_nodes1[my_tag][1]
                for nbr_tag in list(arms_dict.keys()):
                    if my_tag < nbr_tag:
                        r_link = np.zeros((2,3))
                        if not nbr_tag in tmp_nodes1.keys():
                            print("cannot find neighbor")
                            print([my_tag, nbr_tag])
                            continue
                        nbr_coords = tmp_nodes1[nbr_tag][0][0:3]
                        r_link[0,:] = my_coords
                        r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                        if (not False) or np.max(np.absolute(r_link)) <= L/2:
                            p_link1 = np.append(p_link1, [r_link[0,:], r_link[1,:]])

            for my_tag in list(tmp_nodes3.keys()):
                my_coords = tmp_nodes3[my_tag][0][0:3]
                arms_dict = tmp_nodes3[my_tag][1]
                for nbr_tag in list(arms_dict.keys()):
                    if my_tag < nbr_tag:
                        r_link = np.zeros((2,3))
                        if not nbr_tag in tmp_nodes3.keys():
                            print("cannot find neighbor")
                            print([my_tag, nbr_tag])
                            continue
                        nbr_coords = tmp_nodes3[nbr_tag][0][0:3]
                        r_link[0,:] = my_coords
                        r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                        if (not False) or np.max(np.absolute(r_link)) <= L/2:
                            p_link3 = np.append(p_link3, [r_link[0,:], r_link[1,:]])

        ls1 = p_link1.reshape((-1,2,3))
        ls3 = p_link3.reshape((-1,2,3))

        lc1 = Line3DCollection(ls1, linewidths=0.5, colors='b')
        lc3 = Line3DCollection(ls3, linewidths=0.5, colors='g')

        ax.add_collection(lc1)
        ax.add_collection(lc3)

        ax.scatter(rn1[:,0], rn1[:,1], rn1[:,2], c='b', s=10, marker='o', label='CPU')
        ax.scatter(rn3[:,0], rn3[:,1], rn3[:,2], c='g', s=10, marker='^', label='old_CPU')
        ax.legend()
        ax.set_xlim(tmp_bounds1[0][0]/3, tmp_bounds1[1][0]/3)
        ax.set_ylim(tmp_bounds1[0][1]/3, tmp_bounds1[1][1]/3)
        ax.set_zlim(tmp_bounds1[0][2]/3, 0)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_box_aspect([1,1,1])
        
        plt.draw()
        plt.show(block=False)
        plt.title('rs'+str(i+1).zfill(4)+'.data')
        plt.pause(0.5)

    for i in range(24):
        tmp_nodes2 = data2[i][0]
        tmp_bounds2 = data2[i][1]
        tmp_nodes3 = data3[i][0]
        tmp_bounds3 = data3[i][1]

        tmp_nodes_vel2 = vel2[i]
        tmp_nodes_vel3 = vel3[i]

        tmp_nodes_force2 = force2[i]
        tmp_nodes_force3 = force3[i]

        L = tmp_bounds2[1][0] - tmp_bounds2[0][0]

        rn2 = np.array([tmp_nodes2[j][0].tolist() for j in list(tmp_nodes2.keys())])        
        rn3 = np.array([tmp_nodes3[j][0].tolist() for j in list(tmp_nodes3.keys())])        
        p_link2 = np.empty((0,6))
        p_link3 = np.empty((0,6))

        res_vel = {key: tmp_nodes_vel2[key] - tmp_nodes_vel3.get(key,0) for key in tmp_nodes_vel2.keys()}
        for key in res_vel.keys():
             if res_vel[key][0]<1e-1 and res_vel[key][1]<1e-1 and res_vel[key][2]<1e-1:
                 print('The comparison between the GPU and old CPU versions about Node'+str(key)+' in the file vel'+str(i+1).zfill(4)+':'+bcolors.GRN+'PASSED'+bcolors.RESET)
             else:
                 print('The comparison between the GPU and old CPU versions about Node'+str(key)+' in the file vel'+str(i+1).zfill(4)+':'+bcolors.RED+'FAILED'+bcolors.RESET)

        res_force = {key: tmp_nodes_force2[key] - tmp_nodes_force3.get(key,0) for key in tmp_nodes_force2.keys()}
        for key in res_force.keys():
             if res_force[key][0]<1e-5 and res_force[key][1]<1e-5 and res_force[key][2]<1e-5:
                 print('The comparison between the GPU and old CPU versions about Node'+str(key)+' in the file force'+str(i+1).zfill(4)+':'+bcolors.GRN+'PASSED'+bcolors.RESET)
             else:
                 print('The comparison between the GPU and old CPU versions about Node'+str(key)+' in the file force'+str(i+1).zfill(4)+':'+bcolors.RED+'FAILED'+bcolors.RESET)

        plt.cla()
        if plot_links:
            for my_tag in list(tmp_nodes2.keys()):
                my_coords = tmp_nodes2[my_tag][0][0:3]
                arms_dict = tmp_nodes2[my_tag][1]
                for nbr_tag in list(arms_dict.keys()):
                    if my_tag < nbr_tag:
                        r_link = np.zeros((2,3))
                        if not nbr_tag in tmp_nodes2.keys():
                            print("cannot find neighbor")
                            print([my_tag, nbr_tag])
                            continue
                        nbr_coords = tmp_nodes2[nbr_tag][0][0:3]
                        r_link[0,:] = my_coords
                        r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                        if (not False) or np.max(np.absolute(r_link)) <= L/2:
                            p_link2 = np.append(p_link2, [r_link[0,:], r_link[1,:]])

            for my_tag in list(tmp_nodes3.keys()):
                my_coords = tmp_nodes3[my_tag][0][0:3]
                arms_dict = tmp_nodes3[my_tag][1]
                for nbr_tag in list(arms_dict.keys()):
                    if my_tag < nbr_tag:
                        r_link = np.zeros((2,3))
                        if not nbr_tag in tmp_nodes3.keys():
                            print("cannot find neighbor")
                            print([my_tag, nbr_tag])
                            continue
                        nbr_coords = tmp_nodes3[nbr_tag][0][0:3]
                        r_link[0,:] = my_coords
                        r_link[1,:] = pbc_position_L(my_coords, nbr_coords, L)
                        if (not False) or np.max(np.absolute(r_link)) <= L/2:
                            p_link3 = np.append(p_link3, [r_link[0,:], r_link[1,:]])

        ls2 = p_link2.reshape((-1,2,3))
        ls3 = p_link3.reshape((-1,2,3))

        lc2 = Line3DCollection(ls2, linewidths=0.5, colors='r')
        lc3 = Line3DCollection(ls3, linewidths=0.5, colors='g')

        ax.add_collection(lc2)
        ax.add_collection(lc3)

        ax.scatter(rn2[:,0], rn2[:,1], rn2[:,2], c='r', s=10, marker='v', label='GPU')
        ax.scatter(rn3[:,0], rn3[:,1], rn3[:,2], c='g', s=10, marker='^', label='old_CPU')
        ax.legend()
        ax.set_xlim(tmp_bounds1[0][0]/3, tmp_bounds1[1][0]/3)
        ax.set_ylim(tmp_bounds1[0][1]/3, tmp_bounds1[1][1]/3)
        ax.set_zlim(tmp_bounds1[0][2]/3, 0)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_box_aspect([1,1,1])
        
        plt.draw()
        plt.show(block=False)
        plt.title('rs'+str(i+1).zfill(4)+'.data')
        plt.pause(0.5)

if __name__ == "__main__":
     main()
