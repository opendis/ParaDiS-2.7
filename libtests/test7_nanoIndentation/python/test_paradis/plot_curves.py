from ctypes import *
import numpy as np
import time, sys, os
import matplotlib.pyplot as plt

'''
Main Program Starts Here
'''
def main(argv):
    # can plot the indendata.txt file in a folder
    if len(sys.argv) > 1:
        fpath = sys.argv[1]
    else:
        print('Please input the directory for indendata.txt')
        return

    if not os.path.exists(fpath):
        print(fpath + ' does not exist')
        return

    data = np.loadtxt(os.path.join(fpath,'indendata.txt'), unpack = True)

    # Magnitude of Burgers vector (m)
    b = 2.860e-10
    
    fig = plt.figure(figsize=(8,4))

    # Force-displacement curve
    x = data[3,]*b*1e6
    y = data[5,]*b**2*1e3
    plt.subplot(1, 2, 1)
    plt.plot(x,y)
    plt.xlabel("Displacement (\u03bcm)")
    plt.ylabel("Force (\u03bcN)")

    # Dislocation density
    x = data[3,]*b*1e6
    y = data[7,]
    plt.subplot(1, 2, 2)
    plt.plot(x,y)
    plt.xlabel("Displacement (\u03bcm)")
    plt.ylabel("Density (m$^{-2}$)")

    plt.show()

if __name__ == "__main__":
     main(sys.argv[1:])
