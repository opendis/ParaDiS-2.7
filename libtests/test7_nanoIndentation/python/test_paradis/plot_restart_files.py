from ctypes import *
import numpy as np
import time, sys, os
import glob
from paradis_util import *

'''
Main Program Starts Here
'''
def main(argv):
    # can plot one restart (*rs*.data) file or all restart files in a folder
    if len(sys.argv) > 1:
        fpath = sys.argv[1]
    else:
        fpath = '.'

    if not os.path.exists(fpath):
        print(fpath + ' does not exist')
        return

    if os.path.isfile(fpath):
        datafile = fpath
        nodes, bounds = read_nodes_to_dict(datafile)
        plot_nodes_dict(nodes, bounds, block=False)
        plt.pause(0.5)
    else:
        fig = plt.figure(figsize=(8,8))
        for datafile in sorted(glob.glob(os.path.join(fpath,'*rs*.data'))):
            nodes, bounds = read_nodes_to_dict(datafile)
            plot_nodes_dict(nodes, bounds, fig=fig, block=False)
            plt.title(os.path.basename(datafile))
            plt.pause(0.5)

if __name__ == "__main__":
     main(sys.argv[1:])
