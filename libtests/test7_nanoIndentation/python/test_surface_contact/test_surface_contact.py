from ctypes import *
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('HS_Home_gpu' if use_GPU else 'HS_Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

'''
Main Program Starts Here
'''
def main():
    global home, param, halfspace

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    halfspace = paradis.hs_init(home)

    param.contents.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home, halfspace)

    t_begin = time.time()  

    maxstep = param.contents.maxstep
    for tstep in range(maxstep):
        t0 = time.time()  
        home.contents.cycle = tstep

        paradis.SortNativeNodes(home)
        paradis.CellCharge(home)
        paradis.HS_Step(home,halfspace)
        paradis.PrintSurfaceDisplacement(home,halfspace)
        paradis.PrintSurfaceSegments(home,halfspace)

        paradis.BuildSurfaceSegList(home, halfspace)
        paradis.ParadisStep(home,halfspace);
        t1 = time.time()
        print('step = %d/%d  time used = %g s'%(tstep, maxstep, t1-t0))

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    paradis.ParadisFinish(home, halfspace)

if __name__ == "__main__":
     main()
