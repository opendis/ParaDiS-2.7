from ctypes import *
import numpy as np
import time, sys, os
import matplotlib.pyplot as plt
import glob
import math
import re

class bcolors:
    RED = '\033[31m'
    GRN = '\033[32m'
    YEL = '\033[33m'
    BLU = '\033[34m'
    MAG = '\033[35m'
    CYN = '\033[36m'
    BOLD = '\033[1m'
    RESET = '\033[0m'
    UNDERLINE = '\033[4m'

'''
Main Program Starts Here
'''
def main():
    # plot the surfdisp_*.out files in a folder
    fpath1 = 'test_CPU_results'
    fpath2 = 'test_GPU_results'
    fpath3 = 'test_old_CPU_results'

    data1=[]
    data2=[]
    data3=[]

    # Magnitude of Burgers vector (m)
    b = 2.860e-10
    for CPU_outfile in sorted(glob.glob(os.path.join(fpath1,'*surfdisp_*.out')), key=lambda r: int(re.split('[_ .]', r)[-2])):
        data1.append(np.loadtxt(CPU_outfile, unpack = True))
        Index=int(re.split('[_ .]', CPU_outfile)[-2])
    for GPU_outfile in sorted(glob.glob(os.path.join(fpath2,'*surfdisp_*.out')), key=lambda r: int(re.split('[_ .]', r)[-2])):
        data2.append(np.loadtxt(GPU_outfile, unpack = True))
    for old_CPU_outfile in sorted(glob.glob(os.path.join(fpath3,'*surfdisp_*.out')), key=lambda r: int(re.split('[_ .]', r)[-2])):
        data3.append(np.loadtxt(old_CPU_outfile, unpack = True))

    n=data1[0].shape[1]
    ng=int(math.sqrt(n))
    Nfile=len(data1)
    Interval=int(Index/Nfile)
    fig = plt.figure(figsize=(8,8))
        
    for i in range(Nfile):
        x1=np.transpose(data1[i][0,].reshape(ng,ng))
        y1=np.transpose(data1[i][1,].reshape(ng,ng))
        z1=np.transpose(data1[i][2,].reshape(ng,ng))
        Uzind1=np.transpose(data1[i][3,].reshape(ng,ng))
        Uzelas1=np.transpose(data1[i][4,].reshape(ng,ng))
        Uzplas1=np.transpose(data1[i][5,].reshape(ng,ng))
        Uztot1=Uzelas1+Uzplas1
        xl1=x1[ng//2,]
        yl1=y1[ng//2,]
        Uzindl1=Uzind1[ng//2,]
        Uzelasl1=Uzelas1[ng//2,]
        Uzplasl1=Uzplas1[ng//2,]
        Uztotl1=Uzelasl1+Uzplasl1
        x_cpu_1 = xl1*b*1e6
        y_cpu_1 = Uzindl1*b*1e6
        y_cpu_2 = Uzelasl1*b*1e6

        x2=np.transpose(data2[i][0,].reshape(ng,ng))
        y2=np.transpose(data2[i][1,].reshape(ng,ng))
        z2=np.transpose(data2[i][2,].reshape(ng,ng))
        Uzind2=np.transpose(data2[i][3,].reshape(ng,ng))
        Uzelas2=np.transpose(data2[i][4,].reshape(ng,ng))
        Uzplas2=np.transpose(data2[i][5,].reshape(ng,ng))
        Uztot2=Uzelas2+Uzplas2
        xl2=x2[ng//2,]
        yl2=y2[ng//2,]
        Uzindl2=Uzind2[ng//2,]
        Uzelasl2=Uzelas2[ng//2,]
        Uzplasl2=Uzplas2[ng//2,]
        Uztotl2=Uzelasl2+Uzplasl2
        x_gpu_1 = xl2*b*1e6
        y_gpu_1 = Uzindl2*b*1e6
        y_gpu_2 = Uzelasl2*b*1e6

        if np.max(np.abs(y_cpu_1-y_gpu_1))<1e-9 and np.max(np.abs(y_cpu_2-y_gpu_2))<1e-9:
            print('The comparison between the CPU and GPU versions about surfdisp_'+str((i+1)*Interval)+'.out:'+bcolors.GRN+'PASSED'+bcolors.RESET)
        else:
            print('The comparison between the CPU and GPU versions about surfdisp_'+str((i+1)*Interval)+'.out:'+bcolors.RED+'FAILED'+bcolors.RESET)

        plt.cla()
        line1, =plt.plot(x_cpu_1,y_cpu_1,color='red',linestyle='None',marker='^',ms=6,label='CPU_indenter')
        line2, =plt.plot(x_cpu_1,y_cpu_2,color='blue',linestyle='None',marker='^',ms=6,label='CPU_surface_elastic')
        line3, =plt.plot(x_gpu_1,y_gpu_1,color='yellow',linestyle='-',linewidth=2,marker='None',label='GPU_indenter')
        line4, =plt.plot(x_gpu_1,y_gpu_2,color='pink',linestyle='-',linewidth=2,marker='None',label='GPU_surface_elastic')
        plt.legend(handles=[line1, line2, line3, line4])
        plt.xlabel("x (\u03bcm)")
        plt.ylabel("Uz (\u03bcm)")
        plt.xlim(-1,1)
        plt.ylim(-2,2)
        plt.draw()
        plt.title('surfdisp_'+str((i+1)*Interval)+'.out')
        plt.show(block=False)
        plt.pause(0.5)

    for i in range(Nfile):
        x1=np.transpose(data1[i][0,].reshape(ng,ng))
        y1=np.transpose(data1[i][1,].reshape(ng,ng))
        z1=np.transpose(data1[i][2,].reshape(ng,ng))
        Uzind1=np.transpose(data1[i][3,].reshape(ng,ng))
        Uzelas1=np.transpose(data1[i][4,].reshape(ng,ng))
        Uzplas1=np.transpose(data1[i][5,].reshape(ng,ng))
        Uztot1=Uzelas1+Uzplas1
        xl1=x1[ng//2,]
        yl1=y1[ng//2,]
        Uzindl1=Uzind1[ng//2,]
        Uzelasl1=Uzelas1[ng//2,]
        Uzplasl1=Uzplas1[ng//2,]
        Uztotl1=Uzelasl1+Uzplasl1
        x_cpu_1 = xl1*b*1e6
        y_cpu_1 = Uzindl1*b*1e6
        y_cpu_2 = Uzelasl1*b*1e6

        x3=np.transpose(data3[i][0,].reshape(ng,ng))
        y3=np.transpose(data3[i][1,].reshape(ng,ng))
        z3=np.transpose(data3[i][2,].reshape(ng,ng))
        Uzind3=np.transpose(data3[i][3,].reshape(ng,ng))
        Uzelas3=np.transpose(data3[i][4,].reshape(ng,ng))
        Uzplas3=np.transpose(data3[i][5,].reshape(ng,ng))
        Uztot3=Uzelas3+Uzplas3
        xl3=x3[ng//2,]
        yl3=y3[ng//2,]
        Uzindl3=Uzind3[ng//2,]
        Uzelasl3=Uzelas3[ng//2,]
        Uzplasl3=Uzplas3[ng//2,]
        Uztotl3=Uzelasl3+Uzplasl3
        x_old_cpu_1 = xl3*b*1e6
        y_old_cpu_1 = Uzindl3*b*1e6
        y_old_cpu_2 = Uzelasl3*b*1e6

        if np.max(np.abs(y_cpu_1-y_old_cpu_1))<1e-9 and np.max(np.abs(y_cpu_2-y_old_cpu_2))<1e-9:
            print('The comparison between the current CPU and old CPU versions about surfdisp_'+str((i+1)*Interval)+'.out:'+bcolors.GRN+'PASSED'+bcolors.RESET)
        else:
            print('The comparison between the current CPU and old CPU versions about surfdisp_'+str((i+1)*Interval)+'.out:'+bcolors.RED+'FAILED'+bcolors.RESET)

        plt.cla()
        line1, =plt.plot(x_cpu_1,y_cpu_1,color='red',linestyle='None',marker='^',ms=6,label='CPU_indenter')
        line2, =plt.plot(x_cpu_1,y_cpu_2,color='blue',linestyle='None',marker='^',ms=6,label='CPU_surface_elastic')
        line3, =plt.plot(x_old_cpu_1,y_old_cpu_1,color='yellow',linestyle='-',linewidth=2,marker='None',label='old_CPU_indenter')
        line4, =plt.plot(x_old_cpu_1,y_old_cpu_2,color='pink',linestyle='-',linewidth=2,marker='None',label='old_CPU_surface_elastic')
        plt.legend(handles=[line1, line2, line3, line4])
        plt.xlabel("x (\u03bcm)")
        plt.ylabel("Uz (\u03bcm)")
        plt.xlim(-1,1)
        plt.ylim(-2,2)
        plt.draw()
        plt.title('surfdisp_'+str((i+1)*Interval)+'.out')
        plt.show(block=False)
        plt.pause(0.5)

    for i in range(Nfile):
        x2=np.transpose(data2[i][0,].reshape(ng,ng))
        y2=np.transpose(data2[i][1,].reshape(ng,ng))
        z2=np.transpose(data2[i][2,].reshape(ng,ng))
        Uzind2=np.transpose(data2[i][3,].reshape(ng,ng))
        Uzelas2=np.transpose(data2[i][4,].reshape(ng,ng))
        Uzplas2=np.transpose(data2[i][5,].reshape(ng,ng))
        Uztot2=Uzelas2+Uzplas2
        xl2=x2[ng//2,]
        yl2=y2[ng//2,]
        Uzindl2=Uzind2[ng//2,]
        Uzelasl2=Uzelas2[ng//2,]
        Uzplasl2=Uzplas2[ng//2,]
        Uztotl2=Uzelasl2+Uzplasl2
        x_gpu_1 = xl2*b*1e6
        y_gpu_1 = Uzindl2*b*1e6
        y_gpu_2 = Uzelasl2*b*1e6

        x3=np.transpose(data3[i][0,].reshape(ng,ng))
        y3=np.transpose(data3[i][1,].reshape(ng,ng))
        z3=np.transpose(data3[i][2,].reshape(ng,ng))
        Uzind3=np.transpose(data3[i][3,].reshape(ng,ng))
        Uzelas3=np.transpose(data3[i][4,].reshape(ng,ng))
        Uzplas3=np.transpose(data3[i][5,].reshape(ng,ng))
        Uztot3=Uzelas3+Uzplas3
        xl3=x3[ng//2,]
        yl3=y3[ng//2,]
        Uzindl3=Uzind3[ng//2,]
        Uzelasl3=Uzelas3[ng//2,]
        Uzplasl3=Uzplas3[ng//2,]
        Uztotl3=Uzelasl3+Uzplasl3
        x_old_cpu_1 = xl3*b*1e6
        y_old_cpu_1 = Uzindl3*b*1e6
        y_old_cpu_2 = Uzelasl3*b*1e6

        if np.max(np.abs(y_gpu_1-y_old_cpu_1))<1e-9 and np.max(np.abs(y_gpu_2-y_old_cpu_2))<1e-9:
            print('The comparison between the GPU and old CPU versions about surfdisp_'+str((i+1)*Interval)+'.out:'+bcolors.GRN+'PASSED'+bcolors.RESET)
        else:
            print('The comparison between the GPU and old CPU versions about surfdisp_'+str((i+1)*Interval)+'.out:'+bcolors.RED+'FAILED'+bcolors.RESET)

        plt.cla()
        line1, =plt.plot(x_old_cpu_1,y_old_cpu_1,color='red',linestyle='None',marker='^',ms=6,label='old_CPU_indenter')
        line2, =plt.plot(x_old_cpu_1,y_old_cpu_2,color='blue',linestyle='None',marker='^',ms=6,label='old_CPU_surface_elastic')
        line3, =plt.plot(x_gpu_1,y_gpu_1,color='yellow',linestyle='-',linewidth=2,marker='None',label='GPU_indenter')
        line4, =plt.plot(x_gpu_1,y_gpu_2,color='pink',linestyle='-',linewidth=2,marker='None',label='GPU_surface_elastic')
        plt.legend(handles=[line1, line2, line3, line4])
        plt.xlabel("x (\u03bcm)")
        plt.ylabel("Uz (\u03bcm)")
        plt.xlim(-1,1)
        plt.ylim(-2,2)
        plt.draw()
        plt.title('surfdisp_'+str((i+1)*Interval)+'.out')
        plt.show(block=False)
        plt.pause(0.5)
        
if __name__ == "__main__":
     main()
