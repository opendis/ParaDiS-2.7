from ctypes import *
import numpy as np
import time, sys, os
import glob
from collections import defaultdict

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('HS_Home_gpu' if use_GPU else 'HS_Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

class dpaths:
    RESTARTFILEDIR = "../test_results/restart/"
    DATAFILE  = "../prismatic_surface_step_FCC_highest.data"

class tols:
    TEST_V = 3e-3     # sqrt(1e-9)

def run_paradis(home, halfspace):
    param   = home.contents.param
    cycleStart = param.contents.cycleStart
    cycleEnd   = param.contents.cycleStart + param.contents.maxstep
    forces_velocities = {}

    for home.contents.cycle in range(cycleStart, cycleEnd):
        paradis.SortNativeNodes(home)
        paradis.CellCharge(home)
        paradis.HS_Step(home, halfspace)
        paradis.PrintSurfaceDisplacement(home, halfspace)
        paradis.PrintSurfaceSegments(home, halfspace)
        paradis.BuildSurfaceSegList(home, halfspace)
        paradis.ParadisStep(home, halfspace)

    forces_velocities[gids.FULL] = paradis.extract_nodal_force_vel(home)
    paradis.ParadisFinish(home, halfspace)

    return forces_velocities


def check_nodes_on_glide_plane(datafile,restartdir):
    nodes, bounds = read_nodes_to_dict(datafile)
    num_nodes = len(nodes)
    print("Number of node in data file:", num_nodes)
    
    Initial_node_pos = []
    Initial_plane_normal = []
    Ref_dot_product = []
    for key, value in nodes.items():
        Initial_node_pos.append(np.array([value[0][0], value[0][1], value[0][2]]))
        first_key = next(iter(value[1]))
        first_value = value[1][first_key]
        Initial_plane_normal.append(np.array([first_value[3], first_value[4], first_value[5]]))
    
    print("Initial_Node_pos:",Initial_node_pos)
    print("Initial_plane_normal:",Initial_plane_normal)

    for i in range(len(Initial_node_pos)):
        #print('i:',i)
        Ref_dot_product.append(np.dot(Initial_node_pos[i], Initial_plane_normal[i]))
    
    print("Ref_dot_product:",Ref_dot_product)

    rsfile_names = [file_name for file_name in os.listdir(restartdir) if file_name.startswith('rs') and file_name.endswith('.data')]

    for i in range(len(rsfile_names)):
        print(restartdir+rsfile_names[i])
        rs_nodes, rs_bounds = read_nodes_to_dict(restartdir+rsfile_names[i])
        Node_pos = []
        for key, value in rs_nodes.items():
            Node_pos.append(np.array([value[0][0], value[0][1], value[0][2]]))
        #print("i:",i)
        #print(Node_pos)

        plane_index = [0 for _ in range(len(Node_pos))]
        print("plane_index_before:",plane_index)

        for j in range(len(Node_pos)):
            for k in range(len(Initial_plane_normal)):
                dot_product = np.dot(Node_pos[j], Initial_plane_normal[k])
                if abs(dot_product - Ref_dot_product[k]) < tols.TEST_V:
                    plane_index[j] = k+1
        
        print("plane_index_after:",plane_index)

        zero_indices = []
        for index, value in enumerate(plane_index):
            if value == 0:
                zero_indices.append(index)
        
        if len(zero_indices) > 0:
            for m in range(len(zero_indices)):
                print("(%13.6e, %13.6e, %13.6e) is not on the original glide planes"%(Node_pos[zero_indices[m]][0],Node_pos[zero_indices[m]][1],Node_pos[zero_indices[m]][2]))
            print("Test" + bcolors.RED + " FAILED" + bcolors.RESET)
            return 0
    print("All nodes are on the original glide planes. Test"+ bcolors.GRN + " PASSED" + bcolors.RESET)
    return 1

'''
Main Program Starts Here
'''
def main():
    global home, param, halfspace

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    print("use_GPU = ", use_GPU)

    halfspace = paradis.hs_init(home)

    param.contents.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home, halfspace)

    t_begin = time.time()  

    maxstep = 5
    forces_velocities = run_paradis(home, halfspace)

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    test_result = check_nodes_on_glide_plane(dpaths.DATAFILE,dpaths.RESTARTFILEDIR)

    return 0 if test_result else 1

if __name__ == "__main__":
    sys.exit( main() )
