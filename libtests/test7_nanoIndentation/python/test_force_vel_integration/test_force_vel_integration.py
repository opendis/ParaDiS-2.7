from ctypes import *
import numpy as np
import time, sys, os
import glob
from collections import defaultdict

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('HS_Home_gpu' if use_GPU else 'HS_Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

class dpaths:
    REFDATADIR = "../ref_old_CPU_results"
    USRDATADIR = "../test_results"
    FULLDATAFILE  = "NodeForceVel_FULL.out"
    FORCEDATAFILE  = "force/force0001"
    VELDATAFILE  = "velocity/vel0001"

class tols:
    TESTA_F = 3e-4     # sqrt(1e-9)
    TESTA_V = 3e-3     # sqrt(1e-9)
    TESTB_F = 3e-4     # sqrt(1e-9)
    TESTB_V = 3e-3     # sqrt(1e-9)
    MIN_V_MAG = 1e-1 # values below this threshold are considered as zero
    MIN_F_MAG = 1e-5

def run_paradis(home, halfspace):
    param   = home.contents.param
    cycleStart = param.contents.cycleStart
    cycleEnd   = param.contents.cycleStart + param.contents.maxstep
    forces_velocities = {}

    for home.contents.cycle in range(cycleStart, cycleEnd):
        paradis.SortNativeNodes(home)
        paradis.CellCharge(home)
        paradis.HS_Step(home, halfspace)
        paradis.PrintSurfaceDisplacement(home, halfspace)
        paradis.PrintSurfaceSegments(home, halfspace)
        paradis.BuildSurfaceSegList(home, halfspace)
        paradis.ParadisStep(home, halfspace)

    forces_velocities[gids.FULL] = paradis.extract_nodal_force_vel(home)
    paradis.ParadisFinish(home, halfspace)

    return forces_velocities

def read_force_vel_from_file(datadir):
    ref_forces_velocities = {}
    #for j in range(gids.GROUP0, gids.GROUP4+1):
    #    ref_forces_velocities[j] = np.loadtxt(os.path.join(datadir,dpaths.GROUPDATAFILE%(j-gids.GROUP0)))
    ref_forces_velocities[gids.FULL] = np.loadtxt(os.path.join(datadir,'Ref_%s'%dpaths.FULLDATAFILE))
    return ref_forces_velocities

def compare_force_vel_integration(ref_data_dir, data_dir, ref_forces_velocities, forces_velocities, do_testA_VEL=True, do_testB=True, do_testB_VEL=True):
    print()
    print("***********************************************************************************************************************")
    print("testA compares the FULL forces and velocities against reference values after integration but before topological changes.")
    print("***********************************************************************************************************************")
    ref_nodes_force = read_force_to_dict(os.path.join(ref_data_dir,dpaths.FORCEDATAFILE))
    test_nodes_force = read_force_to_dict(os.path.join(data_dir,dpaths.FORCEDATAFILE))
    force_diff = {key: ref_nodes_force[key] - test_nodes_force.get(key,0) for key in ref_nodes_force.keys()}
    force_diff_norm = np.linalg.norm(np.array(list(force_diff.values())),axis=1)
    force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
    force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(np.array(list(ref_nodes_force.values())),axis=1), tols.MIN_F_MAG) )
    print("max(force_rel_error) = %e"%np.amax(force_rel_err))
    if np.amax(force_rel_err) < tols.TESTA_F:
        print("testA (force)" + bcolors.GRN + " PASSED" + bcolors.RESET)
        testA_FORCE_PASSED = True
    else:
        print("testA (force)" + bcolors.RED + " FAILED" + bcolors.RESET)
        testA_FORCE_PASSED = False
        max_err_id = np.argmax(force_rel_err)
        node_id = max_err_id
        print("Forces_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,test_nodes_force[max_err_id][0],test_nodes_force[max_err_id][1],test_nodes_force[max_err_id][2]))
        print("Forces_full_ref (%d):   %13.6e %13.6e %13.6e "%(node_id,ref_nodes_force[max_err_id][0],ref_nodes_force[max_err_id][1],ref_nodes_force[max_err_id][2]))
        print("Forces_diff_max (%d):   %13.6e %13.6e %13.6e "%(node_id,force_diff[max_err_id][0],force_diff[max_err_id][1],force_diff[max_err_id][2]))

    testAll_PASSED = testA_FORCE_PASSED

    if do_testA_VEL:
        ref_nodes_vel = read_vel_to_dict(os.path.join(ref_data_dir,dpaths.VELDATAFILE))
        test_nodes_vel = read_vel_to_dict(os.path.join(data_dir,dpaths.VELDATAFILE))
        vel_diff = {key: ref_nodes_vel[key] - test_nodes_vel.get(key,0) for key in ref_nodes_vel.keys()}
        vel_diff_norm = np.linalg.norm(np.array(list(vel_diff.values())),axis=1)
        vel_diff_norm[ vel_diff_norm < tols.MIN_V_MAG ] = 0
        vel_rel_err = np.divide( vel_diff_norm, np.maximum(np.linalg.norm(np.array(list(ref_nodes_vel.values())),axis=1), tols.MIN_V_MAG) )
        print("max(vel_rel_error) = %e"%np.amax(vel_rel_err))
        if np.amax(vel_rel_err) < tols.TESTA_V:
            print("testA (velocity)" + bcolors.GRN + " PASSED" + bcolors.RESET)
            testA_VEL_PASSED = True
        else:
            print("testA (velocity)" + bcolors.RED + " FAILED" + bcolors.RESET)
            testA_VEL_PASSED = False
            max_err_id = np.argmax(vel_rel_err)
            node_id = max_err_id
            print("Velocities_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,test_nodes_vel[max_err_id][0],test_nodes_vel[max_err_id][1],test_nodes_vel[max_err_id][2]))
            print("Velocities_full_ref (%d):   %13.6e %13.6e %13.6e "%(node_id,ref_nodes_vel[max_err_id][0],ref_nodes_vel[max_err_id][1],ref_nodes_vel[max_err_id][2]))
            print("Velocities_diff_max (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_diff[max_err_id][0],vel_diff[max_err_id][1],vel_diff[max_err_id][2]))   
    
        testAll_PASSED = testAll_PASSED and testA_VEL_PASSED

    if do_testB:
        print()
        print("*****************************************************************************************************************")
        print("testB compares the FULL forces and velocities against reference values after integration and topological changes.")
        print("*****************************************************************************************************************")
        ref_force_full = ref_forces_velocities[gids.FULL][:,1:4]
        force_full = forces_velocities[gids.FULL][:,1:4]
        print("The node number after topological changes in the reference: %d"%len(ref_forces_velocities[gids.FULL]))
        print("The node number after topological changes in the test: %d"%len(forces_velocities[gids.FULL]))
        if len(ref_forces_velocities[gids.FULL]) != len(forces_velocities[gids.FULL]):
            print("The node number after topological changes in the test is not equal to the reference result.")
            print("testB" + bcolors.RED + " FAILED" + bcolors.RESET)
            testB_PASSED = False
            testAll_PASSED = testAll_PASSED and testB_PASSED
        else:
            force_diff = force_full - ref_force_full
            force_diff_norm = np.linalg.norm(force_diff,axis=1)
            force_diff_norm[ force_diff_norm < tols.MIN_F_MAG ] = 0
            force_rel_err = np.divide( force_diff_norm, np.maximum(np.linalg.norm(ref_force_full,axis=1), tols.MIN_F_MAG) )
            print("max(force_rel_error) = %e"%np.amax(force_rel_err))
            if np.amax(force_rel_err) < tols.TESTB_F:
                print("testB (force)" + bcolors.GRN + " PASSED" + bcolors.RESET)
                testB_FORCE_PASSED = True
            else:
                print("testB (force)" + bcolors.RED + " FAILED" + bcolors.RESET)
                testB_FORCE_PASSED = False
                max_err_id = np.argmax(force_rel_err)
                node_id = forces_velocities[gids.FULL][max_err_id,0]
                print("Forces_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,force_full[max_err_id][0],force_full[max_err_id][1],force_full[max_err_id][2]))
                print("Forces_full_ref (%d):   %13.6e %13.6e %13.6e "%(node_id,ref_force_full[max_err_id][0],ref_force_full[max_err_id][1],ref_force_full[max_err_id][2]))
                print("Forces_diff_max (%d):   %13.6e %13.6e %13.6e "%(node_id,force_diff[max_err_id][0],force_diff[max_err_id][1],force_diff[max_err_id][2]))

            testAll_PASSED = testAll_PASSED and testB_FORCE_PASSED

            if do_testB_VEL:
                vel_full = forces_velocities[gids.FULL][:,4:7]
                ref_vel_full = ref_forces_velocities[gids.FULL][:,4:7]
                vel_diff = vel_full - ref_vel_full
                vel_diff_norm = np.linalg.norm(vel_diff,axis=1)
                vel_diff_norm[ vel_diff_norm < tols.MIN_V_MAG ] = 0
                vel_rel_err = np.divide( vel_diff_norm, np.maximum(np.linalg.norm(ref_vel_full,axis=1), tols.MIN_V_MAG) )
                print("max(vel_rel_error) = %e"%np.amax(vel_rel_err))
                if np.amax(vel_rel_err) < tols.TESTB_V:
                    print("testB (velocity)" + bcolors.GRN + " PASSED" + bcolors.RESET)
                    testB_VEL_PASSED = True
                else:
                    print("testB (velocity)" + bcolors.RED + " FAILED" + bcolors.RESET)
                    testB_VEL_PASSED = False
                    max_err_id = np.argmax(vel_rel_err)
                    node_id = forces_velocities[gids.FULL][max_err_id,0]
                    print("Velocities_full_user(%d):   %13.6e %13.6e %13.6e "%(node_id,vel_full[max_err_id][0],vel_full[max_err_id][1],vel_full[max_err_id][2]))
                    print("Velocities_full_ref (%d):   %13.6e %13.6e %13.6e "%(node_id,ref_vel_full[max_err_id][0],ref_vel_full[max_err_id][1],ref_vel_full[max_err_id][2]))
                    print("Velocities_diff_max (%d):   %13.6e %13.6e %13.6e "%(node_id,vel_diff[max_err_id][0],vel_diff[max_err_id][1],vel_diff[max_err_id][2]))

                testAll_PASSED = testAll_PASSED and testB_VEL_PASSED

    return testAll_PASSED

'''
Main Program Starts Here
'''
def main():
    global home, param, halfspace

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    print("use_GPU = ", use_GPU)

    halfspace = paradis.hs_init(home)

    param.contents.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home, halfspace)

    t_begin = time.time()  

    maxstep = 1
    forces_velocities = run_paradis(home, halfspace)
    ref_forces_velocities = read_force_vel_from_file(dpaths.REFDATADIR)
    testAll_PASSED = compare_force_vel_integration(dpaths.REFDATADIR, dpaths.USRDATADIR, ref_forces_velocities, forces_velocities, do_testA_VEL=(not use_GPU), do_testB=(not use_GPU), do_testB_VEL=(not use_GPU))

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0 if testAll_PASSED else 1

if __name__ == "__main__":
    sys.exit( main() )
