#include ../../../../makefile.setup
#include ../../../../makefile.sys

.PHONY: all help build install run run_GPU run_CPU_GPU clean deepclean

all: help run_CPU_GPU

# modify the PYTHONPATH variable
PYTHONPATH=$(abspath $(CURDIR)/../../../../lib):$(abspath $(CURDIR)/../../../../python)

help: 
	@echo "you need to log in the GPU node"
	@echo "make sure environmental variable PYTHONPATH is present"
	@echo "you may include the following line in $$HOME/.bash_profile"
	@echo " export PYTHONPATH="
	@echo "make sure to set environmental variables correctly"
	@echo " your PYTHONPATH=$(PYTHONPATH)"
	@echo "      CTYPESGEN_DIR=$(CTYPESGEN_DIR)"

build/bin/libparadishs.so:
	make build

build:
	mkdir -p build ; cd build ; cmake -S ../../../../../ $(CMAKE_SPEC)
	cmake --build build -j 4

install: build/bin/libparadishs.so
	cmake --build build --target install

../../../../lib/HS_Home.py: ../../../../CMakeLists.txt ../../../../halfspace/CMakeLists.txt
	make install

../../../../lib/HS_Home_gpu.py: ../../../../CMakeLists.txt ../../../../halfspace/CMakeLists.txt
	make install

run: test_force_vel_without_integration.py ../../../../lib/HS_Home.py prismatic_surface_step.ctrl prismatic_surface_step.data
	echo 'run the CPU version of Paradis code'
	python3 test_force_vel_without_integration.py prismatic_surface_step
	mv test_results test_CPU_results

run_GPU: test_force_vel_without_integration.py ../../../../lib/HS_Home_gpu.py prismatic_surface_step.ctrl prismatic_surface_step.data
	echo 'run the GPU versions of Paradis code and need to log in the GPU node'
	python3 test_force_vel_without_integration.py prismatic_surface_step use_GPU
	mv test_results test_GPU_results

run_CPU_GPU: test_force_vel_without_integration.py ../../../../lib/HS_Home.py ../../../../lib/HS_Home_gpu.py prismatic_surface_step.ctrl prismatic_surface_step.data
	echo 'take turns to run the CPU and GPU versions of Paradis code and need to log in the GPU node'
	# run CPU version
	make run
	# run GPU version
	make run_GPU

clean: 
	rm -rf test_CPU_results test_GPU_results test_results

deepclean: clean
	rm -rf build
