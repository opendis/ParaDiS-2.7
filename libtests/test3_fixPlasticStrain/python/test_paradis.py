from ctypes import *
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

def cal_nodal_force_vel(home):
    paradis.CellCharge(home)
    paradis.SegSegListMaker(home, gids.FULL)

    zeroOnErr = 1
    doAll = 1
    paradis.NodeForce(home, gids.FULL)
    paradis.CalcNodeVelocities(home, zeroOnErr, doAll)
    forces_velocities = paradis.extract_nodal_force_vel(home)

    return forces_velocities

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.contents.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    t_begin = time.time()  

    global forces_velocities
    forces_velocities = cal_nodal_force_vel(home)
    '''
    print("node distance before ParadisStep", [home.contents.nodeKeys[17].contents.y - home.contents.nodeKeys[21].contents.y, 
                                               home.contents.nodeKeys[17].contents.z - home.contents.nodeKeys[21].contents.z])
    '''
    global nodes_before
    nodes_before = convert_nodes_to_dict(home)

    maxstep = 1
    #param.contents.maxDT = 3e-9
    #param.contents.maxDT = 4e-9
    #param.contents.maxDT = 9e-9
    #param.contents.maxDT = 8.73e-9
    #param.contents.maxDT = 1e-8
    #param.contents.maxDT = 2e-8
    for tstep in range(maxstep):
        t0 = time.time()  
        home.contents.cycle = tstep
        paradis.ParadisStep(home)
        t1 = time.time()
        print('step = %d/%d  time used = %g s'%(tstep, maxstep, t1-t0))

    t_end = time.time()  
    '''
    print("node distance after  ParadisStep", [home.contents.nodeKeys[17].contents.y - home.contents.nodeKeys[21].contents.y, 
                                               home.contents.nodeKeys[17].contents.z - home.contents.nodeKeys[21].contents.z])
    '''
    global nodes_after
    nodes_after = convert_nodes_to_dict(home)

    print('Total time used = %g s'%(t_end-t_begin))


if __name__ == "__main__":
     main()
