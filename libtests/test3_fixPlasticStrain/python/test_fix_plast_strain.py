from ctypes import *
import numpy as np
import pickle
import time, sys, os

use_GPU = True if len(sys.argv)> 2 and sys.argv[2]=='use_GPU' else False

home_lib = __import__('Home_gpu' if use_GPU else 'Home')
from paradis_util import *

paradis = ParaDiS(home_lib)

class nsteps:
    LOAD    = 20
    REVLOAD = 20
    RELAX   = 20
    PROJECT = 1

class tols:
    STRAIN = 1e-10

def find_end_node(home, node):
    # find which of the two segments (0-1) or (2-4) does the node belongs to
    nextNode, NodeList, Count = node, [node.contents.myTag.index], 1
    while nextNode.contents.constraint != constraints.PINNED_NODE:
        if nextNode.contents.numNbrs > 2:
            print("Node with more than 2 arms should not be present here, node (%d,%d)"
                  %(nextNode.contents.myTag.domainID,nextNode.contents.myTag.index))
            return None

        for j in range(0, nextNode.contents.numNbrs):
            nbr = paradis.GetNeighborNode(home, nextNode, j)
            if nbr.contents.myTag.index in NodeList: # nbr is already contained in NodeList
                continue
            else:
                nextNode = nbr
                NodeList.append(nextNode.contents.myTag.index)
                Count = len(NodeList)
                break

    return nextNode.contents.myTag.index

def project_nodes_to_line(home):
    # project the position of nodes to straight lines connecting end nodes
    for key in range(home.contents.newNodeKeyPtr):
        node = home.contents.nodeKeys[key]
        if node and node.contents.numNbrs > 2:
            print(bcolors.MAG + "multiarm node still exists, skip project_nodes_to_line" + bcolors.RESET)
            return

    for key in range(home.contents.newNodeKeyPtr):
        node = home.contents.nodeKeys[key]
        if node: # node is not NULL
            endNodeKey = find_end_node(home, node)
            if endNodeKey == 0 or endNodeKey == 1:
                node0 = home.contents.nodeKeys[0]
                node1 = home.contents.nodeKeys[1]
            elif endNodeKey == 2 or endNodeKey == 4:
                node0 = home.contents.nodeKeys[2]
                node1 = home.contents.nodeKeys[4]
            r  = np.array([node.contents.x,  node.contents.y,  node.contents.z ])
            r0 = np.array([node0.contents.x, node0.contents.y, node0.contents.z])
            r1 = np.array([node1.contents.x, node1.contents.y, node1.contents.z])
            pj_line  = r1 - r0
            pos_line = r - r0
            pj_line = pj_line / np.linalg.norm(pj_line)
            rnew = np.dot(pos_line, pj_line) * pj_line + r0

            node.contents.x = rnew[0]
            node.contents.y = rnew[1]
            node.contents.z = rnew[2]

def paradis_run_load_unload(home, sigma):
    param   = home.contents.param
    cycleStart = param.contents.cycleStart
    cycleEnd   = cycleStart + nsteps.LOAD + nsteps.REVLOAD + nsteps.RELAX + nsteps.PROJECT

    param.contents.appliedStress[0:6] = sigma

    home.contents.cycle = cycleStart
    while home.contents.cycle < cycleEnd:
        if home.contents.cycle < cycleEnd-1:
            paradis.ParadisStep(home)
        else:
            print(bcolors.BLU + "project nodes to straight line" + bcolors.RESET)
            project_nodes_to_line(home)
            paradis.DeltaPlasticStrain(home) # calculates param->delpStrain
            deltaStress = np.zeros((3,3))
            paradis.LoadCurve.argtypes = [POINTER(paradis.Home_t),   POINTER(c_double*int(3)*int(3)) ] # override argument type
            paradis.LoadCurve(home, cast(deltaStress.ctypes, POINTER(c_double*int(3)*int(3)))) # calculates param->totpStrain
            paradis.GenerateOutput(home, stages.STAGE_CYCLE)

        if home.contents.cycle == nsteps.LOAD: # finished loading
            print(bcolors.BLU + "reverse stress" + bcolors.RESET)
            param.contents.appliedStress[0:6] = [-1.0*s for s in sigma]
        elif home.contents.cycle == nsteps.LOAD + nsteps.REVLOAD: # finished reverse loading
            print(bcolors.BLU + "set stress to zero" + bcolors.RESET)
            param.contents.appliedStress[0:6] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

def find_residual_plastic_strain(home):
    param   = home.contents.param
    plastic_strain = np.array([param.contents.totpStn[0], param.contents.totpStn[1], param.contents.totpStn[2],
                               param.contents.totpStn[3], param.contents.totpStn[4], param.contents.totpStn[5]])
    return plastic_strain

'''
Main Program Starts Here
'''
def main():
    global home, param

    taskname = sys.argv[1]
    home = paradis.paradis_init(taskname)
    param = home.contents.param

    param.contents.timestepIntegrator = b'forceBsubcycle'
    if use_GPU:
        param.contents.subInteg0Integ1 = b'GPU'
    else:
        param.contents.subInteg0Integ1 = b'RKF-RKF'

    if use_GPU: paradis.InitializeParadisGPU(home)

    t_begin = time.time() 
    paradis_run_load_unload(home, [2.0e6,2.0e6,2.0e6,2.0e6,2.0e6,2.0e6])
    plastic_strain = find_residual_plastic_strain(home)
    paradis.ParadisFinish(home)
    os.chdir('..')

    one_norm = np.linalg.norm(plastic_strain, ord=1)
    print('residual plastic strain = ' + str(plastic_strain))
    print('one norm = %e'%(one_norm))

    if one_norm < tols.STRAIN:
        print("test (residual plastic strain)" + bcolors.GRN + " PASSED" + bcolors.RESET)
        test_passed = True
    else:
        print("test (residual plastic strain)" + bcolors.RED + " FAILED" + bcolors.RESET)
        test_passed = False

    t_end = time.time()  
    print('Total time used = %g s'%(t_end-t_begin))

    return 0 if test_passed else 1

if __name__ == "__main__":
     sys.exit( main() )
