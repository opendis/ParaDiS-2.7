
file(COPY ${PROJECT_SOURCE_DIR}/libtests/test3_fixPlasticStrain/python/fix_plasticStrain.data
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

file(COPY ${PROJECT_SOURCE_DIR}/libtests/test3_fixPlasticStrain/python/test_fix_plast_strain.py
          ${PROJECT_SOURCE_DIR}/libtests/test3_fixPlasticStrain/python/test_paradis.py
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

add_string_before_and_after_file(
FRONT_TEXT "# Some lines are added at the end of file by CMake\n"
END_TEXT   "\n# Lines added by CMake
fmCorrectionTbl = \"${PROJECT_SOURCE_DIR}/inputs/fm-ctab.Cu.m2.t5.dat\"
# End of lines added by CMake\n"
FROM_DIR   ${PROJECT_SOURCE_DIR}/libtests/test3_fixPlasticStrain/python
TO_DIR     ${CMAKE_CURRENT_BINARY_DIR}
FILE_NAME  fix_plasticStrain.ctrl)


add_test(NAME test3_fixPlasticStrain_run_CPU
    COMMAND ${Python3_EXECUTABLE} test_fix_plast_strain.py fix_plasticStrain
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
set_property(TEST test3_fixPlasticStrain_run_CPU PROPERTY 
    ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")

if ((NOT ${CMAKE_CUDA_COMPILER} MATCHES "NOTFOUND") AND (${EXE_MODE} STREQUAL SERIAL))
    add_test(NAME test3_fixPlasticStrain_run_GPU
         COMMAND ${Python3_EXECUTABLE} test_fix_plast_strain.py fix_plasticStrain use_GPU
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    set_property(TEST test3_fixPlasticStrain_run_GPU PROPERTY 
        ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")
endif()

add_test(NAME test3_paradis_run_CPU
    COMMAND ${Python3_EXECUTABLE} test_paradis.py fix_plasticStrain
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
set_property(TEST test3_paradis_run_CPU PROPERTY 
    ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")

if ((NOT ${CMAKE_CUDA_COMPILER} MATCHES "NOTFOUND") AND (${EXE_MODE} STREQUAL SERIAL))
    add_test(NAME test3_paradis_run_GPU
         COMMAND ${Python3_EXECUTABLE} test_paradis.py fix_plasticStrain use_GPU
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    set_property(TEST test3_paradis_run_GPU PROPERTY 
        ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")
endif()

#add_test(NAME test3_plotRestartFiles_run_CPU
#    COMMAND ${Python3_EXECUTABLE} plot_restart_files.py test_results/restart
#    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
#set_property(TEST test3_plotRestartFiles_run_CPU PROPERTY 
#    ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")

