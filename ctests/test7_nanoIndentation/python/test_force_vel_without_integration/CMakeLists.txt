
file(COPY ${PROJECT_SOURCE_DIR}/libtests/test7_nanoIndentation/python/test_force_vel_without_integration/prismatic_surface_step.data
          ${PROJECT_SOURCE_DIR}/libtests/test7_nanoIndentation/python/test_force_vel_without_integration/ref_results
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

file(COPY ${PROJECT_SOURCE_DIR}/libtests/test7_nanoIndentation/python/test_force_vel_without_integration/test_force_vel_without_integration.py
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

add_string_before_and_after_file(
FRONT_TEXT "# Some lines are added at the end of file by CMake\n"
END_TEXT   "\n# Lines added by CMake
Rijmfile = \"${PROJECT_SOURCE_DIR}/inputs/Rijm.cube.out\"
RijmPBCfile = \"${PROJECT_SOURCE_DIR}/inputs/RijmPBC.cube.out\"
# End of lines added by CMake\n"
FROM_DIR   ${PROJECT_SOURCE_DIR}/libtests/test7_nanoIndentation/python/test_force_vel_without_integration
TO_DIR     ${CMAKE_CURRENT_BINARY_DIR}
FILE_NAME  prismatic_surface_step.ctrl)


add_test(NAME test7_nanoIndentation_force_vel_without_integration_run_CPU
    COMMAND ${Python3_EXECUTABLE} test_force_vel_without_integration.py prismatic_surface_step
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
set_property(TEST test7_nanoIndentation_force_vel_without_integration_run_CPU PROPERTY 
    ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")

if ((NOT ${CMAKE_CUDA_COMPILER} MATCHES "NOTFOUND") AND (${EXE_MODE} STREQUAL SERIAL))
    add_test(NAME test7_nanoIndentation_force_vel_without_integration_run_GPU
         COMMAND ${Python3_EXECUTABLE} test_force_vel_without_integration.py prismatic_surface_step use_GPU
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
    set_property(TEST test7_nanoIndentation_force_vel_without_integration_run_GPU PROPERTY 
        ENVIRONMENT "PYTHONPATH=${PROJECT_SOURCE_DIR}/lib:${PROJECT_SOURCE_DIR}/python")
endif()


