# This folder provides an example of how to construct c and python test cases

add_executable(print_size
               print_size.c)

add_test(NAME test_print_size
         COMMAND print_size)

file(COPY        ${CMAKE_CURRENT_SOURCE_DIR}/test_python.py
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR} )

add_test(NAME test_run_python
         COMMAND ${Python3_EXECUTABLE} test_python.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
