/*
 * To compile: icc print_size.c -o print_size
 * To run:     ./print_size
 */

#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

int main (int argc, char *argv[]) 
{
    long mylongint;
    size_t mysize;
    int i;

    printf("SIZE_MAX = %zu\n", SIZE_MAX);

    mylongint = 10000;

    for(i=0; i<50; i++)
    {
        mylongint *= 2;
        mysize = mylongint;
        printf("mylongint = %ld  mysize = %zu\n", mylongint, mysize);
    }

    return(0);

    //this will make the test fail
    //return(1);

    //this will also make the test fail
    //assert(1 > 2);
}


