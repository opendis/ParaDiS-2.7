/*-------------------------------------------------------------------------
 *
 *      Function:     SubcycleIntegrator
 *      Description:  This function time integrates the system one time
 *					  step using a "subcycling" approach. Two groups of
 *					  nodes are established and these two groups are then
 *					  time integrated independently. 
 *
 *-----------------------------------------------------------------------*/

#ifdef _SUBCYCLING
#include "Home.h"
#include "Comm.h"
#include "sys/stat.h"
#include "sys/types.h"

#ifdef _GPU_SUBCYCLE
#include "SubcycleGPU.h"
#endif

#define PI 3.14159265

void ForwardProgressCheckNodeB(Home_t *home, int *group1size)
{
		int		i;
		real8	oldV[3], V[3];
		Node_t	*node;
		for (i=0; i < home->newNodeKeyPtr; i++) {
			node = home->nodeKeys[i];
            if (node == (Node_t *)NULL) continue;
			if (node->subgroup==0) {
				continue;
			}
			oldV[0] = node->oldvX;
			oldV[1] = node->oldvY;
			oldV[2] = node->oldvZ;
			V[0] = node->vX;
			V[1] = node->vY;
			V[2] = node->vZ;
			if (DotProduct(oldV,V)<0) {
				node->subgroup = 0;
				(*group1size)--;
				//printf("Removed node %i from subcycling due to velocity reversal\n",node->myTag.index);
			}
		}

		return;
}

/*******************************************************************************
 *******************************************************************************
 *******************************************************************************
 *******************************************************************************/

void ForwardProgressCheckForceB(Home_t *home)
{
		int		i;
		real8	oldV[3], V[3];
		Node_t	*node;
		
		//TimerStart(home, FORWARD_PROGRES_CHECK);
		
		#ifdef _OPENMP
		#pragma omp parallel for default(none) schedule(static) \
			private(i , node , oldV , V) \
			shared (home  )
		#endif
		for (i=0; i < home->newNodeKeyPtr; i++) {
			node = home->nodeKeys[i];
			if (node == (Node_t *)NULL) continue;
			if (node->subgroup == 0) continue;

		//	oldV[0] = node->oldvXsub1;
		//	oldV[1] = node->oldvYsub1;
		//	oldV[2] = node->oldvZsub1;
			oldV[0] = node->oldvX;
			oldV[1] = node->oldvY;
			oldV[2] = node->oldvZ;
			V[0] = node->vX;
			V[1] = node->vY;
			V[2] = node->vZ;
			if (DotProduct(oldV,V)<0) node->subgroup = 0 ;
		}
		
		node = home->ghostNodeQ;
		while (node) {
			if (node->subgroup == 0) {
				node = node->next;
				continue;
			}
			oldV[0] = node->oldvX;
			oldV[1] = node->oldvY;
			oldV[2] = node->oldvZ;
			V[0] = node->vX;
			V[1] = node->vY;
			V[2] = node->vZ;
			if (DotProduct(oldV,V)<0) node->subgroup = 0 ;
			node = node->next;
		}

		//TimerStop(home, FORWARD_PROGRES_CHECK);
		return;
}

/*******************************************************************************
 *******************************************************************************
 *******************************************************************************
 *******************************************************************************/

void FlagSubcycleNodes(Home_t *home, int subGroup)
{
		int         i, j ,nSeg, nSegSeg;
        Segm_t      *SegList;
        SegSeg_t    *SegSegList;
		Node_t      *node;
		Subcyc_t    *subcyc;
		
		//TimerStart(home, FLAG_SUBCYC_NODES);
		subcyc = home->subcyc;

/*
 *      In the FULL case, just flag everything and return.
 */
        if (subGroup == FULL) {
		    #ifdef _OPENMP
			#pragma omp parallel for default(none) schedule(static) \
				private(i , j , node) \
				shared (home  )
			#endif
			for (i=0; i < home->newNodeKeyPtr; i++) {
			    if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
                node->subgroup = 1;
				
                for (j = 0 ; j < 5 ; j++) node->CommSend[j] = 1;
		    }

		    node = home->ghostNodeQ;
		    while (node) {
			    node->subgroup = 1;
			    node = node->next;
		    }
            return;
        }
/*
 *      Initially set all native nodes for subcycling.
 *		Ghost nodes are initially unflaged and then flaged below if appropriate
 */
		
		#ifdef _OPENMP
		#pragma omp parallel for default(none) schedule(static) \
			private(i , node) \
			shared (home  )
		#endif
		for (i=0; i < home->newNodeKeyPtr; i++) {
			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
            node->subgroup = 1;
        //    node->CommSend = 0;
		}

		node = home->ghostNodeQ;
		while (node) {
			node->subgroup = 0;
			node = node->next;
		}

/*
 *      Loop over the appropriate seg and seg-seg lists and flag all
 *      nodes for subcycling.
 */
		if (subGroup == GROUP0) {
			SegList    = subcyc->SegListG0;
			nSeg       = subcyc->SegListG0_cnt;
			SegSegList = subcyc->SegSegListG0;
			nSegSeg    = subcyc->SegSegListG0_cnt;
			
		} else if (subGroup == GROUP1) {
			SegList    = subcyc->SegListG1;
			nSeg       = subcyc->SegListG1_cnt;
			SegSegList = subcyc->SegSegListG1;
			nSegSeg    = subcyc->SegSegListG1_cnt;
			
		} else if (subGroup == GROUP2) {
			nSeg       = 0;
			SegSegList = subcyc->SegSegListG2;
			nSegSeg    = subcyc->SegSegListG2_cnt;
			
		} else if (subGroup == GROUP3) {
			nSeg       = 0;
			SegSegList = subcyc->SegSegListG3;
			nSegSeg    = subcyc->SegSegListG3_cnt;
			
		} else if (subGroup == GROUP4) {
			nSeg       = 0;
			SegSegList = subcyc->SegSegListG4;
			nSegSeg    = subcyc->SegSegListG4_cnt;
		}

		#ifdef _OPENMP
		#pragma omp parallel for default(none) schedule(dynamic,1) \
			private(i) \
			shared (SegList , nSeg)
		#endif
		for (i=0; i<nSeg; i++) {
			if (SegList[i].flag == 1) {
				SegList[i].seg->node1->subgroup = 1;
				SegList[i].seg->node2->subgroup = 1;
			}
		}

		#ifdef _OPENMP
		#pragma omp parallel for default(none) schedule(dynamic,1) \
			private(i) \
			shared (SegSegList , nSegSeg)
		#endif
		for (i=0; i<nSegSeg; i++) {
			if (SegSegList[i].flag == 1) { 
				SegSegList[i].seg1->node1->subgroup = 1;
				SegSegList[i].seg1->node2->subgroup = 1;
				SegSegList[i].seg2->node1->subgroup = 1;
				SegSegList[i].seg2->node2->subgroup = 1;
			}
		}
	
/*
 *      Loop over the ghost seg list and flag nodes on segments
 *      in the appropriate subGroup for communication.
 */
	/*	for (i=0; i<SegListGhost_cnt; i++) {
			if (SegListGhost[i].flag == subGroup) { 
				SegListGhost[i].seg->node1->CommSend = 1;
				SegListGhost[i].seg->node2->CommSend = 1;
				SegListGhost[i].seg->node1->subgroup = 1;
				SegListGhost[i].seg->node2->subgroup = 1;
			}
		}*/

		//TimerStop(home, FLAG_SUBCYC_NODES);
}

/*******************************************************************************
 *******************************************************************************
 *******************************************************************************
 *******************************************************************************/
#ifdef _THINFILM
void SubcycleIntegratorForceB(Home_t *home, ThinFilm_t *thinfilm)
#else
void SubcycleIntegratorForceB(Home_t *home)
#endif
{
		int       TotalSize , doAll = 1 , mobIterError;
		int       GroupSizeLocal[5] , GroupSizeGlobal[5];
		int       i , j;
		Subcyc_t  *subcyc;
		Param_t	  *param;
		Node_t    *node ;
		struct    timeval WCT00 , WCT0 , WCT1 , WCT2 , WCT3 , WCT4;
		struct    timeval WCTold, WCTnew;
		real8     dWCT0 , dWCT1, dWCT2, dWCT3, dWCT4;

		param  = home->param;
		subcyc = home->subcyc;
		
/*
 * 		When we are using subcycling and a friction stress, we need to
 * 		pre-compute the friction stresses that will be treated as
 * 		nodal back-forces during the time-integration.
 */
		param->inSubcycling = 1;
#ifdef _MOBILITY_FIELD
		if (param->FricStress > 0.0 || param->frictionField == 1) {
#else
		if (param->FricStress > 0.0) {
#endif
			for (i = 0; i < home->newNodeKeyPtr; i++) {
				if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
				Mobility_FCC_0_pre_friction(home, node);
			}
			node = home->ghostNodeQ;
			while (node) {
				Mobility_FCC_0_pre_friction(home, node);
				node = node->next;
			}
		}

/*
 *      Store the current positions.
 */
		for (i = 0; i < home->newNodeKeyPtr; i++) {
			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
			node->oldervX = node->vX;   node->olderx = node->x;
			node->oldervY = node->vY;   node->oldery = node->y;
			node->oldervZ = node->vZ;   node->olderz = node->z;
		}
		node = home->ghostNodeQ;
		while (node) {
			node->oldervX = node->vX;   node->olderx = node->x;
			node->oldervY = node->vY;   node->oldery = node->y;
			node->oldervZ = node->vZ;   node->olderz = node->z;
			
			node = node->next;
		}
		

/*
 *		Making group0 through group4 lists. These lists contain segment
 *		and segment pair information used for force calculations. 
 */
		SegSegListMaker(home, FULL);
		GroupSizeLocal[0] = subcyc->SegSegListG0_cnt + subcyc->SegListG0_cnt;
		GroupSizeLocal[1] = subcyc->SegSegListG1_cnt + subcyc->SegListG1_cnt;
		GroupSizeLocal[2] = subcyc->SegSegListG2_cnt ;
		GroupSizeLocal[3] = subcyc->SegSegListG3_cnt ;
		GroupSizeLocal[4] = subcyc->SegSegListG4_cnt ;
#if PARALLEL		
		MPI_Allreduce(GroupSizeLocal, GroupSizeGlobal, 5, 
		              MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
		for (i=0 ; i<5 ; i++) GroupSizeGlobal[i] = GroupSizeLocal[i];
#endif		
		TotalSize  = GroupSizeGlobal[0] + GroupSizeGlobal[1] + GroupSizeGlobal[2]
		           + GroupSizeGlobal[3] + GroupSizeGlobal[4] ;
		subcyc->Group1Frac = 1.0 * GroupSizeGlobal[1] / TotalSize;
		subcyc->Group2Frac = 1.0 * GroupSizeGlobal[2] / TotalSize;
		subcyc->Group3Frac = 1.0 * GroupSizeGlobal[3] / TotalSize;
		subcyc->Group4Frac = 1.0 * GroupSizeGlobal[4] / TotalSize;
		
	//	if (home->myDomain == 0) gettimeofday( &WCT00, NULL );
	
		int totSeg = subcyc->SegListG0_cnt + subcyc->SegListG1_cnt;
		int totSegSeg = subcyc->SegSegListG0_cnt + subcyc->SegSegListG1_cnt +
		subcyc->SegSegListG2_cnt + subcyc->SegSegListG3_cnt + subcyc->SegSegListG4_cnt;
#if 0
		printf("--Group0 - SegSeg: %d, Seg: %d\n", subcyc->SegSegListG0_cnt, subcyc->SegListG0_cnt);
		printf("--Group1 - SegSeg: %d, Seg: %d\n", subcyc->SegSegListG1_cnt, subcyc->SegListG1_cnt);
		printf("--Group2 - SegSeg: %d, Seg: %d\n", subcyc->SegSegListG2_cnt, 0);
		printf("--Group3 - SegSeg: %d, Seg: %d\n", subcyc->SegSegListG3_cnt, 0);
		printf("--Group4 - SegSeg: %d, Seg: %d\n", subcyc->SegSegListG4_cnt, 0);
		printf("--TOTAL  - SegSeg: %d, Seg: %d\n", totSegSeg, totSeg);
#else
		printf(" Time integration of %d interactions...\n", totSegSeg);
#endif


#ifdef _GPU_SUBCYCLE
/*
 * 		GPU SUBCYCLING: Call the GPU subcycling integrator. Note that there
 *      are currently some limitations to the use of the GPU subcycling,
 *      e.g. it can only be used in serial mode. See SubcycleGPU.cu for
 *      more information.
 */
		{
/*
 *			Perform subcycling on the GPU
 */
			TimerStart(home, SUBCYCLING_GPU);
#ifdef _THINFILM
			SubcycleIntegratorGPU(home, thinfilm);
#else
			SubcycleIntegratorGPU(home);
#endif
			TimerStop(home, SUBCYCLING_GPU);

/*
 *			We are done with subcycling. Now restore the old positions
 *			needed for topological changes.
 */
			CommSendCoord(home, FULL);

			for (i=0; i < home->newNodeKeyPtr; i++) {
				if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;

				node->oldx = node->olderx;   node->oldvX = node->oldervX;
				node->oldy = node->oldery;   node->oldvY = node->oldervY;
				node->oldz = node->olderz;   node->oldvZ = node->oldervZ;
			}

			return;
		}

#else
		if (strcmp(param->subInteg0Integ1, "GPU") == 0) {
			Fatal("GPU subcycling can only be used with compile flag -D_GPU_SUBCYCLE");
		}
#endif


/*
 *		Flag the nodes necessary for GROUP0 subcycling and update the forces
 *		and velocities.
 */
		FlagSubcycleNodes(home, GROUP0);

#ifdef _THINFILM
        NodeForce(home, thinfilm, GROUP0);
#else
        NodeForce(home, GROUP0);
#endif
        mobIterError = CalcNodeVelocities(home, 0, doAll);
        CommSendVelocitySub(home, GROUP0);
		
	//	printf("myDomain = %d , GroupSizeLocal = %7d %5d %5d %5d %5d\n", home->myDomain, GroupSizeLocal[0],
	//	        GroupSizeLocal[1], GroupSizeLocal[2], GroupSizeLocal[3], GroupSizeLocal[4]);

/*
 *		Time integrate group0 forces.
 */	
		if (strcmp(param->subInteg0Integ1, "RKF-RKF") == 0) {
#ifdef _THINFILM
			if (GroupSizeGlobal[0] > 0) RKFIntegrator(home, thinfilm, GROUP0);
			else                        RKFIntegrator(home, thinfilm, GROUP0);
#else
			if (GroupSizeGlobal[0] > 0) RKFIntegrator(home, GROUP0);
			else                        RKFIntegrator(home, GROUP0  );
#endif
		} else {
			Fatal("SubcycleIntegratorForceB only available for RKF-RKF scheme");
		}
		
/*
 *		It is likely that the size of group 4 changed during the 
 *		time-integration of group 0. So adjust its global value 
 *		before proceeding with subcycling.
 */
		if ((param->nTry > 0) && (GroupSizeGlobal[0] > 0)) {
#if PARALLEL
			MPI_Allreduce(&(subcyc->SegSegListG4_cnt), 
			              &(GroupSizeGlobal[4]), 1, 
		                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
			GroupSizeGlobal[4] = subcyc->SegSegListG4_cnt;
#endif
			subcyc->Group4Frac = 1.0 * GroupSizeGlobal[4] / TotalSize;
		}
		
/*
 *		Storing nodal forces corresponding to group0. 
 */
		for (i=0; i < home->newNodeKeyPtr; i++) {
			if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
			node->olderfX = node->fX;
			node->olderfY = node->fY;
			node->olderfZ = node->fZ;
		}
		node = home->ghostNodeQ;
		while (node) {			
			node->olderfX = node->fX;
			node->olderfY = node->fY;
			node->olderfZ = node->fZ;
			node = node->next;
		}
		

/*
 *		Precompute long-range stresses before subcycling
 */		
		subcyc->remoteSigbSub = 1; // WARNING: not defined everywhere else!
								   // 0: do not pre-compute remote sigb
								   // 1: pre-compute remote sigb at segments mid-point
								   // 2: pre-compute remote sigb at segments integration points (FMM only)
		if (subcyc->remoteSigbSub > 0) {
			TimerStart(home, REMOTE_FORCE);
			subcyc->sigbRem = (real8**)malloc(subcyc->SegListG1_cnt*sizeof(real8*));
			
			if (param->fmEnabled == 0) {
				PreComputeSegSigbRem(home);
			} else {
				//if (param->forceCutOff == 0) {
					//PreComputeSegSigbRemFMM(home);
				//}
			}
			TimerStop(home, REMOTE_FORCE);
		}

/*
 *		Time integrate group 1, 2, 3 and 4 interactions (subcycle).
 */		
        real8   subTime1, subTime2, subTime3, subTime4;
        real8   totalsubDT, nextDTsub, oldDTsub, newDTsub;
        int     subGroup, cutDT;

        //Initialize the time for each group based on whether it has any forces in it
        if (GroupSizeGlobal[1] > 0) subTime1 = 0.0;
        else                        subTime1 = param->realdt;
		
        if (GroupSizeGlobal[2] > 0) subTime2 = 0.0;
        else                        subTime2 = param->realdt;
		
        if (GroupSizeGlobal[3] > 0) subTime3 = 0.0;
        else                        subTime3 = param->realdt;
		
        if (GroupSizeGlobal[4] > 0) subTime4 = 0.0;
        else                        subTime4 = param->realdt;
		
        //Initialize some other stuff
		if ( home->cycle==0 ) nextDTsub = param->realdt;
		totalsubDT = 0.0;
        subcyc->numSubCycle1 = 0;
        subcyc->numSubCycle2 = 0;
        subcyc->numSubCycle3 = 0;
        subcyc->numSubCycle4 = 0;
        int oldGroup = -1;
        int nSubcyc;
        
        //Allocate memory for array where "old" velocities can be stored,
        //to be used with the forward progress check.
        real8 **oldV, **oldVghost;
        //Native nodes
		oldV = malloc(home->newNodeKeyPtr * sizeof(real8 *));
		for ( i = 0 ; i < home->newNodeKeyPtr ; i++ ) {
			oldV[i] = malloc(3 * sizeof(real8));
		}
        //Ghost nodes
        int ghostsize1 = home->newNodeKeyPtr, ghostsize2;
        if (ghostsize1 == 0) ghostsize1 = 1;
		oldVghost = malloc(ghostsize1 * sizeof(real8 *));
        i = 0;
        node = home->ghostNodeQ;
        while (node) {
            oldVghost[i] = malloc(3 * sizeof(real8));
            i++;
            if (i >= ghostsize1) {
                ghostsize1 *= 2;			
                oldVghost = realloc(oldVghost, ghostsize1 * sizeof(real8 *));
            }
	        node = node->next;
        }
        ghostsize2 = i-1;		
		
/*
 *		Subcycle until the subcycle group times (subTimei) catch up
 *		with the global group time (realdt). Note that nodal forces 
 *		will reset to zero when subcycling is performed
 */
		while ( subTime1 < param->realdt || subTime2 < param->realdt ||
                subTime3 < param->realdt || subTime4 < param->realdt ) {
            cutDT = 0;

            //The group that is furthest behind goes first
            if        ( subTime4 <= subTime3 && subTime4 <= subTime2 && subTime4 <= subTime1 ) {
                subGroup   = GROUP4;
                nextDTsub  = param->nextDTsub4;
                totalsubDT = subTime4;
				
            } else if ( subTime3 < subTime4 && subTime3 <= subTime2 && subTime3 <= subTime1 ) {
                subGroup   = GROUP3;
                nextDTsub  = param->nextDTsub3;
                totalsubDT = subTime3;
				
            } else if ( subTime2 < subTime4 && subTime2 < subTime3 && subTime2 <= subTime1 ) {
                subGroup   = GROUP2;
                nextDTsub  = param->nextDTsub2;
                totalsubDT = subTime2;
				
            } else {
                subGroup   = GROUP1;
                nextDTsub  = param->nextDTsub;
                totalsubDT = subTime1;
            }
    
#if 1
			if (subGroup == GROUP1) {

				TimerStart(home, REMOTE_FORCE);
#ifdef _THINFILM
				InitSegSigbRem(home, FULL);
#endif

#ifndef FULL_N2_FORCES
				if (param->fmEnabled == 0) {
					if (subcyc->remoteSigbSub > 0) {
						RetrieveSegSigbRem(home);
					} else {
						ComputeSegSigbRem(home, FULL);
					}
				}
#endif
				TimerStop(home, REMOTE_FORCE);
				
#ifdef _THINFILM
/*		
 *      		When _THINFILM is enabled with subcycling, pre-compute
 * 				image stress contributions before subcycling group 1 to
 * 				save computation time.
 */
				TimerStart(home, IMAGE_FORCE);
				ComputeTFSegSigbRem(home, thinfilm, FULL);
				TimerStop(home, IMAGE_FORCE);
#endif
			}
#endif
    
            //If we switched groups, reset subcycle count and toggle all nodes on
            //for subcycling. Also do bookkeeping with the wall clock times.
            if (subGroup != oldGroup) {

                nSubcyc = 0;
                
				//Flag appropriate nodes for subcycling, they may be unflagged during the forward
				//progress check.
				FlagSubcycleNodes(home, subGroup);

                //Update positions of ghost nodes so they are correct for the new subcycle group.
                CommSendCoord(home, subGroup);

                //Update the forces and velocities using the new group
#ifdef _THINFILM
		        NodeForce(home, thinfilm, subGroup);
#else
		        NodeForce(home, subGroup);
#endif
		        mobIterError = CalcNodeVelocities(home, 0, doAll);
                CommSendVelocitySub(home, subGroup);
            }

            oldGroup = subGroup;

            //Make sure we don't pass the global group in time
			if ( totalsubDT + nextDTsub > param->realdt ) {
				oldDTsub  = nextDTsub;
				nextDTsub = param->realdt - totalsubDT;
				newDTsub  = nextDTsub;					
				cutDT     = 1;
				
				if      (subGroup == GROUP1) param->nextDTsub  = nextDTsub;
				else if (subGroup == GROUP2) param->nextDTsub2 = nextDTsub;
				else if (subGroup == GROUP3) param->nextDTsub3 = nextDTsub;
				else if (subGroup == GROUP4) param->nextDTsub4 = nextDTsub;
			}
			
            //Time integrate the chosen group for one subcycle
			if (strcmp(param->subInteg0Integ1, "RKF-RKF") == 0) {
#ifdef _THINFILM
				RKFIntegrator(home, thinfilm, subGroup);
#else
				RKFIntegrator(home, subGroup);
#endif
			} else {
				Fatal("SubcycleIntegratorForceB only available for RKF-RKF scheme");
			}
			
            //Perform forward progress check
			if (nSubcyc > 3) ForwardProgressCheckForceB(home);
			nSubcyc++;

            //Do bookkeeping on the time step and number of subcycles
			if        (subGroup == GROUP1) {
				if (cutDT && param->realdtsub == newDTsub) param->nextDTsub = oldDTsub;
				subTime1 += param->realdtsub;
				subcyc->numSubCycle1++;
			} else if (subGroup == GROUP2) {
				if (cutDT && param->realdtsub2 == newDTsub) param->nextDTsub2 = oldDTsub;
				subTime2 += param->realdtsub2;
				subcyc->numSubCycle2++;
			} else if (subGroup == GROUP3) {
				if (cutDT && param->realdtsub3 == newDTsub) param->nextDTsub3 = oldDTsub;
				subTime3 += param->realdtsub3;
				subcyc->numSubCycle3++;
			} else if (subGroup == GROUP4) {
				if (cutDT && param->realdtsub4 == newDTsub) param->nextDTsub4 = oldDTsub;
				subTime4 += param->realdtsub4;
				subcyc->numSubCycle4++;
			}
		}

/*
 *      Make sure all of the ghost node positions are up to date.
 */
       // FlagSubcycleNodes(home, FULL);
        CommSendCoord(home, FULL);
			
/*
 *			We are done with subcycling. Now restore the old positions
 *			and update the mobilities
 */		
			for (i=0; i < home->newNodeKeyPtr; i++) {
				if ((node = home->nodeKeys[i]) == (Node_t *)NULL) continue;
				
				node->oldx = node->olderx;   node->oldvX = node->oldervX;
				node->oldy = node->oldery;   node->oldvY = node->oldervY;
				node->oldz = node->olderz;   node->oldvZ = node->oldervZ;
				
			/*	node->fX   = node->olderfX;
				node->fY   = node->olderfY;
				node->fZ   = node->olderfZ;
			*/
			}
			
			node = home->ghostNodeQ;
			while (node) {
				node->oldx = node->olderx;   node->oldvX = node->oldervX;
				node->oldy = node->oldery;   node->oldvY = node->oldervY;
				node->oldz = node->olderz;   node->oldvZ = node->oldervZ;
				
			/*	node->fX   = node->olderfX;
				node->fY   = node->olderfY;
				node->fZ   = node->olderfZ;*/
				node = node->next;
			}
#ifdef _THINFILM
			NodeForce(home, thinfilm, FULL);
#else
			NodeForce(home, FULL);
#endif
			mobIterError = CalcNodeVelocities(home, 0, doAll);
			CommSendVelocity(home);
//		}

		/* Indicate that we are living the subcycling integration */
		param->inSubcycling = 0;
		
#if 0
		if (home->myDomain == 0) {
		//	printf("%i %i %i %i subcycles\n",subcyc->numSubCycle1, subcyc->numSubCycle2, subcyc->numSubCycle3, subcyc->numSubCycle4);
			FILE   *fp;
			/*
			fp = fopen("dWCT", "a");
			fprintf(fp, "%f , %e %i %f , %e %i %f , %e %i %f  , %e %i %f\n", dWCT0 , 
						subcyc->Group1Frac , subcyc->numSubCycle1 , dWCT1 ,
						subcyc->Group2Frac , subcyc->numSubCycle2 , dWCT2 ,
						subcyc->Group3Frac , subcyc->numSubCycle3 , dWCT3 ,
						subcyc->Group4Frac , subcyc->numSubCycle4 , dWCT4 );
			*/
			fp = fopen("subcycling.txt", "a");
			fprintf(fp, "%d %d %d %d %d %d\n", home->cycle, 1,
					subcyc->numSubCycle1, subcyc->numSubCycle2, 
					subcyc->numSubCycle3, subcyc->numSubCycle4);
			fclose(fp);
		}
#endif
		
/*
 *		Freeing memory
 */ 
		if (subcyc->remoteSigbSub > 0) {
			for (i = 0; i < subcyc->SegListG1_cnt; i++) {
				free(subcyc->sigbRem[i]);
			}
			free(subcyc->sigbRem);
		}

        for (i=0; i < home->newNodeKeyPtr; i++) {
			free( oldV[i] );
		}
		free( oldV );

        for (i=0; i < ghostsize2; i++) {
			free( oldVghost[i] );
		}
		free( oldVghost );

}
#endif
