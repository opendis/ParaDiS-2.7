/**************************************************************************
 *
 *  Function    : Mobility_FCC_0
 *  Author      : Wei Cai, Seok-Woo Lee (updated 07/14/09)
 *  Description : Generic Mobility Law of FCC metals
 *                Each line has a glide plane from input
 *                and it never changes
 *                If the plane normal is not of {111} type, dislocation
 *                motion is constrained along line direction
 *                If node flag == 7, node velocity is zero
 *                Added friction stress (Nicolas)
 *
 *  Returns:  0 on success
 *            1 if velcoity could not be determined
 *
 ***************************************************************************/

#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#define _ENABLE_LINE_CONSTRAINT 1

#ifdef _MOBILITY_FIELD
/*-------------------------------------------------------------------------
 *
 *      Function:     InterpolateMobilityField
 *      Description:  Interpolate the mobility field from tabulated values
 *
 *-----------------------------------------------------------------------*/
void InterpolateMobilityField(Home_t *home, double px, double py, double pz, double *Mf)
{
		int      i, j, k;
		int      nx, ny, nz, npd, npe, g[3];
		double   p[3], q[3], xi[3], H[3];
		Param_t  *param;

		param = home->param;

		*Mf = 0.0;

		// Trilinear interpolation
		nx = param->mobilityFieldNx;
		ny = param->mobilityFieldNy;
		nz = param->mobilityFieldNz;
		npd = 2;
		if (nz == 1) {
			npe = npd*npd;
		} else {
			npe = npd*npd*npd;
		}

		H[0] = param->Lx / nx;
		H[1] = param->Ly / ny;
		H[2] = param->Lz / nz;

		p[0] = px - param->minSideX - 0.5*H[0];
		p[1] = py - param->minSideY - 0.5*H[1];
		p[2] = pz - param->minSideZ - 0.5*H[2];

		q[0] = p[0] / H[0];
		q[1] = p[1] / H[1];
		q[2] = p[2] / H[2];

		g[0] = (int)floor(q[0]);
		g[1] = (int)floor(q[1]);
		g[2] = (int)floor(q[2]);

		xi[0] = 2.0*(q[0]-g[0]) - 1.0;
		xi[1] = 2.0*(q[1]-g[1]) - 1.0;
		xi[2] = 2.0*(q[2]-g[2]) - 1.0;

		// Apply PBC in the x,y,z directions
		int ind1d[3][npd];
		for (i = 0; i < npd; i++) {
			ind1d[0][i] = ((npd-1)*g[0]+i)%nx;
			if (ind1d[0][i] < 0) ind1d[0][i] += nx;
			ind1d[1][i] = ((npd-1)*g[1]+i)%ny;
			if (ind1d[1][i] < 0) ind1d[1][i] += ny;
			ind1d[2][i] = ((npd-1)*g[2]+i)%nz;
			if (ind1d[2][i] < 0) ind1d[2][i] += nz;
		}

		// 1d shape functions
		double phi1d[3][npd];
		for (i = 0; i < 3; i++) {
			phi1d[i][0] = 0.5*(1.0-xi[i]);
			phi1d[i][1] = 0.5*(1.0+xi[i]);
		}

		int ind[npe];
		double phi[npe];
		if (nz > 1) {
			// 3d shape functions and indices
			for (k = 0; k < npd; k++) {
				for (j = 0; j < npd; j++) {
					for (i = 0; i < npd; i++) {
						phi[i+j*npd+k*npd*npd] = phi1d[0][i]*phi1d[1][j]*phi1d[2][k];
						ind[i+j*npd+k*npd*npd] = ind1d[0][i] + ind1d[1][j]*nx + ind1d[2][k]*nx*ny;
					}
				}
			}
		} else {
			// 2d shape functions and indices
			for (j = 0; j < npd; j++) {
				for (i = 0; i < npd; i++) {
					phi[i+j*npd] = phi1d[0][i]*phi1d[1][j];
					ind[i+j*npd] = ind1d[0][i] + ind1d[1][j]*nx;
				}
			}
		}

		// Interpolate from the grid points
		*Mf = 0.0;
		for (j = 0; j < npe; j++) {
			*Mf += phi[j] * param->mobilityFieldPhi[ind[j]];
		}

		return;
}
#endif

#ifdef _GPU_SUBCYCLE
/*-------------------------------------------------------------------------
 *
 *      Function:     Mobility_FCC_0_matrix_GPU
 *      Description:  This function precomputes a nodal mobility matrix
 *                    operator lumping the glide and line constraints
 *                    to evaluate the mobility from forces.
 *
 *-----------------------------------------------------------------------*/
int Mobility_FCC_0_matrix_GPU(Home_t *home, Node_t *node, double mobMatrix[3][3])
{
		int numNonZeroLenSegs = 0;
		Param_t *param;
		real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
		int i, j, k, nc, nconstraint, nlc;
		real8 normX[100], normY[100], normZ[100];
		real8 normx[100], normy[100], normz[100];
		real8 lineX[100], lineY[100], lineZ[100];
		real8 a, b;
		real8 dx, dy, dz, lx, ly, lz, lr, LtimesB;
		real8 lcx, lcy, lcz, normdotlc, lc[3];
		Node_t *nbr;
		real8 MobScrew, MobEdge, Mob;
		real8 bx, by, bz, br, dangle;
		real8 nForce[3];

		param = home->param;

		MobScrew = param->MobScrew;
		MobEdge  = param->MobEdge;

		nc = node->numNbrs;

/*
 *  	Initialize mobility matrix operator
 */
		//node->mobMatrixUse = 1;
		for (i = 0; i < 3; i++)
			for (j = 0; j < 3; j++)
				mobMatrix[i][j] = 0.0;

/*
 *  	If node is 'pinned' in place by constraints, or the node has any arms
 *  	with a burgers vector that has explicitly been set to be sessile (via
 *  	control file inputs), the node may not be moved so just zero the velocity
 *  	and return
 */
		if ((node->constraint == PINNED_NODE) || NodeHasSessileBurg(home, node)) {
			return 0;
		}
/*
 * 		If we are using the subcycling integrator and a friction stress, we need
 * 		to zero the velocity of nodes whose total force is below the friction force.
 */
#ifdef _MOBILITY_FIELD
		if (param->FricStress > 0.0 || param->frictionField == 1) {
#else
		if (param->FricStress > 0.0) {
#endif
			if (strcmp(param->timestepIntegrator, "forceBsubcycle") == 0) {
				if (node->fricPinned) return 0;
			}
		}

/*
 *  	It's possible this function was called for a node which had only zero-
 *  	length segments (during SplitSurfaceNodes() for example).  If that is
 *  	the case, just set the velocity to zero and return.
 */
		for (i = 0; i < nc; i++) {
			if ((nbr = GetNeighborNode(home, node, i)) == (Node_t *)NULL) continue;
			dx = node->x - nbr->x;
			dy = node->y - nbr->y;
			dz = node->z - nbr->z;
			if ((dx*dx + dy*dy + dz*dz) > 1.0e-12) {
				numNonZeroLenSegs++;
			}
		}

		if (numNonZeroLenSegs == 0) {
			return 0;
		}


/*
 *      Copy glide plane constraints and determine
 *      line constraints.
 */
		for(i=0;i<nc;i++) {

			normX[i] = normx[i] = node->nx[i];
			normY[i] = normy[i] = node->ny[i];
			normZ[i] = normz[i] = node->nz[i];

//#ifndef _THINFILM
/*
 *      	If needed, rotate the glide plane normals from the
 *      	laboratory frame to the crystal frame.
 */
			if (param->useLabFrame) {
				real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
				real8 normRot[3];

				Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);

				normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
				normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
			}
//#endif
			if ( (fabs(fabs(normX[i]) - fabs(normY[i])) > FFACTOR_NORMAL) ||
                 (fabs(fabs(normY[i]) - fabs(normZ[i])) > FFACTOR_NORMAL) ) {

				/* not {111} plane */
				if ((nbr=GetNeighborNode(home,node,i)) == (Node_t *)NULL) {
					Fatal("Neighbor not found at %s line %d\n",__FILE__,__LINE__);
				}

				lineX[i] = nbr->x - node->x;
				lineY[i] = nbr->y - node->y;
				lineZ[i] = nbr->z - node->z;
				ZImage (param, lineX+i, lineY+i, lineZ+i);

//#ifndef _THINFILM
/*
 *          	If needed, rotate the line sense from the laboratory frame to
 *          	the crystal frame.
 */
				if (param->useLabFrame) {
					real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
					real8 lDirRot[3];

					Matrix33Vector3Multiply(home->rotMatrixInverse, lDir, lDirRot);

					lineX[i] = lDirRot[0];
					lineY[i] = lDirRot[1];
					lineZ[i] = lDirRot[2];
				}
//#endif
			} else {
				/* no line constraint */
				lineX[i] = lineY[i] = lineZ[i] = 0;
			}
		}

#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	    
        
        real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		lineX[nc] = 0.0; lineY[nc] = 0.0; lineZ[nc] = 0.0;
		nc++;
    }
#endif

/*
 * 		Normalize glide plane normal vectors
 *		and lc line vectors
 */
		for(i=0;i<nc;i++) {

			a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
			b=sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

			if(a>0) {
				normX[i]/=a;
				normY[i]/=a;
				normZ[i]/=a;

				normx[i]/=a;
				normy[i]/=a;
				normz[i]/=a;
			}
			if(b>0) {
				lineX[i]/=b;
				lineY[i]/=b;
				lineZ[i]/=b;
			}
		}

/*
 * 		Find independent glide constraints
 */
		nconstraint = nc;
		for(i=0;i<nc;i++) {
			for(j=0;j<i;j++) {
				Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
			}
			if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH) {
				normX[i] = normY[i] = normZ[i] = 0;
				nconstraint--;
			}
		}

/*
 * 		Find independent line constraints
 */
		nlc = 0;
		for(i=0;i<nc;i++) {
			for(j=0;j<i;j++) {
				Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
			}
			if((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH) {
				lineX[i] = lineY[i] = lineZ[i] = 0;
			}
			else {
				nlc++;
			}
		}

/*
 * 		Do not use mobility matrix if node has one line constraint
 */
		if (nlc == 1) {
			//node->mobMatrixUse = 0;
			// DO SOMETHING HERE...
			//return 0;
		}

/*
 *  	Velocity is simply proportional to total force per unit length
 */
		for (i = 0; i < 3; i++)
			mobMatrix[i][i] = 1.0;

/*
 *  	Orthogonalize with glide plane constraints
 */
		for(i=0;i<nc;i++) {
			if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0)) {

				real8 OrthMatrix[3][3], tmpMob[3][3];
				a = normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i];

				OrthMatrix[0][0] = 1.0-normX[i]*normX[i]/a;
				OrthMatrix[0][1] = -normX[i]*normY[i]/a;
				OrthMatrix[0][2] = -normX[i]*normZ[i]/a;

				OrthMatrix[1][0] = -normY[i]*normX[i]/a;
				OrthMatrix[1][1] = 1.0-normY[i]*normY[i]/a;
				OrthMatrix[1][2] = -normY[i]*normZ[i]/a;

				OrthMatrix[2][0] = -normZ[i]*normX[i]/a;
				OrthMatrix[2][1] = -normZ[i]*normY[i]/a;
				OrthMatrix[2][2] = 1.0-normZ[i]*normZ[i]/a;

				Matrix33Mult33(OrthMatrix, mobMatrix, tmpMob);

				for (j = 0; j < 3; j++)
					for (k = 0; k < 3; k++)
						mobMatrix[j][k] = tmpMob[j][k];
			}
		}


/*
 * 		Any dislocation with glide plane not {111} type
 * 		can only move along its length. This rule includes
 * 		LC junction which is on {100} plane
 */
#if _ENABLE_LINE_CONSTRAINT

		if(nlc==1) {

			/* only one line constraint */
			for(i=0;i<nc;i++) {
				if((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0)) {
					lc[0] = lineX[i];
					lc[1] = lineY[i];
					lc[2] = lineZ[i];
					break;
				}
			}

			real8 ProjMatrix[3][3], tmpMob[3][3];
			Vec3TransposeAndMult(lc, ProjMatrix);

			Matrix33Mult33(ProjMatrix, mobMatrix, tmpMob);

			for (j = 0; j < 3; j++)
				for (k = 0; k < 3; k++)
					mobMatrix[j][k] = tmpMob[j][k];

			if (nconstraint<=0) {
				//Fatal("MobilityLaw_FCC_0: nconstraint <= 0, nlc = 1 is impossible!");

				/* set velocity to zero if line is not on every plane */
				for (j = 0; j < 3; j++)
					for (k = 0; k < 3; k++)
						mobMatrix[j][k] = 0.0;

			} else if(nconstraint>=1) {
				/* a few plane constraints and one line constraint */
				for(i=0;i<nc;i++) {
					normdotlc = normx[i]*lc[0] + normy[i]*lc[1] + normz[i]*lc[2];
					if(fabs(normdotlc)>FFACTOR_ORTH) {

						/* set velocity to zero if line is not on every plane */
						for (j = 0; j < 3; j++)
							for (k = 0; k < 3; k++)
								mobMatrix[j][k] = 0.0;

						break;
					}
				}
			}
		} else if (nlc>=2) {

			/* Velocity is zero when # of independnet lc
			 * constratins is equal to or more than 2 */
			for (j = 0; j < 3; j++)
				for (k = 0; k < 3; k++)
					mobMatrix[j][k] = 0.0;
		}
#endif

#ifdef _MOBILITY_FIELD
	double Mf = 1.0;
	if (param->mobilityField == 1) {

		InterpolateMobilityField(home, node->x, node->y, node->z, &Mf);
		Mf = (1.0-Mf);

		double xm = param->mobilityFieldScale;
		for (j = 0; j < 3; j++)
			for (k = 0; k < 3; k++)
				mobMatrix[j][k] *= ((1.0-xm) + xm*Mf);
	}
#endif

		return 0;
}
#endif

/*-------------------------------------------------------------------------
 *
 *      Function:     Mobility_FCC_0_pre_friction
 *      Description:  This function pre-calculates the friction stress to
 * 					  be handled as a nodal back-force when using the
 * 					  subcycling integrator.
 *
 *-----------------------------------------------------------------------*/
void Mobility_FCC_0_pre_friction(Home_t *home, Node_t *node)
{
		int     j;
		double  dx, dy, dz, lr;
		double  bx, by, bz, br;
		double  fricStress, FricForce;
		double  nForce[3], nFmag;
		Node_t  *nbr;
		Param_t *param;

		param = home->param;

		/* Default values */
		node->fricPinned = 0;
		node->fricForce[0] = 0.0;
		node->fricForce[1] = 0.0;
		node->fricForce[2] = 0.0;

#ifdef _MOBILITY_FIELD
		if (param->frictionField == 1) {
			InterpolateMobilityField(home, node->x, node->y, node->z, &fricStress);
			fricStress *= param->mobilityFieldScale;
		} else
#endif
		{
			fricStress = param->FricStress;
		}

		if (fricStress > 0.0) {

			/* Computes friction force */
			FricForce = 0.0;
			for(j = 0; j < node->numNbrs; j++) {

				if ((nbr=GetNeighborNode(home,node,j)) == (Node_t *)NULL) continue;
				dx=nbr->x - node->x;
				dy=nbr->y - node->y;
				dz=nbr->z - node->z;
				ZImage (param, &dx, &dy, &dz);
				lr=sqrt(dx*dx+dy*dy+dz*dz);

				if (lr != 0) {
					bx = node->burgX[j];
					by = node->burgY[j];
					bz = node->burgZ[j];
					br = sqrt(bx*bx+by*by+bz*bz);

					FricForce += fricStress*br*lr;
				}
			}
			FricForce /= 2.0;

			nForce[0] = node->fX;
			nForce[1] = node->fY;
			nForce[2] = node->fZ;
			nFmag = sqrt(nForce[0]*nForce[0]+nForce[1]*nForce[1]+nForce[2]*nForce[2]);

			if (nFmag > FricForce) {
				node->fricPinned = 0;
				node->fricForce[0] = -nForce[0]*FricForce/nFmag;
				node->fricForce[1] = -nForce[1]*FricForce/nFmag;
				node->fricForce[2] = -nForce[2]*FricForce/nFmag;
			} else {
				node->fricPinned = 1;
			}
		}

		return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     Mobility_FCC_0
 *      Description:  This function computes the velocity of a node from
 *                    the force in the case of FCC crystals.
 *
 *-----------------------------------------------------------------------*/
int Mobility_FCC_0(Home_t *home, Node_t *node)
{
    int numNonZeroLenSegs = 0;
    Param_t *param;
    real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
    int i, j, nc, nconstraint, nlc;
    real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
    real8 lineX[100], lineY[100], lineZ[100];
    real8 a, b;
    real8 dx, dy, dz, lx, ly, lz, lr, LtimesB, fricStress;
    real8 lcx, lcy, lcz, normdotlc;
    Node_t *nbr;
    real8 MobScrew, MobEdge, Mob;
    real8 bx, by, bz, br, dangle;
    real8 nForce[3], nFmag, FricForce;

    param = home->param;

    MobScrew = param->MobScrew;
    MobEdge  = param->MobEdge;
    
    nc = node->numNbrs;


    
/*
 *  If node is 'pinned' in place by constraints, or the node has any arms 
 *  with a burgers vector that has explicitly been set to be sessile (via
 *  control file inputs), the node may not be moved so just zero the velocity
 *  and return
 */
    if ((node->constraint == PINNED_NODE) ||
        NodeHasSessileBurg(home, node))
    {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }

/*
 *  It's possible this function was called for a node which had only zero-
 *  length segments (during SplitSurfaceNodes() for example).  If that is
 *  the case, just set the velocity to zero and return.
 */
    for (i = 0; i < nc; i++) {
        if ((nbr = GetNeighborNode(home, node, i)) == (Node_t *)NULL) continue;
        dx = node->x - nbr->x;
        dy = node->y - nbr->y;
        dz = node->z - nbr->z;
        if ((dx*dx + dy*dy + dz*dz) > 1.0e-12) {
            numNonZeroLenSegs++;
        }
    }

    if (numNonZeroLenSegs == 0) {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }


    /* copy glide plane constraints and determine line constraints */
    for(i=0;i<nc;i++)
    {
        normX[i] = normx[i] = node->nx[i];
        normY[i] = normy[i] = node->ny[i];
        normZ[i] = normz[i] = node->nz[i];

//#ifndef _THINFILM
/*
 *      If needed, rotate the glide plane normals from the
 *      laboratory frame to the crystal frame.
 */
        if (param->useLabFrame) {
            real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
            real8 normRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);
            
            normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
            normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
        }
//#endif
		if ( (fabs(fabs(normX[i]) - fabs(normY[i])) > FFACTOR_NORMAL) ||
             (fabs(fabs(normY[i]) - fabs(normZ[i])) > FFACTOR_NORMAL) )
        {	
			/* not {111} plane */		
            if ((nbr=GetNeighborNode(home,node,i)) == (Node_t *)NULL) {
                Fatal("Neighbor not found at %s line %d\n",__FILE__,__LINE__);
            }
            lineX[i] = nbr->x - node->x;
            lineY[i] = nbr->y - node->y; 
            lineZ[i] = nbr->z - node->z;
            ZImage (param, lineX+i, lineY+i, lineZ+i);

//#ifndef _THINFILM
/*
 *          If needed, rotate the line sense from the laboratory frame to
 *          the crystal frame.
 */
            if (param->useLabFrame) {
                real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
                real8 lDirRot[3];

                Matrix33Vector3Multiply(home->rotMatrixInverse, lDir, lDirRot);

                lineX[i] = lDirRot[0];
                lineY[i] = lDirRot[1];
                lineZ[i] = lDirRot[2];
            }
//#endif
	}
	else
	{ /* no line constraint */
	    lineX[i] = lineY[i] = lineZ[i] = 0;
	}
    }

#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	    
        
        real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		lineX[nc] = 0.0; lineY[nc] = 0.0; lineZ[nc] = 0.0;
		nc++;
    }
#endif
    
    /* normalize glide plane normal vectors and lc line vectors*/
    for(i=0;i<nc;i++)
    {
        a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
	b=sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

        if(a>0)
        {
            normX[i]/=a;
            normY[i]/=a;
            normZ[i]/=a;

            normx[i]/=a;
            normy[i]/=a;
            normz[i]/=a;
        }
        if(b>0)
        {
            lineX[i]/=b;
            lineY[i]/=b;
            lineZ[i]/=b;
        }
    }


    /* Find independent glide constraints */ 
    nconstraint = nc;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
        }
        if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH)
        {
            normX[i] = normY[i] = normZ[i] = 0;
            nconstraint--;
        }
    }

    /* Find independent line constraints */
    nlc = 0;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
        }
        if((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH)
        {
            lineX[i] = lineY[i] = lineZ[i] = 0;
        }
        else
        {
            nlc ++;
        }
    }
      
    /* find total dislocation length times drag coefficent (LtimesB)*/

#ifdef _MOBILITY_FIELD
	double Mf = 1.0;
	if (param->mobilityField == 1) {
		InterpolateMobilityField(home, node->x, node->y, node->z, &Mf);
		Mf = (1.0-Mf);
	}
	if (param->frictionField == 1) {
		InterpolateMobilityField(home, node->x, node->y, node->z, &fricStress);
		fricStress *= param->mobilityFieldScale;
	} else
#endif
	{
		fricStress = param->FricStress;
	}

    LtimesB=0;
    FricForce = 0.0;
    for(j=0;j<node->numNbrs;j++)
    {
        if ((nbr=GetNeighborNode(home,node,j)) == (Node_t *)NULL) continue;
        dx=nbr->x - node->x;
        dy=nbr->y - node->y;
        dz=nbr->z - node->z;
        ZImage (param, &dx, &dy, &dz) ;

//#ifndef _THINFILM
/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
        if (param->useLabFrame) {
            real8 dTmp[3] = {dx, dy, dz};
            real8 dRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, dTmp, dRot);

            dx = dRot[0]; dy = dRot[1]; dz = dRot[2];
        }
//#endif

        lr=sqrt(dx*dx+dy*dy+dz*dz);
 
        if (lr==0)
        { /* zero arm segment can happen after node split 
           * it is OK to have plane normal vector == 0
           * Skip (do nothing)
           */
        }
        else 
        {
           if((node->nx[j]==0)&&(node->ny[j]==0)&&(node->nz[j]==0))
           {
              printf("Mobility_FCC_0: (%d,%d) glide plane norm = 0\n"
                     "for segment with nonzero length lr = %e!\n",
                     node->myTag.domainID, node->myTag.index, lr);
           }

           lx=dx/lr; ly=dy/lr; lz=dz/lr;

           bx = node->burgX[j];
           by = node->burgY[j];
           bz = node->burgZ[j];

//#ifndef _THINFILM
/*
 *         If needed, rotate the burgers vector from the laboratory frame to
 *         the crystal frame.
 */
           if (param->useLabFrame) {
               real8 bTmp[3] = {bx, by, bz};
               real8 bRot[3];

               Matrix33Vector3Multiply(home->rotMatrixInverse, bTmp, bRot);

               bx = bRot[0]; by = bRot[1]; bz = bRot[2];
           }
//#endif

           br = sqrt(bx*bx+by*by+bz*bz);
           bx/=br; by/=br; bz/=br; /* unit vector along Burgers vector */

           dangle = fabs(bx*lx+by*ly+bz*lz);
           Mob=MobEdge+(MobScrew-MobEdge)*dangle;

#ifdef _MOBILITY_FIELD
			if (param->mobilityField == 1) {
				double xm = param->mobilityFieldScale;
				Mob *= ((1.0-xm) + xm*Mf);
			}
#endif

           LtimesB+=(lr/Mob);
           FricForce += fricStress*br*lr;
	}
    }
    LtimesB/=2;
    FricForce /= 2.0;
    
#if 0
#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) {
		// WARNING
		/* Modify mobility of surface nodes */
		LtimesB *= 5.0;
	}
#endif
#endif

    nForce[0] = node->fX;
    nForce[1] = node->fY;
    nForce[2] = node->fZ;

//#ifndef _THINFILM
/*
 *  If needed, rotate the force vector from the laboratory frame to the
 *  crystal frame
 */
    if (param->useLabFrame) {
        real8 rotForce[3];
        Matrix33Vector3Multiply(home->rotMatrixInverse, nForce, rotForce);
        VECTOR_COPY(nForce, rotForce);
    }
//#endif

/*
 * 	Apply friction stress. If we are using subcycling,
 * 	the friction stress is integrated as a back-force.
 */
	if (fricStress > 0.0) {
		if (strcmp(param->timestepIntegrator, "forceBsubcycle") == 0 &&
		    param->inSubcycling) {
			if (node->fricPinned) {
				nForce[0] = 0.0;
				nForce[1] = 0.0;
				nForce[2] = 0.0;
			}
		} else {
			nFmag = sqrt(nForce[0]*nForce[0]+nForce[1]*nForce[1]+nForce[2]*nForce[2]);
			if (nFmag > FricForce) {
				nForce[0] -= nForce[0]*FricForce/nFmag;
				nForce[1] -= nForce[1]*FricForce/nFmag;
				nForce[2] -= nForce[2]*FricForce/nFmag;
			} else {
				nForce[0] = 0.0;
				nForce[1] = 0.0;
				nForce[2] = 0.0;
			}
		}
	}

    /* Velocity is simply proportional to total force per unit length */
    VelxNode = nForce[0]/LtimesB;
    VelyNode = nForce[1]/LtimesB;
    VelzNode = nForce[2]/LtimesB;
    

    /* Orthogonalize with glide plane constraints */
    if (nconstraint>=3){
		VelxNode = VelyNode = VelzNode = 0.0;
	} else {
		for(i=0;i<nc;i++)
		{
			if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
			{
			 Orthogonalize(&VelxNode,&VelyNode,&VelzNode,
							 normX[i],normY[i],normZ[i]);
			}
		}
	}
	


    /* Any dislocation with glide plane not {111} type can only move along its length
     * This rule includes LC junction which is on {100} plane
     */
#if _ENABLE_LINE_CONSTRAINT
    if(nlc==1)
    { /* only one line constraint */
        for(i=0;i<nc;i++)
        {
            if((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0))
            { 
   	        lcx = lineX[i];
	        lcy = lineY[i];
	        lcz = lineZ[i];
                break;
            }
        }

        /* project velocity along line */
        Veldotlcr = VelxNode*lcx+VelyNode*lcy+VelzNode*lcz;
        VelxNode = Veldotlcr*lcx;
        VelyNode = Veldotlcr*lcy;
        VelzNode = Veldotlcr*lcz;

	if (nconstraint<=0)
	{	
            Fatal("MobilityLaw_FCC_0: nconstraint <= 0, nlc = 1 is impossible!");
        }
        else if(nconstraint>=1)
  	{ /* a few plane constraints and one line constraint */
            for(i=0;i<nc;i++)
            {
		normdotlc = normx[i]*lcx + normy[i]*lcy + normz[i]*lcz;
		if(fabs(normdotlc)>FFACTOR_ORTH)
		{
                    /* set velocity to zero if line is not on every plane */
                    VelxNode = VelyNode = VelzNode = 0;
		    break;
		}
                else
                {
                   /* do nothing. Skip */
                }
	    }
	}
    }
    else if (nlc>=2)
    {
        /* Velocity is zero when # of independnet lc constratins is equal to or more than 2 */
        VelxNode = VelyNode = VelzNode = 0;
    }
    else
    { 
        /* nlc == 0, do nothing */
    }
#endif

//#ifndef _THINFILM
/*
 *  If needed, rotate the velocity vector back to the laboratory frame
 *  from the crystal frame
 */
    if (param->useLabFrame) {
        real8 vTmp[3] = {VelxNode, VelyNode, VelzNode};
        real8 vRot[3];

        Matrix33Vector3Multiply(home->rotMatrix, vTmp, vRot);

        VelxNode = vRot[0];
        VelyNode = vRot[1];
        VelzNode = vRot[2];
    }
//#endif

    node->vX = VelxNode;
    node->vY = VelyNode;
    node->vZ = VelzNode;


#ifdef _THINFILM
    /* put surface node velocity on surface 
     * by adding velocity along line direction 
     */ 
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	
        if (node->numNbrs == 1)
        {
           /* compute line direction */
           nbr=GetNeighborNode(home,node, 0);
           dx=nbr->x - node->x;
           dy=nbr->y - node->y;
           dz=nbr->z - node->z;
           ZImage (param, &dx, &dy, &dz) ;
           lr=sqrt(dx*dx+dy*dy+dz*dz);
           lx = dx/lr; ly = dy/lr; lz = dz/lr;

           //printf("surface node lx = %e ly = %e lz = %e\n", lx, ly, lz);

           if (fabs(lz) > 0.05)
           {
              node->vX = VelxNode - VelzNode * lx / lz ;
              node->vY = VelyNode - VelzNode * ly / lz ;
              node->vZ = 0.0;
           }
           else
           {
              node->vX = VelxNode * lz - VelzNode * lx ;
              node->vY = VelyNode * lz - VelzNode * ly ;
              node->vZ = 0.0;
           }
        }
        else
        {
              node->vX = 0.0;
              node->vY = 0.0;
              node->vZ = 0.0;
        }

	real8 vmag;
        vmag = sqrt(node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ);
        if (fabs(node->vZ) > (vmag*1e-4)) 
        if (fabs(node->vZ) > 0.0) 
        {
            printf("surface node vX = %e vY = %e vZ = %e vmag = %e nc = %d\n",
                    node->vX,node->vY,node->vZ,vmag,nc);
            for (i=0;i<nc;i++) printf("norm[%d] = (%e, %e, %e)\n",i,normX[i],normY[i],normZ[i]);
            Fatal("MobilityLaw_FCC_0: surface node vZ non-zero!");
        } 
    }
#endif

/*
#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) {
        node->vZ = 0.0;
    } 
#endif */

    return(0);
}
