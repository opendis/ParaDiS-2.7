#include "Home.h"


#ifdef _THINFILM

#include "TF.h"


void PrintStress(Home_t *home,ThinFilm_t *thinfilm)
{
/***************************************************************************
 * This program computes the stress at a point x in the domain
 * Sylvie Aubry, Apr 25 2008
 *
 **************************************************************************/

  FILE *fpInf,*fpImg,*fpTot;
  char format[15];
  int i,j,k,l,m,ii,jj;
  int nx, ny;
  real8 r[3], stress[3][3],s1[3][3],s1loc[3][3],s2[3][3],s[3][3],Ys[3][3];
  double** GridPts[3];
  
#ifndef _TFIMGSTRESS
  real8 difX, difY, x, y;
  nx = 201;
  ny = 201;

  for (i=0;i<3;i++) 
    GridPts[i] = (double **)malloc(sizeof(double*)*nx);

  for (i=0;i<3;i++) 
    for (k=0;k<nx;k++) 
      {
	GridPts[i][k] = (double *)malloc(sizeof(double)*ny);
      }

  difX = home->param->Lx/(1.0*nx);
  difY = home->param->Ly/(1.0*ny);

  for (i=0; i<nx; i++) 
    {
      x = home->param->minSideX + difX*i;
      for (j=0; j<ny; j++) 
	{
	  y = home->param->minSideY + difY*j;
	  GridPts[0][i][j] = x;
	  GridPts[1][i][j] = y;
	  GridPts[2][i][j] = thinfilm->t;
	}
    }
#else
  nx = thinfilm->nx;
  ny = thinfilm->ny;

  for (i=0;i<3;i++) GridPts[i] = thinfilm->Grid[i];
#endif
  
  if (home->myDomain == 0) 
    printf(" \n\n WRITING STRESS TO FILE %d x %d points \n\n",nx, ny);
  
#if 0
  printf("\nr[50]=%f %f %f\n",GridPts[0][50][50],GridPts[1][50][50],
	 GridPts[2][50][50]);
  AllSegmentStress(home,thinfilm,GridPts[0][50][50],GridPts[1][50][50],
		   GridPts[2][50][50],s1loc);
  printf("\nr[51]=%f %f %f\n",GridPts[0][51][51],GridPts[1][51][51],
	 GridPts[2][51][51]);
  AllSegmentStress(home,thinfilm,GridPts[0][51][51],GridPts[1][51][51],
		   GridPts[2][51][51],s1loc);
  printf("\nr[151]=%f %f %f\n",GridPts[0][151][151],GridPts[1][151][151],
	 GridPts[2][151][151]);
  AllSegmentStress(home,thinfilm,GridPts[0][151][151],GridPts[1][151][151],
		   GridPts[2][151][151],s1loc);
  exit(0);
#endif


  sprintf(format, "InfStress%d.out",home->myDomain);
  fpInf = fopen(format,"w");

  sprintf(format, "ImgStress%d.out",home->myDomain);
  fpImg = fopen(format,"w");

  sprintf(format, "TotStress%d.out",home->myDomain);
  fpTot = fopen(format,"w");

  for (i=0; i<nx; i++)
    {
      if (home->myDomain == 0)  printf("doing i=%d\n",i);
      for (j=0; j<ny; j++) 
	{
	  r[0] = GridPts[0][i][j];
	  r[1] = GridPts[1][i][j];
	  r[2] = GridPts[2][i][j]; /* Top surface */

	  /* infinite medium stress */
	  Init3x3(s1loc);
	  AllSegmentStress(home,thinfilm,r[0],r[1],r[2],s1loc);

#ifdef PARALLEL
	  MPI_Allreduce(s1loc, s1, 9, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);
#else
        for (ii = 0; ii < 3; ii++)
            for (jj = 0; jj < 3; jj++)
                s1[ii][jj] = s1loc[ii][jj];
#endif

	  
	  /* image stress */	
	  Init3x3(s2);
#ifdef _TFIMGSTRESS
	  DispStress(thinfilm,r,s2);
#endif


#ifndef _NOYOFFESTRESS
	  /* Yoffe stress */
	  Init3x3(Ys);
	  AllYoffeStress(home, thinfilm, r[0], r[1], r[2], Ys);
	  for (ii = 0; ii < 3; ii++)
	    for (jj = 0; jj < 3; jj++)
	      {
		s2[ii][jj] += Ys[ii][jj];
	      } 
#endif

	  /* total stress */  
	  for (ii = 0; ii < 3; ii++)
	    for (jj = 0; jj < 3; jj++)
	      {
		s[ii][jj] = s1[ii][jj] + s2[ii][jj];
	      } 

	  /* Print stresses */ 

	  fprintf(fpInf,"%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
		  r[0],r[1],r[2], s1[0][0],s1[1][1],s1[2][2],s1[1][2],s1[2][0],s1[0][1]);

	  fprintf(fpImg,"%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
		  r[0],r[1],r[2], s2[0][0],s2[1][1],s2[2][2],s2[1][2],s2[2][0],s2[0][1]);

	  fprintf(fpTot,"%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
		  r[0],r[1],r[2], s[0][0],s[1][1],s[2][2],s[1][2],s[2][0],s[0][1]);

	}
    }

  fclose(fpImg);
  fclose(fpInf);
  fclose(fpTot);


#ifndef _TFIMGSTRESS
  for (i=0;i<3;i++) 
    for (k=0;k<nx;k++) 
      {
	free(GridPts[i][k]);
      }

  for (i=0;i<3;i++) 
    {
      free(GridPts[i]);
    }  
#endif

  exit(0);
  
  return;
}


/***************************************************************************
 * Plot the RSS arising from the image stres (+indenter) on a slip plane
 **************************************************************************/
void PrintRSS(Home_t *home,ThinFilm_t *thinfilm)
{

	FILE    *fpImg;
	char    format[256];
	int     i, j, ii, jj, cnt;
	int     nx, ny;
	real8   HSLx, HSLy;
	real8   x, y, r[3], rp[3], s[3][3], rss;
	real8   n[3], b[3], xdir[3], ydir[3], c[3], zdir[3];
	Node_t  *node1;
	Param_t *param;
	
	param = home->param;
	
	//if (!param->savecn) return;
	if (home->cycle % 100 != 0) return;
	//cnt = param->savecncounter;

#ifdef _TFIMGSTRESS

	printf("PrintRSS\n");

	// Grab the first node
	if (home->newNodeKeyPtr == 0) return;
	for (i = 0; i < home->newNodeKeyPtr; i++) {
		node1 = home->nodeKeys[i];
		if (!node1) continue;
		else break;
	}
	if (!node1) return;

	HSLx = thinfilm->TFLx;
	HSLy = thinfilm->TFLy;
	
	nx = 256;
	ny = 256;
	
	/* plane normal */
	n[0] = 1.0;
	n[1] = 1.0;
	n[2] = 1.0;
	
	/* Burgers direction */
	b[0] = node1->burgX[0];
	b[1] = node1->burgY[0];
	b[2] = node1->burgZ[0];
	
	/* center of the plane */
	c[0] = node1->x;
	c[1] = node1->y;
	c[2] = node1->z;
	
	NormalizeVec(n);
	NormalizeVec(b);
	
	zdir[0] = 0.0;
	zdir[1] = 0.0;
	zdir[2] = 1.0;
	NormalizedCrossVector(n,zdir,ydir);
	NormalizedCrossVector(ydir,n,xdir);

	if (home->myDomain == 0) {
		printf(" \n\n Writing RSS to file for %d x %d points\n\n",nx,ny);
	}

	sprintf(format, "ImgStress_%d.out", home->cycle+1);
	fpImg = fopen(format,"w");

	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
	
			/* in local plane */
			//x = ((1.0*i)/nx)*HSLx-0.5*HSLx;
			//y = ((1.0*j)/ny)*HSLy-0.5*HSLy;
			
			x = ((1.5*i)/nx)*HSLx-0.75*HSLx;
			y = ((1.5*j)/ny)*HSLy-0.75*HSLy;

			/* in global */
			r[0] = x*xdir[0] + y*ydir[0] + c[0];
			r[1] = x*xdir[1] + y*ydir[1] + c[1];
			r[2] = x*xdir[2] + y*ydir[2] + c[2];
			
			/* Image stress */	
			Init3x3(s);
			
			/* pbc */
			VECTOR_COPY(rp,r);
			if (r[0] < param->minSideX) rp[0] += HSLx;
			if (r[0] > param->maxSideX) rp[0] -= HSLx;
			if (r[1] < param->minSideY) rp[1] += HSLy;
			if (r[1] > param->maxSideY) rp[1] -= HSLy;
			
			if (r[2] > param->minSideZ && r[2] < 0.0) {
				DispStress(thinfilm,rp,s);
			}

			/* Calculate RSS in the direction of the Burgers vector */
			rss = 0.0;
			for (ii=0; ii<3; ii++) {
				for (jj=0; jj<3; jj++) {
					rss += s[ii][jj]*n[ii]*b[jj];
				}
			}

			/* Print stresses */ 
			fprintf(fpImg,"%e %e %e %e %e %e %e %e %e %e\n",
					r[0],r[1],r[2],rss,s[0][0],s[1][1],s[2][2],s[1][2],s[2][0],s[0][1]);
					
			//if (r[0] > param->minSideX && r[0] < param->maxSideX &&
			    //r[1] > param->minSideY && r[1] < param->maxSideY &&
			    //r[2] > param->minSideZ && r[2] < 0.0) {
					
				//DispStress(thinfilm,r,s);
				
				///* Calculate RSS in the direction of the Burgers vector */
				//rss = 0.0;
				//for (ii=0; ii<3; ii++) {
					//for (jj=0; jj<3; jj++) {
						//rss += s[ii][jj]*n[ii]*b[jj];
					//}
				//}

				///* Print stresses */ 
				//fprintf(fpImg,"%e %e %e %e %e %e %e %e %e %e\n",
						//r[0],r[1],r[2],rss,s[0][0],s[1][1],s[2][2],s[1][2],s[2][0],s[0][1]);
					
			//}
			
		}
	}

	fclose(fpImg);

#endif

	return;
}

#ifdef _INDENTATION
/***************************************************************************
 * Plot Uz_indenter, Uz_elast, Tzext on free surface
 **************************************************************************/
void PrintSurfaceDisplacement(Home_t *home, ThinFilm_t *thinfilm)
{
	int    i, j, nx, ny, ind;
	real8  r[3];
	FILE   *fp;
	char   format[100];
	
	if (home->myDomain != 0) return;

	Param_t *param = home->param;

	if (!param->surfacedisp || !param->surfacedispfreq) return;
	if ((home->cycle+1) % param->surfacedispfreq != 0) return;

	nx = thinfilm->nx;
	ny = thinfilm->ny;
	
	//sprintf(format, "indenter_%d.out", home->cycle);
	sprintf(format, "surfdisp_%d.out", home->cycle+1);
	fp = fopen(format,"w");
	printf(" +++ Writing surface displacement file %s\n", format);

	for (i=0; i<nx; i++) {
		for (j=0; j<ny; j++) {
			
			ind = j+i*ny;
			
			r[0] = thinfilm->Grid[0][i][j];
			r[1] = thinfilm->Grid[1][i][j];
			r[2] = thinfilm->Grid[2][i][j];
			
			fprintf(fp,"%e %e %e %e %e %e %e\n",
			        r[0],r[1],r[2],thinfilm->Uz_indenter[ind],thinfilm->Uz_elas[ind],
			        thinfilm->Uz_plas[ind],thinfilm->Tzext[ind]);
		}
	}
	
	fclose(fp);
	
	return;
}
/***************************************************************************
 * Plot indenter stress on a given slice within the volume (debug function)
 **************************************************************************/
void PrintIndenterFields(Home_t *home,ThinFilm_t *thinfilm)
{
	int    i, j, n1, n2, ind;
	real8  r[3], s[3][3], p1, p2;
	FILE   *fp;
	char   format[100];
	
#ifdef _TFIMGSTRESS

	Param_t *param = home->param;
	
	if (home->cycle % 10 != 0) return;

	printf("PrintIndenterFields\n");
	
#if 1
	// INDENTER STRESS
	
	sprintf(format, "indenter_stress_%d.out", home->cycle);
	fp = fopen(format,"w");
	
	n1 = thinfilm->nx;
	n2 = thinfilm->nx;

	for (i=0; i<n1; i++) {
		for (j=0; j<n2; j++) {
			
			p1 = param->minSideY + i*(param->maxSideY-param->minSideY)/(n1-1);
			p2 = j*param->minSideZ/(n2-1);
			//if (j == 0) p2 -= 1.0; // avoid probing at z=0.0
			
			r[0] = 0.0; //param->minSideX + 0.3*(param->maxSideX-param->minSideX);
			r[1] = p1;
			r[2] = p2;
			
			Init3x3(s);
			DispStress(thinfilm,r,s);

			fprintf(fp,"%e %e %e %e %e %e %e %e %e\n", r[0], r[1], r[2], s[0][0], s[1][1], s[2][2], s[0][1], s[0][2], s[1][2]);
		}
	}
	
	fclose(fp);
	
	sprintf(format, "indenter_stress_line_%d.out", home->cycle);
	fp = fopen(format,"w");
	
	n1 = 128;
	n2 = thinfilm->ny;
	
	for (i=0; i<n1; i++) {
		p1 = param->minSideY + i*(param->maxSideY-param->minSideY)/(n1-1);
		p2 = n2/4*param->minSideZ/(n2);
		r[0] = 0.0;
		r[1] = p1;
		r[2] = p2;
		Init3x3(s);
		DispStress(thinfilm,r,s);
		fprintf(fp,"%e %e %e %e %e %e %e %e %e\n", r[0], r[1], r[2], s[0][0], s[1][1], s[2][2], s[0][1], s[0][2], s[1][2]);
	}
	fclose(fp);
#endif	
	
#if 0
	// INDENTER STRAIN

	sprintf(format, "indenter_strain_%d.out", home->cycle);
	fp = fopen(format,"w");
	
	n1 = 60;
	n2 = 60;
	
	double e11, e22, e33;
	double nu = param->pois;
	double E = 2.0*param->shearModulus*(1.0+nu);

	for (i=0; i<n1; i++) {
		for (j=0; j<n2; j++) {
			
			p1 = param->minSideY + i*(param->maxSideY-param->minSideY)/(n1-1);
			p2 = j*param->minSideZ/(n2-1);
			
			r[0] = 0.0; //param->minSideX + 0.3*(param->maxSideX-param->minSideX);
			r[1] = p1;
			r[2] = p2;
			
			Init3x3(s);
			DispStress(thinfilm,r,s);
			
			e11 = 1.0/E*(s[0][0]-nu*(s[1][1]+s[2][2]));
			e22 = 1.0/E*(s[1][1]-nu*(s[0][0]+s[2][2]));
			e33 = 1.0/E*(s[2][2]-nu*(s[0][0]+s[1][1]));

			fprintf(fp,"%e %e %e %e %e %e\n", r[0],r[1],r[2],e11,e22,e33);
		}
	}
	
	fclose(fp);
#endif
	
	
#endif
	return;
}
#endif

#ifdef _SURFACE_STEPS
/***************************************************************************
 * PrintSurfaceSegments
 **************************************************************************/
void PrintSurfaceSegments(Home_t *home,ThinFilm_t *thinfilm)
{
		int    i, j, nx, ny, ind;
		real8  r[3], s[3][3];
		FILE   *fp;
		char   format[100];

		Param_t *param = home->param;

#if 0				
/*	
 *  	Print surface steps for visualization, debug, 
 *  	and simulation restart (now done in WriteRestart.c)
 */	
		if (param->savecn && (home->cycle+1) % param->savecnfreq == 0) {
			
			
			sprintf(format, "surfSegList_%d.surf", home->cycle+1);
			fp = fopen(format,"w");
			printf(" +++ Writing surface segments file %s\n", format);

			surfaceStepSeg_t *surfStepSegList = thinfilm->surfStepSegList;
			for (i = 0; i < thinfilm->surfStepSegCount; i++) {
				
				int id1, id2;
				if (surfStepSegList[i].virtual1 == 0) {
					id1 = surfStepSegList[i].node1->myTag.index;
				} else {
					id1 = -1;
				}
				if (surfStepSegList[i].virtual2 == 0) {
					id2 = surfStepSegList[i].node2->myTag.index;
				} else {
					id2 = -1;
				}
				
				fprintf(fp,"%d %d %e %e %e %e %e %e %e %e %e %e %e %e\n",
						id1, id2,
						surfStepSegList[i].pos1[0],
						surfStepSegList[i].pos1[1],
						surfStepSegList[i].pos1[2],
						surfStepSegList[i].pos2[0],
						surfStepSegList[i].pos2[1],
						surfStepSegList[i].pos2[2],
						surfStepSegList[i].burg[0],
						surfStepSegList[i].burg[1],
						surfStepSegList[i].burg[2],
						surfStepSegList[i].norm[0],
						surfStepSegList[i].norm[1],
						surfStepSegList[i].norm[2]);
				
			}
			
			fclose(fp);
		}
#endif
	
/*	
 *  	Print surface steps for visualization in Paraview
 */
		if (param->paraview) {
			
			if ((home->cycle % param->paraviewfreq) == 0) {
				
				// not correct but should do the trick
				int totSegs = home->newNodeKeyPtr + thinfilm->surfStepSegCount; 
			
				char baseName[128];
				snprintf(baseName, sizeof(baseName), "paraview_surfseg%04d",
						 param->paraviewcounter);
				WriteParaviewSpec(home, thinfilm, baseName, totSegs, totSegs, 1);
			}
		}
		
		return;
}
#endif

void PrintTractions(Home_t *home,ThinFilm_t *thinfilm)
{
  FILE *fpInf;
  char format[50];
  int i,j,k,l,m,ii,jj;
  int nx, ny;
  real8 r[3], s[3][3],s1loc[3][3];
  double** GridPts[3];
  
#ifndef _TFIMGSTRESS
  real8 difX, difY, x, y;
  nx = 100;
  ny = 100;

  difX = home->param->Lx/(1.0*nx);
  difY = home->param->Ly/(1.0*ny);

  for (i=0;i<3;i++) 
    GridPts[i] = (double **)malloc(sizeof(double*)*nx);

  for (i=0;i<3;i++) 
    for (k=0;k<nx;k++) 
      {
	GridPts[i][k] = (double *)malloc(sizeof(double)*ny);
      }

  for (i=0; i<nx; i++) 
    {
      x = home->param->minSideX + difX*i;
      for (j=0; j<ny; j++) 
	{
	  y = home->param->minSideY + difY*j;
	  GridPts[0][i][j] = x;
	  GridPts[1][i][j] = y;
	  GridPts[2][i][j] = thinfilm->t;
	}
    }
#else
  nx = thinfilm->nx;
  ny = thinfilm->ny;

  for (i=0;i<3;i++) 
    GridPts[i] = (double **)malloc(sizeof(double*)*nx);

  for (i=0;i<3;i++) 
    for (k=0;k<nx;k++) 
      {
	GridPts[i][k] = (double *)malloc(sizeof(double)*ny);
      }

  for (k=0;k<3;k++)   
    for (i=0; i<nx; i++) 
      for (j=0; j<ny; j++) 
	GridPts[k][i][j] = thinfilm->Grid[k][i][j];
#endif
  
  real8 *tx, *ty, *tz, *tx_tot, *ty_tot, *tz_tot;
  
  // Allocate tx, ty, tz
  tx = malloc(sizeof(double)*nx*ny*2);
  ty = malloc(sizeof(double)*nx*ny*2);
  tz = malloc(sizeof(double)*nx*ny*2);
  
  tx_tot = malloc(sizeof(double)*nx*ny*2);
  ty_tot = malloc(sizeof(double)*nx*ny*2);
  tz_tot = malloc(sizeof(double)*nx*ny*2);

  if (home->myDomain == 0) 
    printf(" \n\n WRITING  Tractions TO FILE %d x %d points \n\n",nx, ny);
  
  sprintf(format, "Tractions%d.out",home->myDomain);
  fpInf = fopen(format,"w");

  // Bottom Surface
  for (i=0; i<nx; i++)
    {
      if (home->myDomain == 0)  printf("Bottom doing i=%d\n",i);
      for (j=0; j<ny; j++) 
	{
	  r[0] =  GridPts[0][i][j];
	  r[1] =  GridPts[1][i][j];
	  r[2] = -GridPts[2][i][j]; 
	  
	  /* infinite medium stress */
	  Init3x3(s1loc);
	  AllSegmentStress(home,thinfilm,r[0],r[1],r[2],s1loc);


#ifdef PARALLEL
	  MPI_Allreduce(s1loc, s, 9, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);
#else
        for (ii = 0; ii < 3; ii++)
            for (jj = 0; jj < 3; jj++)
                s[ii][jj] = s1loc[ii][jj];
#endif


        tx[j+i*ny] = s[0][2];
        ty[j+i*ny] = s[1][2];
        tz[j+i*ny] = s[2][2];
	}
    }


  //Top surface
  for (i=0; i<nx; i++)
    {
      if (home->myDomain == 0)  printf("Top doing i=%d\n",i);
      for (j=0; j<ny; j++) 
	{
	  r[0] = GridPts[0][i][j];
	  r[1] = GridPts[1][i][j];
	  r[2] = GridPts[2][i][j]; 
	  
	  /* infinite medium stress */
	  Init3x3(s1loc);
	  AllSegmentStress(home,thinfilm,r[0],r[1],r[2],s1loc);


#ifdef PARALLEL
	  MPI_Allreduce(s1loc, s, 9, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);
#else
        for (ii = 0; ii < 3; ii++)
            for (jj = 0; jj < 3; jj++)
                s[ii][jj] = s1loc[ii][jj];
#endif


        tx[j+i*ny+nx*ny] = -s[0][2];
        ty[j+i*ny+nx*ny] = -s[1][2];
        tz[j+i*ny+nx*ny] = -s[2][2];
	  
	}
    }


  for (i=0; i<nx; i++)
      for (j=0; j<ny; j++) 
	{
	  fprintf(fpInf,"%.15e %.15e %.15e      %.15e %.15e %.15e\n",
		  thinfilm->Grid[0][i][j],thinfilm->Grid[1][i][j],-thinfilm->Grid[2][i][j],
		  tx[j+i*ny], ty[j+i*ny], tz[j+i*ny]);
	  fprintf(fpInf,"%.15e %.15e %.15e      %.15e %.15e %.15e\n",
		  thinfilm->Grid[0][i][j],thinfilm->Grid[1][i][j],thinfilm->Grid[2][i][j],
		  tx[j+i*ny+nx*ny], ty[j+i*ny+nx*ny], tz[j+i*ny+nx*ny]);
	}

  fclose(fpInf);


  for (i=0;i<3;i++) 
    for (k=0;k<nx;k++) 
      {
	free(GridPts[i][k]);
      }

  for (i=0;i<3;i++) 
    {
      free(GridPts[i]);
    }  

  exit(0);
  
  return;
}


void Write_sigbRem(Home_t *home,char *format)
{
  FILE *fp;
  char name[40];
  int i,armID12,ti;
  Node_t *node, *nbr;
  
  if (home->myDomain == 0) printf("\nWriting out sigbRem after %s\n",format);
  
  sprintf(name, "sig%dbRem.out",home->myDomain);
  fp = fopen(name,"w");


  for (i=0;i<home->newNodeKeyPtr;i++) 
    {
      node = home->nodeKeys[i];
      if (!node) continue;
          
      for (ti = 0; ti < node->numNbrs; ti++) 
	{
	  nbr = GetNeighborNode(home, node, ti);
	  armID12 = GetArmID(home, node, nbr);
	  
	  fprintf(fp,"%.15e  %.15e  %.15e  %.15e  %.15e  %.15e \n",node->x,node->y,node->z,
		  node->sigbRem[armID12*3],node->sigbRem[armID12*3+1],
		  node->sigbRem[armID12*3+2]);
	}
      
    }
  
  fclose(fp);
  
  exit(0);
}

void PrintdSegImgStress(Home_t *home, ThinFilm_t *thinfilm)
{
  FILE *fp;
  char format[15];
  int i,j,k,l,m;
  real8 r[3], s1[3][3];
  
  int nx = thinfilm->nx;
  int ny = thinfilm->ny;

  if (home->myDomain == 0) 
    printf(" \n\n WRITING dSegImgStress TO FILE %d x %d points \n\n ",nx, ny);

  sprintf(format, "dSegImgStress%d.out",home->myDomain);
  fp = fopen(format,"w");
  
  for (i=0; i<nx; i++)
  {
    for (j=0; j<ny; j++) 
    {
	r[0] = thinfilm->Grid[0][i][j];
	r[1] = thinfilm->Grid[1][i][j];
	r[2] = thinfilm->Grid[2][i][j]; /* Top surface */
	//r[2] =-thinfilm->Grid[2][i][j]; /* Bottom surface */

        dSegImgStress(home, s1, 
                      0.0, 0.0, 0.0, /*  r0 */
                      0.0, 1.0, 0.0, /*  dl */
                      0.0, 0.0, 1.0, /*  burg */
                      r[0], r[1], r[2],
                      0 /* pbc */ );
	
	fprintf(fp,   "%.15e %.15e %.15e      %.15e %.15e %.15e %.15e %.15e %.15e\n",
                      r[0],r[1],r[2], s1[0][0],s1[1][1],s1[2][2],s1[1][2],s1[2][0],s1[0][1]);
      }
  }

  fclose(fp);

  exit(0);
  
  return;
}



#endif
