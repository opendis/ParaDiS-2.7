/**************************************************************************
 *
 *  Function    : Mobility_FCC_Tab
 *  Description : Mobility Law of FCC metals
 *                using tabulated values
 *
 *  Returns:  0 on success
 *            1 if velcoity could not be determined
 * 
 *  Modified : Jaehyun Cho jaehyun.cho@epfl.ch Description for
 *  modification : For CADD3d purpose, some new lines are added in
 *  if(param->mobfile){..}  When 'mobfile' is activated, the damping
 *  coefficients are evaluated based on data provided by the exterenal
 *  mobility files (i.e. mobility.mob)
 ***************************************************************************/

#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#define _ENABLE_LINE_CONSTRAINT 1
#define _TAB_MOB 1

#ifdef _MOBILITY_FCC_TAB

/*-------------------------------------------------------------------------
 *
 *      Function:     EvaluateDrag
 *      Description:  Evaluate drag coefficient and friction stress
 *                    from the tabulated values stored in mobilityAngles
 *                    The drag coefficient Bdrag is given in Pa/(nm/ps)
 *                    The fricion stress is given in Pa
 *
 *------------------------------------------------------------------------*/
void EvaluateDrag(Home_t *home, double angle, double RSS, 
                  double *friction, double *Bdrag, double *effMass)
{
	int     i, angleIndex;
	double  angle0, angle1, angleRatio;
	double  stress0, stress1, b0, b1, e0, e1;
	double  Bdrag0, Bdrag1, effMass0, effMass1;
	Param_t *param;

	param = home->param;
	
	angleIndex = -1;
	angleRatio = 0.0;
	
	/* Find angle bounds and ratio */
	for (i = 0; i < param->angleCount-1; i++) {
		
		angle0 = param->mobilityAngles[i].angle;
		angle1 = param->mobilityAngles[i+1].angle;
		
		if (angle >= angle0 && angle <= angle1) {
			angleIndex = i;
			angleRatio = (angle-angle0)/(angle1-angle0);
			break;
		}
		
	}
	
	/* Interpolate drag coefficient at angle0 (angleIndex) */
	for (i = 0; i < param->mobilityAngles[angleIndex].nstress-1; i++) {
		
		stress0 = param->mobilityAngles[angleIndex].tableB[i].stress;
		stress1 = param->mobilityAngles[angleIndex].tableB[i+1].stress;
		
		b0 = param->mobilityAngles[angleIndex].tableB[i].Bdrag;
		b1 = param->mobilityAngles[angleIndex].tableB[i+1].Bdrag;
		e0 = param->mobilityAngles[angleIndex].tableB[i].effMass;
		e1 = param->mobilityAngles[angleIndex].tableB[i+1].effMass;
		
		if ((i == 0 && RSS < stress0) || 
		    (RSS >= stress0 && RSS <= stress1) ||
		    (i == param->mobilityAngles[angleIndex].nstress-2 && RSS > stress1)) {
			Bdrag0 = b0+(RSS-stress0)*(b1-b0)/(stress1-stress0);
			effMass0 = e0+(RSS-stress0)*(e1-e0)/(stress1-stress0);
			break;
		}
		
	}
	
	/* Interpolate drag coefficient at angle1 (angleIndex+1) */
	for (i = 0; i < param->mobilityAngles[angleIndex+1].nstress-1; i++) {
		
		stress0 = param->mobilityAngles[angleIndex+1].tableB[i].stress;
		stress1 = param->mobilityAngles[angleIndex+1].tableB[i+1].stress;
		
		b0 = param->mobilityAngles[angleIndex+1].tableB[i].Bdrag;
		b1 = param->mobilityAngles[angleIndex+1].tableB[i+1].Bdrag;
		e0 = param->mobilityAngles[angleIndex+1].tableB[i].effMass;
		e1 = param->mobilityAngles[angleIndex+1].tableB[i+1].effMass;
		
		if ((i == 0 && RSS < stress0) || 
		    (RSS >= stress0 && RSS <= stress1) ||
		    (i == param->mobilityAngles[angleIndex+1].nstress-2 && RSS > stress1)) {
			Bdrag1 = b0+(RSS-stress0)*(b1-b0)/(stress1-stress0);
			effMass1 = e0+(RSS-stress0)*(e1-e0)/(stress1-stress0);
			break;
		}
		
	}
	
	/* Interpolate values between angles */
	*friction = (1.0-angleRatio)*param->mobilityAngles[angleIndex].friction +
	            angleRatio*param->mobilityAngles[angleIndex+1].friction;
	 
	*Bdrag    = (1.0-angleRatio)*Bdrag0 + angleRatio*Bdrag1;
	*effMass  = (1.0-angleRatio)*effMass0 + angleRatio*effMass1;
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     Mobility_FCC_Tab
 *      Description:  Evaluate nodal mobility based on tabulated velocities
 *
 *------------------------------------------------------------------------*/
int Mobility_FCC_Tab(Home_t *home, Node_t *node)
{
    int numNonZeroLenSegs = 0;
    Param_t *param;
    real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
    int i, j, nc, nconstraint, nlc;
    real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
    real8 lineX[100], lineY[100], lineZ[100];
    real8 a, b;
    real8 dx, dy, dz, lx, ly, lz, lr, LtimesB;
    real8 lcx, lcy, lcz, normdotlc;
    Node_t *nbr;
    real8 MobScrew, MobEdge, Mob, Bdrag, FricForce, friction, effMass;
    real8 bx, by, bz, br, dangle;
    real8 dangle_deg, distance, cangle;
    real8 nForce[3], nFmag;

    param = home->param;

    MobScrew = param->MobScrew;
    MobEdge  = param->MobEdge;
    
    nc = node->numNbrs;
    
/*
 *  If node is 'pinned' in place by constraints, or the node has any arms 
 *  with a burgers vector that has explicitly been set to be sessile (via
 *  control file inputs), the node may not be moved so just zero the velocity
 *  and return
 */
    if ((node->constraint == PINNED_NODE) ||
        NodeHasSessileBurg(home, node))
    {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }

/*
 *  It's possible this function was called for a node which had only zero-
 *  length segments (during SplitSurfaceNodes() for example).  If that is
 *  the case, just set the velocity to zero and return.
 */
    for (i = 0; i < nc; i++) {
        if ((nbr = GetNeighborNode(home, node, i)) == (Node_t *)NULL) continue;
        dx = node->x - nbr->x;
        dy = node->y - nbr->y;
        dz = node->z - nbr->z;
        if ((dx*dx + dy*dy + dz*dz) > 1.0e-12) {
            numNonZeroLenSegs++;
        }
    }

    if (numNonZeroLenSegs == 0) {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }


    /* copy glide plane constraints and determine line constraints */
    for(i=0;i<nc;i++)
    {
        normX[i] = normx[i] = node->nx[i];
        normY[i] = normy[i] = node->ny[i];
        normZ[i] = normz[i] = node->nz[i];

/*
 *      If needed, rotate the glide plane normals from the
 *      laboratory frame to the crystal frame.
 */
        if (param->useLabFrame) {
            real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
            real8 normRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);
            
            normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
            normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
        }

		if ( (fabs(fabs(normX[i]) - fabs(normY[i])) > FFACTOR_NORMAL) ||
             (fabs(fabs(normY[i]) - fabs(normZ[i])) > FFACTOR_NORMAL) )
        { 	
			/* not {111} plane */	
            if ((nbr=GetNeighborNode(home,node,i)) == (Node_t *)NULL) {
                Fatal("Neighbor not found at %s line %d\n",__FILE__,__LINE__);
            }
            lineX[i] = nbr->x - node->x;
            lineY[i] = nbr->y - node->y; 
            lineZ[i] = nbr->z - node->z;
            ZImage (param, lineX+i, lineY+i, lineZ+i);

/*
 *          If needed, rotate the line sense from the laboratory frame to
 *          the crystal frame.
 */
            if (param->useLabFrame) {
                real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
                real8 lDirRot[3];

                Matrix33Vector3Multiply(home->rotMatrixInverse, lDir, lDirRot);

                lineX[i] = lDirRot[0];
                lineY[i] = lDirRot[1];
                lineZ[i] = lDirRot[2];
            }
	}
	else
	{ /* no line constraint */
	    lineX[i] = lineY[i] = lineZ[i] = 0;
	}
    }

#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	    
        
        real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *          If needed, rotate the line sense from the laboratory frame to
 *          the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		lineX[nc] = 0.0; lineY[nc] = 0.0; lineZ[nc] = 0.0;
		nc++;
    }
#endif
    
    /* normalize glide plane normal vectors and lc line vectors*/
    for(i=0;i<nc;i++)
    {
        a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
	b=sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

        if(a>0)
        {
            normX[i]/=a;
            normY[i]/=a;
            normZ[i]/=a;

            normx[i]/=a;
            normy[i]/=a;
            normz[i]/=a;
        }
        if(b>0)
        {
            lineX[i]/=b;
            lineY[i]/=b;
            lineZ[i]/=b;
        }
    }


    /* Find independent glide constraints */ 
    nconstraint = nc;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
        }
        if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH)
        {
            normX[i] = normY[i] = normZ[i] = 0;
            nconstraint--;
        }
    }

    /* Find independent line constraints */
    nlc = 0;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
        }
        if((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH)
        {
            lineX[i] = lineY[i] = lineZ[i] = 0;
        }
        else
        {
            nlc ++;
        }
    }
      


/*
 *  Compute Resolved Shear Stress
 */
    nForce[0] = node->fX;
    nForce[1] = node->fY;
    nForce[2] = node->fZ;

/*
 *  If needed, rotate the force vector from the laboratory frame to the
 *  crystal frame
 */
    if (param->useLabFrame) {
        real8 rotForce[3];
        Matrix33Vector3Multiply(home->rotMatrixInverse, nForce, rotForce);
        VECTOR_COPY(nForce, rotForce);
    }
    
    /* Orthogonalize with glide plane constraints */
	for(i=0;i<nc;i++)
	{
		if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
		{
			Orthogonalize(&nForce[0],&nForce[1],&nForce[2],
			              normX[i],normY[i],normZ[i]);
		}
	}

	double RSS = sqrt(nForce[0]*nForce[0]+nForce[1]*nForce[1]+nForce[2]*nForce[2]);

	double bL = 0.0;
	for(j = 0; j < node->numNbrs; j++) {
		if ((nbr = GetNeighborNode(home, node, j)) == (Node_t *)NULL) continue;
		dx = nbr->x - node->x;
		dy = nbr->y - node->y;
		dz = nbr->z - node->z;
		ZImage(param, &dx, &dy, &dz);
		lr = sqrt(dx*dx+dy*dy+dz*dz);
		bx = node->burgX[j];
		by = node->burgY[j];
		bz = node->burgZ[j];
		br = sqrt(bx*bx+by*by+bz*bz);
		bL += br*lr;
	}
	RSS = 2.0*RSS/bL;


    LtimesB=0;
    FricForce = 0.0;
    
    /* find total dislocation length times drag coefficent (LtimesB)*/
    for(j=0;j<node->numNbrs;j++)
    {
        if ((nbr=GetNeighborNode(home,node,j)) == (Node_t *)NULL) continue;
        dx=nbr->x - node->x;
        dy=nbr->y - node->y;
        dz=nbr->z - node->z;
        ZImage (param, &dx, &dy, &dz) ;

/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
        if (param->useLabFrame) {
            real8 dTmp[3] = {dx, dy, dz};
            real8 dRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, dTmp, dRot);

            dx = dRot[0]; dy = dRot[1]; dz = dRot[2];
        }

        lr=sqrt(dx*dx+dy*dy+dz*dz);
 
        if (lr==0)
        { /* zero arm segment can happen after node split 
           * it is OK to have plane normal vector == 0
           * Skip (do nothing)
           */
        }
        else 
        {
           if((node->nx[j]==0)&&(node->ny[j]==0)&&(node->nz[j]==0))
           {
              printf("Mobility_FCC_0: (%d,%d) glide plane norm = 0\n"
                     "for segment with nonzero length lr = %e!\n",
                     node->myTag.domainID, node->myTag.index, lr);
           }

           lx=dx/lr; ly=dy/lr; lz=dz/lr;

           bx = node->burgX[j];
           by = node->burgY[j];
           bz = node->burgZ[j];

/*
 *         If needed, rotate the burgers vector from the laboratory frame to
 *         the crystal frame.
 */
           if (param->useLabFrame) {
               real8 bTmp[3] = {bx, by, bz};
               real8 bRot[3];

               Matrix33Vector3Multiply(home->rotMatrixInverse, bTmp, bRot);

               bx = bRot[0]; by = bRot[1]; bz = bRot[2];
           }

           br = sqrt(bx*bx+by*by+bz*bz);
           bx/=br; by/=br; bz/=br; /* unit vector along Burgers vector */

           dangle = fabs(bx*lx+by*ly+bz*lz);
           
#if _TAB_MOB
           dangle_deg = acos(dangle)*180.0/3.14159265359;
           distance = fabs(90.0 - dangle_deg);
           cangle = distance + 90.0;
#if 1
           if (cangle < 90.0 || cangle > 180.0) {
			   Fatal("cangle = %e is out of the [90,180] range", cangle);
		   }
#endif
           EvaluateDrag(home, cangle, RSS, &friction, &Bdrag, &effMass);
           
           /* Convert drag coefficient in Pa.s (ParaDiS units) */
           Bdrag = Bdrag*br*param->burgMag*1e-3;
           Mob = 1.0/Bdrag;
#else
           Mob=MobEdge+(MobScrew-MobEdge)*dangle;
           friction = param->FricStress;
#endif

           LtimesB+=(lr/Mob);
           FricForce += friction*br*lr;
	}
    }
    LtimesB/=2;
    FricForce /= 2.0;

	/* Apply friction stress */
    if (FricForce > 0.0) {
		nFmag = sqrt(nForce[0]*nForce[0]+nForce[1]*nForce[1]+nForce[2]*nForce[2]);
		if (nFmag > FricForce) {
			nForce[0] -= nForce[0]*FricForce/nFmag;
			nForce[1] -= nForce[1]*FricForce/nFmag;
			nForce[2] -= nForce[2]*FricForce/nFmag;
		} else {
			nForce[0] = 0.0;
			nForce[1] = 0.0;
			nForce[2] = 0.0;
		}
	}

    /* Velocity is simply proportional to total force per unit length */
    VelxNode = nForce[0]/LtimesB;
    VelyNode = nForce[1]/LtimesB;
    VelzNode = nForce[2]/LtimesB;
    

#if 0
	double magV = sqrt(VelxNode*VelxNode+VelyNode*VelyNode+VelzNode*VelzNode);
	printf("velocity %d = %e, v = %e nm/ps, M = %e Pa.s, RSS = %e Pa\n",
	node->myTag.index,magV,magV*param->burgMag*1e-3,magV/RSS,RSS);
#endif

    /* Orthogonalize with glide plane constraints */
    for(i=0;i<nc;i++)
    {
        if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
        {
	     Orthogonalize(&VelxNode,&VelyNode,&VelzNode,
                         normX[i],normY[i],normZ[i]);
        }
    }


    /* Any dislocation with glide plane not {111} type can only move along its length
     * This rule includes LC junction which is on {100} plane
     */
#if _ENABLE_LINE_CONSTRAINT
    if(nlc==1)
    { /* only one line constraint */
        for(i=0;i<nc;i++)
        {
            if((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0))
            { 
   	        lcx = lineX[i];
	        lcy = lineY[i];
	        lcz = lineZ[i];
                break;
            }
        }

        /* project velocity along line */
        Veldotlcr = VelxNode*lcx+VelyNode*lcy+VelzNode*lcz;
        VelxNode = Veldotlcr*lcx;
        VelyNode = Veldotlcr*lcy;
        VelzNode = Veldotlcr*lcz;

	if (nconstraint<=0)
	{	
            Fatal("MobilityLaw_FCC_0: nconstraint <= 0, nlc = 1 is impossible!");
        }
        else if(nconstraint>=1)
  	{ /* a few plane constraints and one line constraint */
            for(i=0;i<nc;i++)
            {
		normdotlc = normx[i]*lcx + normy[i]*lcy + normz[i]*lcz;
		if(fabs(normdotlc)>FFACTOR_ORTH)
		{
                    /* set velocity to zero if line is not on every plane */
                    VelxNode = VelyNode = VelzNode = 0;
		    break;
		}
                else
                {
                   /* do nothing. Skip */
                }
	    }
	}
    }
    else if (nlc>=2)
    {
        /* Velocity is zero when # of independnet lc constratins is equal to or more than 2 */
        VelxNode = VelyNode = VelzNode = 0;
    }
    else
    { 
        /* nlc == 0, do nothing */
    }
#endif

/*
 *  If needed, rotate the velocity vector back to the laboratory frame
 *  from the crystal frame
 */
    if (param->useLabFrame) {
        real8 vTmp[3] = {VelxNode, VelyNode, VelzNode};
        real8 vRot[3];

        Matrix33Vector3Multiply(home->rotMatrix, vTmp, vRot);

        VelxNode = vRot[0];
        VelyNode = vRot[1];
        VelzNode = vRot[2];
    }

    node->vX = VelxNode;
    node->vY = VelyNode;
    node->vZ = VelzNode;


#ifdef _THINFILM
    /* put surface node velocity on surface 
     * by adding velocity along line direction 
     */ 
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	
        if (node->numNbrs == 1)
        {
           /* compute line direction */
           nbr=GetNeighborNode(home,node, 0);
           dx=nbr->x - node->x;
           dy=nbr->y - node->y;
           dz=nbr->z - node->z;
           ZImage (param, &dx, &dy, &dz) ;
           lr=sqrt(dx*dx+dy*dy+dz*dz);
           lx = dx/lr; ly = dy/lr; lz = dz/lr;

           //printf("surface node lx = %e ly = %e lz = %e\n", lx, ly, lz);

           if (fabs(lz) > 0.05)
           {
              node->vX = VelxNode - VelzNode * lx / lz ;
              node->vY = VelyNode - VelzNode * ly / lz ;
              node->vZ = 0.0;
           }
           else
           {
              node->vX = VelxNode * lz - VelzNode * lx ;
              node->vY = VelyNode * lz - VelzNode * ly ;
              node->vZ = 0.0;
           }
        }
        else
        {
              node->vX = 0.0;
              node->vY = 0.0;
              node->vZ = 0.0;
        }

	real8 vmag;
        vmag = sqrt(node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ);
        if (fabs(node->vZ) > (vmag*1e-4)) 
        if (fabs(node->vZ) > 0.0) 
        {
            printf("surface node vX = %e vY = %e vZ = %e vmag = %e nc = %d\n",
                    node->vX,node->vY,node->vZ,vmag,nc);
            for (i=0;i<nc;i++) printf("norm[%d] = (%e, %e, %e)\n",i,normX[i],normY[i],normZ[i]);
            Fatal("MobilityLaw_FCC_0: surface node vZ non-zero!");
        } 
    }
#endif

/*
#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) {
        node->vZ = 0.0;
    } 
#endif */

    return(0);
}


/*-------------------------------------------------------------------------
 *
 *      Function:       GetTableB
 *      Description:    Get tabulated B file
 *      Contact:        jaehyun.cho@epfl.ch
 *
 *-----------------------------------------------------------------------*/
void GetTableB(MobilityAngle_t *mobilityAngle, char *mobilityFile)
{
	char line[256];
	
	printf("Reading mobility tab file %s ...\n", mobilityFile);
	
	FILE* file = fopen(mobilityFile, "r");
	if (file == NULL) {
		Fatal("Error occurs during open the B table file");
	} else {
		
		int i = 0;
		
		mobilityAngle->tableB = (TableB_t*)calloc(MAX_NUMBER_B,sizeof(TableB_t));
		while (fgets(line, sizeof(line), file)) {
			
			sscanf(line, "%lf %lf %lf",
			             &mobilityAngle->tableB[i].stress,
			             &mobilityAngle->tableB[i].Bdrag,
			             &mobilityAngle->tableB[i].effMass);
			i++;
		}
		
		mobilityAngle->nstress = i;
		fclose(file);
	}
	
	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:     ReadMobilityFile
 *      Contact:      jaehyun.cho@epfl.ch
 *
 *------------------------------------------------------------------------*/
void ReadMobilityFile(Home_t *home)
{
	ParamList_t *mobilityParamList;
	char     baseFileName[256], tmpFileName[256];
	char     inLine[500], token[256];
	int      maxTokenLen, tokenType, pIndex, okay;
	int      fileSegCount = 1, fileSeqNum = 0;
	FILE     *fpMob;
	int      valType, numVals;
	void     *valList; 
	Param_t  *param;
	int      numDomains, numReadTasks;
	int      *globalMsgCnt, *localMsgCnt, **nodeLists, *listCounts;
	int      distIncomplete, readCount, numNbrs;
	int      nextFileSeg, maxFileSeg, segsPerReader, taskIsReader;
	int      i;
	int      miscData[2];
	char     tableBFile[MAX_STRING_LEN];

	param      = home->param;
	numDomains = home->numDomains;
	mobilityParamList = param->mobilityParamList;
	
	fpMob = (FILE *)NULL;
	memset(inLine, 0, sizeof(inLine));
	maxTokenLen = sizeof(token);

	if (home->myDomain == 0) {
		
		printf("Reading mobility file %s ...\n", param->mobilityFile);
		fpMob = fopen(param->mobilityFile, "r");
		if (fpMob == NULL) {
			fprintf(stderr, "Mobility file (%s) not found!\n", param->mobilityFile);
			exit(-1);
		}

		tokenType = GetNextToken(fpMob, token, maxTokenLen);
		pIndex = LookupParam(mobilityParamList, token);
		
		if (pIndex < 0) {
			//param->Ks = atoi(token);
		} else {
			while ((tokenType != TOKEN_ERR) && (tokenType != TOKEN_NULL)) {
				
				//printf("Parsing parameter (%s), index (%d)\n", token, pIndex);
				
				if (pIndex >= 0) {
					valType = mobilityParamList->varList[pIndex].valType;
					numVals = mobilityParamList->varList[pIndex].valCnt;
					valList = mobilityParamList->varList[pIndex].valList;
					okay = GetParamVals(fpMob, valType, numVals, valList);
					if (!okay) {
						Fatal("Parsing Error obtaining values for mobility %s\n",
						      mobilityParamList->varList[pIndex].varName);
					}
				} else if (strcmp(token, "mobility") == 0)  {
					tokenType = GetNextToken(fpMob, token, maxTokenLen);
					break;
				}
				
				tokenType = GetNextToken(fpMob, token, maxTokenLen);
				pIndex = LookupParam(mobilityParamList, token);
			}
		}
	}

#ifdef PARALLEL
#ifdef PARADIS_IN_LIBMULTISCALE
	MPI_Bcast((char *)param, sizeof(Param_t), MPI_CHAR, 0, home->MPI_COMM_PARADIS);
#else
	MPI_Bcast((char *)param, sizeof(Param_t), MPI_CHAR, 0, MPI_COMM_WORLD);
#endif

#ifdef PARADIS_IN_LIBMULTISCALE
	MPI_Bcast((char *)baseFileName, sizeof(baseFileName),MPI_CHAR, 0, home->MPI_COMM_PARADIS);	    
#else
	MPI_Bcast((char *)baseFileName, sizeof(baseFileName),MPI_CHAR, 0, MPI_COMM_WORLD);
#endif
	numReadTasks = miscData[0];
	fileSegCount = miscData[1];
#ifdef PARADIS_IN_LIBMULTISCALE
	MPI_Bcast(miscData, 2, MPI_INT, 0, home->MPI_COMM_PARADIS);
#else
	MPI_Bcast(miscData, 2, MPI_INT, 0, MPI_COMM_WORLD);
#endif
	numReadTasks = miscData[0];
	fileSegCount = miscData[1];
#endif

	param->mobilityAngles = (MobilityAngle_t*)malloc(param->angleCount*
	                         sizeof(MobilityAngle_t));
	
	for (i = 0; i < param->angleCount; i++) {
		
		if (home->myDomain == 0) {
			Getline(inLine, sizeof(inLine), fpMob);
		}

#ifdef PARALLEL
#ifdef PARADIS_IN_LIBMULTISCALE
	MPI_Bcast((char *)inLine, sizeof(inLine), MPI_CHAR, 0, home->MPI_COMM_PARADIS);
#else
	MPI_Bcast((char *)inLine, sizeof(inLine), MPI_CHAR, 0, MPI_COMM_WORLD);
#endif
#endif

		sscanf(inLine, "%lf %lf %lf %s", &param->mobilityAngles[i].angle, 
		                                 &param->mobilityAngles[i].friction, 
		                                 &param->mobilityAngles[i].tensionf, 
		                                 &tableBFile);
					 
		GetTableB(&param->mobilityAngles[i], tableBFile);
	}
	
	if (home->myDomain == 0) {
		fclose(fpMob);
	}

	return;
}

/*-------------------------------------------------------------------------
 *
 *      Function:       MobilityParamInit
 *      Contact:        jaehyun.cho@epfl.ch
 *
 *-----------------------------------------------------------------------*/
void MobilityParamInit(Param_t *param, ParamList_t *DPList)
{
/*
 *      Note: Parameters need only be initialized if their
 *      default values are non-zero.
 */
        DPList->paramCnt = 0;
        DPList->varList = (VarData_t *)NULL;

        BindVar(DPList, "angleCount", &param->angleCount, V_INT, 1, VFLAG_NULL);

        return;
}

#endif
