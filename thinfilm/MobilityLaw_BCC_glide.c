/****************************************************************************
 *
 *      Module:       Mobility_BCC_glide
 *      Description:  Contains Mobility_BCC_glide() for calculating mobility 
 *                    of nodes in BCC metals.  Based on Mobility_FCC_0().
 *                    This is similar to Mobility_FCC_0() with the only
 *                    difference being that line constraints are ignored.
 *
 *      Authors:      Wu-Rong Jian         
 *
 *      Includes functions:
 *
 *            Mobility_BCC_glide()
 *      Returns:  0 on success
 *                1 if velcoity could not be determined
 *                
 ***************************************************************************/
#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif


#ifdef _MOBILITY_FIELD
/*-------------------------------------------------------------------------
 *
 *      Function:     InterpolateMobilityField
 *      Description:  Interpolate the mobility field from tabulated values
 *
 *-----------------------------------------------------------------------*/
void InterpolateMobilityField(Home_t *home, double px, double py, double pz, double *Mf)
{
		int      i, j, k;
		int      nx, ny, nz, npd, npe, g[3];
		double   p[3], q[3], xi[3], H[3];
		Param_t  *param;

		param = home->param;

		*Mf = 0.0;

		// Trilinear interpolation
		nx = param->mobilityFieldNx;
		ny = param->mobilityFieldNy;
		nz = param->mobilityFieldNz;
		npd = 2;
		if (nz == 1) {
			npe = npd*npd;
		} else {
			npe = npd*npd*npd;
		}

		H[0] = param->Lx / nx;
		H[1] = param->Ly / ny;
		H[2] = param->Lz / nz;

		p[0] = px - param->minSideX - 0.5*H[0];
		p[1] = py - param->minSideY - 0.5*H[1];
		p[2] = pz - param->minSideZ - 0.5*H[2];

		q[0] = p[0] / H[0];
		q[1] = p[1] / H[1];
		q[2] = p[2] / H[2];

		g[0] = (int)floor(q[0]);
		g[1] = (int)floor(q[1]);
		g[2] = (int)floor(q[2]);

		xi[0] = 2.0*(q[0]-g[0]) - 1.0;
		xi[1] = 2.0*(q[1]-g[1]) - 1.0;
		xi[2] = 2.0*(q[2]-g[2]) - 1.0;

		// Apply PBC in the x,y,z directions
		int ind1d[3][npd];
		for (i = 0; i < npd; i++) {
			ind1d[0][i] = ((npd-1)*g[0]+i)%nx;
			if (ind1d[0][i] < 0) ind1d[0][i] += nx;
			ind1d[1][i] = ((npd-1)*g[1]+i)%ny;
			if (ind1d[1][i] < 0) ind1d[1][i] += ny;
			ind1d[2][i] = ((npd-1)*g[2]+i)%nz;
			if (ind1d[2][i] < 0) ind1d[2][i] += nz;
		}

		// 1d shape functions
		double phi1d[3][npd];
		for (i = 0; i < 3; i++) {
			phi1d[i][0] = 0.5*(1.0-xi[i]);
			phi1d[i][1] = 0.5*(1.0+xi[i]);
		}

		int ind[npe];
		double phi[npe];
		if (nz > 1) {
			// 3d shape functions and indices
			for (k = 0; k < npd; k++) {
				for (j = 0; j < npd; j++) {
					for (i = 0; i < npd; i++) {
						phi[i+j*npd+k*npd*npd] = phi1d[0][i]*phi1d[1][j]*phi1d[2][k];
						ind[i+j*npd+k*npd*npd] = ind1d[0][i] + ind1d[1][j]*nx + ind1d[2][k]*nx*ny;
					}
				}
			}
		} else {
			// 2d shape functions and indices
			for (j = 0; j < npd; j++) {
				for (i = 0; i < npd; i++) {
					phi[i+j*npd] = phi1d[0][i]*phi1d[1][j];
					ind[i+j*npd] = ind1d[0][i] + ind1d[1][j]*nx;
				}
			}
		}

		// Interpolate from the grid points
		*Mf = 0.0;
		for (j = 0; j < npe; j++) {
			*Mf += phi[j] * param->mobilityFieldPhi[ind[j]];
		}

		return;
}
#endif

/*-------------------------------------------------------------------------
 *
 *      Function:     Mobility_BCC_glide
 *      Description:  This function computes the velocity of a node from
 *                    the force in the case of BCC crystals.
 *
 *-----------------------------------------------------------------------*/
int Mobility_BCC_glide(Home_t *home, Node_t *node)
{
    int numNonZeroLenSegs = 0;
    Param_t *param;
    real8 VelxNode, VelyNode, VelzNode, Veldotlcr;
    int i, j, nc, nconstraint;
    real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
    real8 a;
    real8 dx, dy, dz, lx, ly, lz, lr, LtimesB, fricStress;
    real8 lcx, lcy, lcz, normdotlc;
    Node_t *nbr;
    real8 MobScrew, MobEdge, Mob;
    real8 bx, by, bz, br, dangle;
    real8 nForce[3], nFmag, FricForce;

    param = home->param;

    MobScrew = param->MobScrew;
    MobEdge  = param->MobEdge;
    
    nc = node->numNbrs;


    
/*
 *  If node is 'pinned' in place by constraints, or the node has any arms 
 *  with a burgers vector that has explicitly been set to be sessile (via
 *  control file inputs), the node may not be moved so just zero the velocity
 *  and return
 */
    if ((node->constraint == PINNED_NODE) ||
        NodeHasSessileBurg(home, node))
    {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }

/*
 *  It's possible this function was called for a node which had only zero-
 *  length segments (during SplitSurfaceNodes() for example).  If that is
 *  the case, just set the velocity to zero and return.
 */
    for (i = 0; i < nc; i++) {
        if ((nbr = GetNeighborNode(home, node, i)) == (Node_t *)NULL) continue;
        dx = node->x - nbr->x;
        dy = node->y - nbr->y;
        dz = node->z - nbr->z;
        if ((dx*dx + dy*dy + dz*dz) > 1.0e-12) {
            numNonZeroLenSegs++;
        }
    }

    if (numNonZeroLenSegs == 0) {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }


    /* copy glide plane constraints */
    for(i=0;i<nc;i++)
    {
        normX[i] = normx[i] = node->nx[i];
        normY[i] = normy[i] = node->ny[i];
        normZ[i] = normz[i] = node->nz[i];

//#ifndef _THINFILM
/*
 *      If needed, rotate the glide plane normals from the
 *      laboratory frame to the crystal frame.
 */
        if (param->useLabFrame) {
            real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
            real8 normRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);
            
            normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
            normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
        }
//#endif
    }

#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	    
        
        real8 zDir[3] = {0.0, 0.0, 1.0};
/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
		if (param->useLabFrame) {
			real8 zRot[3];
			Matrix33Vector3Multiply(home->rotMatrixInverse, zDir, zRot);

			zDir[0] = zRot[0];
			zDir[1] = zRot[1];
			zDir[2] = zRot[2];
		}

		normX[nc] = zDir[0]; normY[nc] = zDir[1]; normZ[nc] = zDir[2];
		nc++;
    }
#endif
    
    /* normalize glide plane normal vectors*/
    for(i=0;i<nc;i++)
    {
        a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);

        if(a>0)
        {
            normX[i]/=a;
            normY[i]/=a;
            normZ[i]/=a;

            normx[i]/=a;
            normy[i]/=a;
            normz[i]/=a;
        }
    }


    /* Find independent glide constraints */ 
    nconstraint = nc;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
        }
        if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH)
        {
            normX[i] = normY[i] = normZ[i] = 0;
            nconstraint--;
        }
    }
      
    /* find total dislocation length times drag coefficent (LtimesB)*/

#ifdef _MOBILITY_FIELD
	double Mf = 1.0;
	if (param->mobilityField == 1) {
		InterpolateMobilityField(home, node->x, node->y, node->z, &Mf);
		Mf = (1.0-Mf);
	}
	if (param->frictionField == 1) {
		InterpolateMobilityField(home, node->x, node->y, node->z, &fricStress);
		fricStress *= param->mobilityFieldScale;
	} else
#endif
	{
		fricStress = param->FricStress;
	}

    LtimesB=0;
    FricForce = 0.0;
    for(j=0;j<node->numNbrs;j++)
    {
        if ((nbr=GetNeighborNode(home,node,j)) == (Node_t *)NULL) continue;
        dx=nbr->x - node->x;
        dy=nbr->y - node->y;
        dz=nbr->z - node->z;
        ZImage (param, &dx, &dy, &dz) ;

//#ifndef _THINFILM
/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
        if (param->useLabFrame) {
            real8 dTmp[3] = {dx, dy, dz};
            real8 dRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, dTmp, dRot);

            dx = dRot[0]; dy = dRot[1]; dz = dRot[2];
        }
//#endif

        lr=sqrt(dx*dx+dy*dy+dz*dz);
 
        if (lr==0)
        { /* zero arm segment can happen after node split 
           * it is OK to have plane normal vector == 0
           * Skip (do nothing)
           */
        }
        else 
        {
           if((node->nx[j]==0)&&(node->ny[j]==0)&&(node->nz[j]==0))
           {
              printf("Mobility_BCC_glide: (%d,%d) glide plane norm = 0\n"
                     "for segment with nonzero length lr = %e!\n",
                     node->myTag.domainID, node->myTag.index, lr);
           }

           lx=dx/lr; ly=dy/lr; lz=dz/lr;

           bx = node->burgX[j];
           by = node->burgY[j];
           bz = node->burgZ[j];

//#ifndef _THINFILM
/*
 *         If needed, rotate the burgers vector from the laboratory frame to
 *         the crystal frame.
 */
           if (param->useLabFrame) {
               real8 bTmp[3] = {bx, by, bz};
               real8 bRot[3];

               Matrix33Vector3Multiply(home->rotMatrixInverse, bTmp, bRot);

               bx = bRot[0]; by = bRot[1]; bz = bRot[2];
           }
//#endif

           br = sqrt(bx*bx+by*by+bz*bz);
           bx/=br; by/=br; bz/=br; /* unit vector along Burgers vector */

           dangle = fabs(bx*lx+by*ly+bz*lz);
           Mob=MobEdge+(MobScrew-MobEdge)*dangle;

#ifdef _MOBILITY_FIELD
			if (param->mobilityField == 1) {
				double xm = param->mobilityFieldScale;
				Mob *= ((1.0-xm) + xm*Mf);
			}
#endif

           LtimesB+=(lr/Mob);
           FricForce += fricStress*br*lr;
	}
    }
    LtimesB/=2;
    FricForce /= 2.0;
    
#if 0
#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) {
		// WARNING
		/* Modify mobility of surface nodes */
		LtimesB *= 5.0;
	}
#endif
#endif

    nForce[0] = node->fX;
    nForce[1] = node->fY;
    nForce[2] = node->fZ;

//#ifndef _THINFILM
/*
 *  If needed, rotate the force vector from the laboratory frame to the
 *  crystal frame
 */
    if (param->useLabFrame) {
        real8 rotForce[3];
        Matrix33Vector3Multiply(home->rotMatrixInverse, nForce, rotForce);
        VECTOR_COPY(nForce, rotForce);
    }
//#endif

/*
 * 	Apply friction stress. If we are using subcycling,
 * 	the friction stress is integrated as a back-force.
 */
	if (fricStress > 0.0) {
		if (strcmp(param->timestepIntegrator, "forceBsubcycle") == 0 &&
		    param->inSubcycling) {
			if (node->fricPinned) {
				nForce[0] = 0.0;
				nForce[1] = 0.0;
				nForce[2] = 0.0;
			}
		} else {
			nFmag = sqrt(nForce[0]*nForce[0]+nForce[1]*nForce[1]+nForce[2]*nForce[2]);
			if (nFmag > FricForce) {
				nForce[0] -= nForce[0]*FricForce/nFmag;
				nForce[1] -= nForce[1]*FricForce/nFmag;
				nForce[2] -= nForce[2]*FricForce/nFmag;
			} else {
				nForce[0] = 0.0;
				nForce[1] = 0.0;
				nForce[2] = 0.0;
			}
		}
	}

    /* Velocity is simply proportional to total force per unit length */
    VelxNode = nForce[0]/LtimesB;
    VelyNode = nForce[1]/LtimesB;
    VelzNode = nForce[2]/LtimesB;
    

    /* Orthogonalize with glide plane constraints */
    if (nconstraint>=3){
		VelxNode = VelyNode = VelzNode = 0.0;
	} else {
		for(i=0;i<nc;i++)
		{
			if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
			{
			 Orthogonalize(&VelxNode,&VelyNode,&VelzNode,
							 normX[i],normY[i],normZ[i]);
			}
		}
	}


//#ifndef _THINFILM
/*
 *  If needed, rotate the velocity vector back to the laboratory frame
 *  from the crystal frame
 */
    if (param->useLabFrame) {
        real8 vTmp[3] = {VelxNode, VelyNode, VelzNode};
        real8 vRot[3];

        Matrix33Vector3Multiply(home->rotMatrix, vTmp, vRot);

        VelxNode = vRot[0];
        VelyNode = vRot[1];
        VelzNode = vRot[2];
    }
//#endif

    node->vX = VelxNode;
    node->vY = VelyNode;
    node->vZ = VelzNode;


#ifdef _THINFILM
    /* put surface node velocity on surface 
     * by adding velocity along line direction 
     */ 
    if (node->constraint == THINFILM_SURFACE_NODE) 
    {	
        if (node->numNbrs == 1)
        {
           /* compute line direction */
           nbr=GetNeighborNode(home,node, 0);
           dx=nbr->x - node->x;
           dy=nbr->y - node->y;
           dz=nbr->z - node->z;
           ZImage (param, &dx, &dy, &dz) ;
           lr=sqrt(dx*dx+dy*dy+dz*dz);
           lx = dx/lr; ly = dy/lr; lz = dz/lr;

           //printf("surface node lx = %e ly = %e lz = %e\n", lx, ly, lz);

           if (fabs(lz) > 0.05)
           {
              node->vX = VelxNode - VelzNode * lx / lz ;
              node->vY = VelyNode - VelzNode * ly / lz ;
              node->vZ = 0.0;
           }
           else
           {
              node->vX = VelxNode * lz - VelzNode * lx ;
              node->vY = VelyNode * lz - VelzNode * ly ;
              node->vZ = 0.0;
           }
        }
        else
        {
              node->vX = 0.0;
              node->vY = 0.0;
              node->vZ = 0.0;
        }

	real8 vmag;
        vmag = sqrt(node->vX*node->vX + node->vY*node->vY + node->vZ*node->vZ);
        if (fabs(node->vZ) > (vmag*1e-4)) 
        if (fabs(node->vZ) > 0.0) 
        {
            printf("surface node vX = %e vY = %e vZ = %e vmag = %e nc = %d\n",
                    node->vX,node->vY,node->vZ,vmag,nc);
            for (i=0;i<nc;i++) printf("norm[%d] = (%e, %e, %e)\n",i,normX[i],normY[i],normZ[i]);
            Fatal("MobilityLaw_BCC_glide: surface node vZ non-zero!");
        } 
    }
#endif

/*
#ifdef _THINFILM
    if (node->constraint == THINFILM_SURFACE_NODE) {
        node->vZ = 0.0;
    } 
#endif */

    return(0);
}

